# SnowChemExchange

This repository is set-up for the further development and application of the snowpack chemistry and exchange model orginally developed by Murray et al. to study in-snowpack chemical cycling and exchange of ozone, NOx and other reactive compounds.