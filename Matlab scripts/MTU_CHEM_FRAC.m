clc
clear all
close all



%dir='MTU4_output/';
sp_array={'NO2';'NO';'O3';'HO2';'HNO4'};
sp_array={'NO2'};
adjust_flag=1
for jkl=1:numel(sp_array)
    chem_choice=sp_array{jkl}
    
        
%April Reserve
dir='~/Documents/April_test_old/';
%dir='/Users/Keenan/Documents/MTU_long2_output/';

% path=dir;
mkdir(dir)
directory='~/Documents/output_March29_April_1_30/';
directory='~/Documents/output_April16_April_1_30/';
%directory='/Users/Keenan/Documents/MTU_long2/output/';

year=2009
month=4
end_month=month
day=1
end_day=30
hour=0
m=0
    
%     dir='/Users/Keenan/Documents/MTU_long4_output/';
%     dir='MTU_output/';
%     path=dir;
%     mkdir(dir)
%     %directory='MTU4/output/';
%     directory='/Users/Keenan/Documents/MTU_long4/output_March1/';
%     directory='MTU/output/';
    %[flux]=MTU_get_flux(directory);
    spc_name=MTU_get_spcs(directory);
    depths=MTU_get_depths(directory);
    time=MTU_get_times(directory);
    depths2=[];
    time2=[];
    
    
    for i=1:(length(depths)-1)
        depths2(2*i-1) =depths(i);
        depths2(2*i)=depths(i+1)*0.99;
        time2(2*i-1)=time(i);
        time2(2*i)=time(i);
    end
    %time=time2;
    depths=depths2;
    
    
    eqs=MTU_get_eqs(directory);
    cons_eqs=cell(1,numel(eqs))';
    
    prod_eqs=cell(1,numel(eqs))';
    co=zeros(1,numel(eqs));
    aq=zeros(1,numel(eqs));
    
    
    chem_choice_aq=[chem_choice '-aq'];
    
    for i=1:numel(eqs)
        eq=eqs(i);
        
        eq=strrep(eq,'to',' ');
        
        [cons_eqs(i) prod_eqs(i)]=strtok(eq);
        cons_eqs{i}=['_' cons_eqs{i}];
        prod_eqs{i}=[prod_eqs{i} '_'];
        
        cons_eqs{i}=strrep(cons_eqs{i},'-',' -');
        cons_eqs{i}=strrep(cons_eqs{i},'_aq','-aq');
        cons_eqs{i}=strrep(cons_eqs{i},'_',' ');
        
        prod_eqs{i}=strrep(prod_eqs{i},'-',' -');
        prod_eqs{i}=strrep(prod_eqs{i},'_aq','-aq');
        prod_eqs{i}=strrep(prod_eqs{i},'_',' ');
        
        [sp line]=strtok(cons_eqs{i});
        while (~isempty(sp))
            
            %checks to see if the chemical coefficent is negative
            index=isstrprop(sp(1),'punct');
            if (index)
                if (strcmp(sp(1),'-'))
                    n_co=-1;
                    sp=sp(2:end);
                else
                    n_co=1;
                end
            else
                n_co=1;
            end
            %checks for coefficent infront of chemcial species
            index=isstrprop(sp,'digit');
            index2=find(index==0,1,'first');
            index2=index2-1;
            if (index2==0)
                temp_co=1;
            else
                ten=1;
                for jk=1:index2
                    if (str2num(sp(jk))==0)
                        
                        ten=ten*100;
                        
                        
                    else
                        break
                        
                    end
                    
                end
                temp_co=str2num(sp(1:index2))/ten;
                
            end
            
            while( temp_co>=10)
                temp_co=temp_co/10;
            end
            
            sp=sp(index2+1:end);
            if strcmp(sp,chem_choice)
                co(i)=co(i)-temp_co*n_co;
                
            elseif strcmp(sp,chem_choice_aq)
                co(i)=co(i)-temp_co*n_co;
                aq(i)=1;
                
            end
            
            [sp line]=strtok(line);
        end
        
        [sp line]=strtok(prod_eqs{i});
        while (~isempty(sp))
            
            %checks to see if the chemical coefficent is negative
            index=isstrprop(sp(1),'punct');
            if (index)
                if (strcmp(sp(1),'-'))
                    n_co=-1;
                    sp=sp(2:end);
                else
                    n_co=1;
                end
            else
                n_co=1;
            end
            %checks for coefficent infront of chemcial species
            index=isstrprop(sp,'digit');
            index2=find(index==0,1,'first');
            index2=index2-1;
            if (index2==0)
                temp_co=1;
            else
                ten=1;
                for jk=1:index2
                    if (str2num(sp(jk))==0)
                        
                        ten=ten*100;
                        
                        
                    else
                        break
                        
                    end
                    
                end
                temp_co=str2num(sp(1:index2))/ten;
                
            end
            while( temp_co>=10)
                temp_co=temp_co/10;
            end
            sp=sp(index2+1:end);
            if strcmp(sp,chem_choice)
                co(i)=co(i)+temp_co*n_co;
                
            elseif strcmp(sp,chem_choice_aq)
                co(i)=co(i)+temp_co*n_co;
                aq(i)=1;
            end
            
            [sp line]=strtok(line);
        end
        
        
        
        %[num2str(co(i)) ' ' eqs{i}]
    end
    
    master_index=find(co~=0);
    chemr=MTU_get_chemr(directory);
    chemr=chemr(:,:,master_index);
    co=co(master_index);
    aq=aq(master_index);
    eqs=eqs(master_index);
    
    conc_file=[directory 'model_gas_ppx.out']
    para_file=[directory 'model_para.out']
    data=load(para_file);
    NUM_DEPTH=data(1);
    NUM_SPEC=data(2);
    NUM_TIME=data(3);
    MAX_j=data(4);
    NUM_TEND=data(5);
    NUM_EQ=data(6);
    %data=load(conc_file);
    %rj=MTU_get_rj(directory);
    %tend=MTU_get_tend(directory);
    %rj=reshape(data,MAX_j,NUM_TIME,NUM_SPEC,NUM_DEPTH);
    %temperature=MTU_get_temp(directory);
    %QLL=MTU_get_QLL(directory);
    VOL=MTU_get_VOL(directory);
    % keyboard
    
    
    
    %data=reshape(data,NUM_DEPTH,NUM_TIME,NUM_SPEC);
    %data2=zeros(2*NUM_DEPTH,NUM_TIME,NUM_SPEC);
    %rj2=zeros(MAX_j,NUM_TIME,NUM_SPEC,2*NUM_DEPTH);
    %tend2=zeros(NUM_TEND,NUM_TIME,NUM_SPEC,2*NUM_DEPTH);
    %temperature2=zeros(2*NUM_DEPTH,NUM_TIME);
    %QLL2=zeros(2*NUM_DEPTH,NUM_TIME);
    VOL2=zeros(2*NUM_DEPTH,NUM_TIME);
    chemr2=zeros(2*NUM_DEPTH,NUM_TIME,size(chemr,3));
    
    for i=1:NUM_TIME
        for j=1:NUM_DEPTH
            %data2(2*j-1,i,:)=data(j,i,:);
            %data2(2*j,i,:)=data(j,i,:);
            %temperature2(2*j-1,i)=temperature(j,i);
            %temperature2(2*j,i)=temperature(j,i);
            %QLL2(2*j-1,i)=QLL(j,i);
            %QLL2(2*j,i)=QLL(j,i);
            VOL2(2*j-1,i)=VOL(j,i);
            VOL2(2*j,i)=VOL(j,i);
            chemr2(2*j-1,i,:)=chemr(j,i,:);
            chemr2(2*j,i,:)=chemr(j,i,:);
            for k=1:MAX_j
                %rj2(k,i,:,2*j-1)=rj(k,i,:,j);
                %rj2(k,i,:,2*j)=rj(k,i,:,j);
            end
            for k=1:NUM_TEND
                %tend2(k,i,:,2*j-1)=tend(k,i,:,j);
                %tend2(k,i,:,2*j)=tend(k,i,:,j);
            end
        end
    end
    
    %data=data2;
    %rj=rj2;
    %tend=tend2;
    %temperature=temperature2;
    %QLL=QLL2;
    VOL=VOL2;
    chemr=chemr2;
    clear('VOL2','chemr2')
    
    
    prod_frac=zeros(size(chemr));
    cons_frac=zeros(size(chemr));
    for i=1:size(chemr,3)
        
        if(co(i)<0)
            cons_frac(:,:,i)=chemr(:,:,i)*co(i);
        elseif (co(i)>0)
            prod_frac(:,:,i)=chemr(:,:,i)*co(i);
            
        end
        
        if aq(i)
            cons_frac(:,:,i)=cons_frac(:,:,i).*VOL;
            prod_frac(:,:,i)=prod_frac(:,:,i).*VOL;
            
        end
        
        for j=1:size(prod_frac,1)
            for k=1:size(prod_frac,2)
                if (isnan(prod_frac(j,k,i))|| prod_frac(j,k,i)<0 )
                    prod_frac(j,k,i)=0;
                end
                if (isnan(cons_frac(j,k,i)) || cons_frac(j,k,i)>0)
                    cons_frac(j,k,i)=0;
                end
            end
        end
        
        
    end
    %clear('chem_r','aq')
    
    %keyboard
    prod=sum(prod_frac,3);
    cons=sum(cons_frac,3);
    if (adjust_flag)
        if (strcmp(chem_choice,'NO2'))
            
            
            temp=prod_frac(:,:,6);
            prod_frac(:,:,6)=prod_frac(:,:,6)+cons_frac(:,:,5);
            cons_frac(:,:,5)=cons_frac(:,:,5)+temp;
            
            temp=prod_frac(:,:,12);
            prod_frac(:,:,12)=prod_frac(:,:,12)+cons_frac(:,:,11);
            cons_frac(:,:,11)=cons_frac(:,:,11)+temp;
            
            
        elseif (strcmp(chem_choice,'HO2'))
            
            
            
            
            temp=prod_frac(:,:,6);
            prod_frac(:,:,6)=prod_frac(:,:,6)+cons_frac(:,:,5);
            cons_frac(:,:,5)=cons_frac(:,:,5)+temp;
            
            
        elseif ( strcmp(chem_choice,'NO'))
            
            
            
            
            temp=prod_frac(:,:,1);
            prod_frac(:,:,1)=prod_frac(:,:,1)+cons_frac(:,:,2);
            cons_frac(:,:,2)=cons_frac(:,:,2)+temp;
            
            
        elseif ( strcmp(chem_choice,'HNO4'))
            
            
            
            temp=prod_frac(:,:,1);
            prod_frac(:,:,1)=prod_frac(:,:,1)+cons_frac(:,:,2);
            cons_frac(:,:,2)=cons_frac(:,:,2)+temp;
            
            
        elseif (strcmp(chem_choice,'O3') )
            
            
            
            
            temp=prod_frac(:,:,1);
            prod_frac(:,:,1)=prod_frac(:,:,1)+cons_frac(:,:,2);
            cons_frac(:,:,2)=cons_frac(:,:,2)+temp;
            
            
            
            
        end
    end
    total_prod_cons=prod+cons;
    
    for i=1:size(prod_frac,3)
        prod_frac(:,:,i)=prod_frac(:,:,i)./prod;
        cons_frac(:,:,i)=cons_frac(:,:,i)./cons;
        for j=1:size(prod_frac,1)
            for k=1:size(prod_frac,2)
                if (isnan(prod_frac(j,k,i))|| prod_frac(j,k,i)<0 )
                    prod_frac(j,k,i)=0;
                end
                if (isnan(cons_frac(j,k,i)) || cons_frac(j,k,i)<0)
                    cons_frac(j,k,i)=0;
                end
            end
        end
    end
    
    time=MTU_get_times(directory);
    date_tick=linspace(time(1),time(end),5);
    %date_tick=linspace(datenum(2009,4,11,0,0,0),datenum(2009,4,30,0,0,0),5);
    date_x=floor(time(1)):floor(time(end));
    date_y=[min(depths(:)) max(depths(:))];
    [date_x date_y]=meshgrid(date_x,date_y);
    %keyboard
    [time, depths]=meshgrid(time,depths);
    
    
    
    frac=prod_frac+cons_frac;
    frac=frac*100;
    for i =1:size(prod_frac,3)
        figure('Visible','off')
        
        clim_vector=0:1:100;
        if (co(i)>0)
            
            contourf(time,depths,frac(:,:,i),clim_vector,'LineStyle','none')
            colorbar
            set(gca,'XTick',date_tick);
            datetick('x',2,'keepticks')
            ylim([-2 2])
            xlabel('Date [mm/dd/yy]')
            ylabel('Depth')
            
            title_str=eqs{i};
            title_str=strrep(title_str,'_to_',' -> ');
            title_str=strrep(title_str,'_aq','*');
            title_str=strrep(title_str,'_',' + ');
            title_str=strrep(title_str,'*','-aq');
            
            title({title_str;['Percent production of ' chem_choice ]})
            
            hold all
            plot(date_x,date_y,'-.','Color','w')
            
            if (adjust_flag)
                saveas(gcf,[dir 'chem_frac_adj_pro_' chem_choice '__' eqs{i} '_' num2str(i) '.jpeg'])
            else
                saveas(gcf,[dir 'chem_frac_pro_' chem_choice '__' eqs{i} '_' num2str(i) '.jpeg'])
                
            end
            close(gcf)
            
            
        elseif(co(i)<0)
            contourf(time,depths,frac(:,:,i),clim_vector,'LineStyle','none')
            colorbar
            set(gca,'XTick',date_tick);
            datetick('x',2,'keepticks')
            ylim([-2 2])
            xlabel('Date [mm/dd/yy]')
            ylabel('Depth')
            
            title_str=eqs{i};
            title_str=strrep(title_str,'_to_',' -> ');
            title_str=strrep(title_str,'_aq','*');
            title_str=strrep(title_str,'_',' + ');
            title_str=strrep(title_str,'*','-aq');
            
            title({title_str;['Percent consumption of ' chem_choice ]})
            
            hold all
            plot(date_x,date_y,'-.','Color','w')
            
            if ( adjust_flag)
                saveas(gcf,[dir 'chem_frac_adj_cons_' chem_choice '__' eqs{i} '_' num2str(i) '.jpeg'])
            else
                saveas(gcf,[dir 'chem_frac_cons_' chem_choice '__' eqs{i} '_' num2str(i) '.jpeg'])
            end
            
            close(gcf)
            
            
        end
        
    end
    
    
    
    
    figure('Visible','off')
    contourf(time,depths,total_prod_cons,100,'LineStyle','none')
    colorbar
    set(gca,'XTick',date_tick);
    datetick('x',2,'keepticks')
    ylim([-2 2])
    xlabel('Date [mm/dd/yy]')
    ylabel('Depth')
    
    
    title({['Total chemical production/consumption of ' chem_choice];'[molecules/cm^3 s]'})
    
    saveas(gcf,[dir 'total_prod_cons_' chem_choice  '.jpeg'])
    close(gcf)
    
    % figure('Visible','off')
    % clim_vector=0:.1:2;
    % contourf(time,depths,abs(prod_frac(:,:,21)./cons_frac(:,:,20)),clim_vector,'LineStyle','none')
    % colorbar
    %  set(gca,'XTick',date_tick);
    % datetick('x',2,'keepticks')
    %  ylim([-2 2])
    %  xlabel('Date [mm/dd/yy]')
    %  ylabel('Depth')
    %
    %   hold all
    %  plot(date_x,date_y,'-.','Color','w')
    %
    %  title({['Ratio of ' chem_choice ' production/consumption by HNO4 thermal cycle']})
    %
    %  saveas(gcf,[dir 'HNO4_thermal_prod_cons_' chem_choice  '.jpeg'])
    %  close(gcf)
    
    % %ratio of flux to chemical rates
    % %switching flux sign to make postive fluxes release to ABL
    % flux=-flux*100; %convert to molecules/cm2 s
    % dx=abs(diff(depths))*100; %layer thickness in cm
    % ave_prod_cons=total_prod_cons(1:end-1,:).*dx;
    % spc_index=strcmp(chem_choice,spc_name);
    % spc_flux=flux(spc_index,:);
    %
    % flux_ratio=spc_flux./sum(ave_prod_cons)*100;
    %
    % figure('Visible','off')
    % subplot(2,1,1)
    % plot(time(1,:),flux_ratio)
    %  set(gca,'XTick',date_tick);
    % datetick('x',2,'keepticks')
    % %xlabel('Date [mm/dd/yy]')
    %  ylabel(' percent')
    %
    % title(['Ratio of flux to chemical rates - ' chem_choice])
    %
    % subplot(2,1,2)
    % plot(time(1,:),spc_flux)
    % set(gca,'XTick',date_tick);
    % datetick('x',2,'keepticks')
    % xlabel('Date [mm/dd/yy]')
    %  ylabel('flux [ molecules/cm2 s]')
    %
    %  %Do i need to take porosity into account?'
    %
    % saveas(gcf,[dir 'flux_chem_ratio_' chem_choice  '.jpeg'])
    % close(gcf)
    
end