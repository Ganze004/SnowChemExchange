function spc_name=MTU_get_spcs(directory)

file='model_spcs.out'
fid=fopen([directory file]);

line=fgetl(fid);

spc_name=cell(0);
while ~isempty(line)
 [token line]=strtok(line);   
 if ~isempty(token)
     spc_name{end+1}=token
 end
end
fclose(fid)
end