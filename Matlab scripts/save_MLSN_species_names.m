clc
clear all
close all

MLSN_names={
      'NH3'; 
 'RAD'; 
 'CH3O2H'; 
 'SO4'; 
 'ACTA'; 
 'R3N2 ';
 'SUCA';
 'GLUA'; 
 'ADIA'; 
 'PRN2'; 
 'PRPN'; 
 'OZID'; 
 'MSA'; 
 'CH3O'; 
 'DOL6'; 
 'DOL7'; 
 'DOL8'; 
 'CPET'; 
 'CHEX'; 
 'MACA'; 
 'PYVA'; 
 'DMS'; 
 'C2H6'; 
 'C3H8'; 
 'MPAN'; 
 'IPAN'; 
 'TPAN'; 
 'H2O2'; 
 'HOBr'; 
 'HOCl'; 
 'BrCl'; 
 'AROM'; 
 'HONO'; 
 'N2O5'; 
 'RAN2'; 
 'Br';
 'Cl'; 
 'CH4'; 
 'ALKA'; 
 'PAN'; 
 'HNO4'; 
 'ACO2'; 
 'BrO';
 'ClO'; 
 'GLYX'; 
 'DIAL'; 
 'CRES'; 
 'AHO2'; 
 'RAN1'; 
 'ZO2'; 
 'ETHE'; 
 'CO'; 
 'MGLY'; 
 'MAN2'; 
 'TO2'; 
 'MVN2'; 
 'R3O2'; 
 'EO2'; 
 'PO2'; 
 'MRO2'; 
 'HAC';
 'ETO2'; 
 'PRN1'; 
 'RIO2'; 
 'HNO3';
 'TCO3';
 'HACO';
 'INO2';
 'MGGY'; 
 'VRO2'; 
 'RAO2'; 
 'KET'; 
 'KO2';
 'ALKE'; 
 'ISOP'; 
 'MO2';
 'MAO3'; 
 'MCRG'; 
 'CRO2';
 'MVKO'; 
 'MAOO'; 
 'ROOH'; 
 'MCO3';
 'MACR';
 'O3';
 'MVK';
 'NO';
 'OH'; 
 'HCHO'; 
 'HO2'; 
 'ALD2'; 
 'SO2'; 
 'CHO2'; 
 'NO3'; 
 'NO2'; 


      };

  for i=1:size(MLSN_names,1)
     MLSN_names{i,2}=i; 
      
      
  end
  
  
  
  
  
  save('MLSN_species.mat','MLSN_names')