clc
clear all
close all

file='jval_aq.out';
fid=fopen(file);

A=fscanf(fid,'%d %d %d ',3);
num_depth=A(1)+1;
num_jrate=A(2);
num_steps=A(3);

data=fscanf(fid,'%f %d %f %f %f %f %f %f %f %f %f %f',[12,inf]); 
fclose(fid);

depth=data(1,:);
jday=data(2,:);
jtime=data(3,:);
sza=data(4,:);
jrates=data(5:end,:);

index=depth<=0.0;

jday=jday(index);
jtime=jtime(index);
depth=depth(index);
jrates=jrates(:,index);

depth=reshape(depth,num_depth, num_steps);
depth=flipud(depth);


linear_time=(jday-jday(1))*86400; %Keeps the time in order for new days when plotting
jtime=jtime+linear_time;
jtime=reshape(jtime,num_depth, num_steps);
for i=1:num_steps
    M=5;
    D=floor(jtime(:,i)/(24*60*60));
    jtime(:,i)=jtime(:,i)-D*(24*60*60);
    H=floor(jtime(:,i)/(60*60));
    jtime(:,i)=jtime(:,i)-H*60*60;
    minute=floor(jtime(:,i)/60);
    jtime(:,i)=jtime(:,i)-minute*60;
    S=jtime(:,i);
    
    D=D+30
   jtime(:,i)=datenum(2011,M,D,H,minute,S); 
    
end




jrate_labels={'O3 -> H2O2';'H2O2 -> 2OH';'NO3- -> NO2 + OH';'NO2- -> NO + OH';'NO3- -> NO2- + O';'NO3 -> NO + O';'NO3 -> NO2 +O';'HONO -> NO + OH'};


jrates_cell=cell(num_jrate,1);
for i=1:num_jrate
  jr=jrates(i,:);
  jrates_cell{i}=flipud(reshape(jr,num_depth,num_steps));
end

for i=1:num_jrate
   x=jtime;
   y=depth;
   z=jrates_cell{i};
   
   figure
   contourf(x,y,z,100,'LineColor','none')
   datetick('x',6,'keepticks')
   xlabel('Date')
   ylabel('Depth [m]')
   colorbar
   title({'Photolysis constant [s^-^1]';jrate_labels{i};})
    saveas(gcf,['jrate' num2str(i) '.jpeg'])
    
end
