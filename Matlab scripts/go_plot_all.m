clc
clear all
close all

% file='noxiv_final_conc_sscnt_reprocessed.dat'
% dates=[datenum(6722,6,25) datenum(6722,7,10)]
% plot_conc(file,dates)%

%2009
% 4/11-4/25
% 4/27-5/6
% 5/11-5/18
% 5/28-6/11
% 6/25-7/12
%May 31st August18th
%6/1-8/7 2009

%2010
%4/13-4/27
%4/28-5/10
%5/10-5/18

Y=[ 2009
    2010
    2010
    2010
    2009
    2009
    2009
    2009
    2009
    2010
    2010
    2010
    2009];

M_min=[
    8
    6
    5
    5
    4
    4
    5
    5
    6
    4
    4
    5
    1];

M_max=[
    8
    6
    5
    5
    4
    5
    5
    6
    7
    4
    5
    5
    12];
D_min=[
    1
    10
    11
    14
    11
    27
    11
    24
    23
    13
    26
    8
    1];
D_max=[
    30
    13
    14
    15
    25
    6
    18
    8
    12
    27
    10
    18
    30];

% Y=[
%   2009];
% 
% 
% M_min=[
%  1];
% 
% M_max=[
% 12];
% D_min=[
% 1];
% D_max=[
%     30];


%Gradients with wind/irradiance 1 plot per month, 2009-2010
%March-Sept


%[NO_time NO NO_sd NO2_time NO2 NO2_sd] =get_snow_level_data(files);


date_min=datenum(Y,M_min,D_min);
date_max=datenum(Y,M_max,D_max);

for kk=5%1:length(date_min)-1
   plot_all2(date_min,date_max,kk,Y,M_min,M_max,D_min,D_max) 
    
end