clc
clear all
close all

load summit_snow_data.mat


date_min=datenum(2009,6,25);
date_max=datenum(2009,7,10);

NO_time_2=cell(0);
NO2_time_2=cell(0);
for i=1:numel(files)
[row,col]=find(NO_time(i,:)>date_min & NO_time(i,:)<date_max);

NO_time_2(i,1:numel(NO_time(i,col)))=cellstr(datestr(NO_time(i,col)));
NO2_time_2(i,1:numel(NO2_time(i,col)))=cellstr(datestr(NO2_time(i,col)));
NO_2(i,1:numel(NO(i,col)))=NO(i,col);
NO2_2(i,1:numel(NO2(i,col)))=NO2(i,col);
NO_sd_2(i,1:numel(NO_sd(i,col)))=NO_sd(i,col);
NO2_sd_2(i,1:numel(NO2_sd(i,col)))=NO2_sd(i,col);
%NO_time_2(i,1:numel(NO_time(i,col)))=NO_time(i,col);
end
NO_time=NO_time_2;
NO2_time=NO2_time_2;
NO=NO_2;
NO2=NO2_2;
NO_sd=NO_sd_2;
NO2_sd=NO2_sd_2;

%NO_time(NO_time==0)=NaN;
%NO2_time(NO2_time==0)=NaN;
NO_sd(NO==0)=NaN;
NO2_sd(NO2==0)=NaN;
NO(NO==0)=NaN;
NO2(NO2==0)=NaN;
NO_sd(NO_sd==0)=NaN;
NO2_sd(NO2_sd==0)=NaN;

file_name='Brie_data.xls'
header=write_snow_data(NO_time, NO, NO_sd, NO2_time, NO2, NO2_sd, file_name)