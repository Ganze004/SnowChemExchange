function [x_time_av x_av]=min_30_average(time,x)
%The matrix a should be in format mxn where m is different layers and n is
%the time series. Time and x should be same size. Time must be in Julian


    minute_30=datenum(0,0,0,0,30,0);
    day_span=floor(min(time(:))):minute_30:ceil(max(time(:)));
    x_av=nan(size(time,1),length(day_span)-1);
x_time_av=nan(size(time,1),length(day_span)-1);



for i=1:size(time,1)
    day_time=time(i,:);

    
    
    day_x=zeros(1,length(day_span)-1);
    temp_x=x(i,:);
    for j=1:length(day_span)-1
        
       index=day_time>=day_span(j) & day_time<=day_span(j+1);
       
       
       


       day_x(j)=nanmean(temp_x(index));
        
        
    end
  % try
    if ~isempty(day_x)
    x_av(i,:)=day_x;
    x_time_av(i,:)=day_span(1:end-1)+minute_30/2; %places time in middle of time interval
    end
%    catch
%         keyboard
%     end
%     
    
end
