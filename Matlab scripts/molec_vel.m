function v=molec_vel(T,M)
R=8.314*1000;    % g m^2/ s^2 K mol
v=sqrt(8*R*T./(M*pi));

end