function [x y z ]=D3_zgrid(surf_x,surf_y,surf_z,max_depth,num_nodes)

z=zeros(size(surf_z,1),size(surf_z,2),num_nodes);
x=zeros(size(surf_z,1),size(surf_z,2),num_nodes);
y=zeros(size(surf_z,1),size(surf_z,2),num_nodes);
for i=1:size(surf_z,1)
    
    for j=1:size(surf_z,2)
        if surf_z(i,j)>0
        temp=logspace(log10(surf_z(i,j)),log10(max_depth+2*surf_z(i,j)),num_nodes)*-1;
       
        temp=temp+2*surf_z(i,j);
        
        else
            ten_exp=-5;
           temp=logspace(ten_exp,log10(max_depth+surf_z(i,j)),num_nodes)*-1; 
           temp=temp+2*10^ten_exp;
        end
     
        
        z(i,j,:)=temp;
    end
    
end
% temp=z(:,:,2);
% z(:,:,1)=temp+mean(temp(:));
for i=1:num_nodes
    x(:,:,i)=surf_x;
    y(:,:,i)=surf_y;
    
end