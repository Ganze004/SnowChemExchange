% clc
% clear all
% close all
% 
% % file='noxiv_final_conc_sscnt_reprocessed.dat'
% % dates=[datenum(6722,6,25) datenum(6722,7,10)]
% % plot_conc(file,dates)%
% 
% %2009
% % 4/11-4/25
% % 4/27-5/6
% % 5/11-5/18
% % 5/28-6/11
% % 6/25-7/12
% 
% 
% 
% %2010
% %4/13-4/27
% %4/28-5/10
% %5/10-5/18
% 
% Y=[
%     2009
%     2009
%     2009
%     2009
%     2009
%     2010
%     2010
%     2010];
% 
% M_min=[
%     4
%     4
%     5
%     5
%     6
%     4
%     4
%     5];
% 
% M_max=[
%     4
%     5
%     5
%     6
%     7
%     4
%     5
%     5];
% D_min=[
%     11
%     27
%     11
%     28
%     25
%     13
%     26
%     10];
% D_max=[
%     25
%     6
%     18
%     11
%     12
%     27
%     10
%     18];
% 
% 
% 
% % 
% % %Gradients with wind/irradiance 1 plot per month, 2009-2010
% % %March-Sept
% % 
% % 
% % %[NO_time NO NO_sd NO2_time NO2 NO2_sd] =get_snow_level_data(files);
% % 
% kk=1
% date_min=datenum(Y,M_min,D_min);
% date_max=datenum(Y,M_max,D_max);
function plot_all(date_min,date_max,kk,Y,M_min,M_max,D_min,D_max)
load noaa.mat
load cu_rad.mat
load summit_snow_data.mat
load depths_data.mat

master_NO_time=NO_time;
master_NO2_time=NO2_time;
master_NO=NO;
master_NO2=NO2;
master_NO_sd=NO_sd;
master_NO2_sd=NO2_sd;


scrsz = get(0,'ScreenSize');

 %for kk=7%:length(date_min)

days=date_min(kk):date_max(kk);

for jjj=1:length(days)

load noaa.mat
load cu_rad.mat
load summit_snow_data.mat
load depths_data.mat

index=find(noaa_jd>date_min(kk) & noaa_jd<date_max(kk));

wind_jd=noaa_jd(index);
wind_speed=noaa_ws(index);
wind_dir=noaa_wd(index);

wind_speed(wind_speed==9999)=NaN;

index=find(rad_jd>date_min(kk) & rad_jd<date_max(kk));
irr_down=rad_down(index);
irr_up=rad_up(index);
irr_jd=rad_jd(index);
irr_down(irr_down>2000)=NaN;


index=find(depths_jd>date_min(kk) & depths_jd<date_max(kk));

if isempty(index)
   index(1,1)=find(depths_jd<date_min(kk),1,'last');
   index(2,1)=find(depths_jd>date_max(kk),1,'first');
    
end

index=[index(1)-1 ;index ; index(end)+1];

depths=depths(index,:);
depths_jd=depths_jd(index,:);

%     for i=1:numel(files)
%         [row,col]=find(NO_time(i,:)>date_min(kk) & NO_time(i,:)<date_max(kk));
%
%         NO_time_2(i,1:numel(NO_time(i,col)))=NO_time(i,col);
%         NO2_time_2(i,1:numel(NO2_time(i,col)))=NO2_time(i,col);
%         NO_2(i,1:numel(NO(i,col)))=NO(i,col);
%         NO2_2(i,1:numel(NO2(i,col)))=NO2(i,col);
%         NO_sd_2(i,1:numel(NO_sd(i,col)))=NO_sd(i,col);
%         NO2_sd_2(i,1:numel(NO2_sd(i,col)))=NO2_sd(i,col);
%
%     end

for i=1:numel(files)
    index=find(master_NO_time(i,:)>date_min(kk) & master_NO_time(i,:)<date_max(kk));
    
    NO_time_2(i,1:numel(master_NO_time(i,index)))=master_NO_time(i,index);
    NO2_time_2(i,1:numel(master_NO2_time(i,index)))=master_NO2_time(i,index);
    NO_2(i,1:numel(master_NO(i,index)))=master_NO(i,index);
    NO2_2(i,1:numel(master_NO2(i,index)))=master_NO2(i,index);
    NO_sd_2(i,1:numel(master_NO_sd(i,index)))=master_NO_sd(i,index);
    NO2_sd_2(i,1:numel(master_NO2_sd(i,index)))=master_NO2_sd(i,index);
    
end


NO_time=NO_time_2;
NO2_time=NO2_time_2;
NO=NO_2;
NO2=NO2_2;
NO_sd=NO_sd_2;
NO2_sd=NO2_sd_2;
depths_grid=NaN(size(NO_time));


for jj=1:size(depths,2)
    %if length(depths_jd)>1
   yi=interp1(depths_jd,depths(:,jj),NO_time(jj,:),'nearest','extrap');
   depths_grid((size(depths,2)-(jj-1)),:)=yi;
   % else
    
    
    %end
end




[x y]=meshgrid(NO_time(1,:),1:numel(files));

n=100;
top_inlet_num=2;
bot_inlet_num=6;
clim_NOx=linspace(0,600,100);
clim_NO2=clim_NOx;
clim_NO=linspace(0,200,100);
lim_depths=[-3 2];
tick_depths=lim_depths(1):lim_depths(2);
lim_ws=[0 25];
tick_ws=lim_ws(1):5:lim_ws(2);
lim_irr=[0 1000];
tick_irr=lim_irr(1):200:lim_irr(2);


NO_time(NO_time==0)=NaN;
NO2_time(NO2_time==0)=NaN;
NO(NO==0)=NaN;
NO2(NO2==0)=NaN;


NOx=NO+NO2;
NOx(isnan(NOx))=0;
num_interp=3;
XI=NO_time;%interp2(NO_time,num_interp);
YI=depths_grid;%interp2(depths_grid,num_interp);
NOx_I=NOx;%interp2(NOx,num_interp);
size(NOx_I)
% for jk=1:14
% figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
% 
% 
%     
%     plot(NO_time(jk,:),NO(jk,:),'o')
%     datetick('x')
%     file=['Test_' num2str(jk) '.jpeg'];
%     saveas(gcf,file)
%     
% end
% close all
figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
subplot(2,1,1)


contourf(NO_time, depths_grid,NOx,clim_NOx,'LineStyle','none')%NO_time, y,
%contourf(NO_time, y,interp2(NOx,4),n,'LineStyle','none')%NO_time, y,

colorbar
datetick('x','mm/dd','keepticks')
set(gca,'XLim',[date_min(kk) date_max(kk)])
set(gca,'YLim',lim_depths)
set(gca,'YTick',tick_depths)
xlabel('Dates')
ylabel('Depth [m]')
title('NO_x [ppt_v]')

subplot(2,1,2)

[AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);

xlabel('Date')
set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
ylim(AX(1),lim_irr)
ylim(AX(2),lim_ws)
set(AX(1),'YTick',tick_irr)
set(AX(2),'YTick',tick_ws)
title('Surface Irradiance and Wind Speed Measurements')
datetick(AX(1),'x','mm/dd','keepticks')
datetick(AX(2),'x','mm/dd','keepticks')
set(AX(1),'XLim',[date_min(kk) date_max(kk)])
set(AX(2),'XLim',[date_min(kk) date_max(kk)])
set(h1,'Linestyle','o')
legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')

file=['NOx_depth_Wind_Irradiance_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '.jpeg']
saveas(gcf,file)



figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
subplot(2,1,1)
contourf(NO_time, depths_grid,NO,clim_NO,'LineStyle','none')%NO_time, y,
%contourf(NO_time, y,interp2(NOx,4),n,'LineStyle','none')%NO_time, y,

colorbar
datetick('x','mm/dd','keepticks')
set(gca,'XLim',[date_min(kk) date_max(kk)])
set(gca,'YLim',lim_depths)
set(gca,'YTick',tick_depths)
xlabel('Dates')
ylabel('Depth [m]')
title('NO [ppt_v]')

subplot(2,1,2)

[AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);

xlabel('Date')
set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
ylim(AX(1),lim_irr)
ylim(AX(2),lim_ws)
set(AX(1),'YTick',tick_irr)
set(AX(2),'YTick',tick_ws)
title('Surface Irradiance and Wind Speed Measurements')
datetick(AX(1),'x','mm/dd','keepticks')
datetick(AX(2),'x','mm/dd','keepticks')
set(AX(1),'XLim',[date_min(kk) date_max(kk)])
set(AX(2),'XLim',[date_min(kk) date_max(kk)])
set(h1,'Linestyle','o')
legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')

file=['NO_depth_Wind_Irradiance_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '.jpeg']
saveas(gcf,file)

figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
subplot(2,1,1)
contourf(NO2_time, depths_grid,NO2,clim_NO2,'LineStyle','none')%NO_time, y,
%contourf(NO_time, y,interp2(NOx,4),n,'LineStyle','none')%NO_time, y,

colorbar
datetick('x','mm/dd','keepticks')
set(gca,'XLim',[date_min(kk) date_max(kk)])
set(gca,'YLim',lim_depths)
set(gca,'YTick',tick_depths)
xlabel('Dates')
ylabel('Depth [m]')
title('NO_2 [ppt_v]')

subplot(2,1,2)

[AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);

xlabel('Date')
set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
ylim(AX(1),lim_irr)
ylim(AX(2),lim_ws)
set(AX(1),'YTick',tick_irr)
set(AX(2),'YTick',tick_ws)
title('Surface Irradiance and Wind Speed Measurements')
datetick(AX(1),'x','mm/dd','keepticks')
datetick(AX(2),'x','mm/dd','keepticks')
set(AX(1),'XLim',[date_min(kk) date_max(kk)])
set(AX(2),'XLim',[date_min(kk) date_max(kk)])
set(h1,'Linestyle','o')
legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')

file=['NO2_depth_Wind_Irradiance_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '.jpeg']
saveas(gcf,file)

% figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
% subplot(2,1,1)
% 
% 
% 
% 
% [AX h1 h2]=plotyy(NO_time(top_inlet_num,:),NO(top_inlet_num,:),NO2_time(top_inlet_num,:),NO2(top_inlet_num,:));  %,wind_jd,wind_speed);
% hold all
% 
% 
% xlabel('Date')
% set(get(AX(2),'Ylabel'),'String','NO2 [ppt_v]')
% set(get(AX(1),'Ylabel'),'String','NO [ppt_v] ')
% title(['NO and NO2 measurements from inlet ' num2str(top_inlet_num) ])
% datetick(AX(1),'x','mm/dd','keepticks')
% datetick(AX(2),'x','mm/dd','keepticks')
% set(AX(1),'XLim',[date_min(kk) date_max(kk)])
% set(AX(2),'XLim',[date_min(kk) date_max(kk)])
% set(h1,'Linestyle','o')
% set(h2,'Linewidth',2)
% legend([h1 h2],'NO','NO2','Location','NorthWest')
% subplot(2,1,2)
% 
% 
% 
% [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
% 
% xlabel('Date')
% set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
% set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
% title('Surface Irradiance and Wind Speed Measurements')
% datetick(AX(1),'x','mm/dd','keepticks')
% datetick(AX(2),'x','mm/dd','keepticks')
% set(AX(1),'XLim',[date_min(kk) date_max(kk)])
% set(AX(2),'XLim',[date_min(kk) date_max(kk)])
% set(h1,'Linestyle','o')
% legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
% file=['NO_NO2_Wind_Irradiance_inlet_' num2str(top_inlet_num) '_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '.jpeg'];
% saveas(gcf,file)
% 
% 
% figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
% subplot(2,1,1)
% 
% 
% 
% 
% [AX h1 h2]=plotyy(NO_time(bot_inlet_num,:),NO(bot_inlet_num,:),NO2_time(bot_inlet_num,:),NO2(bot_inlet_num,:));  %,wind_jd,wind_speed);
% hold all
% 
% 
% xlabel('Date')
% set(get(AX(2),'Ylabel'),'String','NO2 [ppt_v]')
% set(get(AX(1),'Ylabel'),'String','NO [ppt_v] ')
% title(['NO and NO2 measurements from inlet ' num2str(bot_inlet_num) ])
% datetick(AX(1),'x','mm/dd','keepticks')
% datetick(AX(2),'x','mm/dd','keepticks')
% set(AX(1),'XLim',[date_min(kk) date_max(kk)])
% set(AX(2),'XLim',[date_min(kk) date_max(kk)])
% set(h1,'Linestyle','o')
% set(h2,'Linewidth',2)
% legend([h1 h2],'NO','NO2','Location','NorthWest')
% subplot(2,1,2)
% 
% 
% [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
% 
% xlabel('Date')
% set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
% set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
% title('Surface Irradiance and Wind Speed Measurements')
% datetick(AX(1),'x','mm/dd','keepticks')
% datetick(AX(2),'x','mm/dd','keepticks')
% set(AX(1),'XLim',[date_min(kk) date_max(kk)])
% set(AX(2),'XLim',[date_min(kk) date_max(kk)])
% set(h1,'Linestyle','o')
% legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
% file=['NO_NO2_Wind_Irradiance_inlet_' num2str(bot_inlet_num) '_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '.jpeg'];
% saveas(gcf,file)

close all
end
% end
%end
