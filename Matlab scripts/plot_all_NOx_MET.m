clc
clear all
close all

load summit_MET_data.mat
load noaa.mat
load summit_MET_ozone_data.mat

NO_path='final_data_plots/NO_MET/';
NO2_path='final_data_plots/NO2_MET/';
O3_path='final_data_plots/O3_MET/';

mkdir(NO_path)
mkdir(NO2_path)
mkdir(O3_path)
lim_NOx=[0 1000];%linspace(0,600,100);
lim_NO2=[0 1000];
lim_NO=[0 200];
lim_O3=[0 100];
lim_wd=[0 360];
lim_ws=[0 25];

grad_factor=10;

num_y_ticks=5;
num_date_ticks=5;


dates=[ 7 2008
    8 2008
    9 2008
    10 2008
    11 2008
    12 2008
    1 2009
    2 2009
    3 2009
    4 2009
    5 2009
    6 2009
    7 2009
    8 2009
    9 2009
    10 2009
    11 2009
    12 2009
    1 2010
    2 2010
    3 2010
    4 2010
    5 2010
    6 2010
    7 2010]

for dd=1:size(dates,1)
    close all
    
    load summit_MET_data.mat
    load noaa.mat
    load summit_MET_ozone_data.mat
    
    month=dates(dd,1);
    year=dates(dd,2);
    
    dates(dd,:)
    
    
    day_span=[datenum(year,month,1,0,0,0) datenum(year,month+1,1,0,0,0)];
    
    date_min=day_span(1);
    date_max=day_span(2);
    
    
    
    
    
    
    
    [NO_time1 NO_1]=date_index(day_span,NO_time1,NO_1);
    [NO2_time1 NO2_1]=date_index(day_span,NO2_time1,NO2_1);
    
    [NO_time2 NO_2]=date_index(day_span,NO_time2,NO_2);
    [NO2_time2 NO2_2]=date_index(day_span,NO2_time2,NO2_2);
    
    
    index=find(noaa_jd>date_min & noaa_jd<date_max);
    
    noaa_jd=noaa_jd(index);
    noaa_wd=noaa_wd(index);
    noaa_ws=noaa_ws(index);
    
    noaa_wd(noaa_wd==9999)=NaN;
    
    index=find(O3_time>date_min & O3_time<date_max);
    
    O3_time=O3_time(index);
    O3_1=O3_1(index);
    O3_2=O3_2(index);
    
    
    %
    %     [O3_time_t O3_1]=date_index(day_span,O3_time, O3_1);
    %     [O3_time O3_2]=date_index(day_span,O3_time, O3_2);
    
    
    
    
    
    
    
    try
        figure('Visible','off')
        subplot(2,1,1)
        
        NO_1(NO_1<0)=0;
        NO_2(NO_2<0)=0;
        
        [AX h1 h2]=plotyy(NO_time2,NO_2,NO_time1,NO_1);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        set(get(AX(2),'Ylabel'),'String','NO - Inlet 1 [ppt_v]')
        set(get(AX(1),'Ylabel'),'String','NO - Inlet 2 [ppt_v] ')
        title(['MET NO' ])
        set(AX(1),'XLim',[date_min date_max])
        set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
        set(AX(2),'XLim',[date_min date_max])
        set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
        datetick(AX(1),'x','mm/dd/yy','keepticks')
        datetick(AX(2),'x','mm/dd/yy','keepticks')
        set(AX(1),'YLim',lim_NO)
        set(AX(2),'YLim',lim_NO)
        set(h1,'Linestyle',':')
        set(h2,'Linestyle',':')
        set(h1,'Marker','o')
        set(h2,'Marker','sq')
        set(AX(1),'YTick',linspace(lim_NO(1),lim_NO(2),num_y_ticks))
        set(AX(2),'YTick',linspace(lim_NO(1),lim_NO(2),num_y_ticks))
        
        legend([h1 h2],'Inlet 2','Inlet 1','Location','NorthWest')
        
        subplot(2,1,2)
        
        [AX h1 h2]=plotyy(noaa_jd,noaa_wd,noaa_jd,noaa_ws);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s]')
        set(get(AX(1),'Ylabel'),'String','Wind Direction [Degrees] ')
        title(['Wind Profile' ])
        set(AX(1),'XLim',[date_min date_max])
        set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
        set(AX(2),'XLim',[date_min date_max])
        set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
        datetick(AX(1),'x','mm/dd/yy','keepticks')
        datetick(AX(2),'x','mm/dd/yy','keepticks')
        set(AX(1),'YLim',lim_wd)
        set(AX(2),'YLim',lim_ws)
        set(h1,'Linestyle',':')
        set(h2,'Linestyle',':')
        set(h1,'Marker','o')
        set(h2,'Marker','sq')
        set(AX(1),'YTick',linspace(lim_wd(1),lim_wd(2),num_y_ticks))
        set(AX(2),'YTick',linspace(lim_ws(1),lim_ws(2),num_y_ticks))
        
        legend([h1 h2],'Wind Dir.','Wind Sp.','Location','NorthWest')
        
        saveas(gcf,[NO_path 'NO_' num2str(year) '_' num2str(month) '.jpeg'])
    catch
        disp(['NO for' num2str(year) '-' num2str(month) ' could not be plotted'])
    end
    
    
    try
        %NO2
        figure('Visible','off')
        subplot(2,1,1)
        
        NO2_1(NO2_1<0)=0;
        NO2_2(NO2_2<0)=0;
        
        [AX h1 h2]=plotyy(NO2_time2,NO2_2,NO2_time1,NO2_1);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        set(get(AX(2),'Ylabel'),'String','NO - Inlet 1 [ppt_v]')
        set(get(AX(1),'Ylabel'),'String','NO - Inlet 2 [ppt_v] ')
        title(['MET NO_2' ])
        set(AX(1),'XLim',[date_min date_max])
        set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
        set(AX(2),'XLim',[date_min date_max])
        set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
        datetick(AX(1),'x','mm/dd/yy','keepticks')
        datetick(AX(2),'x','mm/dd/yy','keepticks')
        set(AX(1),'YLim',lim_NO2)
        set(AX(2),'YLim',lim_NO2)
        set(h1,'Linestyle',':')
        set(h2,'Linestyle',':')
        set(h1,'Marker','o')
        set(h2,'Marker','sq')
        set(AX(1),'YTick',linspace(lim_NO2(1),lim_NO2(2),num_y_ticks))
        set(AX(2),'YTick',linspace(lim_NO2(1),lim_NO2(2),num_y_ticks))
        
        
        
        subplot(2,1,2)
        
        [AX h1 h2]=plotyy(noaa_jd,noaa_wd,noaa_jd,noaa_ws);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s]')
        set(get(AX(1),'Ylabel'),'String','Wind Direction [Degrees] ')
        title(['Wind Profile' ])
        set(AX(1),'XLim',[date_min date_max])
        set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
        set(AX(2),'XLim',[date_min date_max])
        set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
        datetick(AX(1),'x','mm/dd/yy','keepticks')
        datetick(AX(2),'x','mm/dd/yy','keepticks')
        set(AX(1),'YLim',lim_wd)
        set(AX(2),'YLim',lim_ws)
        set(h1,'Linestyle',':')
        set(h2,'Linestyle',':')
        set(h1,'Marker','o')
        set(h2,'Marker','sq')
        set(AX(1),'YTick',linspace(lim_wd(1),lim_wd(2),num_y_ticks))
        set(AX(2),'YTick',linspace(lim_ws(1),lim_ws(2),num_y_ticks))
        
        legend([h1 h2],'Wind Dir.','Wind Sp.','Location','NorthWest')
        saveas(gcf,[NO2_path 'NO2_' num2str(year) '_' num2str(month) '.jpeg'])
        
    catch
        disp(['NO2 for' num2str(year) '-' num2str(month) ' could not be plotted'])
    end
    
    
    
    
    %O3
    
    try
        
        figure('Visible','off')
        subplot(2,1,1)
        O3_1(O3_1<0)=0;
        O3_2(O3_2<0)=0;
        
        [AX h1 h2]=plotyy(O3_time,O3_2,O3_time,O3_1);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        set(get(AX(2),'Ylabel'),'String','O_3 - Inlet 1 [ppt_b]')
        set(get(AX(1),'Ylabel'),'String','O_3 - Inlet 2 [ppt_b] ')
        title(['MET O_3' ])
        set(AX(1),'XLim',[date_min date_max])
        set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
        set(AX(2),'XLim',[date_min date_max])
        set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
        datetick(AX(1),'x','mm/dd/yy','keepticks')
        datetick(AX(2),'x','mm/dd/yy','keepticks')
        set(AX(1),'YLim',lim_O3)
        set(AX(2),'YLim',lim_O3)
        set(h1,'Linestyle',':')
        set(h2,'Linestyle',':')
        set(h1,'Marker','o')
        set(h2,'Marker','sq')
        set(AX(1),'YTick',linspace(lim_O3(1),lim_O3(2),num_y_ticks))
        set(AX(2),'YTick',linspace(lim_O3(1),lim_O3(2),num_y_ticks))
        
        
        subplot(2,1,2)
        
        [AX h1 h2]=plotyy(noaa_jd,noaa_wd,noaa_jd,noaa_ws);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s]')
        set(get(AX(1),'Ylabel'),'String','Wind Direction [Degrees] ')
        title(['Wind Profile' ])
        set(AX(1),'XLim',[date_min date_max])
        set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
        set(AX(2),'XLim',[date_min date_max])
        set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
        datetick(AX(1),'x','mm/dd/yy','keepticks')
        datetick(AX(2),'x','mm/dd/yy','keepticks')
        set(AX(1),'YLim',lim_wd)
        set(AX(2),'YLim',lim_ws)
        set(h1,'Linestyle',':')
        set(h2,'Linestyle',':')
        set(h1,'Marker','o')
        set(h2,'Marker','sq')
        set(AX(1),'YTick',linspace(lim_wd(1),lim_wd(2),num_y_ticks))
        set(AX(2),'YTick',linspace(lim_ws(1),lim_ws(2),num_y_ticks))
        
        legend([h1 h2],'Wind Dir.','Wind Sp.','Location','NorthWest')
        
        
        saveas(gcf,[O3_path 'O3_' num2str(year) '_' num2str(month) '.jpeg'])
        
    catch
        disp(['O3 for' num2str(year) '-' num2str(month) ' could not be plotted'])
    end
    
    
    
    
    
    
    
    
    
    
    
    try
        figure('Visible','off')
        
        subplot(2,1,1)
        
        
        plot(NO_time2,NO_2-NO_1);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        ylabel('Conc. Grad. [ppt_v]')
        title({['MET NO Grad' ];'Inlet 2 - Inlet1'})
        set(gca,'XLim',[date_min date_max])
        set(gca,'XTick',linspace(date_min,date_max,num_date_ticks))
        
        datetick(gca,'x','mm/dd/yy','keepticks')
        
        set(gca,'YLim',lim_NO/grad_factor)
        
        
        set(gca,'YTick',linspace(lim_NO(1),lim_NO(2)/grad_factor,num_y_ticks))
        
        
        subplot(2,1,2)
        
        [AX h1 h2]=plotyy(noaa_jd,noaa_wd,noaa_jd,noaa_ws);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s]')
        set(get(AX(1),'Ylabel'),'String','Wind Direction [Degrees] ')
        title(['Wind Profile' ])
        set(AX(1),'XLim',[date_min date_max])
        set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
        set(AX(2),'XLim',[date_min date_max])
        set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
        datetick(AX(1),'x','mm/dd/yy','keepticks')
        datetick(AX(2),'x','mm/dd/yy','keepticks')
        set(AX(1),'YLim',lim_wd)
        set(AX(2),'YLim',lim_ws)
        set(h1,'Linestyle',':')
        set(h2,'Linestyle',':')
        set(h1,'Marker','o')
        set(h2,'Marker','sq')
        set(AX(1),'YTick',linspace(lim_wd(1),lim_wd(2),num_y_ticks))
        set(AX(2),'YTick',linspace(lim_ws(1),lim_ws(2),num_y_ticks))
        
        legend([h1 h2],'Wind Dir.','Wind Sp.','Location','NorthWest')
        
        
        saveas(gcf,[NO_path 'NO_Grad_' num2str(year) '_' num2str(month) '.jpeg'])
    catch
        disp(['NO Grad for' num2str(year) '-' num2str(month) ' could not be plotted'])
    end
    
    try
        figure('Visible','off')
        subplot(2,1,1)
        
        
        
        plot(NO2_time2,NO2_2-NO2_1);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        ylabel('Conc. Grad. [ppt_v]')
        title({['MET NO2 Grad' ];'Inlet 2 - Inlet1'})
        set(gca,'XLim',[date_min date_max])
        set(gca,'XTick',linspace(date_min,date_max,num_date_ticks))
        
        datetick(gca,'x','mm/dd/yy','keepticks')
        
        set(gca,'YLim',lim_NO2/grad_factor)
        
        
        set(gca,'YTick',linspace(lim_NO2(1),lim_NO2(2)/grad_factor,num_y_ticks))
        subplot(2,1,2)
        
        [AX h1 h2]=plotyy(noaa_jd,noaa_wd,noaa_jd,noaa_ws);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s]')
        set(get(AX(1),'Ylabel'),'String','Wind Direction [Degrees] ')
        title(['Wind Profile' ])
        set(AX(1),'XLim',[date_min date_max])
        set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
        set(AX(2),'XLim',[date_min date_max])
        set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
        datetick(AX(1),'x','mm/dd/yy','keepticks')
        datetick(AX(2),'x','mm/dd/yy','keepticks')
        set(AX(1),'YLim',lim_wd)
        set(AX(2),'YLim',lim_ws)
        set(h1,'Linestyle',':')
        set(h2,'Linestyle',':')
        set(h1,'Marker','o')
        set(h2,'Marker','sq')
        set(AX(1),'YTick',linspace(lim_wd(1),lim_wd(2),num_y_ticks))
        set(AX(2),'YTick',linspace(lim_ws(1),lim_ws(2),num_y_ticks))
        
        legend([h1 h2],'Wind Dir.','Wind Sp.','Location','NorthWest')
        
        
        saveas(gcf,[NO2_path 'NO2_Grad_' num2str(year) '_' num2str(month) '.jpeg'])
    catch
        disp(['NO2 Grad for' num2str(year) '-' num2str(month) ' could not be plotted'])
    end
    
    
    
    
    %O3
    
    try
        
        figure('Visible','off')
        subplot(2,1,1)
        
        plot(O3_time,O3_2-O3_1);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        ylabel('Conc. Grad. [ppb_v]')
        
        title(['MET Grad O_3' ])
        set(gca,'XLim',[date_min date_max])
        set(gca,'XTick',linspace(date_min,date_max,num_date_ticks))
        
        datetick(gca,'x','mm/dd/yy','keepticks')
        
        set(gca,'YLim',lim_O3/grad_factor)
        
        
        set(gca,'YTick',linspace(lim_O3(1),lim_O3(2)/grad_factor,num_y_ticks))
       subplot(2,1,2)
        
        [AX h1 h2]=plotyy(noaa_jd,noaa_wd,noaa_jd,noaa_ws);  %,noaa_jd,wind_speed);
        hold all
        
        
        xlabel('Date')
        set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s]')
        set(get(AX(1),'Ylabel'),'String','Wind Direction [Degrees] ')
        title(['Wind Profile' ])
        set(AX(1),'XLim',[date_min date_max])
        set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
        set(AX(2),'XLim',[date_min date_max])
        set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
        datetick(AX(1),'x','mm/dd/yy','keepticks')
        datetick(AX(2),'x','mm/dd/yy','keepticks')
        set(AX(1),'YLim',lim_wd)
        set(AX(2),'YLim',lim_ws)
        set(h1,'Linestyle',':')
        set(h2,'Linestyle',':')
        set(h1,'Marker','o')
        set(h2,'Marker','sq')
        set(AX(1),'YTick',linspace(lim_wd(1),lim_wd(2),num_y_ticks))
        set(AX(2),'YTick',linspace(lim_ws(1),lim_ws(2),num_y_ticks))
        
        legend([h1 h2],'Wind Dir.','Wind Sp.','Location','NorthWest')
        saveas(gcf,[O3_path 'O3_Grad_' num2str(year) '_' num2str(month) '.jpeg'])
        
    catch
        disp(['O3 Grad for' num2str(year) '-' num2str(month) ' could not be plotted'])
    end
    
    
    

    
end
