function [time_x x]=date_index_1D_fast(date_span,time_x,x)
%This function is used for large data_sets for 1D data as the function
%date_index is unable to perform in a timely fashion.


        index=find(time_x>date_span(1) & time_x<date_span(2));
   
        x=x(index);
        time_x=time_x(index);


end