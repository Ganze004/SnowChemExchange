clc
clear all
close all

data=load('noaa.txt');


noaa_jd=datenum(data(:,2),data(:,1),data(:,3),data(:,4),data(:,5),data(:,6));
noaa_ws=data(:,7);
noaa_wd=data(:,8);


save('noaa.mat','noaa_jd','noaa_ws','noaa_wd')
