clc
clear all
close all

% file='noxiv_final_conc_sscnt_reprocessed.dat'
% dates=[datenum(6722,6,25) datenum(6722,7,10)]
% plot_conc(file,dates)%


%[NO_time NO NO_sd NO2_time NO2 NO2_sd] =get_snow_level_data(files);
load summit_snow_data.mat


date_min=datenum(2009,3,15);
date_max=datenum(2009,7,15);


for i=1:numel(files)
[row,col]=find(NO_time(i,:)>date_min & NO_time(i,:)<date_max);

NO_time_2(i,1:numel(NO_time(i,col)))=NO_time(i,col);
NO2_time_2(i,1:numel(NO2_time(i,col)))=NO2_time(i,col);
NO_2(i,1:numel(NO(i,col)))=NO(i,col);
NO2_2(i,1:numel(NO2(i,col)))=NO2(i,col);
NO_sd_2(i,1:numel(NO_sd(i,col)))=NO_sd(i,col);
NO2_sd_2(i,1:numel(NO2_sd(i,col)))=NO2_sd(i,col);
NO_time_2(i,1:numel(NO_time(i,col)))=NO_time(i,col);
end
NO_time=NO_time_2;
NO2_time=NO2_time_2;
NO=NO_2;
NO2=NO2_2;
NO_sd=NO_sd_2;
NO2_sd=NO2_sd_2;

NO_time(NO_time==0)=NaN;
NO2_time(NO2_time==0)=NaN;
% NO(isnan(NO))=0;
% NO2(NO2==0)=NaN;
% NO_sd(NO_sd==0)=NaN;
% NO2_sd(NO2_sd==0)=NaN;
%clear out NaN

[x y]=meshgrid(NO_time(1,:),1:numel(files));
%x=meshgrid(NO_time(1,:),1:numel(files));
NO_v=0:10:200;
NO2_v=0:10:700;
NOx_v=0:5:530;

% NO(isnan(NO))=0;
% NO2(isnan(NO2))=0;















figure
contourf(x, y,NO,NO_v,'LineStyle','none')%NO_time, y,
colorbar
 datetick('x','mm/dd')
 set(gca,'YDir','Reverse','XLim',[date_min date_max])
xlabel('Dates')
ylabel('Inlet number')
title('NO [ppt_v]')



% figure
% contourf(x, y,NO2,NO2_v,'LineStyle','none')%NO_time, y,
% colorbar
%  datetick('x','mm/dd')
%  set(gca,'YDir','Reverse','XLim',[date_min date_max])
% xlabel('Dates')
% ylabel('Inlet number')
% title('NO_2 [ppt_v]')
% 
%  
% figure
% contourf(x, y,NO+NO2,NOx_v,'LineStyle','none')%NO_time, y,
% colorbar
%  datetick('x','mm/dd')
%  set(gca,'YDir','Reverse','XLim',[date_min date_max])
% xlabel('Dates')
% ylabel('Inlet number')
% title('NO_x [ppt_v]')
