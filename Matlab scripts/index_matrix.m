function varargout=index_matrix(index,varargin)
optargin = size(varargin,2);

for i=1:optargin
    varargout{i}=[];
    temp=varargin{i};
    
    for j=1:size(temp,1)
        temp_index=index{j};

         cmd_str=['array' num2str(j) '=temp(' num2str(j) ',temp_index);'];
         eval(cmd_str)
  
    end
    
    cmd_str=['varargout{' num2str(i) '}=stack_matrix(array1'];
    
    for j=2:size(temp,1)
        cmd_str=[cmd_str ',array' num2str(j)];
    end
    
    cmd_str=[cmd_str ');'];
    eval(cmd_str)
           
end



end