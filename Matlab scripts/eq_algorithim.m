clc
clear all
close all

CO3=10^-10;
HCO3=10^-6;
CO2=10^-5;
H=10^-5.67;

CT_CO3=CO3+HCO3+CO2;

Ka1_CO2=4.3^-7;
Ka2_CO2=4.68^-11;
Kw=10^-14;

f=@(x) x+ CT_CO3/(1+Ka1_CO2/x +Ka1_CO2*Ka2_CO2/(x^2))-(Kw/x+ CT_CO3/(1+x/Ka1_CO2 +x^2/(Ka1_CO2*Ka2_CO2)))

x=fzero(f,H)