clc

clear all

close all


T=270;
P=1;
M=14+16*2;
R=8.205746*10^-5; %m^3 atm mol^-1 K^-1
rho_air=P/(R*T);

k=8*10^-10; %m^2
rho=1.3; %kg/m^3
u=1.6*10^-5;  % Pa s

rho_snow=0.3; %g/cm^3
rho_ice=0.9167; %g/cm^3
por=1-rho_snow/rho_ice;

H=[0.015 0.15];
L=H./0.03;
alpha=1;
U10=0:15;
z=0;

flux=zeros(length(H),length(U10));

ppb_surface=60;
 surf_conc=ppb_surface*rho_air*10^-9;
distance=1.5;
time=3600;
for i=1:length(H)

U=WP(U10,z,k,L(i),H(i),rho,u,alpha);


flux(i,:)=U*time/distance*ppb_surface/por;
end


scrsz = get(0,'ScreenSize');
figure('Position',[1 0 1.5*scrsz(3) 1.5*scrsz(4)])

 [AX,H1,H2] =plotyy(U10,flux(1,:),U10,flux(2,:));
  set(AX(1),'FontSize',25)
  set(AX(2),'FontSize',25)
  set(get(AX(1),'YLabel'),'String','ppb_v added per hour for Case 1')
  
  set(get(AX(2),'YLabel'),'String','ppb_v added per hour for Case 2')
  
    set(get(AX(1),'YLabel'),'FontSize',25)
  
  set(get(AX(2),'YLabel'),'FontSize',25)
  set(H1,'LineStyle','none')
  set(H1,'Marker','o')
  set(H1,'MarkerSize',20)
set(H2,'LineStyle','none')
set(H2,'Marker','*')
set(H2,'MarkerSize',20)
%  hold all
%  plot(U10,flux(2,:),'o','MarkerSize',10)
 xlabel('Surface wind speed 10 meters high [m/s]')
 
 title({['Affect of flux into snowpack with a surface concentration of = ' num2str(ppb_surface) ' ppb_v' ];['  Flux is confined to the top ' num2str(distance) ' meters of the snowpack']})
 
 legend('Case 1','Case 2','Location','NorthWest')
 saveas(gcf,'Surface_flux_over_time.pdf')