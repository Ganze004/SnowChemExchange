function j_time=MTU_get_times(directory)



file='model_time.out'
data=dlmread([directory file]);
j_time=[];
for i=1:size(data,1)
j_time(end+1)=datenum(data(i,1),data(i,2),data(i,3),data(i,4),0,0);
end
end