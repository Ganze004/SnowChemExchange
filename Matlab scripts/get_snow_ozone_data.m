%function [NO_time NO NO_sd NO2_time NO2 NO2_sd] =get_snow_level_data(files)
clc
clear all
close all

files={'data/snow_ozone/snowtowerozone_summit_200806.txt';
    'data/snow_ozone/snowtowerozone_summit_200807.txt';
    'data/snow_ozone/snowtowerozone_summit_200808.txt';
    'data/snow_ozone/snowtowerozone_summit_200809.txt';
    'data/snow_ozone/snowtowerozone_summit_200810.txt';
    'data/snow_ozone/snowtowerozone_summit_200811.txt';
    'data/snow_ozone/snowtowerozone_summit_200812.txt';
    'data/snow_ozone/snowtowerozone_summit_200901.txt';
    'data/snow_ozone/snowtowerozone_summit_200902.txt';
    'data/snow_ozone/snowtowerozone_summit_200903.txt';
    'data/snow_ozone/snowtowerozone_summit_200904.txt';
    'data/snow_ozone/snowtowerozone_summit_200905.txt';
    'data/snow_ozone/snowtowerozone_summit_200906.txt';
    'data/snow_ozone/snowtowerozone_summit_200907.txt';
    'data/snow_ozone/snowtowerozone_summit_200908.txt';
    'data/snow_ozone/snowtowerozone_summit_200909.txt';
    'data/snow_ozone/snowtowerozone_summit_200910.txt';
    'data/snow_ozone/snowtowerozone_summit_200911.txt';
    'data/snow_ozone/snowtowerozone_summit_200912.txt';
    'data/snow_ozone/snowtowerozone_summit_201001.txt';
    'data/snow_ozone/snowtowerozone_summit_201002.txt';
    'data/snow_ozone/snowtowerozone_summit_201003.txt';
    'data/snow_ozone/snowtowerozone_summit_201004.txt';
    'data/snow_ozone/snowtowerozone_summit_201005.txt';
    'data/snow_ozone/snowtowerozone_summit_201006.txt';
    'data/snow_ozone/snowtowerozone_summit_201007.txt';};

master_data=[];
O3_1=[];
O3_2=[];
O3_3=[];
O3_4=[];
O3_5=[];
O3_6=[];
O3_7=[];
O3_8=[];
O3_9=[];
O3_10=[];
O3_11=[];
O3_12=[];
O3_13=[];
O3_14=[];

O3_time1=[];
O3_time2=[];
O3_time3=[];
O3_time4=[];
O3_time5=[];
O3_time6=[];
O3_time7=[];
O3_time8=[];
O3_time9=[];
O3_time10=[];
O3_time11=[];
O3_time12=[];
O3_time13=[];
O3_time14=[];


num_depth=14;

for i=1:numel(files)
    files{i}
    master_data=load(files{i});
    
    inlets=master_data(:,9);
    for j=1:num_depth
        
        index=inlets==j;
        
        switch j
        case 1
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time1=[O3_time1;datenum(time_matrix)];
        O3_1=[O3_1;master_data(index,8)];
        
        case 2
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time2=[O3_time2;datenum(time_matrix)];
        O3_2=[O3_2;master_data(index,8)];
        case 3
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time3=[O3_time3;datenum(time_matrix)];
        O3_3=[O3_3;master_data(index,8)];
        case 4
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time4=[O3_time4;datenum(time_matrix)];
        O3_4=[O3_4;master_data(index,8)];
        case 5
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time5=[O3_time5;datenum(time_matrix)];
        O3_5=[O3_5;master_data(index,8)];
        case 6
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time6=[O3_time6;datenum(time_matrix)];
        O3_6=[O3_6;master_data(index,8)];
        case 7
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time7=[O3_time7;datenum(time_matrix)];
        O3_7=[O3_7;master_data(index,8)];
        case 8
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time8=[O3_time8;datenum(time_matrix)];
        O3_8=[O3_8;master_data(index,8)];
        case 9
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time9=[O3_time9;datenum(time_matrix)];
        O3_9=[O3_9;master_data(index,8)];
        case 10
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time10=[O3_time10;datenum(time_matrix)];
        O3_10=[O3_10;master_data(index,8)];
        case 11
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time11=[O3_time11;datenum(time_matrix)];
        O3_11=[O3_11;master_data(index,8)];
        case 12
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time12=[O3_time12;datenum(time_matrix)];
        O3_12=[O3_12;master_data(index,8)];
        case 13
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time13=[O3_time13;datenum(time_matrix)];
        O3_13=[O3_13;master_data(index,8)];
        case 14
        time_matrix=[master_data(index,1),master_data(index,3),master_data(index,4),master_data(index,5),master_data(index,6),zeros(length(master_data(index,6)),1)];
        O3_time14=[O3_time14;datenum(time_matrix)];
        O3_14=[O3_14;master_data(index,8)];
        

        end
        
        
    end
end


snow_O3=stack_matrix(O3_1,O3_2,O3_3,O3_4,O3_5,O3_6,O3_7,O3_8,O3_9,O3_10,O3_11,O3_12,O3_13,O3_14);
snow_O3=flipud(snow_O3);

snow_O3_time=stack_matrix(O3_time1,O3_time2,O3_time3,O3_time4,O3_time5,O3_time6,O3_time7,O3_time8,O3_time9,O3_time10,O3_time11,O3_time12,O3_time13,O3_time14);
snow_O3_time=flipud(snow_O3_time);

snow_O3(snow_O3==(-9999))=NaN;

save('summit_snow_ozone_data.mat','snow_O3_time','snow_O3')



