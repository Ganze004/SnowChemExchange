clc
clear all
close all

load summit_snow_data.mat
load depths_data.mat
load summit_snow_ozone_data.mat

NO_path='final_data_plots/NO/';
NO2_path='final_data_plots/NO2/';
O3_path='final_data_plots/O3/';

mkdir(NO_path)
mkdir(NO2_path)
mkdir(O3_path)
clim_NOx=linspace(0,1000,100);%linspace(0,600,100);
clim_NO2=clim_NOx;
clim_NO=linspace(0,200,100);
clim_O3=linspace(0,100,100);



dates=[ 7 2008
    8 2008
    9 2008
    10 2008
    11 2008
    12 2008
    1 2009
    2 2009
    3 2009
    4 2009
    5 2009
    6 2009
    7 2009
    8 2009
    9 2009
    10 2009
    11 2009
    12 2009
    1 2010
    2 2010
    3 2010
    4 2010
    5 2010
    6 2010
    7 2010]

for dd=1:size(dates,1)
    close all
    
    load summit_snow_data.mat
    load depths_data.mat
    load summit_snow_ozone_data.mat
    
    month=dates(dd,1);
    year=dates(dd,2);
    
    dates(dd,:)
    
    
    day_span=[datenum(year,month,1,0,0,0) datenum(year,month+1,1,0,0,0)];
    tick_dates=linspace(day_span(1),day_span(2),5);
    
    [NO_time NO]=date_index(day_span,NO_time,NO);
    [NO2_time NO2]=date_index(day_span,NO2_time,NO2);
    [O3_time O3]=date_index(day_span,snow_O3_time, snow_O3);
        
    
    NO_time=NO_time';
    NO=NO';
    [depths_jd yy]=meshgrid(depths_jd,1:size(NO_time,2));
    
    
    O3_time=O3_time';
    O3=O3';
    
    
    depths_jd=depths_jd';
    
    O3_depths=nearest_neighbor(O3_time,depths_jd,depths);
    depths=nearest_neighbor(NO_time,depths_jd,depths);
    
    
    NO=NO';
    NO_time=NO_time';
    O3=O3';
    O3_time=O3_time';
    O3_depths=O3_depths';
    depths=depths';
    
    try
    figure('Visible','off')
    
    NO(NO==0)=NaN;
    NO(NO<0)=0;
    %NO(NO>NO_cutoff)=0;
    %NO(isnan(NO))=0;
    NO_time(NO_time==0)=NaN;
    %
   % [NO_time yy]=meshgrid(NO_time(14,:)',1:size(NO_time,1));
    %
    %
    contourf(NO_time,depths,NO,clim_NO,'LineStyle','none')
    set(gca,'XTick',tick_dates)
    datetick('x','keepticks')
    ylim([min(depths(:)) 0])
    colorbar
    xlabel('Date')
    ylabel('Depth [m]')
    title(['NO [ppt_v] - ' num2str(year)])
    saveas(gcf,[NO_path 'NO_' num2str(year) '_' num2str(month) '.jpeg'])
    catch
        disp(['NO for' num2str(year) '-' num2str(month) ' could not be plotted'])
    end
    
    
    try
        %NO2
        figure('Visible','off')
        NO2(NO2==0)=NaN;
        NO2(NO2<0)=0;
        %NO2(NO2>NO2_cutoff)=0;
        %NO(isnan(NO))=0;
        NO2_time(NO2_time==0)=NaN;
        %
     %   [NO2_time yy]=meshgrid(NO2_time(14,:)',1:size(NO2_time,1));
        %
        %  [NO_time_day NO_time]=daily_average(NO_time,NO);
        
        contourf(NO2_time,depths,NO2,clim_NO2,'LineStyle','none')
        set(gca,'XTick',tick_dates)
        datetick('x','keepticks')
        ylim([min(depths(:)) 0])
        colorbar
        xlabel('Date')
        ylabel('Depth [m]')
        title(['NO2 [ppt_v] - ' num2str(year)])
        saveas(gcf,[NO2_path 'NO2_' num2str(year) '_' num2str(month) '.jpeg'])
        
    catch
        disp(['NO2 for' num2str(year) '-' num2str(month) ' could not be plotted'])
    end
    
    
        try
        
        figure('Visible','off')
        O3(O3==0)=NaN;
        O3(O3<0)=0;
        %NO2(NO2>NO2_cutoff)=0;
        %NO(isnan(NO))=0;
        O3_time(O3_time==0)=NaN;
        %
     %   [O3_time yy]=meshgrid(O3_time(14,:)',1:size(O3_time,1));
        %
        %  [NO_time_day NO_time]=daily_average(NO_time,NO);
        
        contourf(O3_time,O3_depths,O3,clim_O3,'LineStyle','none')
        set(gca,'XTick',tick_dates)
        datetick('x','keepticks')
        ylim([min(depths(:)) 0])
        colorbar
        xlabel('Date')
        ylabel('Depth [m]')
        title(['O3 [ppt_b] - ' num2str(year)])
        saveas(gcf,[O3_path 'O3_' num2str(year) '_' num2str(month) '.jpeg'])
        
    catch
        disp(['O3 for' num2str(year) '-' num2str(month) ' could not be plotted'])
    end
    
    
    
%     
%     
%     
%     %Daily averages
%     
%     
%     [NO_time_day NO_day]=daily_average(NO_time,NO);
%     
%     load depths_data.mat
%     
%     NO_time_day=NO_time_day';
%     NO_day=NO_day';
%     [depths_jd yy]=meshgrid(depths_jd,1:size(NO_time_day,2));
%     depths_jd=depths_jd';
%     
%     depths=nearest_neighbor(NO_time_day,depths_jd,depths);
%     
%     NO_day=NO_day';
%     NO_time_day=NO_time_day';
%     depths=depths';
%     
%     
%     try
%         figure('Visible','off')
%         
%         
%         
%         
%         
%         contourf(NO_time_day,depths,NO_day,clim_NO,'LineStyle','none')
%         set(gca,'XTick',tick_dates)
%         datetick('x','keepticks')
%         ylim([min(depths(:)) 0])
%         colorbar
%         xlabel('Date')
%         ylabel('Depth [m]')
%         title(['NO daily averages [ppt_v] - ' num2str(year)])
%         saveas(gcf,[NO_path 'NO_average_' num2str(year) '_' num2str(month) '.jpeg'])
%     catch
%         disp(['NO daily for' num2str(year) '-' num2str(month) ' could not be plotted'])
%     end
%     
%     [NO2_time_day NO2_day]=daily_average(NO2_time,NO2);
%     
%     try
%         figure('Visible','off')
%         
%         
%         
%         
%         contourf(NO2_time_day,depths,NO2_day,clim_NO2,'LineStyle','none')
%         set(gca,'XTick',tick_dates)
%         datetick('x','keepticks')
%         ylim([min(depths(:)) 0])
%         colorbar
%         xlabel('Date')
%         ylabel('Depth [m]')
%         title(['NO2 daily averages [ppt_v] - ' num2str(year)])
%         saveas(gcf,[NO2_path 'NO2_average_' num2str(year) '_' num2str(month) '.jpeg'])
%     catch
%         disp(['NO2 daily for' num2str(year) '-' num2str(month) ' could not be plotted'])
%     end
    
end
