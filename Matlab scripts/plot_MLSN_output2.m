clc
clear all
close all


path='MLSN_CHEM/output/';
out_path='MLSN_CHEM/output/graphs/';
mkdir(out_path);
file='NO.out';
snow_depth=2; %meters
year=2009
month=4
day=11
hour=0
m=0
%timestep_size=60 % in seconds
num_depths=10;
clim_O3=linspace(0,100,100);
clim_NO=linspace(0,200,100);
clim_NO2=linspace(0,1000,100);
clim_HONO=linspace(0,15,100);
lim_depth=[-2 2];
data=load([path file]);

%time=datenum(year,month,day,hour, m,timestep_size*data(:,1))';
    



for i=1:num_depths+1
   index=data(:,2)==data(i,2);
   NO(i,:)=data(index,3);
   NO_depth(i,:)=data(index,2);
   NO_time(i,:)=datenum(year,month,day,hour, m,data(index,1));
    
end
file='NO2.out';
data=load([path file]);
for i=1:num_depths+1
   index=data(:,2)==data(i,2);
   NO2(i,:)=data(index,3);
   NO2_depth(i,:)=data(index,2);
   NO2_time(i,:)=datenum(year,month,day,hour, m,data(index,1));
    
end
file='O3.out';
data=load([path file]);
for i=1:num_depths+1
   index=data(:,2)==data(i,2);
   O3(i,:)=data(index,3);
   O3_depth(i,:)=data(index,2);
   O3_time(i,:)=datenum(year,month,day,hour, m,data(index,1));
    
end


tick_dates=linspace(NO_time(1,1),NO_time(1,end),5);
%Manually lower surface layer reference heights
dz=abs(NO_depth(3,1)-NO_depth(2,1));


NO_depth_U=NO_depth+0.5*dz;
NO_depth_L=NO_depth-0.5*dz;
NO_depth_U(1,:)=2;
NO_depth_L(1,:)=0.0001;

for i=1:size(NO_depth_U,1)
    NO_depth((2*i-1),:)=NO_depth_U(i,:);
    NO_depth((2*i),:)=NO_depth_L(i,:);
    
    
    NO_time_temp((2*i-1),:)=NO_time(i,:);
    NO_time_temp((2*i),:)=NO_time(i,:);
    NO_temp((2*i-1),:)=NO(i,:);
    NO_temp((2*i),:)=NO(i,:);
    NO2_temp((2*i-1),:)=NO2(i,:);
    NO2_temp((2*i),:)=NO2(i,:);
    O3_temp((2*i-1),:)=O3(i,:);
    O3_temp((2*i),:)=O3(i,:);
end

O3_depth=NO_depth;
NO2_depth=NO_depth;

O3=O3_temp;
NO=NO_temp;
NO2=NO2_temp;

NO_time=NO_time_temp;
NO2_time=NO_time;
O3_time=NO_time;



figure('Visible','off')

contourf(O3_time,O3_depth,O3,clim_O3,'LineStyle','none')
set(gca,'CLim',[min(clim_O3) max(clim_O3)])
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
ylim(lim_depth)
xlabel('Date')
colorbar


ylabel('Depth [m]')
title('Modeled O3 profile [ppb_v]')
saveas(gcf,[out_path 'O3_profile.jpeg'])
% 
% 
figure('Visible','off')

contourf(NO_time,NO_depth,NO,clim_NO,'LineStyle','none')
set(gca,'CLim',[min(clim_NO) max(clim_NO)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(lim_depth)
title('Modeled NO profile [ppt_v]')
saveas(gcf,[out_path 'NO_profile.jpeg'])
% 
% 
figure('Visible','off')

contourf(NO2_time,NO2_depth,NO2,clim_NO2,'LineStyle','none')
set(gca,'CLim',[min(clim_NO2) max(clim_NO2)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(lim_depth)
title('Modeled NO2 profile [ppt_v]')
saveas(gcf,[out_path 'NO2_profile.jpeg'])
figure('Visible','off')
% 
% contourf(time(1:4,:),depths(1:4,:),OH,'LineStyle','none')
% %set(gca,'CLim',[min(clim_NO2) max(clim_NO2)])
% colorbar
% set(gca,'XTick',tick_dates)
% datetick('x','keepticks')
% xlabel('Date')
% ylabel('Depth [m]')
% title('Modeled OH profile [1E6 molec. cm^{-3}]')
% saveas(gcf,[out_path 'OH_profile.jpeg'])
% 
% 
% 
% figure('Visible','off')
% 
% contourf(time,depths,HONO,clim_HONO,'LineStyle','none')
% set(gca,'CLim',[min(clim_HONO) max(clim_HONO)])
% colorbar
% set(gca,'XTick',tick_dates)
% datetick('x','keepticks')
% xlabel('Date')
% ylabel('Depth [m]')
% title('Modeled HONO profile [ppt_v]')
% saveas(gcf,[out_path 'HONO_profile.jpeg'])
% 
% 
% 
% 
% figure('Visible','off')
% 
% 
% contourf(time,depths,jNO2,'LineStyle','none')
% %set(gca,'CLim',[-15 max(jNO2(:))])
% % contourf(time,depths,log10(jNO2),'LineStyle','none')
% % set(gca,'CLim',[-15 max(log10(jNO2(:)))])
% 
% 
% %ylim([min(depths(:)) 0])
% colorbar
% set(gca,'XTick',tick_dates)
% datetick('x','keepticks')
% xlabel('Date')
% ylabel('Depth [m]')
% title('Modeled jNO2 profile [s^{-1}]')
% %title('Modeled log_{10}(jNO2) profile [log_{10}(s^{-1})]')
% saveas(gcf,[out_path 'jNO2_profile.jpeg'])
% 
% figure('Visible','off')
% 
% semilogy(time(3,:),VdNO3,'d')
% hold all
% semilogy(time(3,:),VdNO2,'sq')
% 
% semilogy(time(3,:),VdO3,'o')
% set(gca,'XTick',tick_dates)
% datetick('x','keepticks')
% xlabel('Date')
% ylabel('Dry Deposition [cm s^{-1}]')
% title(' Dry deposition rates in the top snow layer')
% legend({'NO3';'NO2';'O3'}, 'Location','EastOutside')
% 
% saveas(gcf,[ out_path 'Dry_depostion.jpeg'])
% 
% figure('Visible','off')
% 
% 
% 
% plot(time(3,:),snow_flux_NO,'o')
% hold all
% plot(time(3,:),snow_flux_NO2,'sq')
% %ylim([-10^11 10^11]) 
% set(gca,'XTick',tick_dates)
% datetick('x','keepticks')
% xlabel('Date')
% ylabel('Snow-atmosphere flux [molec. m^{-2} s^{-1}]')
% title(' Snow-atmosphere flux  of NO_x')
% legend({'NO';'NO2'}, 'Location','EastOutside')
% 
% saveas(gcf,[ out_path 'Snow_atmosphere_flux_NOx.jpeg'])
% 
% 
% figure('Visible','off')
% 
% plot(time(3,:),snow_flux_O3,'o')
% 
% set(gca,'XTick',tick_dates)
% datetick('x','keepticks')
% xlabel('Date')
% ylabel('O3 Snow-atmosphere flux [molec. m^{-2} s^{-1}]')
% title(' Snow-atmosphere flux of ozone')
% 
% saveas(gcf,[ out_path 'Snow_atmosphere_flux_O3.jpeg'])

file2='contour_plots_2009_4_11_2009_4_25/obs.out';

data=load(file2);
index=find(data(:,2)==data(1,2),2,'first');
num_depth_obs=index(2)-index(1);

time_obs=data(:,1);
time_obs=datenum(year,month,day,hour,m,time_obs);
num_time_obs=length(time_obs)/num_depth_obs;
IDT_NO_obs=3;
IDT_NO2_obs=4;
IDT_O3_obs=5;
time_obs=reshape(time_obs,num_depth_obs,num_time_obs);
depths_obs=reshape(data(:,2),num_depth_obs,num_time_obs);
NO_obs=reshape(data(:,IDT_NO_obs),num_depth_obs,num_time_obs);
NO2_obs=reshape(data(:,IDT_NO2_obs),num_depth_obs,num_time_obs);
O3_obs=reshape(data(:,IDT_O3_obs),num_depth_obs,num_time_obs);
snow_temp_obs=reshape(data(:,6),num_depth_obs,num_time_obs);
u_ws_obs=reshape(data(:,7),num_depth_obs,num_time_obs);
v_ws_obs=reshape(data(:,8),num_depth_obs,num_time_obs);
rad_obs=reshape(data(:,9),num_depth_obs,num_time_obs);

index=~isnan(depths_obs(:,1));
depths_obs=depths_obs(index,:);
O3_obs=O3_obs(index,:);
time_obs=time_obs(index,:);
snow_temp_obs=snow_temp_obs(index,:);
NO_obs=NO_obs(index,:);
NO2_obs=NO2_obs(index,:);


% index=depths_obs(:,1)<0;
% depths_obs=depths_obs(index,:);
% O3_obs=O3_obs(index,:);
% time_obs=time_obs(index,:);
% snow_temp_obs=snow_temp_obs(index,:);
% NO_obs=NO_obs(index,:);
% NO2_obs=NO2_obs(index,:);

figure('Visible','off')
subplot(3,1,1)
contourf(NO_time,NO_depth,NO,clim_NO,'LineStyle','none')
set(gca,'CLim',[min(clim_NO) max(clim_NO)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(lim_depth)
title('Modeled NO profile [ppt_v]')
P=get(gca,'Position');

subplot(3,1,2)

contourf(NO2_time,NO2_depth,NO2,clim_NO2,'LineStyle','none')
set(gca,'CLim',[min(clim_NO2) max(clim_NO2)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(lim_depth)
title('Modeled NO2 profile [ppt_v]')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)

subplot(3,1,3)
plot(time_obs(1,:),rad_obs(1,:),'-o')
set(gca,'XTick',tick_dates);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Irradiance [W/(m^2)]')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
saveas(gcf,[out_path 'NO_NO2_rad_profile.jpeg'])

%NO2 modeled, observed, rad
figure('Visible','off')
subplot(3,1,1)
contourf(time_obs,depths_obs,NO2_obs,clim_NO2,'LineStyle','none')
set(gca,'CLim',[min(clim_NO2) max(clim_NO2)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(lim_depth)
title('Observed NO2 profile [ppt_v]')
P=get(gca,'Position');

subplot(3,1,2)

contourf(NO2_time,NO2_depth,NO2,clim_NO2,'LineStyle','none')
set(gca,'CLim',[min(clim_NO2) max(clim_NO2)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(lim_depth)
title('Modeled NO2 profile [ppt_v]')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)

subplot(3,1,3)
plot(time_obs(1,:),rad_obs(1,:),'-o')
set(gca,'XTick',tick_dates);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Irradiance [W/(m^2)]')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
saveas(gcf,[out_path 'NO2_obs_mod_rad_profile.jpeg'])




