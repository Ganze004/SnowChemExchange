% MAQ_lg_20180320+ 
% Matlab script to read the file model_temp.out (and some other output
% files) to at the end plot a comparison of the simulated and observed
% temperatures
%
clc
clear all
close all

dir='../MTU4/output/May/';

path=dir;
mkdir(dir)
directory='../MTU4/output/'

year=2009
month=5
end_month=month
day=1
end_day=2
hour=0
m=0

file2=['../contour results/contour_plots_' num2str(year) '_' num2str(month) '_' num2str(day) '_' num2str(year) '_' num2str(end_month) '_11/obs.out'];

spc_name=MTU_get_spcs(directory);
depths=MTU_get_depths(directory);
time=MTU_get_times(directory);
depths2=[];
time2=[];


for i=1:(length(depths)-1)
    depths2(2*i-1) =depths(i);
    depths2(2*i)=depths(i+1)*0.99;
    time2(2*i-1)=time(i);
    time2(2*i)=time(i);
end
%time=time2;
depths=depths2;

conc_file=[directory 'model_gas_PPX.out']
para_file=[directory 'model_para.out']
data=load(para_file);
NUM_DEPTH=data(1);
NUM_SPEC=data(2);
NUM_TIME=data(3);
MAX_j=data(4);
NUM_TEND=data(5);
NUM_EQ=data(6);
temperature=MTU_get_temp(directory);

data=load(file2);
index=find(data(:,2)==data(1,2),2,'first');
num_depth_obs=index(2)-index(1);

time_obs=data(:,1);
time_obs=datenum(year,month,day,hour,m,time_obs);%-datenum(0,0,0,2,0,0);%offset for time zone
num_time_obs=length(time_obs)/num_depth_obs;
IDT_NO_obs=3;
IDT_NO2_obs=4;
IDT_O3_obs=5;
time_obs=reshape(time_obs,num_depth_obs,num_time_obs);
depths_obs=reshape(data(:,2),num_depth_obs,num_time_obs);
NO_obs=reshape(data(:,IDT_NO_obs),num_depth_obs,num_time_obs);
NO2_obs=reshape(data(:,IDT_NO2_obs),num_depth_obs,num_time_obs);
O3_obs=reshape(data(:,IDT_O3_obs),num_depth_obs,num_time_obs);
snow_temp_obs=reshape(data(:,6),num_depth_obs,num_time_obs);
u_ws_obs=reshape(data(:,7),num_depth_obs,num_time_obs);
v_ws_obs=reshape(data(:,8),num_depth_obs,num_time_obs);
rad_obs=reshape(data(:,9),num_depth_obs,num_time_obs);
wind_obs=sqrt(u_ws_obs.^2+v_ws_obs.^2);
clim_NOx=linspace(0,600,100);%linspace(0,600,100);
clim_NO2=clim_NOx;
clim_NO=linspace(0,200,100);
clim_O3=linspace(0,70,100);
clim_temp=linspace(-100,10,100);
clim_diff=linspace(-200,200,100);

temperature2=zeros(2*NUM_DEPTH,NUM_TIME);
for i=1:NUM_TIME
    for j=1:NUM_DEPTH
        
        temperature2(2*j-1,i)=temperature(j,i);
        temperature2(2*j,i)=temperature(j,i);
        
    end
end

temperature=temperature2;
time=MTU_get_times(directory);
date_tick=linspace(time(1),time(end),5);
%keyboard
[time, depths]=meshgrid(time,depths);
max_temp=max([max(temperature(:)) max(snow_temp_obs(:))]);
min_temp=max([min(temperature(:)) min(snow_temp_obs(:))]);
clim_temp=linspace(min_temp,max_temp,100);

figure('Visible','off')
% plotting the observed temperature
subplot(2,1,1)
contourf(time_obs,depths_obs,snow_temp_obs+273.15,clim_temp,'LineStyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',13,'keepticks')
ylim([min(depths_obs(:)) 1.0])
 
% plotting the simulated temperature
subplot(2,1,2)
contourf(time,depths,temperature,clim_temp,'LineStyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',13,'keepticks')
ylim([min(depths_obs(:)) 1.0])
saveas(gcf,[dir 'model_vs_obs_temp.jpeg'])
close(gcf)
 