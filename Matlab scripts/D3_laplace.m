function [u]=D3_laplace(x,y,z,top_nueman,surf_conc)
%This function assume zero flux on all boundaries except the top x-y plane,
%which is nueman for fluxes out of the system, and Dirchlet for fluxes into
%the system set to the variable surf concentration.
debug=0;
u=0;
num_nodes=numel(z);
num_row=size(z,1);
num_col=size(z,2);
num_depth=size(z,3);
if debug==1
    A=zeros(num_nodes,num_nodes);
    
else
    c_ban=zeros(num_nodes,1);
    xr_ban=zeros(num_nodes,1);
    xl_ban=zeros(num_nodes,1);
    yu_ban=zeros(num_nodes,1);
    yd_ban=zeros(num_nodes,1);
    zu_ban=zeros(num_nodes,1);
    zd_ban=zeros(num_nodes,1);
    
end
b=zeros(num_nodes,1);
node_zu=-num_row*num_col;
node_zd=num_row*num_col;
node_xr=1;
node_xl=-1;
node_yu=-num_col;
node_yd=num_col;
node_c=0;


for depth=1:num_depth
    for row=1:num_row
        for col=1:num_col
            
            
            node=(depth-1)*num_row*num_col+(row-1)*num_col+col;
            if debug==1;
                node_zu=node-num_row*num_col;
                node_zd=node+num_row*num_col;
                node_xr=node+1;
                node_xl=node-1;
                node_yu=node-num_col;
                node_yd=node+num_col;
                
            end
            
            
            %Z Boundaries
            if depth==1
%                 dz_d=abs(z(row,col,depth)-z(row,col,depth+1));
%                 c_ban(node)=-1;
%                 zd_ban(node+node_zd)=1/(2*top_nueman(row,col)*dz_d);
%                 b(node)=-surf_conc/(2*top_nueman(row,col)*dz_d);
                
                
                if top_nueman(row,col)>0
                    %                     if abs(x(row,col,1))<0.01 && abs(y(row,col,1))<0.01
                    
                    if debug==1
                        A(node,node)=1;
                    else
                        c_ban(node)=1;
                    end
                    b(node)=surf_conc;
                    
                    continue
                    
                   
                    
                    %                     else
                    %                         dz_d=abs(z(row,col,depth)-z(row,col,depth+1));
                    %                         A(node,node)=A(node,node)+4/(2*dz_d)*(top_nueman(row,col)-1/dz_d);
                    %                         A(node,node)=A(node,node)+4/(2*dz_d)*(-1/dz_d);
                    %                         A(node,node_zd)=4/(2*dz_d)*(1/dz_d);
                    % %                                                 b(node)=-4/(2*dz_d)*(top_nueman(row,col)*surf_conc);
                    %
                    %                     end
                    
                else
                    dz_d=abs(z(row,col,depth)-z(row,col,depth+1));
                    if debug==1
                        A(node,node)=A(node,node)+4/(2*dz_d)*(top_nueman(row,col)-1/dz_d);
                        A(node,node_zd)=4/(2*dz_d)*(1/dz_d);
                    else
                        c_ban(node)=c_ban(node)+4/(2*dz_d)*(top_nueman(row,col)-1/dz_d);
                        zd_ban(node+node_zd)=4/(2*dz_d)*(1/dz_d);
                    end
                    %b(node)=4/(2*dz_d)*top_nueman(row,col);
                    continue
                end
                
            elseif depth==num_depth
                dz_u=abs(z(row,col,depth)-z(row,col,depth-1));
                if debug==1
                    A(node,node)=A(node,node)+4/(2*dz_u)*(-1/dz_u);
                    A(node,node_zu)=4/(2*dz_u)*(1/dz_u);
                else
                    c_ban(node)=c_ban(node)+4/(2*dz_u)*(-1/dz_u);                  
                    zu_ban(node+node_zu)=4/(2*dz_u)*(1/dz_u);
%                         c_ban(node)=1;
%                         b(node)=0;
%                         continue
                end
            else
                dz_u=abs(z(row,col,depth)-z(row,col,depth-1));
                dz_d=abs(z(row,col,depth)-z(row,col,depth+1));
                if debug==1
                    A(node,node)=A(node,node)-2*(1/(dz_u+dz_d)*(1/dz_u+1/dz_d));
                    A(node,node_zu)=2*(1/(dz_u*(dz_u+dz_d)));
                    A(node,node_zd)=2*(1/(dz_d*(dz_u+dz_d)));
                else
                    c_ban(node)=c_ban(node)-2*(1/(dz_u+dz_d)*(1/dz_u+1/dz_d));
                    zu_ban(node+node_zu)=2*(1/(dz_u*(dz_u+dz_d)));
                    zd_ban(node+node_zd)=2*(1/(dz_d*(dz_u+dz_d)));
                end
                
            end
            
            
            %Y Boundaries
            if row==1
                dy_d=abs(y(row,col,depth)-y(row+1,col,depth));
                if debug==1
                    A(node,node)=A(node,node)+4/(2*dy_d)*(-1/dy_d);
                    A(node,node_yd)=4/(2*dy_d)*(1/dy_d);
                else
                    c_ban(node)=c_ban(node)+4/(2*dy_d)*(-1/dy_d);
                    yd_ban(node+node_yd)=4/(2*dy_d)*(1/dy_d);
                end
            elseif row==num_row
                
                dy_u=abs(y(row,col,depth)-y(row-1,col,depth));
                if debug==1
                    A(node,node)=A(node,node)+4/(2*dy_u)*(-1/dy_u);
                    A(node,node_yu)=4/(2*dy_u)*(1/dy_u);
                else
                    c_ban(node)=c_ban(node)+4/(2*dy_u)*(-1/dy_u);
                    yu_ban(node+node_yu)=4/(2*dy_u)*(1/dy_u);
                end
                
            else
                
                
                dy_u=abs(y(row,col,depth)-y(row-1,col,depth));
                dy_d=abs(y(row,col,depth)-y(row+1,col,depth));
                if debug==1
                    A(node,node)=A(node,node)-2/(dy_u+dy_d)*(1/dy_u+1/dy_d);
                    A(node,node_yu)=2*(1/(dy_u*(dy_u+dy_d)));
                    A(node,node_yd)=2*(1/(dy_d*(dy_u+dy_d)));
                else
                    c_ban(node)=c_ban(node)-2/(dy_u+dy_d)*(1/dy_u+1/dy_d);
                    yu_ban(node+node_yu)=2*(1/(dy_u*(dy_u+dy_d)));
                    yd_ban(node+node_yd)=2*(1/(dy_d*(dy_u+dy_d)));
                end
                
            end
            
            
            %X Boundaries
            
            if col==1
                dx_r=abs(x(row,col,depth)-x(row,col+1,depth));
                if debug==1
                    A(node,node)=A(node,node)+4/(2*dx_r)*(-1/dx_r);
                    A(node,node_xr)=4/(2*dx_r)*(1/dx_r);
                else
                    c_ban(node)=c_ban(node)+4/(2*dx_r)*(-1/dx_r);
                    xr_ban(node+node_xr)=4/(2*dx_r)*(1/dx_r);
                end
            elseif col==num_col
                dx_l=abs(x(row,col,depth)-x(row,col-1,depth));
                
                if debug==1
                    A(node,node)=A(node,node)+4/(2*dx_l)*(-1/dx_l);
                    A(node,node_xl)=4/(2*dx_l)*(1/dx_l);
                else
                    c_ban(node)=c_ban(node)+4/(2*dx_l)*(-1/dx_l);
                    xl_ban(node+node_xl)=4/(2*dx_l)*(1/dx_l);
                end
            else
                
                dx_r=abs(x(row,col,depth)-x(row,col+1,depth));
                dx_l=abs(x(row,col,depth)-x(row,col-1,depth));
                
                if debug==1
                    A(node,node)=A(node,node)-2*(1/(dx_r+dx_l)*(1/dx_r+1/dx_l));
                    A(node,node_xr)=2*(1/(dx_r*(dx_r+dx_l)));
                    A(node,node_xl)=2*(1/(dx_l*(dx_r+dx_l)));
                else
                    c_ban(node)=c_ban(node)-2*(1/(dx_r+dx_l)*(1/dx_r+1/dx_l));
                    xr_ban(node+node_xr)=2*(1/(dx_r*(dx_r+dx_l)));
                    xl_ban(node+node_xl)=2*(1/(dx_l*(dx_r+dx_l)));
                end
            end
            
            
            
            
            
            
        end
        
    end
    
    
end
if debug~=1
d=[node_zu node_yu node_xl node_c node_xr node_yd  node_zd  ]';

A=spdiags([zu_ban yu_ban  xl_ban c_ban xr_ban yd_ban  zd_ban  ],d,num_nodes,num_nodes);
end

if debug==1
    
    [A d]=spdiags(A);
    
end
u=A\b;
%keyboard
%Reshape into a square matrix
u=reshape(u,num_row,num_col,num_depth);
end