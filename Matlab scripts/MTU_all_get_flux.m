function rj=MTU_all_get_flux(directory)

% MAQ_lg_20180320+ 
% Matlab script to read the flux output files
% --------------------------------------------------------------

spc_name=MTU_get_spcs(directory)
rj_file=[directory 'ALL_FLUX.out']
para_file=[directory 'model_para.out']
data=load(para_file);
NUM_DEPTH=data(1);
NUM_SPEC=data(2);
NUM_TIME=data(3);
MAX_j=data(4);

% depths=MTU_get_depths(directory)
% for i=1,length(depths)-1
%    depths(i) =mean(depths(i:i+1));
%     
% end
% 
% depths=depths(1:end-1);
%  time=1:NUM_TIME;
% [time, depths]=meshgrid(time,depths);
% 
%data=load(rj_file);
data=dlmread(rj_file);  % using dlmread instead of load to deal with a matrix that is not completely filled

rj=reshape(data,NUM_DEPTH,NUM_TIME,NUM_SPEC);

% rj=rj';
%  for j=1:MAX_j
%  for i=130%: NUM_SPEC
% rj=squeeze(data(j,:,i,:))';
% figure('Visible','off') 
%  contourf(time,depths,rj,'LineStyle','none')
%  colorbar
%  ylim([min(depths(:)) 0.05])
%  saveas(gcf,[dir 'rj_' spc_name{i} '_' num2str(j) '.jpeg'])
%  close(gcf)
%  
%  end
%  end
end