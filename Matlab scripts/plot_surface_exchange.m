clc
clear all
close all

% definition of diagnostics output
dir='../MTU4/output/May/';
path=dir;
mkdir(dir)
directory='../MTU4/output/';

year=2009
month=5
end_month=month
day=1
end_day=11
hour=0
m=0

file2=['../contour results/contour_plots_' num2str(year) '_' num2str(month) '_' num2str(day) '_' num2str(year) '_' num2str(end_month) '_11/obs.out'];

time_diff=3;  %Time difference in hours for time zones  2 for SUMMIT

% %Raw MET data
% load summit_MET_data.mat
% load summit_MET_hts.mat
% load summit_MET_temperature
% load summit_eddy_covar.mat
% load summit_MET_ozone_data.mat
% 
% MET_temp_bot(MET_temp_bot==-9999)=NaN;
% MET_temp_mid(MET_temp_mid==-9999)=NaN;
%     
% index=NO2_time2==0;
% NO2_time2=NO2_time2(~index);
% NO2_2=NO2_2(~index);
%     
% index=NO2_time1==0;
% NO2_time1=NO2_time1(~index);
% NO2_1=NO2_1(~index);
%     
% index=NO_time2==0;
% NO_time2=NO_time2(~index);
% NO_2=NO_2(~index);
%     
% index=NO_time1==0;
% NO_time1=NO_time1(~index);
% NO_1=NO_1(~index);
%     
% index=O3_time==0;
% O3_time=O3_time(~index);
% O3_1=O3_1(~index);
% O3_2=O3_2(~index);
%    
%     
%     
% NO2_1=interp1(NO2_time1,NO2_1,NO2_time2,'linear','extrap');
% NO_1=interp1(NO_time1,NO_1,NO2_time2,'linear','extrap');
% NO_2=interp1(NO_time2,NO_2,NO2_time2,'linear','extrap');
% O3_1=interp1(O3_time,O3_1,NO2_time2,'linear','extrap');
% O3_2=interp1(O3_time,O3_2,NO2_time2,'linear','extrap');
% in_2_ht=interp1(ht_jd,in_2_ht,NO2_time2,'linear','extrap');
% in_1_ht=interp1(ht_jd,in_1_ht,NO2_time2,'linear','extrap');
% MET_temp_mid=interp1(MET_temp_jd,MET_temp_mid,NO2_time2,'linear','extrap');
% MET_temp_bot=interp1(MET_temp_jd,MET_temp_bot,NO2_time2,'linear','extrap');
%     
% MET_NO2=[NO2_2;NO2_1];
% MET_NO=[NO_2;NO_1];
% MET_O3=[O3_2;O3_1];
% MET_NOx_time=[NO2_time2;NO2_time2];
% MET_O3_time=[O3_time;O3_time];
% MET_temp=[MET_temp_mid;MET_temp_bot]+273.15;
    
spc_name=MTU_get_spcs(directory);
depths=MTU_get_depths(directory);
temperature=MTU_get_temp(directory);
min_30=1/(24*2);
min_day=1;
min_2hr=min_30*4;
min_4hr=2*min_2hr;

conc_file=[directory 'model_gas_PPX.out']
para_file=[directory 'model_para.out']
data=load(para_file);
NUM_DEPTH=data(1)%+1;
NUM_SPEC=data(2);

NUM_TIME=data(3);
MAX_j=data(4);
NUM_TEND=data(5);
%data=load(conc_file);
data=dlmread(conc_file);  % using dlmread instead of load to deal with a matrix that is not completely filled

%rj=MTU_get_rj(directory);
%tend=MTU_get_tend(directory);
%rj=reshape(data,MAX_j,NUM_TIME,NUM_SPEC,NUM_DEPTH);
%temperature=MTU_get_temp(directory);
%keyboard

[obs_irr model_irr]=MTU_get_irr(directory);
[flux]=MTU_get_flux(directory);
flux=flux*100^3; %convert from m molecules/cm3s to molecules/m2 s
[all_flux]=MTU_all_get_flux(directory);
all_flux=all_flux*100^3; %convert from m molecules/cm3s to molecules/m2 s
conc=reshape(data,NUM_DEPTH,NUM_TIME,NUM_SPEC);
O3=conc(:,:,strcmp(spc_name,'O3'))*10^9;
NO2=conc(:,:,strcmp(spc_name,'NO2'))*10^12;
NO=conc(:,:,strcmp(spc_name,'NO'))*10^12;
NOy=conc(:,:,strcmp(spc_name,'NO'))+conc(:,:,strcmp(spc_name,'NO2'))+conc(:,:,strcmp(spc_name,'HNO4'))+conc(:,:,strcmp(spc_name,'HNO3'))+conc(:,:,strcmp(spc_name,'HONO'))+conc(:,:,strcmp(spc_name,'N2O5'))+conc(:,:,strcmp(spc_name,'NO3'))+conc(:,:,strcmp(spc_name,'PAN'));
time=MTU_get_times(directory);

%date_tick=linspace(time(1),time(end),5);
%keyboard
edge_times=time;
edge_depths=depths;
[edge_times, edge_depths]=meshgrid(edge_times,edge_depths);
for i=1:NUM_DEPTH
    depths(i)=(depths(i)+depths(i+1))/2;
end
depths=depths(1:end-1);
[time, depths]=meshgrid(time,depths);

data=load(file2);
index=find(data(:,2)==data(1,2),2,'first');
num_depth_obs=index(2)-index(1);

time_obs=data(:,1);
time_obs=datenum(year,month,day,hour,m,time_obs);%-datenum(0,0,0,2,0,0);%offset for time zone
num_time_obs=length(time_obs)/num_depth_obs;
IDT_NO_obs=3;
IDT_NO2_obs=4;
IDT_O3_obs=5;
time_obs=reshape(time_obs,num_depth_obs,num_time_obs);
depths_obs=reshape(data(:,2),num_depth_obs,num_time_obs);
NO_obs=reshape(data(:,IDT_NO_obs),num_depth_obs,num_time_obs);
NO2_obs=reshape(data(:,IDT_NO2_obs),num_depth_obs,num_time_obs);
O3_obs=reshape(data(:,IDT_O3_obs),num_depth_obs,num_time_obs);
snow_temp_obs=reshape(data(:,6),num_depth_obs,num_time_obs);
u_ws_obs=reshape(data(:,7),num_depth_obs,num_time_obs);
v_ws_obs=reshape(data(:,8),num_depth_obs,num_time_obs);
rad_obs=reshape(data(:,9),num_depth_obs,num_time_obs);
wind_obs=sqrt(u_ws_obs.^2+v_ws_obs.^2);
clim_NOx=linspace(0,600,100);%linspace(0,600,100);
clim_NO2=clim_NOx;
clim_NO=linspace(0,200,100);
clim_O3=linspace(0,70,100);
clim_temp=linspace(-100,10,100);
clim_diff=linspace(-200,200,100);

index=~isnan(depths_obs(:,1));
depths_obs=depths_obs(index,:);
O3_obs=O3_obs(index,:);
time_obs=time_obs(index,:);
snow_temp_obs=snow_temp_obs(index,:);
NO_obs=NO_obs(index,:);
NO2_obs=NO2_obs(index,:);

%Convert Celsius to Kelvin
snow_temp_obs=snow_temp_obs+273.15;

%calculate nd of measured species in moelcules/ m3
R=8.2057*10^-5; %gas constant in m3 atm/ K mol
P=1; %atmosphere
Av=6.022*10^23;  %Avogadros number

nd_air=P./(R.*snow_temp_obs)*Av;
nd_NO2_obs=nd_air.*NO2_obs*10^-12;
nd_NO_obs=nd_air.*NO_obs*10^-12;
nd_O3_obs=nd_air.*O3_obs*10^-9;

%keyboard
% index=depths_obs(:,1)<0;
% depths_obs=depths_obs(index,:);
% O3_obs=O3_obs(index,:);
% time_obs=time_obs(index,:);
% snow_temp_obs=snow_temp_obs(index,:);
% NO_obs=NO_obs(index,:);
% NO2_obs=NO2_obs(index,:);
% 
% index=depths(:,1)<0;
% depths=depths(index,:);
% O3=O3(index,:);
% time=time(index,:);
% NO=NO(index,:);
% NO2=NO2(index,:);

%we use the top NO,NO2, and ozone measuremnts to drive the model,so im
%setting the height of the top layer reference to the heights measured
O3_int=interp2(time,depths,O3,time_obs,depths_obs,'linear');
NO_int=interp2(time,depths,NO,time_obs,depths_obs,'linear');
NO2_int=interp2(time,depths,NO2,time_obs,depths_obs,'linear');
% rad_int=interp2(time,depths,rad,time_obs,depths_obs,'linear');
% grad_int=interp2(time,depths,grad,time_obs,depths_obs,'linear');
% usnow_int=interp2(time,depths,usnow,time_obs,depths_obs,'linear');
% Ts_int=interp2(time,depths,Ts,time_obs,depths_obs,'linear');


% 
% O3_obs_int=interp2(time_obs,depths_obs,O3_obs,time,depths,'linear');
% NO_obs_int=interp2(time_obs,depths_obs,NO_obs,time,depths,'linear');
% NO2_obs_int=interp2(time_obs,depths_obs,NO2_obs,time,depths,'linear');
F=TriScatteredInterp(time_obs(:),depths_obs(:),NO_obs(:));
NO_obs_int=F(time,depths);
F=TriScatteredInterp(time_obs(:),depths_obs(:),NO2_obs(:));
NO2_obs_int=F(time,depths);
F=TriScatteredInterp(time_obs(:),depths_obs(:),O3_obs(:));
O3_obs_int=F(time,depths);



% O3_int(1,:)=O3_obs(1,:);
% NO_int(1,:)=NO_obs(1,:);
% NO2_int(1,:)=NO2_obs(1,:);

date_tick=linspace(time_obs(1,1),time_obs(1,end),5);
date_tick=linspace(datenum(2009,month,15,0,0,0),datenum(2009,month,30,0,0,0),5);
date_tick2=linspace(time_obs(1,1),time_obs(1,end),3);

day_span=[min(date_tick) max(date_tick)];
date_x=floor(time_obs(1,1)):floor(time_obs(1,end));
    date_y=[min(depths(:)) max(depths(:))];
    [date_x date_y]=meshgrid(date_x,date_y);

%Raw MET data
load ../Summit.data/summit_MET_data.mat
load ../Summit.data/summit_MET_hts.mat
load ../Summit.data/summit_MET_temperature
load ../Summit.data/summit_MET_eddy_covar.mat
load ../Summit.data/summit_MET_ozone_data.mat

MET_temp_bot(MET_temp_bot==-9999)=NaN;
MET_temp_mid(MET_temp_mid==-9999)=NaN;
    
index=NO2_time2==0;
NO2_time2=NO2_time2(~index);
NO2_2=NO2_2(~index);

index=NO2_time1==0;
NO2_time1=NO2_time1(~index);
NO2_1=NO2_1(~index);

index=NO_time2==0;
NO_time2=NO_time2(~index);
NO_2=NO_2(~index);

index=NO_time1==0;
NO_time1=NO_time1(~index);
NO_1=NO_1(~index);

index=O3_time==0;
O3_time=O3_time(~index);
O3_1=O3_1(~index);
O3_2=O3_2(~index);


%adjust for UTC from GMT
NO2_time2=NO2_time2-1/24*time_diff;
NO2_time1=NO2_time1-1/24*time_diff;
NO_time2=NO_time2-1/24*time_diff;
NO_time1=NO_time1-1/24*time_diff;
O3_time=O3_time-1/24*time_diff;

[O3_time O3_1 O3_2]=date_index(day_span,O3_time,O3_1, O3_2);
[NO_time2 NO_2]=date_index(day_span,NO_time2,NO_2);
[NO2_time2 NO2_2]=date_index(day_span, NO2_time2, NO2_2);
[NO_time1 NO_1]=date_index(day_span,NO_time1,NO_1);
[NO2_time1 NO2_1]=date_index(day_span, NO2_time1, NO2_1);
[ht_jd in_1_ht in_2_ht]=date_index(day_span, ht_jd, in_1_ht, in_2_ht);
[MET_temp_jd MET_temp_bot MET_temp_mid]=date_index(day_span,MET_temp_jd, MET_temp_bot, MET_temp_mid);


%NO_2(NO_2==0)=NaN;
NO_2(NO_2<0)=0;
NO_time2(NO_time2==0)=NaN;
%NO2_2(NO2_2==0)=NaN;
NO2_2(NO2_2<0)=0;
NO2_time2(NO2_time2==0)=NaN;
%O3_2(O3_2==0)=NaN;
O3_2(O3_2<0)=0;
O3_time(O3_time==0)=NaN;
%NO_1(NO_1==0)=NaN;
NO_1(NO_1<0)=0;
NO_time1(NO_time1==0)=NaN;
%NO2_1(NO2_1==0)=NaN;
NO2_1(NO2_1<0)=0;
NO2_time1(NO_time1==0)=NaN;
%O3_1(O3_1==0)=NaN;
O3_1(O3_1<0)=0;

NO2_1=interp1(NO2_time1,NO2_1,NO2_time2,'linear','extrap');
NO_1=interp1(NO_time1,NO_1,NO2_time2,'linear','extrap');
NO_2=interp1(NO_time2,NO_2,NO2_time2,'linear','extrap');
O3_1=interp1(O3_time,O3_1,NO2_time2,'linear','extrap');
O3_2=interp1(O3_time,O3_2,NO2_time2,'linear','extrap');
in_2_ht=interp1(ht_jd,in_2_ht,NO2_time2,'linear','extrap');
in_1_ht=interp1(ht_jd,in_1_ht,NO2_time2,'linear','extrap');
MET_temp_mid=interp1(MET_temp_jd,MET_temp_mid,NO2_time2,'linear','extrap');
MET_temp_bot=interp1(MET_temp_jd,MET_temp_bot,NO2_time2,'linear','extrap');

MET_NO2=[NO2_2;NO2_1];
MET_NO=[NO_2;NO_1];
MET_O3=[O3_2;O3_1];
MET_NOx_time=[NO2_time2;NO2_time2];
MET_O3_time=[O3_time;O3_time];
MET_temp=[MET_temp_mid;MET_temp_bot]+273.15;
MET_ht=[in_2_ht;in_1_ht];
MET_NOx=MET_NO+MET_NO2;


MET_nd_air=P./(R.*MET_temp)*Av;
MET_NOx=MET_nd_air.*MET_NOx*10^-12;
MET_NO=MET_nd_air.*MET_NO*10^-12;
MET_NO2=MET_nd_air.*MET_NO2*10^-12;
MET_O3=MET_nd_air.*MET_O3*10^-9;

%set(0,'defaultaxeslinestyleorder',{'*','+','o','sq','Color','g'}) %or whatever you want

% 
O3_2=zeros(size(O3,1)*2,size(O3,2));
NO_2=zeros(size(NO,1)*2,size(NO,2));
NO2_2=zeros(size(NO2,1)*2,size(NO2,2));
time_2=zeros(size(time,1)*2,size(time,2));
depths_2=zeros(size(depths,1)*2,size(depths,2));
temperature_2=zeros(size(depths,1)*2,size(depths,2));
time2=time; %used for all_fluxes

for i=1:NUM_DEPTH
   O3_2(2*i-1,:)=O3(i,:);
   O3_2(2*i,:)=O3(i,:);
   NO_2(2*i-1,:)=NO(i,:);
   NO_2(2*i,:)=NO(i,:);
   NO2_2(2*i-1,:)=NO2(i,:);
   NO2_2(2*i,:)=NO2(i,:);
   time_2(2*i-1,:)=time(i,:);
   time_2(2*i,:)=time(i,:);
   depths_2(2*i-1,:)=edge_depths(i,:);
   depths_2(2*i,:)=edge_depths(i+1,:);
   temperature_2(2*i-1,:)=temperature(i,:);
   temperature_2(2*i,:)=temperature(i,:);
end

O3=O3_2;
NO=NO_2;
NO2=NO2_2;
time=time_2;
depths=depths_2;
temperature=temperature_2;
nd_air_SE=P./(R.*temperature);


index=find(depths(:,1)>0,1,'last');
mean_O3_SE=0.5*(O3(index,:)+O3(index+1,:)).*(nd_air_SE(index,:)+nd_air_SE(index+1,:)).*Av*10^-9;
mean_nd_O3_obs=0.5*(nd_O3_obs(1,:)+nd_O3_obs(2,:));

%keyboard
spin_up=3;  %days
X=[time_obs(end,1),time_obs(end,1),time_obs(end,1)+spin_up,time_obs(end,1)+spin_up];
Y=[depths_obs(end,1),0,0,depths_obs(end,1)];
C=zeros(size(Y));


%index species to date_tick
xy=meshgrid(time(1,:),1:size(flux,1));

[xy flux]=date_index(day_span,xy,flux);

[time O3 NO NO2 depths temperature nd_air_SE mean_O3_SE NOy]=date_index(day_span,time,O3,NO, NO2,depths,temperature,nd_air_SE, mean_O3_SE, NOy);
[time_obs O3_obs NO_obs NO2_obs depths_obs snow_temp_obs nd_air  mean_nd_O3_obs nd_NO2_obs nd_NO_obs nd_O3_obs wind_obs ]=date_index(day_span,time_obs,O3_obs,NO_obs, NO2_obs,depths_obs,snow_temp_obs,nd_air, mean_nd_O3_obs,nd_NO2_obs,nd_NO_obs,nd_O3_obs,wind_obs(1:size(O3_obs,1),:));
     
%Fluxes and surface exchange
cor_var=@(x,y) sum((x-mean(x)).*(y-mean(y))./(length(x)*sqrt(std(x)*std(y))));
[Kh]=calculate_Kh(time_obs(1,:));
[Kh2]=calculate_Kh(MET_NOx_time(1,:));

%Ylim and ticks for fluxes
NO2_YTick=logspace(10,14,5);
NO2_YTick_Labels = cellstr(num2str(round(log10(NO2_YTick(:))), '10^%d'));

NO_YTick=logspace(10,14,5);
NO_YTick_Labels = cellstr(num2str(round(log10(NO_YTick(:))), '10^%d'));

O3_YTick=logspace(-5,0,6);
O3_YTick_Labels = cellstr(num2str(round(log10(O3_YTick(:))), '10^%d'));

O3_SE_YTick=logspace(-5,0,6);
O3_SE_YTick_Labels = cellstr(num2str(round(log10(O3_YTick(:))), '10^%d'));

% % %NO2
% 
% MET_flux=-((nd_NO2_obs(1,:)-nd_NO2_obs(2,:))./(depths_obs(1,:)-depths_obs(2,:))).*Kh;
% %MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
% MET_flux_mean=mean(MET_flux);
% MET_flux_std=std(MET_flux);
% SE=flux(find(1==strcmp(spc_name,'NO2')),:);
% MET_time=time_obs(1,:);
% [MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);
% 
% index=MET_flux>0;
% up_MET=MET_flux(index);
% up_MET_time=MET_time(index);
% index=MET_flux<0;
% down_MET=abs(MET_flux(index));
% down_MET_time=MET_time(index);
% 
% 
% SE_time=time(1,:);
% [SE_time SE]=average_x(SE_time,SE,min_day);
% index=SE<0;
% down_SE=abs(SE(index));
% down_SE_time=SE_time(index);
% index=SE>0;
% up_SE=SE(index);
% up_SE_time=SE_time(index);
% 
% SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
% MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));
% 
% % SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% % MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% % SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% % MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));
% % 
% %figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% figure('Visible','off')
% subplot(4,1,1)
% contourf(time_obs,depths_obs,NO2_obs,clim_NO2,'Linestyle','none')
% ch=colorbar;
% caxis([min(clim_NO2) max(clim_NO2)])
% colorbar('YTick',linspace(min(clim_NO2),max(clim_NO2),4))
% 
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% set(gca,'xtick',[])
% set(gca,'xticklabel',[])
% %xlabel('Date')
% ylabel('Depth [m]')
% %ylim([-2 0])
% ylim([min(depths_obs(:)) max(depths_obs(:))])
% title('Interpolated 30-minute measurements of NO_2 [ppt_v]')
% hold all
% plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
% plot(date_x,date_y,'-.','Color','w')
% ttt=min(time_obs(:)):1/48:max(time_obs(:));
% ddd=zeros(size(ttt));
% dddd=ddd+min(depths_obs(:));
% 
% ttt=[ttt;ttt];
% ddd=[ddd;dddd];
% 
% %plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
% P=get(gca,'Position');
% 
% 
% subplot(4,1,2)
% contourf(time,depths,NO2,clim_NO2,'Linestyle','none')
% colorbar
% caxis([min(clim_NO2) max(clim_NO2)])
% colorbar('YTick',linspace(min(clim_NO2),max(clim_NO2),4))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% set(gca,'xtick',[])
% set(gca,'xticklabel',[])
% %xlabel('Date')
% ylabel('Depth [m]')
% %ylim([-2 0])
% ylim([min(depths_obs(:)) max(depths_obs(:))])
% title('Modeled NO_2 profile [ppt_v]')
% hold all
% 
% plot(date_x,date_y,'-.','Color','w')
% 
% 
% subplot(4,1,3)
% semilogy(up_MET_time,up_MET,'*','Color','b' );
% %plot(up_MET_time,up_MET );
% hold all
% semilogy(down_MET_time,down_MET,'sq','Color','g');
% semilogy(up_SE_time,up_SE,'d','Color','b' );
% semilogy(down_SE_time,down_SE,'x','Color','g');
% %plot(down_MET_time,down_MET,'sq','Color','g');
% 
% set(gca,'XTick',date_tick);
% 
% datetick('x',2,'keepticks')
% set(gca,'xtick',[])
% legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% %xlabel('Date')
% ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
%  ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
%  set(gca,'YTick',NO2_YTick)
%  set(gca,'YTickLabel',NO2_YTick_Labels)
% 
% %ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
% %ylim([min(depths_obs(:)) max(depths_obs(:))])
% title('NO_2 24-hour fluxes ')
% 
% 
% %Modifications 
% 
% grad=nd_NO2_obs(1,:)-nd_NO2_obs(2,:);
% grad_NO=nd_NO_obs(1,:)-nd_NO_obs(2,:);
% grad(grad_NO>0)=grad(grad_NO>0)-grad_NO(grad_NO>0);
% MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
% %MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
% MET_flux_mean=mean(MET_flux);
% MET_flux_std=std(MET_flux);
% SE=flux(find(1==strcmp(spc_name,'NO2')),:);
% MET_time=time_obs(1,:);
% [MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);
% 
% index=MET_flux>0;
% up_MET=MET_flux(index);
% up_MET_time=MET_time(index);
% index=MET_flux<0;
% down_MET=abs(MET_flux(index));
% down_MET_time=MET_time(index);
% 
% 
% SE_time=time(1,:);
% [SE_time SE]=average_x(SE_time,SE,min_day);
% index=SE<0;
% down_SE=abs(SE(index));
% down_SE_time=SE_time(index);
% index=SE>0;
% up_SE=SE(index);
% up_SE_time=SE_time(index);
% 
%  subplot(4,1,4)
% semilogy(up_MET_time,up_MET,'*','Color','b' );
% %plot(up_MET_time,up_MET );
% hold all
% semilogy(down_MET_time,down_MET,'sq','Color','g');
% semilogy(up_SE_time,up_SE,'d','Color','b' );
% semilogy(down_SE_time,down_SE,'x','Color','g');
% %plot(down_MET_time,down_MET,'sq','Color','g');
% 
% set(gca,'XTick',date_tick);
% 
% datetick('x',2,'keepticks')
% %set(gca,'xtick',[])
% legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% xlabel('Date')
% ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
%  ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
%  set(gca,'YTick',NO2_YTick)
%  set(gca,'YTickLabel',NO2_YTick_Labels)
% 
% %ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
% %ylim([min(depths_obs(:)) max(depths_obs(:))])
% title({'Measured Fluxes modified as NO_2 - NO'})
% 
% if(exist([path 'flux_model_surface_exchange_NO2.jpeg'],'file'))
%     delete([path 'flux_model_surface_exchange_NO2_old.jpeg'])
%     movefile([path 'flux_model_surface_exchange_NO2.jpeg'],[path 'flux_model_surface_exchange_NO2_old.jpeg'])
% end
% saveas(gcf,[path 'flux_model_surface_exchange_NO2.jpeg'])

% %NO2

MET_flux=-(nd_NO2_obs(1,:)-nd_NO2_obs(2,:))./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-

grad=(MET_NO2(1,:)-MET_NO2(2,:));
MET_flux=-(grad)./(MET_ht(1,:)-MET_ht(2,:)).*Kh2;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-


MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);

MET_time=MET_NOx_time(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=flux(find(1==strcmp(spc_name,'NO2')),:);
% MET_time=time_obs(1,:);
% [MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
fid=fopen('SE_NO2.txt','w');
fprintf(fid,'%f %f \n',[SE_time'  SE']');
fclose(fid);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
subplot(4,1,1)
contourf(time_obs,depths_obs,NO2_obs,clim_NO2,'Linestyle','none')
colorbar
caxis([min(clim_NO2) max(clim_NO2)])
colorbar('YTick',linspace(min(clim_NO2),max(clim_NO2),3))

set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%title('Measured MET tower([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Interpolated 30-minute measurements of NO_2 [ppt_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
plot(date_x,date_y,'-.','Color','w')
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');


subplot(4,1,2)
contourf(time,depths,NO2,clim_NO2,'Linestyle','none')
colorbar
caxis([min(clim_NO2) max(clim_NO2)])
colorbar('YTick',linspace(min(clim_NO2),max(clim_NO2),3))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Modeled NO_2 profile [ppt_v]')
hold all

plot(date_x,date_y,'-.','Color','w')

subplot(4,1,3)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'x','Color','g');

set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
%xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
 ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
 set(gca,'YTick',NO_YTick)
 set(gca,'YTickLabel',NO_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title('NO_2 24-hour fluxes ')
% 

grad_NO2=-diff(MET_NO2);
grad_NO=-diff(MET_NO);
grad=grad_NO2+grad_NO;

grad(grad_NO>0)=grad(grad_NO>0)-grad_NO(grad_NO>0);
grad_NO(grad_NO>0)=grad_NO(grad_NO>0)-grad_NO(grad_NO>0);
grad(grad_NO2>0)=grad(grad_NO2>0)-grad_NO2(grad_NO2>0);
grad_NO2(grad_NO2>0)=grad_NO2(grad_NO2>0)-grad_NO2(grad_NO2>0);
grad(grad==0)=NaN;
grad_NO(grad==0)=NaN;
grad_NO2(grad==0)=NaN;
MET_flux=-(grad)./(-diff(MET_ht)).*Kh2;
MET_flux_NO=-(grad_NO)./(-diff(MET_ht)).*Kh2;
MET_flux_NO2=-(grad_NO2)./(-diff(MET_ht)).*Kh2;

%MET_flux=MET_flux_NO;

MET_time=MET_NOx_time(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

SE=(flux(find(1==strcmp(spc_name,'NO2')),:));

%MET_flux(MET_flux_NO<0)=MET_flux(MET_flux_NO<0)+MET_flux_NO(MET_flux_NO<0);
%MET_flux(MET_flux_NO2<0)=MET_flux(MET_flux_NO2<0)+MET_flux_NO2(MET_flux_NO2<0);

% MET_time=MET_NOx_time(1,:);
% [MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);
MET_time_NO=MET_NOx_time(1,:);
MET_time_NO2=MET_NOx_time(1,:);
[MET_time_NO MET_flux_NO]=average_x(MET_time_NO,MET_flux_NO,min_day);
[MET_time_NO2 MET_flux_NO2]=average_x(MET_time_NO2,MET_flux_NO2,min_day);
MET_flux=MET_flux_NO2;

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

subplot(4,1,4)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'x','Color','g');

set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
if(isempty(down_MET))
 legend({'MET Up';'Model Up';'Model Down'},'Location','EastOutside')   
else
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
end
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
 ylim(gca,[min(NO_YTick) max(NO_YTick)])
 set(gca,'YTick',NO_YTick)
 set(gca,'YTickLabel',NO_YTick_Labels)
title('"Cleaned" 24-h NO_2 flux ')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)

if(exist([path 'flux_model_surface_exchange_NO2.jpeg'],'file'))
    delete([path 'flux_model_surface_exchange_NO2_old.jpeg'])
    movefile([path 'flux_model_surface_exchange_NO2.jpeg'],[path 'flux_model_surface_exchange_NO2_old.jpeg'])
end
saveas(gcf,[path 'flux_model_surface_exchange_NO2.jpeg'])

% %NO2 mod

MET_flux=-(((nd_NO2_obs(1,:)-nd_NO_obs(1,:))-(nd_NO2_obs(2,:)-nd_NO_obs(2,:)))./(depths_obs(1,:)-depths_obs(2,:))).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=flux(find(1==strcmp(spc_name,'NO2')),:);
MET_time=time_obs(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
subplot(3,1,1)
contourf(time_obs,depths_obs,NO2_obs,clim_NO2,'Linestyle','none')
ch=colorbar;
caxis([min(clim_NO2) max(clim_NO2)])
colorbar('YTick',linspace(min(clim_NO2),max(clim_NO2),4))

set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Interpolated 30-minute measurements of NO_2 [ppt_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
plot(date_x,date_y,'-.','Color','w')
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');


subplot(3,1,2)
contourf(time,depths,NO2,clim_NO2,'Linestyle','none')
colorbar
caxis([min(clim_NO2) max(clim_NO2)])
colorbar('YTick',linspace(min(clim_NO2),max(clim_NO2),4))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Modeled NO_2 profile [ppt_v]')
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(3,1,3)
semilogy(up_MET_time,up_MET,'*','Color','b' );
%plot(up_MET_time,up_MET );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'x','Color','g');
%plot(down_MET_time,down_MET,'sq','Color','g');

set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
 ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
 set(gca,'YTick',NO2_YTick)
 set(gca,'YTickLabel',NO2_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title({'NO_2 24-hour fluxes ';'Measured Fluxes are NO_2 - NO'})
% 
% subplot(4,1,4)
% plot(up_SE_time,up_SE,'*','Color','b' );
% %plot(up_SE_time,up_SE );
% hold all
% 
% plot(down_SE_time,down_SE,'x','Color','g');
% 
% set(gca,'XTick',date_tick);
% 
% datetick('x',2,'keepticks')
% legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% xlabel('Date')
% ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
%  ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
% % 
% % set(gca,'YTick',NO2_YTick)
% % set(gca,'YTickLabel',NO2_YTick_Labels)
% 
% 
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% title('Modeled Surface exchange NO_2 flux ')
if(exist([path 'flux_model_surface_exchange_mod_NO2.jpeg'],'file'))
    delete([path 'flux_model_surface_exchange_mod_NO2_old.jpeg'])
    movefile([path 'flux_model_surface_exchange_mod_NO2.jpeg'],[path 'flux_model_surface_exchange_mod_NO2_old.jpeg'])
end
saveas(gcf,[path 'flux_model_surface_exchange_mod_NO2.jpeg'])


% %NO

MET_flux=-(nd_NO_obs(1,:)-nd_NO_obs(2,:))./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-

grad=(MET_NO(1,:)-MET_NO(2,:));
MET_flux=-(grad)./(MET_ht(1,:)-MET_ht(2,:)).*Kh2;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-


MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=(flux(find(1==strcmp(spc_name,'NO')),:)+flux(find(1==strcmp(spc_name,'NO2')),:));
MET_time=MET_NOx_time(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=flux(find(1==strcmp(spc_name,'NO')),:);
% MET_time=time_obs(1,:);
% [MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
fid=fopen('SE_NO.txt','w');
fprintf(fid,'%f %f \n',[SE_time'  SE']');
fclose(fid);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
subplot(4,1,1)
contourf(time_obs,depths_obs,NO_obs,clim_NO,'Linestyle','none')
colorbar
caxis([min(clim_NO) max(clim_NO)])
colorbar('YTick',linspace(min(clim_NO),max(clim_NO),3))

set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%title('Measured MET tower([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Interpolated 30-minute measurements of NO [ppt_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
plot(date_x,date_y,'-.','Color','w')
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');


subplot(4,1,2)
contourf(time,depths,NO,clim_NO,'Linestyle','none')
colorbar
caxis([min(clim_NO) max(clim_NO)])
colorbar('YTick',linspace(min(clim_NO),max(clim_NO),3))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Modeled NO profile [ppt_v]')
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(4,1,3)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'x','Color','g');

set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
%xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
 ylim(gca,[min(NO_YTick) max(NO_YTick)])
 set(gca,'YTick',NO_YTick)
 set(gca,'YTickLabel',NO_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title('NO 24-hour fluxes ')
% 

grad_NO2=-diff(MET_NO2);
grad_NO=-diff(MET_NO);
grad=grad_NO2+grad_NO;

grad(grad_NO>0)=grad(grad_NO>0)-grad_NO(grad_NO>0);
grad_NO(grad_NO>0)=grad_NO(grad_NO>0)-grad_NO(grad_NO>0);
grad(grad_NO2>0)=grad(grad_NO2>0)-grad_NO2(grad_NO2>0);
grad_NO2(grad_NO2>0)=grad_NO2(grad_NO2>0)-grad_NO2(grad_NO2>0);
grad(grad==0)=NaN;
grad_NO(grad==0)=NaN;
grad_NO2(grad==0)=NaN;
MET_flux=-(grad)./(-diff(MET_ht)).*Kh2;
MET_flux_NO=-(grad_NO)./(-diff(MET_ht)).*Kh2;
MET_flux_NO2=-(grad_NO2)./(-diff(MET_ht)).*Kh2;

%MET_flux=MET_flux_NO;

MET_time=MET_NOx_time(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

SE=(flux(find(1==strcmp(spc_name,'NO')),:));

%MET_flux(MET_flux_NO<0)=MET_flux(MET_flux_NO<0)+MET_flux_NO(MET_flux_NO<0);
%MET_flux(MET_flux_NO2<0)=MET_flux(MET_flux_NO2<0)+MET_flux_NO2(MET_flux_NO2<0);

% MET_time=MET_NOx_time(1,:);
% [MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);
MET_time_NO=MET_NOx_time(1,:);
MET_time_NO2=MET_NOx_time(1,:);
[MET_time_NO MET_flux_NO]=average_x(MET_time_NO,MET_flux_NO,min_day);
[MET_time_NO2 MET_flux_NO2]=average_x(MET_time_NO2,MET_flux_NO2,min_day);
MET_flux=MET_flux_NO;

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

subplot(4,1,4)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'x','Color','g');

set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
if(isempty(down_MET))
 legend({'MET Up';'Model Up';'Model Down'},'Location','EastOutside')   
else
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
end
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
 ylim(gca,[min(NO_YTick) max(NO_YTick)])
 set(gca,'YTick',NO_YTick)
 set(gca,'YTickLabel',NO_YTick_Labels)
title('"Cleaned" 24-h NO flux ')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)

if(exist([path 'flux_model_surface_exchange_NO.jpeg'],'file'))
    delete([path 'flux_model_surface_exchange_NO_old.jpeg'])
    movefile([path 'flux_model_surface_exchange_NO.jpeg'],[path 'flux_model_surface_exchange_NO_old.jpeg'])
end
saveas(gcf,[path 'flux_model_surface_exchange_NO.jpeg'])

% %NO mod

MET_flux=-((nd_NO_obs(1,:)+nd_NO2_obs(1,:))-(nd_NO_obs(2,:)+nd_NO2_obs(2,:)))./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=flux(find(1==strcmp(spc_name,'NO')),:);
MET_time=time_obs(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
subplot(3,1,1)
contourf(time_obs,depths_obs,NO_obs,clim_NO,'Linestyle','none')
colorbar
caxis([min(clim_NO) max(clim_NO)])
colorbar('YTick',linspace(min(clim_NO),max(clim_NO),3))

set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%title('Measured MET tower([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Interpolated 30-minute measurements of NO [ppt_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
plot(date_x,date_y,'-.','Color','w')
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');


subplot(3,1,2)
contourf(time,depths,NO,clim_NO,'Linestyle','none')
colorbar
caxis([min(clim_NO) max(clim_NO)])
colorbar('YTick',linspace(min(clim_NO),max(clim_NO),3))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Modeled NO profile [ppt_v]')
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(3,1,3)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'x','Color','g');

set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
 ylim(gca,[min(NO_YTick) max(NO_YTick)])
 set(gca,'YTick',NO_YTick)
 set(gca,'YTickLabel',NO_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title({'NO 24-hour fluxes ';'Measured fluxes are NO + NO_2'})
% 
% subplot(4,1,4)
% plot(up_SE_time,up_SE,'*','Color','b' );
% hold all
% plot(down_SE_time,down_SE,'sq','Color','g');
% 
% set(gca,'XTick',date_tick);
% 
% datetick('x',2,'keepticks')
% legend({'Up';'Down'},'Location','EastOutside')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% xlabel('Date')
% ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
%  ylim(gca,[min(NO_YTick) max(NO_YTick)])
%  set(gca,'YTick',NO_YTick)
%  set(gca,'YTickLabel',NO_YTick_Labels)
% title('Modeled Surface exchange NO flux ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)

if(exist([path 'flux_model_surface_exchange_mod_NO.jpeg'],'file'))
    delete([path 'flux_model_surface_exchange_mod_NO_old.jpeg'])
    movefile([path 'flux_model_surface_exchange_mod_NO.jpeg'],[path 'flux_model_surface_exchange_mod_NO_old.jpeg'])
end
saveas(gcf,[path 'flux_model_surface_exchange_mod_NO.jpeg'])

% %O3
% mean_O3_SE=0.5*(O3(index,:)+O3(index+1,:)).*nd_air.*Av*10^-9;
% mean_nd_O3_obs=0.5*(nd_O3_obs(1,:)+nd_O3_obs(2,:));

grad=(nd_O3_obs(1,:)-nd_O3_obs(2,:));
MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
MET_flux=MET_flux./mean_nd_O3_obs*100;  %convert to cm per second
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=flux(find(1==strcmp(spc_name,'O3')),:);
SE=SE./mean_O3_SE*100; %convert to cm s
MET_time=time_obs(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
subplot(3,1,1)
contourf(time_obs,depths_obs,O3_obs,clim_O3,'Linestyle','none')
colorbar
caxis([min(clim_O3) max(clim_O3)])
colorbar('YTick',linspace(min(clim_O3),max(clim_O3),3))

set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Interpolated 30-minute measurements of O_3 [ppb_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
plot(date_x,date_y,'-.','Color','w')
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');


subplot(3,1,2)
contourf(time,depths,O3,clim_O3,'Linestyle','none')
colorbar
caxis([min(clim_O3) max(clim_O3)])
colorbar('YTick',linspace(min(clim_O3),max(clim_O3),3))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Modeled O_3 profile [ppb_v]')
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(3,1,3)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'x','Color','g');

set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
if (~isempty(up_SE))
    legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
else
    legend({'MET Up';'MET Down';'Model Down'},'Location','EastOutside')
end

P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
xlabel('Date')
ylabel(gca,{'Deposition Velcoity';'[ cm / s]'})
 ylim(gca,[min(O3_YTick) max(O3_YTick)])
 set(gca,'YTick',O3_YTick)
 set(gca,'YTickLabel',O3_YTick_Labels)

title('O_3 30-minute deposition velocities ')
% 
% subplot(4,1,4)
% semilogy(up_SE_time,up_SE,'*','Color','b' );
% hold all
% semilogy(down_SE_time,down_SE,'sq','Color','g');
% 
% set(gca,'XTick',date_tick);
% 
% datetick('x',2,'keepticks')
% if (isempty(up_SE))
% legend({'Down'},'Location','EastOutside')
% else
%     legend({'Up';'Down'},'Location','EastOutside')
% end
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% xlabel('Date')
% ylabel(gca,{'Deposition Velocity';'[ cm / s]'})
%  %ylim(gca,[min(O3_SE_YTick) max(O3_SE_YTick)])
%  %set(gca,'YTick',O3_YTick)
%  %set(gca,'YTickLabel',O3_YTick_Labels)
% title('Modeled Surface exchange O_3 flux ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)

if(exist([path 'flux_model_surface_exchange_O3.jpeg'],'file'))
    delete([path 'flux_model_surface_exchange_O3_old.jpeg'])
    movefile([path 'flux_model_surface_exchange_O3.jpeg'],[path 'flux_model_surface_exchange_O3_old.jpeg'])
end

saveas(gcf,[path 'flux_model_surface_exchange_O3.jpeg'])





% %NOx
grad=(nd_NO_obs(1,:)+nd_NO2_obs(1,:))-(nd_NO_obs(2,:)+nd_NO2_obs(2,:));
MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;

grad=(MET_NOx(1,:)-MET_NOx(2,:));
MET_flux=-(grad)./(MET_ht(1,:)-MET_ht(2,:)).*Kh2;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=(flux(find(1==strcmp(spc_name,'NO')),:)+flux(find(1==strcmp(spc_name,'NO2')),:));
MET_time=MET_NOx_time(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));

NOx_obs=NO_obs+NO2_obs;
NOx=NO+NO2;
clim_NOx=clim_NO2;
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
subplot(4,1,1)
contourf(time_obs,depths_obs,NOx_obs,clim_NOx,'Linestyle','none')
colorbar
caxis([min(clim_NOx) max(clim_NOx)])
colorbar('YTick',linspace(min(clim_NOx),max(clim_NOx),4))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Interpolated 30-minute Measurements of NO_x [ppt_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
plot(date_x,date_y,'-.','Color','w')
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');


subplot(4,1,2)
contourf(time,depths,NOx,clim_NOx,'Linestyle','none')
colorbar
caxis([min(clim_NOx) max(clim_NOx)])
colorbar('YTick',linspace(min(clim_NOx),max(clim_NOx),4))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Modeled NO_x Profile [ppt_v]')
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(4,1,3)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'X','Color','g');


set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
%xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
set(gca,'YTick',NO2_YTick)
set(gca,'YTickLabel',NO2_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title('NO_x 24-hour Fluxes ')
% 

%Modifications
% grad_NO2=nd_NO2_obs(1,:)-nd_NO2_obs(2,:);
% grad_NO=nd_NO_obs(1,:)-nd_NO_obs(2,:);
% grad=grad_NO2+grad_NO;
grad_NO2=-diff(MET_NO2);
grad_NO=-diff(MET_NO);
grad=grad_NO2+grad_NO;

grad(grad_NO>0)=grad(grad_NO>0)-grad_NO(grad_NO>0);
grad(grad_NO2>0)=grad(grad_NO2>0)-grad_NO2(grad_NO2>0);
grad(grad==0)=NaN;
MET_flux=-(grad)./(-diff(MET_ht)).*Kh2;
MET_flux_NO=-(grad_NO)./(-diff(MET_ht)).*Kh2;
MET_flux_NO2=-(grad_NO2)./(-diff(MET_ht)).*Kh2;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=(flux(find(1==strcmp(spc_name,'NO')),:)+flux(find(1==strcmp(spc_name,'NO2')),:));

%MET_flux(MET_flux_NO<0)=MET_flux(MET_flux_NO<0)+MET_flux_NO(MET_flux_NO<0);
%MET_flux(MET_flux_NO2<0)=MET_flux(MET_flux_NO2<0)+MET_flux_NO2(MET_flux_NO2<0);

MET_time=MET_NOx_time(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);
MET_time_NO=MET_NOx_time(1,:);
MET_time_NO2=MET_NOx_time(1,:);
[MET_time_NO MET_flux_NO]=average_x(MET_time_NO,MET_flux_NO,min_day);
[MET_time_NO2 MET_flux_NO2]=average_x(MET_time_NO2,MET_flux_NO2,min_day);


index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

 subplot(4,1,4)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'X','Color','g');


set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
if(isempty(down_MET))
 legend({'MET Up';'Model Up';'Model Down'},'Location','EastOutside')   
else
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
end
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
set(gca,'YTick',NO2_YTick)
set(gca,'YTickLabel',NO2_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title({'"Cleaned" NO_x 24-hour Fluxes'})
 

if(exist([path 'flux_model_surface_exchange_NOx.jpeg'],'file'))
    delete([path 'flux_model_surface_exchange_NOx_old.jpeg'])
    movefile([path 'flux_model_surface_exchange_NOx.jpeg'],[path 'flux_model_surface_exchange_NOx_old.jpeg'])
end
saveas(gcf,[path 'flux_model_surface_exchange_NOx.jpeg'])

figure('Visible','off')
subplot(2,1,1)
grad_obs=-(NO2_obs(1,:)+ NO_obs(1,:))-(NO2_obs(2,:)+ NO_obs(2,:));
index=grad_obs>0;
down_time=time_obs(1,index);
down_grad=grad_obs(index);
up_time=time_obs(1,~index);
up_grad=abs(grad_obs(~index));


 semilogy(up_time,up_grad,'*')
 hold all
 semilogy(down_time,down_grad,'sq')
 set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
 ylabel('Gradient [ppt_v] ')
 
 subplot(2,1,2)
 plot(time_obs(1,:),Kh)
 set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
 ylabel('Kh [m^2 / s] ')









% %NOx mod
grad=(-nd_NO_obs(1,:)+nd_NO2_obs(1,:))-(-nd_NO_obs(2,:)+nd_NO2_obs(2,:));
MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=(flux(find(1==strcmp(spc_name,'NO')),:)+flux(find(1==strcmp(spc_name,'NO2')),:));
MET_time=time_obs(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));

NOx_obs=NO_obs+NO2_obs;
NOx=NO+NO2;
clim_NOx=clim_NO2;
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
subplot(3,1,1)
contourf(time_obs,depths_obs,NOx_obs,clim_NOx,'Linestyle','none')
colorbar
caxis([min(clim_NOx) max(clim_NOx)])
colorbar('YTick',linspace(min(clim_NOx),max(clim_NOx),4))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Interpolated 30-minute measurements of NO_x [ppt_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
plot(date_x,date_y,'-.','Color','w')
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');


subplot(3,1,2)
contourf(time,depths,NOx,clim_NOx,'Linestyle','none')
colorbar
caxis([min(clim_NOx) max(clim_NOx)])
colorbar('YTick',linspace(min(clim_NOx),max(clim_NOx),4))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Modeled NO_x profile [ppt_v]')
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(3,1,3)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'X','Color','g');


set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
set(gca,'YTick',NO2_YTick)
set(gca,'YTickLabel',NO2_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title({'NO_x 24-hour fluxes ';'Measured fluxes are NO_2 - NO'})
% 


% subplot(4,1,4)
% plot(up_SE_time,up_SE,'*','Color','b' );
% hold all
% plot(down_SE_time,down_SE,'sq','Color','g');
% 
% set(gca,'XTick',date_tick);
% 
% datetick('x',2,'keepticks')
% legend({'Up';'Down'},'Location','EastOutside')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% %xlabel('Date')
% ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
% ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
% set(gca,'YTick',NO2_YTick)
% set(gca,'YTickLabel',NO2_YTick_Labels)
% title('Modeled Surface exchange NO_x flux ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)

if(exist([path 'flux_model_surface_exchange_mod_NOx.jpeg'],'file'))
    delete([path 'flux_model_surface_exchange_mod_NOx_old.jpeg'])
    movefile([path 'flux_model_surface_exchange_mod_NOx.jpeg'],[path 'flux_model_surface_exchange_mod_NOx_old.jpeg'])
end
saveas(gcf,[path 'flux_model_surface_exchange_mod_NOx.jpeg'])

% figure%('Visible','off')
% plot(SE,MET_flux)
% ylim([min(NO2_YTick) max(NO2_YTick)])
% xlim([min(NO2_YTick) max(NO2_YTick)])

% %NOy
grad=(nd_NO_obs(1,:)+nd_NO2_obs(1,:))-(nd_NO_obs(2,:)+nd_NO2_obs(2,:));

MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=(flux(find(1==strcmp(spc_name,'NO')),:)+flux(find(1==strcmp(spc_name,'NO2')),:)+flux(find(1==strcmp(spc_name,'HNO3')),:)+flux(find(1==strcmp(spc_name,'NO3')),:))+flux(find(1==strcmp(spc_name,'HONO')),:)+flux(find(1==strcmp(spc_name,'N2O5')),:)+flux(find(1==strcmp(spc_name,'HNO4')),:);
SE=(flux(find(1==strcmp(spc_name,'NO')),:)+flux(find(1==strcmp(spc_name,'NO2')),:));

MET_time=time_obs(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));

NOx_obs=NO_obs+NO2_obs;
NOx=NO+NO2;
clim_NOx=clim_NO2;
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
subplot(3,1,1)
contourf(time_obs,depths_obs,NOx_obs,clim_NOx,'Linestyle','none')
colorbar
caxis([min(clim_NOx) max(clim_NOx)])
colorbar('YTick',linspace(min(clim_NOx),max(clim_NOx),4))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Interpolated 30-minute measurements of NO_x [ppt_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
plot(date_x,date_y,'-.','Color','w')
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');


subplot(3,1,2)
contourf(time,depths,NOx,clim_NOx,'Linestyle','none')
colorbar
caxis([min(clim_NOx) max(clim_NOx)])
colorbar('YTick',linspace(min(clim_NOx),max(clim_NOx),4))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
%xlabel('Date')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
title('Modeled NO_x profile [ppt_v]')
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(3,1,3)
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'X','Color','g');


set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
%xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
set(gca,'YTick',NO2_YTick)
set(gca,'YTickLabel',NO2_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title({'NO_x 24-hour fluxes';'Surface exchange includes HNO4'})
% 


% subplot(4,1,4)
% plot(up_SE_time,up_SE,'*','Color','b' );
% hold all
% plot(down_SE_time,down_SE,'sq','Color','g');
% 
% set(gca,'XTick',date_tick);
% 
% datetick('x',2,'keepticks')
% legend({'Up';'Down'},'Location','EastOutside')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% %xlabel('Date')
% ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
% ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
% set(gca,'YTick',NO2_YTick)
% set(gca,'YTickLabel',NO2_YTick_Labels)
% title('Modeled Surface exchange NO_x flux ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)

if(exist([path 'flux_model_surface_exchange_NOy.jpeg'],'file'))
    delete([path 'flux_model_surface_exchange_NOy_old.jpeg'])
    movefile([path 'flux_model_surface_exchange_NOy.jpeg'],[path 'flux_model_surface_exchange_NOy_old.jpeg'])
end
saveas(gcf,[path 'flux_model_surface_exchange_NOy.jpeg'])



%model_fluxes

%NOx
chem_choice='NO2';
fluxes=all_flux(:,:,strcmp(spc_name,chem_choice));
chem_choice='NO';
fluxes=fluxes+all_flux(:,:,strcmp(spc_name,chem_choice));
chem_choice='NO_x';
clim_flux=log10(logspace(min(log10(NO2_YTick)),max(log10(NO2_YTick)),length(NO2_YTick)));
clim_flux=10:0.1:13;
up_fluxes=zeros(size(fluxes))+10^1;
down_fluxes=zeros(size(fluxes))+10^1;
index=fluxes>0;
up_fluxes(index)=fluxes(index);
index=fluxes<0;
down_fluxes(index)=abs(fluxes(index));


%plotting
figure('Visible','off')
subplot(2,1,1)
contourf(time2,edge_depths(1:end-1,:),log10(up_fluxes),100,'LineStyle','none')
colorbar
caxis([min(clim_flux) max(clim_flux)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
ylabel('Depth [m]')
ylim([-1 2])
title(['Upward fluxes of ' chem_choice ' log10([molec / m^2 s])'])
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(2,1,2)
contourf(time2,edge_depths(1:end-1,:),log10(down_fluxes),100,'LineStyle','none')
colorbar
caxis([min(clim_flux) max(clim_flux)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-1 2])
title(['Downward fluxes of ' chem_choice ' log10([molec / m^2 s])'])
hold all

plot(date_x,date_y,'-.','Color','w')

 
if(exist([path 'flux_model_' chem_choice '_old.jpeg'],'file'))
    delete([path 'flux_model_' chem_choice '_old.jpeg'])
    movefile([path 'flux_model_' chem_choice '.jpeg'],[path 'flux_model_' chem_choice '_old.jpeg'])
end
saveas(gcf,[path 'flux_model_' chem_choice '.jpeg'])

%NOx
chem_choice='HNO4';
fluxes=all_flux(:,:,strcmp(spc_name,chem_choice));
clim_flux=log10(logspace(min(log10(NO2_YTick)),max(log10(NO2_YTick)),length(NO2_YTick)));
clim_flux=10:0.1:13;
up_fluxes=zeros(size(fluxes))+10^1;
down_fluxes=zeros(size(fluxes))+10^1;
index=fluxes>0;
up_fluxes(index)=fluxes(index);
index=fluxes<0;
down_fluxes(index)=abs(fluxes(index));


%plotting
figure('Visible','off')
subplot(2,1,1)
contourf(time2,edge_depths(1:end-1,:),log10(up_fluxes),100,'LineStyle','none')
colorbar
caxis([min(clim_flux) max(clim_flux)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
ylabel('Depth [m]')
ylim([-1 2])
title(['Upward fluxes of ' chem_choice ' log10([molec / m^2 s])'])
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(2,1,2)
contourf(time2,edge_depths(1:end-1,:),log10(down_fluxes),100,'LineStyle','none')
colorbar
caxis([min(clim_flux) max(clim_flux)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-1 2])
title(['Downward fluxes of ' chem_choice ' log10([molec / m^2 s])'])
hold all

plot(date_x,date_y,'-.','Color','w')

 
if(exist([path 'flux_model_' chem_choice '_old.jpeg'],'file'))
    delete([path 'flux_model_' chem_choice '_old.jpeg'])
    movefile([path 'flux_model_' chem_choice '.jpeg'],[path 'flux_model_' chem_choice '_old.jpeg'])
end
saveas(gcf,[path 'flux_model_' chem_choice '.jpeg'])



% %NOx
grad=(nd_NO_obs(1,:)+nd_NO2_obs(1,:))-(nd_NO_obs(2,:)+nd_NO2_obs(2,:));
MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
depth_index=find(edge_depths(:,1)<depths_obs(1,1),1,'first')

SE=(flux(find(1==strcmp(spc_name,'NO')),:)+flux(find(1==strcmp(spc_name,'NO2')),:));
SE=fluxes(depth_index,:);
MET_time=time_obs(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time2(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));

NOx_obs=NO_obs+NO2_obs;
NOx=NO+NO2;
clim_NOx=clim_NO2;
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'X','Color','g');


set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
%xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
set(gca,'YTick',NO2_YTick)
set(gca,'YTickLabel',NO2_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title({'NO_x 24-hour Fluxes ';['Model flux at ' num2str(edge_depths(depth_index,1)) ' [m] Top measurement height at ~' num2str(mean(depths_obs(1,:))) ' [m]']})
% 



 
 
if(exist([path 'flux_model_MET_' chem_choice '.jpeg'],'file'))
    delete([path 'flux_model_MET_' chem_choice '_old.jpeg'])
    movefile([path 'flux_model_MET_' chem_choice '.jpeg'],[path 'flux_model_MET_' chem_choice '_old.jpeg'])
end
saveas(gcf,[path 'flux_model_MET_' chem_choice '.jpeg'])









%NO

chem_choice='NO';
fluxes=all_flux(:,:,strcmp(spc_name,chem_choice));

clim_flux=log10(logspace(min(log10(NO2_YTick)),max(log10(NO2_YTick)),length(NO2_YTick)));
clim_flux=10:0.1:13;
up_fluxes=zeros(size(fluxes))+10^1;
down_fluxes=zeros(size(fluxes))+10^1;
index=fluxes>0;
up_fluxes(index)=fluxes(index);
index=fluxes<0;
down_fluxes(index)=abs(fluxes(index));


%plotting
figure('Visible','off')
subplot(2,1,1)
contourf(time2,edge_depths(1:end-1,:),log10(up_fluxes),100,'LineStyle','none')
colorbar
caxis([min(clim_flux) max(clim_flux)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
ylabel('Depth [m]')
ylim([-1 2])
title(['Upward fluxes of ' chem_choice ' log10([molec / m^2 s])'])
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(2,1,2)
contourf(time2,edge_depths(1:end-1,:),log10(down_fluxes),100,'LineStyle','none')
colorbar
caxis([min(clim_flux) max(clim_flux)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-1 2])
title(['Downward fluxes of ' chem_choice ' log10([molec / m^2 s])'])
hold all

plot(date_x,date_y,'-.','Color','w')

if(exist([path 'flux_model_' chem_choice '_old.jpeg'],'file'))
    delete([path 'flux_model_' chem_choice '_old.jpeg'])
    movefile([path 'flux_model_' chem_choice '.jpeg'],[path 'flux_model_' chem_choice '_old.jpeg'])
end
saveas(gcf,[path 'flux_model_' chem_choice '.jpeg'])


% %NO
grad=(nd_NO_obs(1,:))-(nd_NO_obs(2,:));
MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
depth_index=find(edge_depths(:,1)<depths_obs(1,1),1,'first')

SE=(flux(find(1==strcmp(spc_name,'NO')),:));
SE=fluxes(depth_index,:);
MET_time=time_obs(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time2(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));

NOx_obs=NO_obs+NO2_obs;
NOx=NO+NO2;
clim_NOx=clim_NO2;
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'X','Color','g');


set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
%xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
set(gca,'YTick',NO2_YTick)
set(gca,'YTickLabel',NO2_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title({[chem_choice ' 24-hour Fluxes '];['Model flux at ' num2str(edge_depths(depth_index,1)) ' [m] Top measurement height at ~' num2str(mean(depths_obs(1,:))) ' [m]']})
% 



 
 
if(exist([path 'flux_model_MET_' chem_choice '.jpeg'],'file'))
    delete([path 'flux_model_MET_' chem_choice '_old.jpeg'])
    movefile([path 'flux_model_MET_' chem_choice '.jpeg'],[path 'flux_model_MET_' chem_choice '_old.jpeg'])
end
saveas(gcf,[path 'flux_model_MET_' chem_choice '.jpeg'])




%NO2

chem_choice='NO2';
fluxes=all_flux(:,:,strcmp(spc_name,chem_choice));

clim_flux=log10(logspace(min(log10(NO2_YTick)),max(log10(NO2_YTick)),length(NO2_YTick)));
clim_flux=10:0.1:13;
up_fluxes=zeros(size(fluxes))+10^1;
down_fluxes=zeros(size(fluxes))+10^1;
index=fluxes>0;
up_fluxes(index)=fluxes(index);
index=fluxes<0;
down_fluxes(index)=abs(fluxes(index));


%plotting
figure('Visible','off')
subplot(2,1,1)
contourf(time2,edge_depths(1:end-1,:),log10(up_fluxes),100,'LineStyle','none')
colorbar
caxis([min(clim_flux) max(clim_flux)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
ylabel('Depth [m]')
ylim([-1 2])
title(['Upward fluxes of ' chem_choice ' log10([molec / m^2 s])'])
hold all

plot(date_x,date_y,'-.','Color','w')


subplot(2,1,2)
contourf(time2,edge_depths(1:end-1,:),log10(down_fluxes),100,'LineStyle','none')
colorbar
caxis([min(clim_flux) max(clim_flux)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-1 2])
title(['Downward fluxes of ' chem_choice ' log10([molec / m^2 s])'])
hold all

plot(date_x,date_y,'-.','Color','w')

if(exist([path 'flux_model_' chem_choice '_old.jpeg'],'file'))
    delete([path 'flux_model_' chem_choice '_old.jpeg'])
    movefile([path 'flux_model_' chem_choice '.jpeg'],[path 'flux_model_' chem_choice '_old.jpeg'])
end
saveas(gcf,[path 'flux_model_' chem_choice '.jpeg'])


% %NO2
grad=(nd_NO2_obs(1,:))-(nd_NO2_obs(2,:));
MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
depth_index=find(edge_depths(:,1)<depths_obs(1,1),1,'first')

SE=(flux(find(1==strcmp(spc_name,'NO2')),:));
SE=fluxes(depth_index,:);
MET_time=time_obs(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time2(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

SE_max=max((mean(up_SE)+2*std(up_SE)),(mean(down_SE)+2*std(down_SE)));
MET_max=max((mean(up_MET)+2*std(up_MET)),(mean(down_MET)+2*std(down_MET)));

% SE_YTick=logspace(0,ceil(log10(SE_max)),ceil(log10(SE_max))+1);
% MET_YTick=logspace(0,ceil(log10(MET_max)),ceil(log10(MET_max))+1);
% SE_YTick_Labels = cellstr(num2str(round(log10(SE_YTick(:))), '10^%d'));
% MET_YTick_Labels = cellstr(num2str(round(log10(MET_YTick(:))), '10^%d'));

NOx_obs=NO_obs+NO2_obs;
NOx=NO+NO2;
clim_NOx=clim_NO2;
% 
%figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
figure('Visible','off')
semilogy(up_MET_time,up_MET,'*','Color','b' );
hold all
semilogy(down_MET_time,down_MET,'sq','Color','g');
semilogy(up_SE_time,up_SE,'d','Color','b' );
semilogy(down_SE_time,down_SE,'X','Color','g');


set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
%set(gca,'xtick',[])
legend({'MET Up';'MET Down';'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
%xlabel('Date')
ylabel(gca,{'Flux';'[ molecules/m^2 s]'})
ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
set(gca,'YTick',NO2_YTick)
set(gca,'YTickLabel',NO2_YTick_Labels)

%ylim([MET_flux_mean-MET_flux_std MET_flux_mean+MET_flux_std])
%ylim([min(depths_obs(:)) max(depths_obs(:))])
title({[chem_choice ' 24-hour Fluxes '];['Model flux at ' num2str(edge_depths(depth_index,1)) ' [m] Top measurement height at ~' num2str(mean(depths_obs(1,:))) ' [m]']})
% 



 
 
if(exist([path 'flux_model_MET_' chem_choice '.jpeg'],'file'))
    delete([path 'flux_model_MET_' chem_choice '_old.jpeg'])
    movefile([path 'flux_model_MET_' chem_choice '.jpeg'],[path 'flux_model_MET_' chem_choice '_old.jpeg'])
end
saveas(gcf,[path 'flux_model_MET_' chem_choice '.jpeg'])




%flux comparisons

%O3 

% MET_flux=-(nd_O3_obs(1,:)-nd_O3_obs(2,:))./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
% %MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
% MET_flux_mean=mean(MET_flux);
% MET_flux_std=std(MET_flux);
% SE=flux(find(1==strcmp(spc_name,'O3')),:);
% MET_time=time_obs(1,:);
% 
% index=MET_flux>0;
% up_MET=MET_flux(index);
% up_MET_time=MET_time(index);
% index=MET_flux<0;
% down_MET=abs(MET_flux(index));
% down_MET_time=MET_time(index);
% 
% 
% SE_time=time(1,:);
% index=SE<0;
% down_SE=abs(SE(index));
% down_SE_time=SE_time(index);
% index=SE>0;
% up_SE=SE(index);
% up_SE_time=SE_time(index);
grad=(nd_O3_obs(1,:)-nd_O3_obs(2,:));
MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
MET_flux=MET_flux./mean_nd_O3_obs*100;  %convert to cm per second
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=flux(find(1==strcmp(spc_name,'O3')),:);
SE=SE./mean_O3_SE*100; %convert to cm s
MET_time=time_obs(1,:);
[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);



if (~isempty(up_SE_time) && length(up_SE_time)>2)
    up_SE_flux_int=interp1(up_SE_time,up_SE,up_MET_time,'spline');
    
else
    up_SE_flux_int=[];
%     up_wind_int=[];
%     up_EC_ustar=[];
%     up_EC_monin=[];
%      up_SE_time=[];
end
up_wind_int=interp1(time_obs(1,:),wind_obs(1,:),up_MET_time,'spline');
    up_EC_ustar_int=interp1(EC_jd,EC_ustar,up_MET_time,'spline');
    up_EC_monin_int=interp1(EC_jd,EC_monin,up_MET_time,'spline');
if (~isempty(down_SE_time) && length(down_SE_time)>2)
down_SE_flux_int=interp1(down_SE_time,down_SE,down_MET_time,'spline');

else
   
    down_SE_flux_int=[];
%     down_wind_int=[];
%     down_EC_ustar=[];
%     down_EC_monin=[];
%      down_SE_time=[];
end


down_wind_int=interp1(time_obs(1,:),wind_obs(1,:),down_MET_time,'spline');
down_EC_ustar_int=interp1(EC_jd,EC_ustar,down_MET_time,'spline');
down_EC_monin_int=interp1(EC_jd,EC_monin,down_MET_time,'spline');

figure('Visible','off')
if(~isempty(up_SE_flux_int))
loglog(up_SE_flux_int, up_MET)
hold all
end
if(~isempty(down_SE_flux_int))
loglog(down_SE_flux_int, down_MET,'sq','Color','g')
hold all
end

plot([min(O3_YTick) max(O3_YTick)],[min(O3_YTick) max(O3_YTick)],'-','Color','k')
%xlim([-10^12 1.5*10^13])
%ylim([-10^10 10^10])
xlabel({'Surface Exchange of O_3 ';'[molecules / m^2 s ]'})
ylabel({'Flux of O_3 ';'[molecules / m^2 s ]'})
ylim([min(O3_YTick) max(O3_YTick)])
xlim([min(O3_YTick) max(O3_YTick)])
if(~isempty(up_SE_flux_int) && ~isempty(down_SE_flux_int))
legend('Up','Down','Location','EastOutside')
elseif(~isempty(up_SE_flux_int))
    legend('Up','Location','EastOutside')
else
    legend('Down','Location','EastOutside')
end
if(exist([path 'flux_vs_surface_exchange_O3.jpeg'],'file'))
    delete([path 'flux_vs_surface_exchange_O3_old.jpeg'])
    movefile([path 'flux_vs_surface_exchange_O3.jpeg'],[path 'flux_vs_surface_exchange_O3_old.jpeg'])
end
saveas(gcf,[path 'flux_vs_surface_exchange_O3.jpeg'])


figure('Visible','off')
semilogy(up_EC_monin_int, up_MET)
hold all
semilogy(down_EC_monin_int, down_MET,'sq','Color','g')
xlim([-10^3 10^3])
%ylim([-10^10 10^10])
xlabel({'Monin-Obukhov Length';'[m]'})
ylabel({'Flux of O_3 ';'[molecules / m^2 s ]'})
ylim([min(O3_YTick) max(O3_YTick)])
legend('Up','Down','Location','EastOutside')
if(exist([path 'flux_vs_monin_O3.jpeg'],'file'))
    delete([path 'flux_vs_monin_O3_old.jpeg'])
    movefile([path 'flux_vs_monin_O3.jpeg'],[path 'flux_vs_monin_O3_old.jpeg'])
end
saveas(gcf,[path 'flux_vs_monin_O3.jpeg'])


%NOx 

grad=(nd_NO_obs(1,:)+nd_NO2_obs(1,:))-(nd_NO_obs(2,:)+nd_NO2_obs(2,:));
MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=(flux(find(1==strcmp(spc_name,'NO')),:)+flux(find(1==strcmp(spc_name,'NO2')),:));
MET_time=time_obs(1,:);
MET_time=time_obs(1,:);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

% %NOx
grad=(nd_NO_obs(1,:)+nd_NO2_obs(1,:))-(nd_NO_obs(2,:)+nd_NO2_obs(2,:));
MET_flux=-(grad)./(depths_obs(1,:)-depths_obs(2,:)).*Kh;
%MET_flux=MET_flux*(100)^3; %convert flux from m molecules cm-3 s-1 to molecules m-2 s-
MET_flux_mean=mean(MET_flux);
MET_flux_std=std(MET_flux);
SE=(flux(find(1==strcmp(spc_name,'NO')),:)+flux(find(1==strcmp(spc_name,'NO2')),:));
MET_time=time_obs(1,:);
%[MET_time MET_flux]=average_x(MET_time,MET_flux,min_day);

index=MET_flux>0;
up_MET=MET_flux(index);
up_MET_time=MET_time(index);
index=MET_flux<0;
down_MET=abs(MET_flux(index));
down_MET_time=MET_time(index);


SE_time=time(1,:);
%[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);
% 


if (~isempty(up_SE_time) && length(up_SE_time)>2)
    up_SE_flux_int=interp1(up_SE_time,up_SE,up_MET_time,'spline');
    
else
    up_SE_flux_int=[];
%     up_wind_int=[];
%     up_EC_ustar=[];
%     up_EC_monin=[];
%      up_SE_time=[];
end
up_wind_int=interp1(time_obs(1,:),wind_obs(1,:),up_MET_time,'spline');
    up_EC_ustar_int=interp1(EC_jd,EC_ustar,up_MET_time,'spline');
    up_EC_monin_int=interp1(EC_jd,EC_monin,up_MET_time,'spline');
if (~isempty(down_SE_time) && length(down_SE_time)>2)
down_SE_flux_int=interp1(down_SE_time,down_SE,down_MET_time,'spline');

else
   
    down_SE_flux_int=[];
%     down_wind_int=[];
%     down_EC_ustar=[];
%     down_EC_monin=[];
%      down_SE_time=[];
end

down_wind_int=interp1(time_obs(1,:),wind_obs(1,:),down_MET_time,'spline');
down_EC_ustar_int=interp1(EC_jd,EC_ustar,down_MET_time,'spline');
down_EC_monin_int=interp1(EC_jd,EC_monin,down_MET_time,'spline');
% 
% figure('Visible','off')
% if(~isempty(up_SE_flux_int))
% loglog(up_SE_flux_int, up_MET,'*','Color','b')
% hold all
% end
% if(~isempty(down_SE_flux_int))
% loglog(down_SE_flux_int, down_MET,'sq','Color','g')
% hold all
% end
% plot([min(NO2_YTick) max(NO2_YTick)],[min(NO2_YTick) max(NO2_YTick)],'-','Color','k')
% %xlim([-10^12 1.5*10^13])
% %ylim([-10^10 10^10])
% xlabel({'Surface Exchange of NO_x ';'[molecules / m^2 s ]'})
% ylabel({'Flux of NO_x ';'[molecules / m^2 s ]'})
% ylim([min(NO2_YTick) max(NO2_YTick)])
% xlim([min(NO2_YTick) max(NO2_YTick)])
% if(~isempty(up_SE_flux_int) && ~isempty(down_SE_flux_int))
% legend('Up','Down','Location','EastOutside')
% elseif(~isempty(up_SE_flux_int))
%     legend('Up','Location','EastOutside')
% else
%     legend('Down','Location','EastOutside')
% end
% if(exist([path 'flux_vs_surface_exchange_NOx.jpeg'],'file'))
%     delete([path 'flux_vs_surface_exchange_NOx_old.jpeg'])
%     movefile([path 'flux_vs_surface_exchange_NOx.jpeg'],[path 'flux_vs_surface_exchange_NOx_old.jpeg'])
% end
% saveas(gcf,[path 'flux_vs_surface_exchange_NOx.jpeg'])


figure('Visible','off')
semilogy(up_EC_monin_int, up_MET,'*','Color','b')
hold all
semilogy(down_EC_monin_int, down_MET,'sq','Color','g')
xlim([-10^1 10^1])
%ylim([-10^10 10^10])
xlabel({'Monin-Obukhov Length';'[m]'})
ylabel({'Flux of O_3 ';'[molecules / m^2 s ]'})
ylim([min(NO2_YTick) max(NO2_YTick)])
legend('Up','Down','Location','EastOutside')
if(exist([path 'flux_vs_monin_NOx.jpeg'],'file'))
    delete([path 'flux_vs_monin_NOx_old.jpeg'])
    movefile([path 'flux_vs_monin_NOx.jpeg'],[path 'flux_vs_monin_NOx_old.jpeg'])
end
saveas(gcf,[path 'flux_vs_monin_NOx.jpeg'])



figure('Visible','off')
plot(time_obs,depths_obs,'*','Color','b' );

set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Observation Heights [m]')
title('Measurement Heights in and above the snowpack')
saveas(gcf,[path 'depths_obs.jpeg'])


% %HNO4
SE=(flux(find(1==strcmp(spc_name,'HNO4')),:));


SE_time=time(1,:);
[SE_time SE]=average_x(SE_time,SE,min_day);
index=SE<0;
down_SE=abs(SE(index));
down_SE_time=SE_time(index);
index=SE>0;
up_SE=SE(index);
up_SE_time=SE_time(index);

figure
semilogy(up_SE_time,up_SE,'d','Color','b' );
hold all
semilogy(down_SE_time,down_SE,'X','Color','g');


set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')
legend({'Model Up';'Model Down'},'Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
%xlabel('Date')
ylabel(gca,{'HNO4 Flux';'[ molecules/m^2 s]'})
%ylim(gca,[min(NO2_YTick) max(NO2_YTick)])
%set(gca,'YTick',NO2_YTick)
%set(gca,'YTickLabel',NO2_YTick_Labels)

figure('Visible','off')
plot(time(1,:),-(NO(40,:)-NO(41,:)));
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Gradient of NO at the surface [pptv]')
title('NO')
saveas(gcf,[path 'NO_grad_surface.jpeg'])

figure('Visible','off')
plot(time(1,:),0.5*(NO(40,:)+NO(41,:)));
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel(' NO at the surface [pptv]')
title('Surface concentration of NO')
saveas(gcf,[path 'NO_surface.jpeg'])

figure('Visible','off')
plot(time(1,:),-(NO2(40,:)-NO2(41,:)));
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Gradient of NO2 at the surface [pptv]')
title('NO2')
saveas(gcf,[path 'NO2_grad_surface.jpeg'])

figure('Visible','off')
plot(time(1,:),0.5*(NO2(40,:)+NO2(41,:)));
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel(' NO2 at the surface [pptv]')
title('Surface concentration of NO2')
saveas(gcf,[path 'NO2_surface.jpeg'])

figure('Visible','off')
plot(time(1,:),NO2(41,:));
hold all
plot(time(1,:),NO2(40,:));
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel(' NO2  [pptv]')
title('Surface concentrations of NO2')
legend('Above','Below','Location', 'EastOutside')
saveas(gcf,[path 'NO2_above_below.jpeg'])




figure('Visible','off')
plot(time(1,:),-(NO2(39,:)-NO2(41,:))-(NO(39,:)-NO(41,:)));
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Gradient of NO2 at the surface [pptv]')
title({'NOx';['Mean ' num2str(mean(-(NO2(39,:)-NO2(41,:))-(NO(39,:)-NO(41,:))))]})
saveas(gcf,[path 'NOx_grad_surface.jpeg'])

figure('Visible','off')
plot(time(1,:),0.5*(NO2(39,:)+NO2(41,:))+(NO(39,:)+NO(41,:)));
figure('Visible','off')
plot(time(1,:),0.5*(NO2(40,:)+NO2(41,:)));
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel(' NO2 at the surface [pptv]')
title('Surface concentration of NO2')
saveas(gcf,[path 'NOx_surface.jpeg'])

figure('Visible','off')
plot(time(1,:),NO2(41,:)+NO(41,:));
hold all
plot(time(1,:),NO2(39,:)+NO(39,:));
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel(' NO2  [pptv]')
title('Surface concentrations of NO2')
legend('Above','Below','Location', 'EastOutside')
saveas(gcf,[path 'NOx_above_below.jpeg'])
