function [ave_time ave_x]=average_x(time,x,time_span)
%This function takes in mxn matrixes time and x and calulates the average
%of x every 'time span' wher timespan is in julian time

% clc
% clear all
% close
% 
% time=1:102;
% time_span=5;
% x=1:length(time)%rand(1,length(time));

min_time=floor(min(time(:)));
max_time=ceil(max(time(:)));
temp_time=min_time:time_span:max_time;
ave_time=[];
ave_x=[];
for i=1:size(time,1)
    ave_time(i,:)=temp_time(1:end-1)+time_span/2;
    
    for j=1:length(temp_time)-1
    
        index=time(i,:)>=temp_time(j) & time(i,:)<temp_time(j+1);
%         index=find(time(i,:)>=temp_time(j) & time(i,:)<temp_time(j));
%         index1=time(i,:)>=temp_time(j);
%         index2=time(i,:)<temp_time(j);
%         index=index1+index2;
%         index=index==2;
        ave_x(i,j)=nanmean(x(index));
        
        
    end
    
    
    
    
    
end

% figure
% plot(time,x)
% hold all
% plot(ave_time, ave_x,'sq')
% keyboard
% close(gcf)

end