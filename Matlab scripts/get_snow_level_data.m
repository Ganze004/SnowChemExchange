%function [NO_time NO NO_sd NO2_time NO2 NO2_sd] =get_snow_level_data(files)
clc
clear all
close all

files={'data/NOx_ppt_snowtower_level1_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level2_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level3_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level4_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level5_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level6_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level7_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level8_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level9_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level10_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level11_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level12_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level13_final_correctNOx_sd.txt';
    'data/NOx_ppt_snowtower_level14_final_correctNOx_sd.txt'};
flag=1;
num_header_lines=11;


files={'data/NOx_ppt_snowtower_reduced_level1_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level2_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level3_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level4_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level5_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level6_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level7_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level8_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level9_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level10_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level11_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level12_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level13_final_correctNOx_sd_03212015.txt';
'data/NOx_ppt_snowtower_reduced_level14_final_correctNOx_sd_03212015.txt'};
flag=0;


num_header_lines=10;


headers=cell(numel(files));


NO_time=[];
NO2_time=[];
NO=[];
NO2=[];
NO_sd=[];
NO2_sd=[];

for i=1:numel(files)
    fid=fopen(files{i},'r');
    files{i}
    head=cell(num_header_lines);
    
    for j=1:num_header_lines
        head{j}=fgetl(fid);
        
    end
    headers{i}=head;
    
    line=fgetl(fid);
    counter=1;
    
    while line~=-1
        [temp line]=strtok(line);
        [temp2 line]=strtok(line);
        temp3{1}=strrep([temp ' ' temp2],'/','-');
        NO_time(i,counter)=datenum(temp3,'mm-dd-yyyy HH:MM:SS');
        
        [temp line]=strtok(line);
        NO(i,counter)=str2num(temp);
        
        [temp line]=strtok(line);
        NO_sd(i,counter)=str2num(temp);
        
        if flag
        [temp line]=strtok(line);
        [temp2 line]=strtok(line);
        temp3{1}=strrep([temp ' ' temp2],'/','-');
        NO2_time(i,counter)=datenum(temp3,'mm-dd-yyyy HH:MM:SS');
        [temp line]=strtok(line);
        
        if ~isempty(str2num(temp))
        NO2(i,counter)=str2num(temp);
        else
            NO2(i,counter)=NaN;
        end
        [temp line]=strtok(line);
        NO2_sd(i,counter)=str2num(temp);
        else
           
        NO2_time(i,counter)=NO_time(i,counter);
        
        [temp line]=strtok(line);
        
        if ~isempty(str2num(temp))
        NO2(i,counter)=str2num(temp);
        else
            NO2(i,counter)=NaN;
        end
        [temp line]=strtok(line);
        NO2_sd(i,counter)=str2num(temp); 
            
            
        end
        counter=counter+1;
        line=fgetl(fid);
    end
    
    
    
    %     data=fscanf(fid,' %20c  %g %g %20c %g %g',[6 inf]);
    %
    %     NO_time(i,1:size(data,2))=data(1,:);
    %     NO2_time(i,1:size(data,2))= data(4,:);
    %     NO(i,1:size(data,2))=data(2,:);
    %     NO2(i,1:size(data,2))=data(5,:);
    %     NO_sd(i,1:size(data,2))=data(3,:);
    %     NO2_sd(i,1:size(data,2))=data(6,:);
    %
    
    fclose(fid);
end


%     NO_time(NO_time==0)=NaN;
%     NO2_time(NO2_time==0)=NaN;
%     NO(NO==0)=NaN;
%     NO2(NO2==0)=NaN;
%     NO_sd(NO_sd==0)=NaN;
%     NO2_sd(NO2_sd==0)=NaN;
%

files=flipud(files);
NO_time=flipud(NO_time);
NO_sd=flipud(NO_sd);
NO=flipud(NO);
NO2_time=flipud(NO2_time);
NO2_sd=flipud(NO2_sd);
NO2=flipud(NO2);

save('summit_snow_data.mat','files','NO_time','NO2_time','NO','NO2','NO_sd','NO2_sd')

 %end

