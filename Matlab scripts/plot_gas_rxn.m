clc
clear all
close all

file='qqk.out';
% fid=fopen(file);
% 
% d=fscanf(fid,'%f',3);
% 
num_depth=68;
num_spc=54;
num_steps=(96*7);

data=load(file)';
% 
% data=fscanf(fid,'%f',[num_spc+5,inf]);
% fclose(fid);


depth=data(1,:);
jday=data(2,:);
jtime=data(3,:);
temperature=data(4,:);

spc=data(5:end,:);

index=depth<0;
depth=depth(index);
jday=jday(index);
jtime=jtime(index);
temperature=temperature(index);

spc=spc(:,index);
num_depth=19;

depth=reshape(depth,num_depth, num_steps);
depth=flipud(depth);


linear_time=(jday-jday(1))*86400; %Keeps the time in order for new days when plotting
jtime=jtime+linear_time;
jtime=reshape(jtime,num_depth, num_steps);
for i=1:num_steps
    M=6;
    D=floor(jtime(:,i)/(24*60*60));
    jtime(:,i)=jtime(:,i)-D*(24*60*60);
    H=floor(jtime(:,i)/(60*60));
    jtime(:,i)=jtime(:,i)-H*60*60;
    minute=floor(jtime(:,i)/60);
    jtime(:,i)=jtime(:,i)-minute*60;
    S=jtime(:,i);
    
    D=D+7;
   jtime(:,i)=datenum(2011,M,D,H,minute,S); 
    
end




% spc_labels={'O + N2 -> O3';
%             'O + O2 -> O3';
%             'O + H2O -> 2OH';
%             'OH + O3 -> HO2 + O2';
%             'OH + HO2 -> H2O + O2';
%             'OH + H2O2 -> HO2 + H2O';
%             'HO2 + O3 -> OH +2O2';
%             'HO2 + HO2 -> H2O2 + O2';
%             'NO + O3 -> NO2 + O2';
%             'NO + OH -> HONO';
%             'NO + HO2 -> NO2 + OH';
%             'NO3 + NO -> 2NO2';
%             'NO2 + OH -> HNO3';
%             'NO2 + HO2 -> HNO4';
%             'NO2 + O3 -> NO3 + O2';
%             'NO3 + NO2 -> N2O5';
%             'NO3 + HO2 -> 0.3HNO3 + 0.7OH + 0.7NO2 + O2';
%             'NO3 + NO3 -> 2NO2 + O2';
%             'N2O5 -> NO2 + NO3';
%             'N2O5 + H2O -> 2HNO3';
%             'HONO + OH -> H2O + NO2';
%             'HNO3 + OH -> H2O + NO3';
%             'HNO4 -> HO2 + NO2';
%             'HNO4 + OH -> H2O + NO2 + O2';
%             'RONO2 + OH -> H2O + NO2';
%             'CO + OH -> HO2 + CO2';
%             'CH4 + OH -> CH3O2 + H2O';
%             'C2H6 + OH -> C2H5O2 + H2O';
%             'C2H4 + OH -> C2H4OHO2';
%             'C2H4 + O3 -> HCHO + 0.4CH2O2 + 0.12HO2 + 0.42CO + 0.06CH4'
%             'HO2 + CH3O2 -> ROOH + O2';
%             'HO2 + C2H5O2 -> ROOH + O2';
%             'HO2 + CH3CO3 -> ROOH + O2';
%             'CH3O2 + CH3O2 -> 1.4HCHO + 0.8HO2 + O2';
%             'C2H5O2 + NO -> ALD + HO2 + NO2';
%             'C2H5O2 + C2H5O2 -> 1.6ALD + 1.2HO2';
%             'C2H4OHO2 + NO -> NO2 + 2HCHO + HO2';
%             'C2H4OHO2 + C2H4OHO2 -> 2.4HCHO + 1.2HO2 + 0.4ALD';
%             'HO2 + C2H4OHO2 -> ROOH + O2';
%             
%            };
       
spc_labels={
         'O1D + N2 =  O3';
         'O1D + O2 =  O3';
         'O1D + H2O =  2.00*OH';
         'OH + O3 =  HO2 + O2';
         'OH + HO2 =  H2O + O2';
         'OH + H2O2 =  HO2 + H2O';
         'HO2 + O3 =  OH + 2.00*O2';
         'HO2 + HO2 =  H2O2 + O2';
         'NO + O3 =  NO2 + O2';
         'NO + OH =  HONO';
         'NO + HO2 =  NO2 + OH';
         'NO3 + NO =  2.00*NO2';
         'NO2 + OH =  HNO3';
         'NO2 + HO2 =  HO2NO2';
         'NO2 + O3 =  NO3 + O2';
         'NO3 + NO2 =  N2O5';
         'NO3 + HO2 =  0.30*HNO3 + 0.70*OH + 0.70*NO2 + O2';
         'NO3 + NO3 =  2.00*NO2 + O2';
         'N2O5 =  NO2 + NO3';
         'N2O5 + H2O =  2.00*HNO3';
         'HONO + OH =  H2O + NO2';
         'HNO3 + OH =  H2O + NO3';
         'HO2NO2 =  HO2 + NO2';
         'HO2NO2 + OH =  H2O + NO2 + O2';
         'RONO2 + OH =  H2O + NO2';
         'CO + OH =  HO2 + CO2';
         'CH4 + OH =  CH3O2 + H2O';
         'C2H6 + OH =  C2H5O2 + H2O';
        ' C2H4 + OH =  C2H4OHO2';
         'C2H4 + O3 =  HCHO + 0.40*CH2O2 + 0.12*HO2 + 0.42*CO + 0.06*CH4';
         'HO2 + CH3O2 =  ROOH + O2';
         'HO2 + C2H5O2 =  ROOH + O2';
         'HO2 + CH3CO3 =  ROOH + O2';
         'CH3O2 + CH3O2 =  1.40*HCHO + 0.80*HO2 + O2';
         'C2H5O2 + NO =  ALD + HO2 + NO2';
         'C2H5O2 + C2H5O2 =  1.60*ALD + 1.20*HO2';
         'C2H4OHO2 + NO =  NO2 + 2.00*HCHO + HO2';
         'C2H4OHO2 + C2H4OHO2 =  2.40*HCHO + 1.20*HO2 + 0.40*ALD';
         'HO2 + C2H4OHO2 =  ROOH + O2';
         'HCHO + OH =  HO2 + CO + H2O';
         'NO3 + HCHO =  HNO3 + HO2 + CO';
         'ALD + OH =  CH3CO3 + H2O';
         'ALD + NO3 =  HNO3 + CH3CO3';
         'CH3O3 + NO =  CH2O2 + HO2 + NO2';
         'CH3O3 + HO2 =  CH2O2 + H2O + O2';
         'CH3O3 + CH3O3 =  2.00*CH2O2 + 2.00*HO2 + 2.00*O2';
         'CH2O2 + OH =  HO2 + H2O + CO2';
         'CH3CO3 + NO2 =  PAN';
         'PAN =  CH3CO3 + NO2';
         'CH3CO3 + NO =  CH3O2 + NO2 + CO2';
         'CH3O2 + NO =  HCHO + NO2 + HO2';
         'ROOH + OH =  0.70*CH3O2 + 0.30*HCHO + 0.30*OH';
         'NH3 =  NH3';
         'CH3OH =  CH3OH';
         };

            
          
            
            
spc_cell=cell(num_spc,1);
for i=1:num_spc
  sp=spc(i,:);
  spc_cell{i}=flipud(reshape(sp,num_depth,num_steps));
 
end

for i=1:num_spc
   x=jtime;
   y=depth;
   z=spc_cell{i};
   
   figure
   contourf(x,y,z,100,'LineColor','none')
   datetick('x',6,'keepticks')
   xlabel('Date')
   ylabel('Depth [m]')
   colorbar
  
   title({'Reaction rate [molecule/(cm^3 s)]';spc_labels{i};})
   folder='qqk';
   mkdir(folder)
    
     saveas(gcf,[ folder '/qqk_' num2str(i) '.jpeg'])
end



file='qqj.out';
% fid=fopen(file);
% 
% d=fscanf(fid,'%f',3);
% 
num_depth=68;
num_spc=16;
num_steps=(96*7);

data=load(file)';
% 
% data=fscanf(fid,'%f',[num_spc+5,inf]);
% fclose(fid);


depth=data(1,:);
jday=data(2,:);
jtime=data(3,:);
temperature=data(4,:);

spc=data(5:end,:);

index=depth<0;
depth=depth(index);
jday=jday(index);
jtime=jtime(index);
temperature=temperature(index);

spc=spc(:,index);
num_depth=19;

depth=reshape(depth,num_depth, num_steps);
depth=flipud(depth);


linear_time=(jday-jday(1))*86400; %Keeps the time in order for new days when plotting
jtime=jtime+linear_time;
jtime=reshape(jtime,num_depth, num_steps);
for i=1:num_steps
    M=6;
    D=floor(jtime(:,i)/(24*60*60));
    jtime(:,i)=jtime(:,i)-D*(24*60*60);
    H=floor(jtime(:,i)/(60*60));
    jtime(:,i)=jtime(:,i)-H*60*60;
    minute=floor(jtime(:,i)/60);
    jtime(:,i)=jtime(:,i)-minute*60;
    S=jtime(:,i);
    
    D=D+7;
   jtime(:,i)=datenum(2011,M,D,H,minute,S); 
    
end


spc_labels={
    'O3 + hv =  O1D + O2';
'H2O2 + hv =  2.00*OH';
'NO3 + hv =  NO + O2';
'N2O5 + hv =  NO2 + NO3';
'HONO + hv =  OH + NO';
'HNO3 + hv =  OH + NO2';
'HO2NO2 + hv =  HO2 + NO2';
'HO2NO2 + hv =  OH + NO3';
'HCHO + hv =  CO + H2';
'NO2 + hv =  NO + O1D';
'NO3 + hv =  NO2 + O1D';
'ALD + hv =  CH3O2 + CO + HO2';
'ALD + hv =  CH4 + CO';
'RONO2 + hv =  NO2';
'HCHO + hv =  2.00*HO2 + CO';
'ROOH + hv =  HCHO + OH + HO2';
      };
    
  
  spc_cell=cell(num_spc,1);
for i=1:num_spc
  sp=spc(i,:);
  spc_cell{i}=flipud(reshape(sp,num_depth,num_steps));
 
end

for i=1:num_spc
   x=jtime;
   y=depth;
   z=spc_cell{i};
   
   figure
   contourf(x,y,z,100,'LineColor','none')
   datetick('x',6,'keepticks')
   xlabel('Date')
   ylabel('Depth [m]')
   colorbar
  
   title({'Reaction rate [molecule/(cm^3 s)]';spc_labels{i};})
   folder='qqj';
   mkdir(folder)
    
     saveas(gcf,[ folder '/qqj_' num2str(i) '.jpeg'])
end
