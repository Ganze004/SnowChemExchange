clc
clear all
close all


% NO_path='episode/';
% NO2_path='episode';
% O3_path='episode/';
%
% mkdir(NO_path)
% mkdir(NO2_path)
% mkdir(O3_path)
clim_NOx=linspace(0,1000,100);%linspace(0,600,100);
clim_NO2=clim_NOx;
clim_NO=linspace(0,200,100);
clim_O3=linspace(0,100,100);
clim_temp=linspace(-100,10,100);

input_file='observation_input.inp';
Dt=1800; %Time step of interpreted data, used for input file
start_time=0.0;  %Start and end time of the observed data for the model, should program in, will ask Laurens about it
end_time=0.0;



dates=[ 3 15 2009 3 30 2009]
%dates=[ 3 1 2009 3 30 2009]
% dates=[ 7 1 2009 7 30 2009]
 %dates=[ 5 1 2009 5 30 2009]

dates=[ 4 15 2009 4 30 2009]
%dates=[ 3 11 2009 3 30 2009]

%dates=[ 10 1 2009 10 15 2009]
% dates=[ 5 21 2009 7 21  2009]

for dd=1:size(dates,1)
    close all
    
    
    load summit_snow_data.mat
    load depths_data.mat
    load summit_snow_ozone_data.mat
    load summit_MET_data.mat
    load summit_MET_ozone_data.mat
    load noaa.mat
    load cu_rad.mat
    load summit_MET_hts.mat
    load summit_snow_temperature
    load summit_MET_temperature
    load summit_eddy_covar.mat
    [noaa_jd noaa_wd noaa_ws rad_down rad_jd rad_up]=horz_arrays(noaa_jd,noaa_wd, noaa_ws, rad_down, rad_jd,rad_up);
    
    month1=dates(dd,1);
    day1=dates(dd,2);
    hour1=0.0;
    year1=dates(dd,3);
    month2=dates(dd,4);
    day2=dates(dd,5);
    hour2=0.0;
    year2=dates(dd,6);
    time_diff=3;  %Time difference in hours for time zones  2 for SUMMIT
    
    path=['contour_plots_' num2str(year1) '_' num2str(month1) '_' num2str(day1) '_' num2str(year2) '_' num2str(month2) '_' num2str(day2) '/']
    mkdir(path)
    dates(dd,:)
    
    
    day_span=[datenum(year1,month1,day1,hour1+time_diff,0,0) datenum(year2,month2,day2,hour2+time_diff,0,0)];
    tick_dates=linspace(day_span(1),day_span(2),5);

    [NO_time NO]=date_index(day_span,NO_time,NO);
    [NO2_time NO2]=date_index(day_span,NO2_time,NO2);
    [snow_O3_time O3]=date_index(day_span,snow_O3_time, snow_O3);
    
    [O3_time O3_1 O3_2]=date_index(day_span,O3_time,O3_1, O3_2);
    [NO_time2 NO_2]=date_index(day_span,NO_time2,NO_2);
    [NO2_time2 NO2_2]=date_index(day_span, NO2_time2, NO2_2);
    [NO_time1 NO_1]=date_index(day_span,NO_time1,NO_1);
    [NO2_time1 NO2_1]=date_index(day_span, NO2_time1, NO2_1);
    
    [noaa_jd noaa_wd noaa_ws]=date_index(day_span,noaa_jd, noaa_wd, noaa_ws);
    [rad_jd rad_down rad_up]=date_index(day_span,rad_jd,rad_down,rad_up);
    [ht_jd in_1_ht in_2_ht]=date_index(day_span, ht_jd, in_1_ht, in_2_ht);
    [temp_jd snow_temp]=date_index(day_span,temp_jd,snow_temp);
    [MET_temp_jd MET_temp_bot MET_temp_mid]=date_index(day_span, MET_temp_jd,MET_temp_bot, MET_temp_mid); 
    EC_jd=EC_jd';
    EC_ustar=EC_ustar';
    EC_monin=EC_monin';
    [EC_jd EC_ustar EC_monin]=date_index(day_span, EC_jd,EC_ustar,EC_monin); 
   
    
    % Meterological modifications
    
    noaa_ws(noaa_ws==9999)=NaN;
    noaa_wd(noaa_wd==9999)=NaN;
    v_ws=sin(noaa_wd*pi/180).*noaa_ws;
    u_ws=cos(noaa_wd*pi/180).*noaa_ws;
    
    rad_down(rad_down>2000)=NaN;
    MET_temp_bot(MET_temp_bot==-9999)=NaN;
    MET_temp_mid(MET_temp_mid==-9999)=NaN;
    
    noaa_jd_org=noaa_jd;
    
    
    % Chemical species modifcations
    NO(NO==0)=NaN;
    NO(NO<0)=0;
    NO_time(NO_time==0)=NaN;
    NO2(NO2==0)=NaN;
    NO2(NO2<0)=0;
    NO2_time(NO2_time==0)=NaN;
    O3(O3==0)=NaN;
    O3(O3<0)=0;
    snow_O3_time(snow_O3_time==0)=NaN;
    
    NO_2(NO_2==0)=NaN;
    NO_2(NO_2<0)=0;
    NO_time2(NO_time2==0)=NaN;
    NO2_2(NO2_2==0)=NaN;
    NO2_2(NO2_2<0)=0;
    NO2_time2(NO2_time2==0)=NaN;
    O3_2(O3_2==0)=NaN;
    O3_2(O3_2<0)=0;
    O3_time(O3_time==0)=NaN;
    NO_1(NO_1==0)=NaN;
    NO_1(NO_1<0)=0;
    NO_time1(NO_time1==0)=NaN;
    NO2_1(NO2_1==0)=NaN;
    NO2_1(NO2_1<0)=0;
    NO2_time1(NO_time1==0)=NaN;
    O3_1(O3_1==0)=NaN;
    O3_1(O3_1<0)=0;
    
    
    
    
    [NO_time NO]=interp_30_min_data(day_span,NO_time,NO);
    [NO2_time NO2]=interp_30_min_data(day_span,NO2_time,NO2);
    [snow_O3_time O3]=interp_30_min_data(day_span,snow_O3_time, O3);
    [O3_time O3_2 O3_1]=interp_30_min_data(day_span,O3_time, O3_2,O3_1);
    [NO_time2 NO_2]=interp_30_min_data(day_span,NO_time2,NO_2);
    [NO2_time2 NO2_2]=interp_30_min_data( day_span,NO2_time2, NO2_2);
    
    [NO_time1 NO_1]=interp_30_min_data(day_span,NO_time1,NO_1);
    [NO2_time1 NO2_1]=interp_30_min_data(day_span, NO2_time1, NO2_1);
    [noaa_jd u_ws v_ws ]=interp_30_min_data(day_span,noaa_jd, u_ws, v_ws);
    [rad_jd rad_down rad_up]=interp_30_min_data(day_span,rad_jd, rad_down, rad_up);
    [ht_jd in_1_ht in_2_ht]=interp_30_min_data(day_span, ht_jd, in_1_ht, in_2_ht);
    
    [temp_jd snow_temp]=interp_30_min_data(day_span,temp_jd, snow_temp);
    [MET_temp_jd MET_temp_bot MET_temp_mid]=interp_30_min_data( day_span,MET_temp_jd,MET_temp_bot, MET_temp_mid); 
    

    [EC_jd EC_ustar EC_monin]=interp_30_min_data( day_span,EC_jd,EC_ustar,EC_monin); 

    
    NO_time=NO_time';
    NO=NO';

    [depths_jd yy]=meshgrid(depths_jd,1:size(NO_time,2));
    
    
    snow_O3_time=snow_O3_time';
    O3=O3';
    
    
    depths_jd=depths_jd';
    
    O3_depths=nearest_neighbor(snow_O3_time,depths_jd,depths);
    depths=nearest_neighbor(NO_time,depths_jd,depths);
    in_1_ht=nearest_neighbor(NO_time1,ht_jd,in_1_ht);
    in_2_ht=nearest_neighbor(NO_time2,ht_jd,in_2_ht);
   
    
    NO=NO';
    NO_time=NO_time';
    O3=O3';
    snow_O3_time=snow_O3_time';
    O3_depths=O3_depths';
    depths=depths';
    
   
    
%     %March 19th
%     index=find(O3_depths(:,1)<0,1,'first');
%     O3_depths=O3_depths(index:end,:);
%     O3=O3(index:end,:);
%     snow_O3_time=snow_O3_time(index:end,:);
%     depths=depths(index:end,:);
%     NO=NO(index:end,:);
%     NO2=NO2(index:end,:);
%     NO2_time=NO2_time(index:end,:);
%     NO_time=NO_time(index:end,:);
%     snow_temp=snow_temp(index:end,:);
%     temp_jd=temp_jd(index:end,:);
%     
    %make snow and Met data in same martix
    MET_depth=[in_2_ht;in_1_ht];
    depths=[MET_depth;depths];
    O3_depths=[MET_depth;O3_depths];
 
    NO=[NO_2;NO_1;NO];
    NO2=[NO2_2;NO2_1;NO2];
    O3=[O3_2;O3_1;O3];
    
    NO_time=[NO_time2;NO_time1;NO_time];
    NO2_time=[NO2_time2;NO2_time1;NO2_time];
    snow_O3_time=[O3_time;O3_time;snow_O3_time];
    
    snow_temp=[MET_temp_mid;MET_temp_bot;snow_temp];
    temp_jd=[temp_jd(1:2,:);temp_jd];
    
        date_x=(floor(day_span(1)):floor(day_span(end)))+datenum(0,0,0,time_diff,0,0);
    date_y=[min(depths(:)) max(depths(:))];
    [date_x date_y]=meshgrid(date_x,date_y);
    
%     date_x=floor(time(1)):floor(time(end));
%     date_y=[min(depths(:)) max(depths(:))];
%     [date_x date_y]=meshgrid(date_x,date_y);
    %
    %    if interp_flag
    %
    %       NO=fill_data(NO_time,NO);
    %       NO2=fill_data(NO2_time,NO2);
    %       O3=fill_data(snow_O3_time,O3);
    %
    %
    %    end
    
    dz=zeros(size(depths,1)-1,size(depths,2));
    dz_ozone=zeros(size(O3_depths,1)-1,size(O3_depths,2));
    for i=1:size(depths,2)
       dz(:,i) =diff(depths(:,i));
        dz_ozone(:,i)=diff(O3_depths(:,i));
    end
    dz=[dz;dz(end,:)];
    dz_ozone=[dz_ozone;dz_ozone(end,:)];
    
%     for i=1:size(depths,1)
%        NO_time2((2*i-1),:)=NO_time(i,:);    
%        NO_time2((2*i),:)=NO_time(i,:);
%        depths2((2*i-1),:)=depths(i,:)-dz(i,:)/2;
%        depths2((2*i),:)=depths(i,:)+dz(i,:)/2+0.001;
%        NO_2((2*i-1),:)=NO(i,:);    
%        NO_2((2*i),:)=NO(i,:);
%        
%        NO2_time2((2*i-1),:)=NO2_time(i,:);    
%        NO2_time2((2*i),:)=NO2_time(i,:);
%         NO2_2((2*i-1),:)=NO2(i,:);    
%        NO2_2((2*i),:)=NO2(i,:);
%        
%        snow_O3_time2((2*i-1),:)=snow_O3_time(i,:);
%          snow_O3_time2((2*i),:)=snow_O3_time(i,:);
%          O3_2((2*i-1),:)=O3(i,:);
%          O3_2((2*i),:)=O3(i,:);
%          
%          O3_depths2((2*i-1),:)=O3_depths(i,:)-dz_ozone(i,:)/2;
%        O3_depths2((2*i),:)=O3_depths(i,:)+dz_ozone(i,:)/2+0.001;
%         
%     end
    
    
    try
        figure('Visible','off')
        
        
        %
        %   [NO_time yy]=meshgrid(NO_time(14,:)',1:size(NO_time,1));
        %
        %
        subplot(2,1,1)
        contourf(NO_time,depths,NO,clim_NO,'LineStyle','none')
        
        set(gca,'XTick',tick_dates)
        datetick('x',2,'keepticks')
        ylim([min(depths(:)) max(depths(:))])
        ylim([min(depths(:)) max(depths(:))])
        colorbar('Location','EastOutside')
        xlabel('Date [mm/dd/yy]')
        ylabel('Depth [m]')
        
        %title([' 30 min interpolated NO [ppt_v]  ' ])
        P=get(gca,'Position');
        subplot(2,1,2)
                plot(rad_jd,rad_down,'-o')
    %     hold all
    %     plot(rad_jd, rad_up,'-sq')
    
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    xlabel('Date [mm/dd/yy]')
    ylabel('Irradiance [W/m^2]')
    ylim([0 max(rad_down)])
    
        P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
              
        %saveas(gcf,[path 'NO.jpeg'])
        set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
        print(gcf,[path 'NO'],'-djpeg','-r800')
    catch
        disp(['NO for' num2str(year1) '-' num2str(month1) ' could not be plotted'])
    end
    
    
    try
        %NO2
        figure('Visible','off')
        subplot(2,1,1)
        %
        %  [NO2_time yy]=meshgrid(NO2_time(14,:)',1:size(NO2_time,1));
        %
        %  [NO_time_day NO_time]=daily_average(NO_time,NO);
        
        contourf(NO2_time,depths,NO2,clim_NO2,'LineStyle','none')
        set(gca,'XTick',tick_dates)
        datetick('x',2,'keepticks')
        ylim([min(depths(:)) max(depths(:))])
        ylim([min(depths(:)) max(depths(:))])
        colorbar('Location','EastOutside')
        xlabel('Date [mm/dd/yy]')
        ylabel('Depth [m]')
        %title(['30 min interpolated NO2 [ppt_v] ' ])
        P=get(gca,'Position');
        subplot(2,1,2)
        plot(rad_jd,rad_down,'-o')
    %     hold all
    %     plot(rad_jd, rad_up,'-sq')
    
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    xlabel('Date [mm/dd/yy]')
    ylabel('Irradiance [W/m^2]')
    ylim([0 max(rad_down)])
        P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
              
        %saveas(gcf,[path 'NO2.jpeg'])
        set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'NO2'],'-djpeg','-r800')
        
    catch
        disp(['NO2 for' num2str(year1) '-' num2str(month1) ' could not be plotted'])
    end
    
    
    try
        
        figure('Visible','off')
        subplot(2,1,1)
        
        contourf(snow_O3_time,O3_depths,O3,clim_O3,'LineStyle','none')
        set(gca,'XTick',tick_dates)
        datetick('x',2,'keepticks')
        ylim([min(depths(:)) max(depths(:))])
        ylim([min(depths(:)) max(depths(:))])

        colorbar('Location','EastOutside')
        xlabel('Date [mm/dd/yy]')
        ylabel('Depth [m]')
        %title(['30 min interpolated O3 [ppt_b]  '])
        P=get(gca,'Position');
        subplot(2,1,2)
        plot(rad_jd,rad_down,'-o')
    %     hold all
    %     plot(rad_jd, rad_up,'-sq')
    
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    xlabel('Date [mm/dd/yy]')
    ylabel('Irradiance [W/m^2]')
    ylim([0 max(rad_down)])
    P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
              
        %saveas(gcf,[path 'O3.jpeg'])
        set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'O3'],'-djpeg','-r800')
    catch
        disp(['O3 for' num2str(year1) '-' num2str(month1) ' could not be plotted'])
    end
    %
    
    
    
    
    
    
     try
        figure('Visible','off')
        
        
        
        contourf(temp_jd,depths,snow_temp,clim_temp,'LineStyle','none')
        hold all
        plot(date_x,date_y,'-.','Color','w','LineWidth',2)
        set(gca,'XTick',tick_dates)
        datetick('x',2,'keepticks')
        ylim([min(depths(:)) max(depths(:))])
        ch=colorbar;ylabel(ch,{'Temperature [C]'});
        xlabel('Date [mm/dd/yy]')
        ylabel('Depth [m]')
        %title([' 30 min interpolated temperature [C] ' ])
        %saveas(gcf,[path 'temperature.jpeg'])
        set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'temperature'],'-djpeg','-r800')    
    catch
        disp(['Temperature for' num2str(year1) '-' num2str(month1) ' could not be plotted'])
    end
    
    
    
    
    figure('Visible','off')
   % subplot(2,1,1)
    plot(noaa_jd,u_ws,'-o')
    hold all
    plot(noaa_jd,v_ws,'-d')
      %plot(date_x,date_y,'-.','Color','k','LineWidth',2)
    %plot(date_x,date_y,'-.','Color','k','LineWidth',2)
    %plot(noaa_jd,noaa_ws,'-sq')
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    xlabel('Date [mm/dd/yy]')
    ylabel('Wind Speed [m/s]')
    %title(['30 min interpolated U and V components of wind ' ])
    legend('U','V','Wind Mag.','Location','EastOutside')
    
    %subplot(2,1,2)
    %rose(noaa_wd*pi/180)
    %hline=findobj(gca,'Type','line');
    %set(hline,'LineWidth',1.5)
    %%title('Wind Rose')
  
    %saveas(gcf,[path 'wind.jpeg'])
  set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'wind'],'-djpeg','-r800')  
    
    
    
    figure('Visible','off')
    %plot(noaa_jd_org,noaa_wd,'o')
    rose(noaa_wd*pi/180,noaa_ws)
     set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    xlabel('Date [mm/dd/yy]')
    ylabel('Wind Direction [Deg.]')
    %title('Wind direction profile')
    
    %saveas(gcf,[path 'wind_dir.jpeg'])
    set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'wind_dir'],'-djpeg','-r800')
    
    figure('Visible','off')
    subplot(4,1,1)
    contourf(snow_O3_time,O3_depths,O3,clim_O3,'LineStyle','none')
    hold on
    plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
        set(gca,'XTick',tick_dates)
        datetick('x',2,'keepticks')
        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
        %ylim([min(depths(:)) 0])
        
        %title('O_3 [ppb_v]')
       ch=colorbar;ylabel(ch,{'O_3 Mixing Ratio','[ppb_v]'});
        x_lim=get(gca,'XLim');
        y_lim=get(gca,'YLim');
        text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'a','FontSize',15,'Color','white')
        ylabel('Depth [m]')
        P=get(gca,'Position');
        
    subplot(4,1,2)
    
    contourf(NO2_time,depths,NO2,clim_NO2,'LineStyle','none')    
    hold on
    plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
        set(gca,'XTick',tick_dates)
        datetick('x',2,'keepticks')
        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
        %ylim([min(depths(:)) 0])
        
        %title('NO_2 [ppt_v]')
        ch=colorbar;ylabel(ch,{'NO_2 Mixing Ratio','[ppt_v]'});
        
        ylabel('Depth [m]')
        P2=get(gca,'Position');
        P2(3)=P(3);
        set(gca,'Position',P2)
        x_lim=get(gca,'XLim');
        y_lim=get(gca,'YLim');
        text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'b','FontSize',15,'Color','white')
       subplot(4,1,3)
       contourf(NO_time,depths,NO,clim_NO,'LineStyle','none')
           hold on
    plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
        set(gca,'XTick',tick_dates)
        datetick('x',2,'keepticks')
        set(gca,'xtick',[])
        set(gca,'xticklabel',[])
        %ylim([min(depths(:)) 0])
        
        %title('NO [ppt_v]')
        ch=colorbar;ylabel(ch,{'NO Mixing Ratio','[ppt_v]'});
        
        ylabel('Depth [m]')
           P2=get(gca,'Position');
        P2(3)=P(3);
        set(gca,'Position',P2)
        x_lim=get(gca,'XLim');
        y_lim=get(gca,'YLim');
        text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'c','FontSize',15,'Color','white')
        
        
        subplot(4,1,4)
        plot(rad_jd,rad_down,'-o')
    %     hold all
    %     plot(rad_jd, rad_up,'-sq')
    
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    xlabel('Date [mm/dd/yy]')
    ylabel('Irradiance [W/m^2]')
    ylim([0 1.1*max(rad_down)])
    x_lim=get(gca,'XLim');
        y_lim=get(gca,'YLim');
        text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'d','FontSize',15,'Color','black')
    
        P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
    %saveas(gcf,[path 'O3_NO2_NO_rad.jpeg'])
        set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'O3_NO2_NO_rad'],'-djpeg','-r800')
    
    
    figure('Visible','off')
    
    plot(rad_jd,rad_down,'-o')
    %     hold all
    %     plot(rad_jd, rad_up,'-sq')
    
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    xlabel('Date [mm/dd/yy]')
    ylabel('Irradiance [W/m^2]')
    %title(['30 min interpolated irradiance ' ])
    % legend('Down','Up','Wind Mag.','Location','EastOutside')
    %saveas(gcf,[path 'Irradiance.jpeg'])
    set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'Irradiance'],'-djpeg','-r800')
    
    %interprated surface temperature

    
    surf_temp=nan(1,size(snow_temp,2));
    
    for i=1:length(surf_temp)
        index=~isnan(snow_temp(:,i));
        xx=snow_temp(index,i);
        xx_depth=depths(index,i);
        surf_temp(i)=interp1(xx_depth,xx,0,'cubic','extrap');
        
    end
    
    figure('Visible','off')
    plot(temp_jd(1,:),surf_temp)
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    xlabel('Date [mm/dd/yy]')
    ylabel('Surface temperature [C]')
    %title(['30 min interpolated surface temperature' ])
    % legend('Down','Up','Wind Mag.','Location','EastOutside')
    %saveas(gcf,[path 'Surf_temp.jpeg'])
    set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'Surf_temp'],'-djpeg','-r800')
    
    
    
    %Create input file for 
    fid=fopen([path input_file],'w');
    fprintf(fid,'NDTG\t      Starttime Endtime No. Day Dt [s]\n');
    num_days=day_span(2)-day_span(1);
    fprintf(fid,'%d%02d%02d%02d     %02.2f\t %02.2f    %d    %d \n\n',year1,month1,day1,hour1,start_time,end_time,num_days, Dt);
    
    header={'Time';'NO[ppt]';'NO2[ppt]';'O3[ppb]';'Temp[C]';'S_Temp[C]';'Wind_U[m/s]';'Wind_V[m/s]';'Irr[W/m^2]';'ustar [m/s]';'monin-O [m]'};
    
    for i=1:numel(header)
        if i==numel(header)
            fprintf(fid,'%s \n',header{i});
        else
        fprintf(fid,'%s \t',header{i});
        end
    end
    %fprintf(fid,'\n');
    
    
    
    time=round((NO_time(1,:)-day_span(1))*24*60*60); %time in seconds
    
    data=[time; NO(2,:); NO2(2,:); O3(2,:); snow_temp(1,:); surf_temp; u_ws; v_ws; rad_down ; EC_ustar; EC_monin];
    
    data(isnan(data))=-9999.99;
   
    fprintf(fid,'%d \t%5.1f \t%5.1f \t%5.1f \t%5.1f \t%5.1f \t%5.1f \t%5.1f \t%5.1f \t%5.5f \t%5.5f\n',data);
    fclose(fid);
    
    
    file='obs.out';
    fid=fopen([path file],'w');
   time=round((NO_time(1,:)-day_span(1))*24*60*60); %time in seconds
    data=[];
    
       for j=1:size(depths,2)
           
           
               time=round((NO_time(:,j)-day_span(1))*24*60*60);
               data2=[time'; depths(:,j)';NO(:,j)'; NO2(:,j)'; O3(:,j)'; snow_temp(:,j)'; ones(size(NO(:,j)))'*u_ws(j); ones(size(NO(:,j)))'*v_ws(j); ones(size(NO(:,j)))'*rad_down(j)];
               data=[data,data2];
               
           
           
       end
       fprintf(fid,'%d \t%5.3f \t%5.3f \t%5.3f \t%5.3f \t%5.3f \t%5.3f \t%5.3f \t%5.3f\n',data);
       fclose(fid);
    
    
    
    
end
