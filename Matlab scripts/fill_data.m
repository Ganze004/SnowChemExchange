function [xx]=fill_data(xx_time,xx)
%This function will iterate through a mxn matrix interperating data points
%with NaN values based upon values within the row. The interp function is a
%cubic.

for i=1:size(xx,1)
   index=isnan(xx(i,:));
   
   if max(index==1)
      y=xx(i,~index);
      x=xx_time(i,~index);
      xi=xx_time(i,index);
      if length(x)>=2
          yi=interp1(x,y,xi,'cubic','extrap');
          xx(i,index)=yi;
      end
       
       
   end
    
    
    
end