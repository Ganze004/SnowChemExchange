function u_firn=wind_pump_ellipsoid(x,y,z,L,H,alpha,U10)

k=8*10^-10; %m^2
rho=1.3; %kg/m^3
u=1.6*10^-5;  % Pa s

d=alpha/sqrt(alpha^2+1)*L/(2*pi);

u_firn=(-6*pi*k*rho)/(u*L)*(H/L)*sqrt(alpha^2+1)/alpha*(U10^2).*cos(2*pi/L*x).*cos(2*pi/(alpha*L)*y).*exp((z-2*H)/d);

a=L/2;
b=alpha*L/2;

ellip=(x/a).^2+(y/b).^2;

index=ellip>1;

u_firn(index)=0;

RMS=2*pi*sqrt(alpha^2+1)*H/(alpha*L)*(1-cos(alpha/sqrt(alpha^2+1)));

disp([' The surface roughness of wind pumping is = ' num2str(RMS)])
end