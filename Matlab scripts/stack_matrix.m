function output=stack_matrix(varargin)
if nargin==0
output=[];
return
end


for i=1:nargin
   cmd_str=['array' num2str(i) '=varargin{' num2str(i) '};'];
       eval(cmd_str)
   cmd_str=['array' num2str(i) '=horz_arrays(array' num2str(i) ');'];
       eval(cmd_str)    
end


%keyboard
output=NaN(size(array1));

for i=1:nargin
   cmd_str=['output(i,1:length(array' num2str(i) '))=array' num2str(i) ';'];
   eval(cmd_str)
end

%output(output==0)=NaN;

end