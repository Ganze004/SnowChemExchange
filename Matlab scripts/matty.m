clc 
clear all
close




s=@(t) 10*sin((1000*pi*t+30)*pi/180);
c=@(t) 10*cos((1000*pi*t+300)*pi/180);


t=0:0.000001:1;



figure
subplot(2,1,1)
xlabel('Time [s]')
ylabel('Voltage ')
title(' Voltage with sin function')
plot(t,s(t),'-','LineWidth',2)
subplot(2,1,2)
plot(t,c(t),'-','LineWidth',2)
xlabel('Time [s]')
ylabel('Voltage ')
title(' Voltage with cos function')