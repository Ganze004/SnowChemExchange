function M=write_snow_data(NO_time, NO, NO_sd, NO2_time, NO2, NO2_sd, file_name)



headers=cell(0);
for i=1:size(NO,1)
   header{i*6-5,1}=['Inlet ' num2str(i) ' NO Time'];
   header{i*6-4,1}=['Inlet ' num2str(i) ' NO [pptv]'];
   header{i*6-3,1}=['Inlet ' num2str(i) ' NO SD [pptv]'];
   header{i*6-2,1}=['Inlet ' num2str(i) ' NO2 Time'];
   header{i*6-1,1}=['Inlet ' num2str(i) ' NO2 [pptv]'];
   header{i*6,1}=['Inlet ' num2str(i) ' NO2 SD [pptv]'];
    
end

M=cell(size(header,1),size(NO,2)+1);
M(:,1)=header(:,1);

for i=1:size(NO,1)
    
    
   M(i*6-5,2:end)=NO_time(i,:);
   M(i*6-4,2:end)=num2cell(NO(i,:));
   M(i*6-3,2:end)=num2cell(NO_sd(i,:));
   M(i*6-2,2:end)=NO2_time(i,:);
   M(i*6-1,2:end)=num2cell(NO2(i,:));
   M(i*6,2:end)=num2cell(NO2_sd(i,:));
    
end

    M=M'
    xlswrite(file_name, M)

end