function [x_time varargout]=interp_30_min_data(time_span,xx_time,varargin)
%This function will interpret the data within the rows of an mxn matrix to
%create data for 30 minute spans using a linear function

optargin = size(varargin,2);
min_30=datenum(0,0,0,0,30,0);
%span=floor(min(xx_time(:))):min_30:ceil(max(xx_time(:)));
span=min(time_span):min_30:max(time_span);
x=nan(size(xx_time,1),length(span));
x_time=nan(size(x));

for j=1:optargin
    xx=varargin{j};
    for i=1:size(xx_time,1)
        
        
        
        index=~isnan(xx(i,:));
        temp_xx=xx(i,index);
        temp_time=xx_time(i,index);
        
        if length(temp_xx)>2
            yi=interp1(temp_time,temp_xx,span,'linear','extrap');
            x(i,:)=yi;
        end
        
        
        
        
        
        
    end
    varargout{j}=x;
end

for i=1:size(x_time,1)
    
    x_time(i,:)=span;
    
end