clc

clear all

close all

T=270;
P=1;
M=14+16*2;

R=8.205746*10^-5; %m^3 atm mol^-1 K^-1
rho_air=P/(R*T);

rho_snow=0.3; %g/cm^3
rho_ice=0.9167; %g/cm^3
por=1-rho_snow/rho_ice;

constric=1;
tort=0.5*pi;
D=gas_diffusion(T,M,P);
%D=2.82*10^-5; %m^2/s
D=D*constric*por/tort;

H=1.5*10^-2; %m
L=H/0.03; %m
alpha=1;
U10=15; %m/s
num_nodes=50;
x=linspace(-0.5*L,0.5*L,num_nodes);%0.5*L
z=linspace(0,-3,num_nodes);

dz=abs(diff(z(1:2)));
dx=abs(diff(x(1:2)));
y=0;


surf_z=micro_z(x,y,H,L,alpha);
u_firn=wind_pump(x,y,surf_z,L,H,alpha,U10);

 ppt_surface=100;
 surf_conc=ppt_surface*rho_air*10^-12;

ppt_bottom=1;
bot_conc=ppt_bottom*rho_air*10^-12;
dz=abs(diff(z(1:2)));
dx=abs(diff(x(1:2)));

x_p=0;
z_p=-1;
index_x=find(x>x_p,1,'first');
index_z=find(z>z_p,1,'last');
set_nodes=[0:num_nodes-1]*num_nodes+index_z;
set_conc=400*rho_air*10^-12*ones(size(set_nodes));
[x z]=meshgrid(x,z);


n=num_nodes-1;
%[ C] = D2_laplace( n,dx,dz,u_firn,D,surf_conc,set_nodes,set_conc);
[ C] = D2_laplace( n,dx,dz,u_firn,D,surf_conc,bot_conc);


C=C/(rho_air*10^-12);
clim_n=1000;%linspace(0,ppt_surface,1000);

figure
contourf(x,z,C,clim_n,'LineStyle','none')
xlabel('X axis - parallel to wind [m]')
ylabel('Z axis - depth in snowpack [m]')
title({['Surface Conc = ' num2str(ppt_surface) ' ppt_v'];[' U_1_0 = ' num2str(U10) ' [m/s]']})
colorbar
saveas(gcf,['X_Z_U10_' num2str(U10) '.jpeg'])
