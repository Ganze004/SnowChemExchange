clc
clear all
close all


path='MLSN_CHEM/output/';
out_path='MLSN_CHEM/output/graphs/';
mkdir(out_path);
file='snow_mlay_2m_NO3.xls';
snow_depth=2; %meters
year=2009
month=4
day=11
hour=0
m=0
timestep_size=60 % in seconds
clim_O3=linspace(0,100,100);
clim_NO=linspace(0,200,100);
clim_NO2=linspace(0,1000,100);
clim_HONO=linspace(0,15,100);

[data,txt]=xlsread([path file]);

header=txt(3:4,:);  %the top row is the name, the bottom row is the units




time=[datenum(year,month,day,hour, m,timestep_size*data(:,1))';
    datenum(year,month,day,hour, m,timestep_size*data(:,1))';
    datenum(year,month,day,hour, m,timestep_size*data(:,1))'];
    
tick_dates=linspace(time(1,1),time(1,end),5);

ozone=data(:,14:16)';
NO2=data(:,20:22)';
NO=data(:,17:19)';
OH=data(:,23:24)';
HONO=data(:,25:27)';

jNO2=data(:,11:13)';
VdO3=data(:,31:32)';
VdNO2=data(:,33)';
VdNO3=data(:,34)';

snow_flux_O3=data(:,39)';
snow_flux_NO=data(:,40)';
snow_flux_NO2=data(:,41)';



snow_depths=[ snow_depth*0.5;snow_depth*0.5-0.01; 0]-snow_depth;
depths=[2;0;-0.01; snow_depths]
depths=[ depths(1).*ones(size(time(1,:)));
    depths(2).*ones(size(time(1,:)));
    depths(3).*ones(size(time(1,:)));
    depths(4).*ones(size(time(1,:)));
    depths(5).*ones(size(time(1,:)));
    depths(6).*ones(size(time(1,:)));
    ];
    

time=[time(1,:);time(1,:);time(2,:);time(2,:);time(3,:);time(3,:);];
ozone=[ozone(1,:);ozone(1,:);ozone(2,:);ozone(2,:);ozone(3,:);ozone(3,:)];
    
NO2=[NO2(1,:);NO2(1,:);NO2(2,:);NO2(2,:);NO2(3,:);NO2(3,:)];
NO=[NO(1,:);NO(1,:);NO(2,:);NO(2,:);NO(3,:);NO(3,:)];
OH=[OH(1,:);OH(1,:);OH(2,:);OH(2,:)];
HONO=[HONO(1,:);HONO(1,:);HONO(2,:);HONO(2,:);HONO(3,:);HONO(3,:)];
jNO2=[jNO2(1,:);jNO2(1,:);jNO2(2,:);jNO2(2,:);jNO2(3,:);jNO2(3,:)];

figure('Visible','off')

contourf(time,depths,ozone,clim_O3,'LineStyle','none')
set(gca,'CLim',[min(clim_O3) max(clim_O3)])
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
colorbar


ylabel('Depth [m]')
title('Modeled O3 profile [ppb_v]')
saveas(gcf,[out_path 'O3_profile.jpeg'])


figure('Visible','off')

contourf(time,depths,NO,clim_NO,'LineStyle','none')
set(gca,'CLim',[min(clim_NO) max(clim_NO)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
title('Modeled NO profile [ppt_v]')
saveas(gcf,[out_path 'NO_profile.jpeg'])


figure('Visible','off')

contourf(time,depths,NO2,clim_NO2,'LineStyle','none')
set(gca,'CLim',[min(clim_NO2) max(clim_NO2)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
title('Modeled NO2 profile [ppt_v]')
saveas(gcf,[out_path 'NO2_profile.jpeg'])
figure('Visible','off')

contourf(time(1:4,:),depths(1:4,:),OH,'LineStyle','none')
%set(gca,'CLim',[min(clim_NO2) max(clim_NO2)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
title('Modeled OH profile [1E6 molec. cm^{-3}]')
saveas(gcf,[out_path 'OH_profile.jpeg'])



figure('Visible','off')

contourf(time,depths,HONO,clim_HONO,'LineStyle','none')
set(gca,'CLim',[min(clim_HONO) max(clim_HONO)])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
title('Modeled HONO profile [ppt_v]')
saveas(gcf,[out_path 'HONO_profile.jpeg'])




figure('Visible','off')


contourf(time,depths,jNO2,'LineStyle','none')
%set(gca,'CLim',[-15 max(jNO2(:))])
% contourf(time,depths,log10(jNO2),'LineStyle','none')
% set(gca,'CLim',[-15 max(log10(jNO2(:)))])


%ylim([min(depths(:)) 0])
colorbar
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
title('Modeled jNO2 profile [s^{-1}]')
%title('Modeled log_{10}(jNO2) profile [log_{10}(s^{-1})]')
saveas(gcf,[out_path 'jNO2_profile.jpeg'])

figure('Visible','off')

semilogy(time(3,:),VdNO3,'d')
hold all
semilogy(time(3,:),VdNO2,'sq')

semilogy(time(3,:),VdO3,'o')
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Dry Deposition [cm s^{-1}]')
title(' Dry deposition rates in the top snow layer')
legend({'NO3';'NO2';'O3'}, 'Location','EastOutside')

saveas(gcf,[ out_path 'Dry_depostion.jpeg'])

figure('Visible','off')



plot(time(3,:),snow_flux_NO,'o')
hold all
plot(time(3,:),snow_flux_NO2,'sq')
%ylim([-10^11 10^11]) 
set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('Snow-atmosphere flux [molec. m^{-2} s^{-1}]')
title(' Snow-atmosphere flux  of NO_x')
legend({'NO';'NO2'}, 'Location','EastOutside')

saveas(gcf,[ out_path 'Snow_atmosphere_flux_NOx.jpeg'])


figure('Visible','off')

plot(time(3,:),snow_flux_O3,'o')

set(gca,'XTick',tick_dates)
datetick('x','keepticks')
xlabel('Date')
ylabel('O3 Snow-atmosphere flux [molec. m^{-2} s^{-1}]')
title(' Snow-atmosphere flux of ozone')

saveas(gcf,[ out_path 'Snow_atmosphere_flux_O3.jpeg'])





