function eqs=MTU_get_eqs(directory)




tend_file=[directory 'model_chem_eq.out']
para_file=[directory 'model_para.out']
data=load(para_file);
NUM_DEPTH=data(1)+1;
NUM_SPEC=data(2);
NUM_TIME=data(3);
MAX_j=data(4);
NUM_TEND=data(5);
NUM_EQ=data(6);

fid=fopen(tend_file);
eqs={};
for i=1:NUM_EQ
   line=fgetl(fid);
   
   line=strrep(line,'-->','_to_');
   line=strrep(line,'+','_');
   line=strrep(line,'.','');
   line=strrep(line,'etc','');
   line=strrep(line,' ','');
   eqs{i}=line;
    
end
end