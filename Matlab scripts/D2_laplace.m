function [ u ] = D2_laplace( n,dx,dz,u_firn,D,surf_conc,bot_conc,varargin)
%solves the 2-D laplace equation for the square system with n boxes on each
%side
debug=0;
optargin = size(varargin,2);


%create matrix and arrays
A=zeros((n+1)*(n+1),(n+1)*(n+1));
b=zeros((n+1)*(n+1),1);

%Array containing node values for the sides
left=1:(n+1);
right=(n*(n+1)+1):(n+1)^2;
top=(1+(n+1)):(n+1):(n+1)*n;
top2=top+1;

bot=(2*(n+1)):(n+1):(n+1)*n;

if optargin==2
    set_nodes=varargin{1};
    set_conc=varargin{2};
else
    set_nodes=[];
    set_conc=[];
end

%Index through colms
for i=1:(n+1)
    %Index through rows
    for j=1:(n+1)
        %Node at given index
        node=(i-1)*(n+1)+j;
        
        %Check to see if node is on the boundary or set
        if max(node==set_nodes)
            index=find(node==set_nodes>0,1);
            A(node,node)=1;
            b(node)=set_conc(index);

        elseif node==left(1)
            
            
             if u_firn(1)>0
                A(node,node)=-2*(dz^-2 + dx^-2+ u_firn(1)/(dz*D));
            else
%                                 A(node,node)=-2*(dz^-2 + dx^-2 );
%                                 b(node)=2*u_firn(1)/(dz*D)*surf_conc;
                
                A(node,node)=1;
                b(node)=surf_conc;
                continue
            end
            
            A(node,node+1)=2*dz^-2;
            A(node,(node+(n+1)))=2*dx^-2;
            %A(node,(node-(n+1)))=dx^-2;
            
            %b(node)=-2*dc_dz(1)*dz^-1;
%             
            
          

        elseif node==right(1)
            
           if u_firn(end)>0
                A(node,node)=-2*(dz^-2 + dx^-2+ u_firn(end)/(dz*D));
            else
%                                 A(node,node)=-2*(dz^-2 + dx^-2 );
%                                 b(node)=2*u_firn(end)/(dz*D)*surf_conc;
                
                A(node,node)=1;
                b(node)=surf_conc;
                continue
            end
            A(node,node+1)=2*dz^-2;
            
            A(node,(node-(n+1)))=2*dx^-2;
            
            
       
        elseif node==left(end)
            
            A(node,node)=-2*(dz^-2 + dx^-2);
            A(node,(node-1))=2*dz^-2;
            A(node,(node+(n+1)))=2*dx^-2;
            
            
        elseif node==right(end)
            
         
                A(node,node)=-2*(dz^-2 + dx^-2);
                A(node,(node-1))=2*dz^-2;
                A(node,(node-(n+1)))=2*dx^-2;
                

        elseif max(node==left)
            
            A(node,node)=-2*(dz^-2 + dx^-2);
            A(node,(node-1))=dz^-2;
            A(node,(node+1))=dz^-2;
            A(node,(node+(n+1)))=2*dx^-2;
            
        elseif  max(node==right)
            
            A(node,node)=-2*(dz^-2 + dx^-2);
            A(node,(node-1))=dz^-2;
            A(node,(node+1))=dz^-2;
            A(node,(node-(n+1)))=2*dx^-2;

        elseif max(node==top)
            
            index=find(node==top>0,1);
           if u_firn(index)>0

                A(node,node)=-2*(dz^-2 + dx^-2 + u_firn(index)/(dz*D));
           else
%%                                 A(node,node)=-2*(dz^-2 + dx^-2 );
%                                 b(node)=2*u_firn(index)/(dz*D)*surf_conc;
                                
                A(node,node)=1;
                b(node)=surf_conc;
                continue
           end
          
            A(node,node+1)=2*dz^-2;
            A(node,(node+(n+1)))=dx^-2;
            A(node,(node-(n+1)))=dx^-2;

        elseif max(node==bot)

            
            A(node,node)=-2*(dz^-2 + dx^-2);
            A(node,(node-1))=2*dz^-2;
            A(node,(node+(n+1)))=dx^-2;
            A(node,(node-(n+1)))=dx^-2;
            
%             A(node,node)=1;
%             b(node)=bot_conc;

        else
            A(node,node)=-2*(dz^-2+dx^-2);
            A(node,(node-1))=dz^-2;
            A(node,(node+1))=dz^-2;
            A(node,(node-(n+1)))=dx^-2;
            A(node,(node+(n+1)))=dx^-2;
            
        end
        
    end
    
    
end
%keyboard
u=A\b;
%Reshape into a square matrix
u=reshape(u,(n+1),(n+1));
