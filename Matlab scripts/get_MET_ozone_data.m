%function [NO_time NO NO_sd NO2_time NO2 NO2_sd] =get_snow_level_data(files)
clc
clear all
close all
path='data/';
files={['AmbientOzone_20080801_R1.txt'];
    ['AmbientOzone_20080901_R1.txt'];
    [ 'AmbientOzone_20081001_R1.txt'];
    [ 'AmbientOzone_20081101_R1.txt'];
    [ 'AmbientOzone_20081201_R1.txt'];
    [ 'AmbientOzone_20090101_R1.txt'];
    [ 'AmbientOzone_20090201_R1.txt'];
    [ 'AmbientOzone_20090301_R1.txt'];
    [ 'AmbientOzone_20090401_R1.txt'];
    [ 'AmbientOzone_20090501_R1.txt'];
    [ 'AmbientOzone_20090601_R1.txt'];
    [ 'AmbientOzone_20090701_R1.txt'];
    [ 'AmbientOzone_20090801_R1.txt'];
    [ 'AmbientOzone_20090901_R1.txt'];
    [ 'AmbientOzone_20091001_R1.txt'];
    [ 'AmbientOzone_20091101_R1.txt'];
    [ 'AmbientOzone_20091201_R1.txt'];
    [ 'AmbientOzone_20100101_R1.txt'];
    [ 'AmbientOzone_20100201_R1.txt'];
    [ 'AmbientOzone_20100301_R1.txt'];
    [ 'AmbientOzone_20100401_R1.txt'];
    [ 'AmbientOzone_20100501_R1.txt'];
    [ 'AmbientOzone_20100601_R1.txt'];
    };




O3_time=[];
O3_1=[];
O3_2=[];


for i=1:numel(files)
    file=files{i};
    data=load([path file]);
    year=str2num(file(14:17));
    
    time=data(:,1);
    year=year*ones(size(time));
    O3_time=[O3_time;datenum(year,0,time,0,0,0)];;
    
    O3_1=[O3_1;data(:,2)];
    O3_2=[O3_2;data(:,3)];
end


%     NO_time(NO_time==0)=NaN;
%     NO2_time(NO2_time==0)=NaN;
%     NO(NO==0)=NaN;
%     NO2(NO2==0)=NaN;
%     NO_sd(NO_sd==0)=NaN;
%     NO2_sd(NO2_sd==0)=NaN;
%

O3_time=O3_time';
O3_1=O3_1';
O3_2=O3_2';


save('summit_MET_ozone_data.mat','O3_time','O3_1','O3_2')

 %end

