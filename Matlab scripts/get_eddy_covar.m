clc
clear all
close all


dir='data/Metek-1/'
files=ls([dir '*.txt'])

[file files]=strtok(files);
final_data=[];
while ~isempty(file)
  year=str2num(file(end-11:end-8));
  month=str2num(file(end-7:end-6));
   day=str2num(file(end-5:end-4)); 
    data=load(file);
   for i=1:size(data,1)
      data(i,1)=datenum(year,1,data(i,1),0,0,0); 
   end
  
  final_data=[final_data;data];
    
    

[file files]=strtok(files);
    
end

EC_jd=final_data(:,1);
EC_temp=final_data(:,2);
EC_temp_flux=final_data(:,3);
EC_uv=final_data(:,4);
EC_vw=final_data(:,5);
EC_uw=final_data(:,6);
EC_ustar=final_data(:,7);
EC_Tstar=final_data(:,8);
EC_monin=final_data(:,9);
EC_Hs=final_data(:,10);
EC_ws=final_data(:,11);
EC_reldir=final_data(:,12);
EC_dir=final_data(:,13);

file='summit_eddy_covar.mat';

save(file,'EC_jd','EC_temp','EC_temp_flux','EC_uv','EC_vw','EC_uw','EC_ustar','EC_Tstar','EC_monin','EC_Hs','EC_ws','EC_reldir','EC_dir')