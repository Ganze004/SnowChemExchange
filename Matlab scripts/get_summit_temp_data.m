clc
clear all
close all


path='data/';
files={'snowtowertemp_summit_20080601_R1.txt';
'snowtowertemp_summit_20080701_R1.txt';
'snowtowertemp_summit_20080801_R1.txt';
'snowtowertemp_summit_20080901_R1.txt';
'snowtowertemp_summit_20081001_R1.txt';
'snowtowertemp_summit_20081101_R1.txt';
'snowtowertemp_summit_20081201_R1.txt';
'snowtowertemp_summit_20090101_R1.txt';
'snowtowertemp_summit_20090201_R1.txt';
'snowtowertemp_summit_20090301_R1.txt';
'snowtowertemp_summit_20090401_R1.txt';
'snowtowertemp_summit_20090501_R1.txt';
'snowtowertemp_summit_20090601_R1.txt';
'snowtowertemp_summit_20090701_R1.txt';
'snowtowertemp_summit_20090801_R2.txt';
'snowtowertemp_summit_20090901_R2.txt';
'snowtowertemp_summit_20091001_R2.txt';
'snowtowertemp_summit_20091101_R2.txt';
'snowtowertemp_summit_20091201_R2.txt';
'snowtowertemp_summit_20100101_R2.txt';
'snowtowertemp_summit_20100201_R2.txt';
'snowtowertemp_summit_20100301_R2.txt';
'snowtowertemp_summit_20100401_R2.txt';
'snowtowertemp_summit_20100501_R2.txt';
'snowtowertemp_summit_20100601_R2.txt';
'snowtowertemp_summit_20100701_R2.txt';    
};
data=[];

for i=1:numel(files)
   data2=load([path files{i}]);
   data=[data;data2];
    
end

temp_jd=datenum(data(:,1),data(:,3),data(:,4),data(:,5),data(:,6),zeros(size(data(:,6))));

temperature=nan(14,length(temp_jd));

temperature(7:end,:)=data(:,8:end)';

temp_jd=horz_arrays(temp_jd);

for i=1:size(temperature,1)
   temp_jd(i,:)=temp_jd(1,:); 
end



temperature(temperature==-9999)=NaN;
snow_temp=temperature;
save('summit_snow_temperature.mat','temp_jd','snow_temp')