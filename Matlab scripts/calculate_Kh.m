function [Kh]=calculate_Kh(time)

von_K=0.4;
min_Kh=10^-7;
%time=datenum(2009,4,11,0,0,0):1/48:datenum(2009,4,30,0,0,0);
load ../Summit.data/summit_MET_eddy_covar.mat
load ../Summit.data/summit_MET_hts.mat

day_span=[min(time(:)) max(time(:))];

EC_jd=EC_jd';
EC_ustar=EC_ustar';
EC_monin=EC_monin';
[EC_jd EC_ustar EC_monin]=date_index(day_span,EC_jd, EC_ustar, EC_monin);
[ht_jd in_1_ht in_2_ht]=date_index(day_span,ht_jd, in_1_ht, in_2_ht);
% in_1_ht=nearest_neighbor(EC_jd',ht_jd,in_1_ht);
% in_2_ht=nearest_neighbor(EC_jd',ht_jd,in_2_ht);

x=zeros(size(EC_jd));
for i=1:length(EC_jd)
index=knnsearch(ht_jd',EC_jd(i));

%keyboard
x(i)=in_1_ht(index);

end
in_1_ht=x;

x=zeros(size(EC_jd));
for i=1:length(EC_jd)
index=knnsearch(ht_jd',EC_jd(i));

%keyboard
x(i)=in_2_ht(index);

end
in_2_ht=x;

mean_ht=(in_1_ht+in_2_ht)/2;
Kh=zeros(size(EC_jd));
min_Kh=5.0*10^(-7);
% f1=@(z,z2,L) 2*L/(3*15)*(1+15*z/L)*(z-2*L/(5*15)*(1+15*z/L))-(2*L/(3*15)*(1+15*z2/L)*(z2-2*L/(5*15)*(1+15*z2/L)));
%  %f2=@(z,z2,L) z^2/2 + 4.7*z^3/(3*L)-(z2^2/2 + 4.7*z2^3/(3*L));
%  f2=@(z,z2,L) z*log(1/z+4.7/L)-(z2*log(1/z2+4.7/L));
%  f3=@(z,z2) z^2/2-(z2^2/2);
% for i=1:length(EC_jd)
%     
%     if(EC_monin(i)<0)
%       phi=(1.0-15*mean_ht(i)/EC_monin(i))^(-0.5); 
%       
%       Kh(i)=f1(in_2_ht(i),in_1_ht(i),EC_monin(i))/(in_2_ht(i)-in_1_ht(i));
%        
%     elseif(EC_monin(i)>0)
%        phi=1.0+4.7*mean_ht(i)/EC_monin(i);
%       
%        Kh(i)=f2(in_2_ht(i),in_1_ht(i),EC_monin(i))/(in_2_ht(i)-in_1_ht(i));
%        
%     else
%        phi=1;
%        
%        Kh(i)=f3(in_2_ht(i),in_1_ht(i))/(in_2_ht(i)-in_1_ht(i));
%     end
%     %Kh(i)=max([von_K*mean_ht(i)*EC_ustar(i)/phi min_Kh]);
%     Kh(i)=max([von_K*Kh(i)*EC_ustar(i) min_Kh]);
%     
% end
%f1=@(z,L) 4/3*(z^4-16/L*z^5)^(3/4)/(4*z^3-16/L*5*z^4);
 f1=@(z,L) -4*L/(5*16)*(1-16*z/L)^(5/4)*(z+4*L/(9*16)*(1-16*z/L));
 f2=@(z,L) (z^2)*(1/2+4.6/3*z/L);
 f3=@(z) (z^2)/2;
for i=1:length(EC_jd)
    
    if(EC_monin(i)<0)
      phi=(1.0-15*mean_ht(i)/EC_monin(i))^(-0.5); 
      
      %phi=f1(in_2_ht(i),EC_monin(i))-f1(in_1_ht(i),EC_monin(i));
      
      
      phi=f1(in_2_ht(i),EC_monin(i))-f1(in_1_ht(i),EC_monin(i));
      
    elseif(EC_monin(i)>0)
       phi=1.0+4.7*mean_ht(i)/EC_monin(i);
      
       phi=f2(in_2_ht(i),EC_monin(i))-f2(in_1_ht(i),EC_monin(i));
       
    else
       phi=1;
       
       phi=f3(in_2_ht(i))-f3(in_1_ht(i));
    end
    %Kh(i)=max([von_K*mean_ht(i)*EC_ustar(i)/phi min_Kh]);
    Kh(i)=von_K*EC_ustar(i)/(phi*(in_2_ht(i)-in_1_ht(i)));
    %Kh(i)=max([Kh(i) min_Kh]);
    
end


Kh1=Kh;
Kh=interp1(EC_jd,Kh,time,'spline');
Kh(Kh<min_Kh)=min_Kh;
end


