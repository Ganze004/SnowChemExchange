function [x_time_av x_av]=daily_average(time,x)
%The matrix a should be in format mxn where m is different layers and n is
%the time series. Time and x should be same size. Time must be in Julian

x_av=[];
x_time_av=[];
for i=1:size(time,1)
    day_time=floor(time(i,:));
    day_span=min(day_time):max(day_time);
    
    
    day_x=zeros(size(day_span));
    temp_x=x(i,:);
    for j=1:length(day_span)
        
       index=day_span(j)==day_time;
       


       day_x(j)=nanmean(temp_x(index));
        
        
    end
    x_av(i,:)=day_x;
    x_time_av(i,:)=day_span;
    
    
end