clc
clear all
close all

H=1.5*10^-2; %m
L=H/0.03; %m
alpha=1;
U10=3; %m/s



a=0.5*L;
b=0.5*alpha*L;
num_points=1000;
x=linspace(-a,a,num_points);
y=linspace(-b,b,num_points);

x_circ=linspace(-a,a,num_points);
y_circ=b*sqrt(1-(x_circ/a).^2);



[x y]=meshgrid(x,y);

circ=(x/a).^2+(y/b).^2;
index=circ>1;
surf_z=micro_z(x,y,H,L,alpha);
 u_firn=wind_pump(x,y,surf_z,L,H,alpha,U10);

 u_firn(index)=NaN;
 figure
 
 contourf(x,y,u_firn)
 xlabel('X axis - parallel to wind [m]')
 ylabel('Y axis - perpendicular to wind [m]')
 title({'Vertical volumetric flux in/from a dune [m/s]';['Into the snowpack is negative flux, U_1_0 = ' num2str(U10) ' [m/s]'];['Relief wavelength = ' num2str(L) ' [m] Relief Amplitude = ' num2str(H) ' [m]'];[' Horizontal aspect ratio of = ' num2str(alpha)]})
 colorbar
 
 hold all
 plot(x_circ,y_circ,'k')
 plot(x_circ,-y_circ,'k')
 
 axis equal
 
 sum_u_firn=u_firn;
 sum_u_firn(isnan(sum_u_firn))=0;
 sum_u_firn=sum(sum_u_firn(:))
 
 saveas(gcf,['Vertical flux_L_' num2str(L) '_H_' num2str(H) '_alpha_' num2str(alpha) '_U10_' num2str(U10) '.jpeg']) 
 
 
 rho_snow=0.3; %g/cm^3
rho_ice=0.9167; %g/cm^3
por=1-rho_snow/rho_ice;
V_snow=1; %m^3
V_air=V_snow*por;

t_flux=abs(V_air./u_firn/60); %m^2 min

 figure
 
 contourf(x,y,t_flux)
 xlabel('X axis - parallel to wind [m]')
 ylabel('Y axis - perpendicular to wind [m]')
 %title({'Vertical volumetric flux in/from a dune [m/s]';['Into the snowpack is negative flux, U_1_0 = ' num2str(U10) ' [m/s]'];['Relief wavelength = ' num2str(L) ' [m] Relief Amplitude = ' num2str(H) ' [m]'];[' Horizontal aspect ratio of = ' num2str(alpha)]})
 colorbar
 
 hold all
 plot(x_circ,y_circ,'k')
 plot(x_circ,-y_circ,'k')
 
 axis equal
 
 min(t_flux(:))
 saveas(gcf,['Vertical_time_flux_L_' num2str(L) '_H_' num2str(H) '_alpha_' num2str(alpha) '_U10_' num2str(U10) '.jpeg']) 

