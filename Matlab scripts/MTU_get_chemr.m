function chemr=MTU_get_chemr(directory)

tend_file=[directory 'model_chem_rates.out']
para_file=[directory 'model_para.out']
data=load(para_file);
NUM_DEPTH=data(1);
NUM_SPEC=data(2);
NUM_TIME=data(3);
MAX_j=data(4);
NUM_TEND=data(5);
NUM_EQ=data(6);

% 
%data=load(tend_file);
data=dlmread(tend_file);  % using dlmread instead of load to deal with a matrix that is not completely filled

chemr=reshape(data,NUM_DEPTH,NUM_TIME,NUM_EQ);
 
% for j=1:MAX_j
% for i=130%: NUM_SPEC
% rj=squeeze(data(j,:,i,:))';
% figure('Visible','off') 
% contourf(time,depths,rj,'LineStyle','none')
% colorbar
% ylim([min(depths(:)) 0.05])
% saveas(gcf,[dir 'rj_' spc_name{i} '_' num2str(j) '.jpeg'])
% close(gcf)
%  
% end
% end
end