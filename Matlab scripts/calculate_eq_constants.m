clc
clear all
close all

R=8.3144621/1000; %Kj/(mol K)
T=298; %Kelvin
file='calculated_Ka.xlsx';
i=1;



acid{i}='acetic acid (CH3COOH)';
%CH3COOH <--> CH3COO- + H+  source: water chem book



%GIBBS CALCULATION

REACTANTS=-396.6; %KJ PER MOL
PRODUCTS=-369.4; 
GIBBS=PRODUCTS-REACTANTS;
Ka(i)=exp(-GIBBS/(R*T));

%ENTHALPY CALCULATION
REACTANTS=-485.8; %KJ PER MOL
PRODUCTS=-486.0; 
ENTHALPY(i)=-(PRODUCTS-REACTANTS)/R;

i=i+1;

