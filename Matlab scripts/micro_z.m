function z=micro_z(x,y,H,L,alpha)


z=H*cos(2*pi/L*x).*cos(2*pi/(alpha*L)*y)+H;



end
