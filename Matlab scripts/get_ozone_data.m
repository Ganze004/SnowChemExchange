clc
clear all
close all


for i=1:14
cmd_str=['NO_' num2str(i) '=load(''summit_ozone_NO_' num2str(i) '.txt'');'];
    eval(cmd_str)
end

ozone_NO=stack_matrix(NO_1,NO_2,NO_3,NO_4,NO_5,NO_6,NO_7,NO_8,NO_9,NO_10,NO_11,NO_12,NO_13,NO_14);

for i=1:14
cmd_str=['NO2_' num2str(i) '=load(''summit_ozone_NO2_' num2str(i) '.txt'');'];
    eval(cmd_str)
end

ozone_NO2=stack_matrix(NO2_1,NO2_2,NO2_3,NO2_4,NO2_5,NO2_6,NO2_7,NO2_8,NO2_9,NO2_10,NO2_11,NO2_12,NO2_13,NO2_14);

save('summit_ozone.mat','ozone_NO','ozone_NO2')