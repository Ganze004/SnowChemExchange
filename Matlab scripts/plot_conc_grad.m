function plot_conc_grad(file,varargin)


if nargin==2
    
    dates=varargin{1};
    date_min=dates(1);
    date_max=dates(2);
    [Y, M_min, D_min] = datevec(date_min);
    [Y, M_max, D_max] = datevec(date_max);
end



data=load(file);
load noaa.mat
load cu_rad.mat


index=find(noaa_jd>date_min & noaa_jd<date_max);

wind_jd=noaa_jd(index);
wind_speed=noaa_ws(index);
wind_dir=noaa_wd(index);

wind_speed(wind_speed==9999)=NaN;

index=find(rad_jd>date_min & rad_jd<date_max);
irr_down=rad_down(index);
irr_up=rad_up(index);
irr_jd=rad_jd(index);

irr_down(irr_down>2000)=NaN;

scrsz = get(0,'ScreenSize');
load jd_idl_matlab_diff.mat
NO_time=data(:,10)+JD_IDL_MATLAB_DIFF;
NO2_time=data(:,13)+JD_IDL_MATLAB_DIFF;
%date filter

if nargin==2
    index=find(NO_time>date_min & NO_time<date_max);
else
    
    index=1:length(NO_time);
end

NO_time=NO_time(index);
NO_pptv=data(index,11);
NO_sscnt=data(index,12);

if nargin==2
    index=find(NO2_time>date_min & NO2_time<date_max);
else
    
    index=1:length(NO2_time);
end


NO2_time=NO2_time(index);
NO2_pptv=data(index,14);
NO2_sscnt=data(index,15);


if isequal(size(NO_pptv),size(NO2_pptv))
NOx_pptv=NO_pptv+NO2_pptv;
else
    if length(NO_pptv) > length(NO2_pptv)
        NOx_pptv=NO_pptv(1:length(NO2_pptv))+NO2_pptv;
        NO_time=NO_time(1:length(NO2_pptv));
        NO_pptv=NO_pptv(1:length(NO2_pptv));
    else
         NOx_pptv=NO_pptv+NO2_pptv(1:length(NO_pptv));
        NO2_time=NO2_time(1:length(NO_pptv));
         NO2_pptv=NO2_pptv(1:length(NO_pptv));
    end
end

lim_NOx=[0 600];
lim_NO2=lim_NOx;
lim_NO=[0 200];
lim_depths=[-3 2];
tick_depths=lim_depths(1):lim_depths(2);
lim_ws=[0 25];
tick_ws=lim_ws(1):5:lim_ws(2);
lim_irr=[0 1000];
tick_irr=lim_irr(1):200:lim_irr(2);
num_date_ticks=10;


figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
%subplot(2,1,1)



[AX h1 h2]=plotyy(NO_time,NO_pptv,NO2_time,NO2_pptv);  %,wind_jd,wind_speed);
hold all


xlabel('Date')
set(get(AX(2),'Ylabel'),'String','\DeltaNO2 [ppt_v]')
set(get(AX(1),'Ylabel'),'String','\DeltaNO [ppt_v] ')
title(['NO and NO2 surface gradients - [Top - Bottom] ' ])
set(AX(1),'XLim',[date_min date_max])
set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
set(AX(2),'XLim',[date_min date_max])
set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
datetick(AX(1),'x','mm/dd/yy','keepticks')
datetick(AX(2),'x','mm/dd/yy','keepticks')

set(h1,'Linestyle','o')
set(h2,'Linestyle','sq')
legend([h1 h2],'NO','NO2','Location','NorthWest')
% subplot(2,1,2)
% 
% if ~isempty(irr_jd) && ~isempty(wind_jd)
% [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
% 
% xlabel('Date')
% set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
% set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
% title('Surface Irradiance and Wind Speed Measurements')
% set(AX(1),'XLim',[date_min date_max])
% set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
% set(AX(2),'XLim',[date_min date_max])
% set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
% ylim(AX(1),lim_irr)
% datetick(AX(1),'x','mm/dd/yy','keepticks')
% datetick(AX(2),'x','mm/dd/yy','keepticks')
% 
% set(AX(1),'YTick',tick_irr)
% ylim(AX(2),lim_ws)
% set(AX(2),'YTick',tick_ws)
% set(h1,'Linestyle','o')
% legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')

%end

if nargin ==2
file=['NO_NO2_grad_' num2str(M_min) '_' num2str(D_min)  '-' num2str(M_max) '_' num2str(D_max) '_' num2str(Y) '.jpeg'];
else
    file=['NO_NO2_Wind_Irradiance_gradient'];
end


saveas(gcf,file)




figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
subplot(2,1,1)


% 
% plot(NO_time,NOx_pptv,'o');  %,wind_jd,wind_speed);
% 
% 
% 
% xlabel('Date')
% ylabel('\DeltaNO_x [ppt_v]')
% title(['NO_x surface gradients - [Top - Bottom] ' ])
% set(gca,'XTick',linspace(date_min,date_max,num_date_ticks))
% datetick('x','mm/dd/yy','keepticks')
% 
% 
% 
% subplot(2,1,2)
% 
% if ~isempty(irr_jd) && ~isempty(wind_jd)
% [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
% 
% xlabel('Date')
% set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
% set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
% title('Surface Irradiance and Wind Speed Measurements')
% datetick(AX(1),'x','mm/dd/yy','keepticks')
% datetick(AX(2),'x','mm/dd/yy','keepticks')
% set(AX(1),'XLim',[date_min date_max])
% set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
% set(AX(2),'XLim',[date_min date_max])
% set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
% ylim(AX(1),lim_irr)
% set(AX(1),'YTick',tick_irr)
% ylim(AX(2),lim_ws)
% set(AX(2),'YTick',tick_ws)
% set(h1,'Linestyle','o')
% legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
% 
% end
% 
% if nargin ==2
% file=['NOx_grad_' num2str(M_min) '_' num2str(D_min)  '-' num2str(M_max) '_' num2str(D_max) '_' num2str(Y) '.jpeg'];
% else
%     file=['NOx_Wind_Irradiance_gradient'];
% end
% 
% 
% saveas(gcf,file)
% 
% 

close all


end