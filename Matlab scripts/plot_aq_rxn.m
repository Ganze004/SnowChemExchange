clc
clear all
close all

file='qqk_aq.out';
% fid=fopen(file);
% 
% d=fscanf(fid,'%f',3);
% 
num_depth=68;
num_spc=48;
num_steps=(96*7);

data=load(file)';
% 
% data=fscanf(fid,'%f',[num_spc+5,inf]);
% fclose(fid);


depth=data(1,:);
jday=data(2,:);
jtime=data(3,:);
temperature=data(4,:);

spc=data(5:end,:);

index=depth<0;
depth=depth(index);
jday=jday(index);
jtime=jtime(index);
temperature=temperature(index);

spc=spc(:,index);
num_depth=19;

depth=reshape(depth,num_depth, num_steps);
depth=flipud(depth);


linear_time=(jday-jday(1))*86400; %Keeps the time in order for new days when plotting
jtime=jtime+linear_time;
jtime=reshape(jtime,num_depth, num_steps);
for i=1:num_steps
    M=6;
    D=floor(jtime(:,i)/(24*60*60));
    jtime(:,i)=jtime(:,i)-D*(24*60*60);
    H=floor(jtime(:,i)/(60*60));
    jtime(:,i)=jtime(:,i)-H*60*60;
    minute=floor(jtime(:,i)/60);
    jtime(:,i)=jtime(:,i)-minute*60;
    S=jtime(:,i);
    
    D=D+7;
   jtime(:,i)=datenum(2011,M,D,H,minute,S); 
    
end





       
spc_labels={
         'O3 + OH =  HO2 + O2';
         'O3 + O2- + H2O =  OH + OH- + 2.00*O2';
         'OH + OH =  H2O2';
        'OH + HO2 =  H2O + O2';
        'OH + O2- =  OH- + O2';
         'OH + H2O2 =  HO2 + H2O';
         'HO2 + HO2 =  H2O2 + O2';
         'HO2 + O2- =  H2O2';
         'O + O2 =  O3';
         'HONO + OH =  NO2 + H2O';
         'HONO + H2O2 + H =  HNO3 + H + H2O';
         'NO3 + OH- =  NO3- + OH';
         'NO2 + NO2 =  HNO3 + HONO';
        'NO2 + HO2 =  HNO4';
        'NO2- + O3 =  NO3- + O2';
         'NO2- + OH =  NO2 + OH-';
         'NO4- =  NO2- + O2';
         'O + NO2- =  NO3-';
        'O + NO3- =  NO2- + O2';
         'NO + NO2 =  2.00*NO2- + 2.00*H';
         'NO + OH =  NO2- + H';
         'NO2 + OH =  NO3- + H';
         'NO2- + NO3 =  NO2 + NO3-';
         'NO3 + HO2 =  NO3- + H + O2';
         'NO3 + O2- =  NO3- + O2';
         'NO3 + H2O2 =  NO3-';
         'O3 + NO2- =  NO2 + OH + O2';
         'HCHO + OH =  HCOOH + HO2';
         'HCOOH + OH =  HO2 + CO2';
         'HCOO- + OH =  OH- + HO2 + CO2';
         'CH3O2 + HO2 =  CH3OOH';
         'CH3O2 + O2- =  CH3OOH + OH-';
         'CH3OH + OH =  HCHO + HO2';
         'CH3OOH + OH =  CH3O2';
         'CH3OOH + OH =  HCHO + OH';
         'CO3- + O2- =  HCO3- + OH-';
         'CO3- + H2O2 =  HCO3- + HO2';
         'CO3- + HCOO- =  2.00*HCO3- + HO2';
         'HCO3- + OH =  CO3-';
         'DOM + OH =  HO2';
         'HCOOH + NO3 =  NO3- + H + CO2 + HO2';
         'HCOOH + O3 =  CO2 + HO2 + OH';
         'HCOO- + NO3 =  NO3- + CO2 + HO2';
         'CH3OH + NO3 =  NO3- + H + HCHO + HO2';
         'HCO3- + O2- =  HO2- + CO3-';
        'CO32- =  CO32-';
         'NH3 =  NH3';
         'NH4 =  NH4';
         };

            
          
            
            
spc_cell=cell(num_spc,1);
for i=1:num_spc
  sp=spc(i,:);
  spc_cell{i}=flipud(reshape(sp,num_depth,num_steps));
 
end

for i=1:num_spc
   x=jtime;
   y=depth;
   z=spc_cell{i};
   
   figure
   contourf(x,y,z,100,'LineColor','none')
   datetick('x',6,'keepticks')
   xlabel('Date')
   ylabel('Depth [m]')
   colorbar
  
   title({'Reaction rate [molecule/(cm^3 s)]';spc_labels{i};})
   folder='qqk_aq';
   mkdir(folder)
    
     saveas(gcf,[ folder '/qqk_aq_' num2str(i) '.jpeg'])
end



file='qqj_aq.out';
% fid=fopen(file);
% 
% d=fscanf(fid,'%f',3);
% 
num_depth=68;
num_spc=8;
num_steps=(96*7);

data=load(file)';
% 
% data=fscanf(fid,'%f',[num_spc+5,inf]);
% fclose(fid);


depth=data(1,:);
jday=data(2,:);
jtime=data(3,:);
temperature=data(4,:);

spc=data(5:end,:);

index=depth<0;
depth=depth(index);
jday=jday(index);
jtime=jtime(index);
temperature=temperature(index);

spc=spc(:,index);
num_depth=19;

depth=reshape(depth,num_depth, num_steps);
depth=flipud(depth);


linear_time=(jday-jday(1))*86400; %Keeps the time in order for new days when plotting
jtime=jtime+linear_time;
jtime=reshape(jtime,num_depth, num_steps);
for i=1:num_steps
    M=6;
    D=floor(jtime(:,i)/(24*60*60));
    jtime(:,i)=jtime(:,i)-D*(24*60*60);
    H=floor(jtime(:,i)/(60*60));
    jtime(:,i)=jtime(:,i)-H*60*60;
    minute=floor(jtime(:,i)/60);
    jtime(:,i)=jtime(:,i)-minute*60;
    S=jtime(:,i);
    
    D=D+7;
   jtime(:,i)=datenum(2011,M,D,H,minute,S); 
    
end


spc_labels={
     'O3 + hv =  H2O2 + O2';
	 'H2O2 + hv =  2.00*OH';
	 'NO3- + hv =  NO2 + OH';
	 'NO2- + hv =  NO + OH';
	 'NO3- + hv =  NO2- + O';
	 'NO3 + hv =  NO + O2';
	 'NO3 + hv =  NO2 + O';
	 'HONO + hv =  NO + OH';
      };
    
  
  spc_cell=cell(num_spc,1);
for i=1:num_spc
  sp=spc(i,:);
  spc_cell{i}=flipud(reshape(sp,num_depth,num_steps));
 
end

for i=1:num_spc
   x=jtime;
   y=depth;
   z=spc_cell{i};
   
   figure
   contourf(x,y,z,100,'LineColor','none')
   datetick('x',6,'keepticks')
   xlabel('Date')
   ylabel('Depth [m]')
   colorbar
  
   title({'Reaction rate [molecule/(cm^3 s)]';spc_labels{i};})
   folder='qqj_aq';
   mkdir(folder)
    
     saveas(gcf,[ folder '/qqj_aq_' num2str(i) '.jpeg'])
end
