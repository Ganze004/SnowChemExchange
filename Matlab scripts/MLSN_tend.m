clc
clear all
close all

path='model_obs_com/';
mkdir(path)
file='MLSN_CHEM/output/tend.out';
data=load(file);

year=2009
month=4
day=11
hour=0
m=0

IDT_O3=2+2;
IDT_NO=47+2;
IDT_NO2=48+2;
IDT_RAD=65;
num_spec=62
index=find(data(:,2)==data(1,2),2,'first');
num_depth=index(2)-index(1);

time=data(:,1);
conc_time=datenum(year,month,day,hour,m,time);
num_time=length(time)/num_depth;
conc_time=reshape(conc_time,num_depth,num_time);
conc_depths=reshape(data(:,2),num_depth,num_time);
conc_data=(10^12)*3600*data(:,3:end);

em=conc_data(:,1:num_spec);
dep=conc_data(:,num_spec+1:2*num_spec);
chem=conc_data(:,2*num_spec+1:3*num_spec);
trans=conc_data(:,3*num_spec+1:4*num_spec);
date_tick=linspace(conc_time(1,1),conc_time(1,end),5);




load MLSN_species.mat
for i=[2 36 47 48 52]%1%:size(MLSN_names,1)%;
    i
    
     em_now=reshape(em(:,i),num_depth,num_time);
     conc_array=em_now(:);
    conc_std=std(conc_array);
    conc_mean=mean(conc_array);
    index=conc_array<(3*conc_std+conc_mean) & conc_array>(-3*conc_std+conc_mean);
    clim_conc=[min(conc_array(index)) max(conc_array(index))];
    if (isempty(clim_conc))
        clim_em=0:100;
    else
    clim_em=linspace(clim_conc(1),clim_conc(2),100);
    end
    
    
     dep_now=reshape(dep(:,i),num_depth,num_time);
     conc_array=dep_now(:);
    conc_std=std(conc_array);
    conc_mean=mean(conc_array);
    index=conc_array<(3*conc_std+conc_mean) & conc_array>(-3*conc_std+conc_mean);
    clim_conc=[min(conc_array(index)) max(conc_array(index))];
    if (isempty(clim_conc))
        clim_dep=0:100;
    else
    clim_dep=linspace(clim_conc(1),clim_conc(2),100);
    end
    
   
     
     chem_now=reshape(chem(:,i),num_depth,num_time);
     conc_array=chem_now(:);
    conc_std=std(conc_array);
    conc_mean=mean(conc_array);
    index=conc_array<(3*conc_std+conc_mean) & conc_array>(-3*conc_std+conc_mean);
    clim_conc=[min(conc_array(index)) max(conc_array(index))];
    if (isempty(clim_conc))
        clim_chem=0:100;
    else
    clim_chem=linspace(clim_conc(1),clim_conc(2),100);
    end
    
     
     trans_now=reshape(trans(:,i),num_depth,num_time);
     conc_array=trans_now(:);
    conc_std=std(conc_array);
    conc_mean=mean(conc_array);
    index=conc_array<(3*conc_std+conc_mean) & conc_array>(-3*conc_std+conc_mean);
    clim_conc=[min(conc_array(index)) max(conc_array(index))];
    if (isempty(clim_conc))
        clim_trans=0:100;
    else
    clim_trans=linspace(clim_conc(1),clim_conc(2),100);
    end
    
    
    index=[MLSN_names{:,2}]==i;
    
figure('Visible','off') 
subplot(4,1,1)
contourf(conc_time,conc_depths,em_now,clim_em,'LineStyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')

ylabel('Depth [m]')
%ylim([min(conc_depths(:)) 1])
title(['Emission tend. of ' MLSN_names{index,1} ' [ppt_v hr^{-1}]'])
P=get(gca,'Position');

subplot(4,1,2)
contourf(conc_time,conc_depths,dep_now,clim_dep,'LineStyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')

ylabel('Depth [m]')
%ylim([min(conc_depths(:)) 1])
title(['Deposition tend. of ' MLSN_names{index,1} ' [ppt_v hr^{-1}]'])
P2=get(gca,'Position');
P2(3)=P(3);
P2(4)=P(4)*7/8;
set(gca,'Position',P2)

subplot(4,1,3)
contourf(conc_time,conc_depths,chem_now,clim_chem,'LineStyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')

ylabel('Depth [m]')
%ylim([min(conc_depths(:)) 1])
title(['Chemistry tend. of ' MLSN_names{index,1} ' [ppt_v hr^{-1}]'])
P2=get(gca,'Position');
P2(3)=P(3);
P2(4)=P(4)*7/8;
set(gca,'Position',P2)

subplot(4,1,4)
contourf(conc_time,conc_depths,trans_now,clim_trans,'LineStyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
%ylim([min(conc_depths(:)) 1])
title(['Transport tend. of ' MLSN_names{index,1} ' [ppt_v hr^{-1}]'])
P2=get(gca,'Position');
P2(3)=P(3);
P2(4)=P(4)*7/8;
set(gca,'Position',P2)


saveas(gcf,[path 'tend_' MLSN_names{index,1} '.jpeg'])
close all
end





