function temperature=MTU_get_temp(directory)

para_file=[directory 'model_para.out']
data=load(para_file);
NUM_DEPTH=data(1);
NUM_SPEC=data(2);
NUM_TIME=data(3);
MAX_j=data(4);

file='model_temp.out'
data=load([directory file]);
temperature=reshape(data,NUM_DEPTH,NUM_TIME);
%keyboard
end