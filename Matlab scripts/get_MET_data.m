%function [NO_time NO NO_sd NO2_time NO2 NO2_sd] =get_snow_level_data(files)
clc
clear all
close all

files={'data/NOx_ppt_maintower_final_correctNOx_sd.txt';
    };

num_header_lines=15;
headers=cell(numel(files));


NO_time1=[];
NO2_time1=[];
NO_1=[];
NO2_1=[];
NO_sd1=[];
NO2_sd1=[];

NO_time2=[];
NO2_time2=[];
NO_2=[];
NO2_2=[];
NO_sd2=[];
NO2_sd2=[];

for i=1:numel(files)
    fid=fopen(files{i},'r');
    files{i}
    head=cell(num_header_lines);
    
    for j=1:num_header_lines
        head{j}=fgetl(fid);
        
    end
    headers{i}=head;
    
    line=fgetl(fid);
    counter=1;
    
    while line~=-1
        [temp line]=strtok(line);
        [temp2 line]=strtok(line);
        temp3{1}=strrep([temp ' ' temp2],'/','-');
        NO_time1(i,counter)=datenum(temp3,'mm-dd-yyyy HH:MM:SS');
        
        [temp line]=strtok(line);
        NO_1(i,counter)=str2num(temp);
        
        [temp line]=strtok(line);
        NO_sd1(i,counter)=str2num(temp);
        
        [temp line]=strtok(line);
        [temp2 line]=strtok(line);
        temp3{1}=strrep([temp ' ' temp2],'/','-');
        NO2_time1(i,counter)=datenum(temp3,'mm-dd-yyyy HH:MM:SS');
        [temp line]=strtok(line);
        
        if ~isempty(str2num(temp))
        NO2_1(i,counter)=str2num(temp);
        else
            NO2_1(i,counter)=NaN;
        end
        [temp line]=strtok(line);
        NO2_sd1(i,counter)=str2num(temp);
        
        
        
         [temp line]=strtok(line);
        [temp2 line]=strtok(line);
        temp3{1}=strrep([temp ' ' temp2],'/','-');
        NO_time2(i,counter)=datenum(temp3,'mm-dd-yyyy HH:MM:SS');
        
        [temp line]=strtok(line);
        NO_2(i,counter)=str2num(temp);
        
        [temp line]=strtok(line);
        NO_sd2(i,counter)=str2num(temp);
        
        [temp line]=strtok(line);
        [temp2 line]=strtok(line);
        temp3{1}=strrep([temp ' ' temp2],'/','-');
        NO2_time2(i,counter)=datenum(temp3,'mm-dd-yyyy HH:MM:SS');
        [temp line]=strtok(line);
        
        if ~isempty(str2num(temp))
        NO2_2(i,counter)=str2num(temp);
        else
            NO2_2(i,counter)=NaN;
        end
        [temp line]=strtok(line);
        NO2_sd2(i,counter)=str2num(temp);
        
        
        counter=counter+1;
        line=fgetl(fid);
    end
    
    
    
    %     data=fscanf(fid,' %20c  %g %g %20c %g %g',[6 inf]);
    %
    %     NO_time(i,1:size(data,2))=data(1,:);
    %     NO2_time(i,1:size(data,2))= data(4,:);
    %     NO(i,1:size(data,2))=data(2,:);
    %     NO2(i,1:size(data,2))=data(5,:);
    %     NO_sd(i,1:size(data,2))=data(3,:);
    %     NO2_sd(i,1:size(data,2))=data(6,:);
    %
    
    fclose(fid);
end


%     NO_time(NO_time==0)=NaN;
%     NO2_time(NO2_time==0)=NaN;
%     NO(NO==0)=NaN;
%     NO2(NO2==0)=NaN;
%     NO_sd(NO_sd==0)=NaN;
%     NO2_sd(NO2_sd==0)=NaN;
%

files=flipud(files);
NO_time1=flipud(NO_time1);
NO_sd1=flipud(NO_sd1);
NO_1=flipud(NO_1);
NO2_time1=flipud(NO2_time1);
NO2_sd1=flipud(NO2_sd1);
NO2_1=flipud(NO2_1);

NO_time2=flipud(NO_time2);
NO_sd2=flipud(NO_sd2);
NO_2=flipud(NO_2);
NO2_time2=flipud(NO2_time2);
NO2_sd2=flipud(NO2_sd2);
NO2_2=flipud(NO2_2);

save('summit_MET_data.mat','files','NO_time1','NO2_time1','NO_1','NO2_1','NO_sd1','NO2_sd1','NO_time2','NO2_time2','NO_2','NO2_2','NO_sd2','NO2_sd2')

 %end

