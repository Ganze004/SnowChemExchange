clc
clear all
close all


% NO_path='episode/';
% NO2_path='episode';
% O3_path='episode/';
%
% mkdir(NO_path)
% mkdir(NO2_path)
% mkdir(O3_path)
clim_NOx=linspace(0,1000,100);%linspace(0,600,100);
clim_NO2=clim_NOx;
clim_NO=linspace(0,200,100);
clim_O3=linspace(0,100,100);
clim_temp=linspace(-100,10,100);

input_file='observation_input.inp';
Dt=1800; %Time step of interpreted data, used for input file
start_time=0.0;  %Start and end time of the observed data for the model, should program in, will ask Laurens about it
end_time=0.0;



dates=[ 4 11 2009 4 25 2009]
%dates=[ 1 1 2009 12 30  2009]

for dd=1:size(dates,1)
    close all
    
    
    load summit_snow_data.mat
    load depths_data.mat
    load summit_snow_ozone_data.mat
    load summit_MET_data.mat
    load summit_MET_ozone_data.mat
    load noaa.mat
    load cu_rad.mat
    load summit_MET_hts.mat
    load summit_snow_temperature
    load summit_MET_temperature
    load summit_eddy_covar.mat
    [noaa_jd noaa_wd noaa_ws rad_down rad_jd rad_up]=horz_arrays(noaa_jd,noaa_wd, noaa_ws, rad_down, rad_jd,rad_up);
    
    month1=dates(dd,1);
    day1=dates(dd,2);
    hour1=0.0;
    year1=dates(dd,3);
    month2=dates(dd,4);
    day2=dates(dd,5);
    hour2=0.0;
    year2=dates(dd,6);
    time_diff=2;  %Time difference in hours for time zones  2 for SUMMIT
    
    path=['flux_' num2str(year1) '_' num2str(month1) '_' num2str(day1) '_' num2str(year2) '_' num2str(month2) '_' num2str(day2) '/']
    mkdir(path)
    dates(dd,:)
    
    
    day_span=[datenum(year1,month1,day1,hour1+time_diff,0,0) datenum(year2,month2,day2,hour2+time_diff,0,0)];
    tick_dates=linspace(day_span(1),day_span(2),5);
    
    [NO_time NO]=date_index(day_span,NO_time,NO);
    [NO2_time NO2]=date_index(day_span,NO2_time,NO2);
    [snow_O3_time O3]=date_index(day_span,snow_O3_time, snow_O3);
    
    [O3_time O3_1 O3_2]=date_index(day_span,O3_time,O3_1, O3_2);
    [NO_time2 NO_2]=date_index(day_span,NO_time2,NO_2);
    [NO2_time2 NO2_2]=date_index(day_span, NO2_time2, NO2_2);
    [NO_time1 NO_1]=date_index(day_span,NO_time1,NO_1);
    [NO2_time1 NO2_1]=date_index(day_span, NO2_time1, NO2_1);
    
    [noaa_jd noaa_wd noaa_ws]=date_index(day_span,noaa_jd, noaa_wd, noaa_ws);
    [rad_jd rad_down rad_up]=date_index(day_span,rad_jd,rad_down,rad_up);
    [ht_jd in_1_ht in_2_ht]=date_index(day_span, ht_jd, in_1_ht, in_2_ht);
    [temp_jd snow_temp]=date_index(day_span,temp_jd,snow_temp);
    [MET_temp_jd MET_temp_bot MET_temp_mid]=date_index(day_span, MET_temp_jd,MET_temp_bot, MET_temp_mid); 
    [EC_jd EC_monin EC_ustar]=date_index(day_span, EC_jd', EC_monin', EC_ustar'); 
   
  
    % Meterological modifications
    stable=@(z,MO) 1+4.6*z/MO;
    unstable=@(z,MO) (1-16*z/MO)^(-1/4);
    int_stable=@(z,MO) log(z)+4.6*z/MO;%z+2.3*z^2/MO;
    int_unstable=@(z,MO) log(z)*(1-16*z/MO)^(-0.25) - 4/MO*(1-16*z/MO)^(-1.25);%-MO/12*(1-16*z/MO)^(3/4);
    
    noaa_ws(noaa_ws==9999)=NaN;
    noaa_wd(noaa_wd==9999)=NaN;
    v_ws=sin(noaa_wd*pi/180).*noaa_ws;
    u_ws=cos(noaa_wd*pi/180).*noaa_ws;
    
    rad_down(rad_down>2000)=NaN;
    MET_temp_bot(MET_temp_bot==-9999)=NaN;
    MET_temp_mid(MET_temp_mid==-9999)=NaN;
    
    noaa_jd_org=noaa_jd;
    
    
    % Chemical species modifcations
    NO(NO==0)=NaN;
    NO(NO<0)=0;
    NO_time(NO_time==0)=NaN;
    NO2(NO2==0)=NaN;
    NO2(NO2<0)=0;
    NO2_time(NO2_time==0)=NaN;
    O3(O3==0)=NaN;
    O3(O3<0)=0;
    snow_O3_time(snow_O3_time==0)=NaN;
    
    NO_2(NO_2==0)=NaN;
    NO_2(NO_2<0)=0;
    NO_time2(NO_time2==0)=NaN;
    NO2_2(NO2_2==0)=NaN;
    NO2_2(NO2_2<0)=0;
    NO2_time2(NO2_time2==0)=NaN;
    O3_2(O3_2==0)=NaN;
    O3_2(O3_2<0)=0;
    O3_time(O3_time==0)=NaN;
    NO_1(NO_1==0)=NaN;
    NO_1(NO_1<0)=0;
    NO_time1(NO_time1==0)=NaN;
    NO2_1(NO2_1==0)=NaN;
    NO2_1(NO2_1<0)=0;
    NO2_time1(NO_time1==0)=NaN;
    O3_1(O3_1==0)=NaN;
    O3_1(O3_1<0)=0;
    
    
    
    
%     [NO_time NO]=daily_average_data(day_span,NO_time,NO);
%     [NO2_time NO2]=daily_average_data(day_span,NO2_time,NO2);
%     [snow_O3_time O3]=daily_average_data(day_span,snow_O3_time, O3);
%     [O3_time O3_2 O3_1]=daily_average_data(day_span,O3_time, O3_2,O3_1);
%     [NO_time2 NO_2]=daily_average_data(day_span,NO_time2,NO_2);
%     [NO2_time2 NO2_2]=daily_average_data( day_span,NO2_time2, NO2_2);
%     
%     [NO_time1 NO_1]=daily_average_data(day_span,NO_time1,NO_1);
%     [NO2_time1 NO2_1]=daily_average_data(day_span, NO2_time1, NO2_1);
%     [noaa_jd u_ws v_ws ]=daily_average_data(day_span,noaa_jd, u_ws, v_ws);
%     [rad_jd rad_down rad_up]=daily_average_data(day_span,rad_jd, rad_down, rad_up);
%     [ht_jd in_1_ht in_2_ht]=daily_average_data(day_span, ht_jd, in_1_ht, in_2_ht);
%     
%     [temp_jd snow_temp]=daily_average_data(day_span,temp_jd, snow_temp);
%     [MET_temp_jd MET_temp_bot MET_temp_mid]=daily_average_data( day_span,MET_temp_jd,MET_temp_bot, MET_temp_mid); 
    
 [NO_time NO]=interp_30_min_data(day_span,NO_time,NO);
    [NO2_time NO2]=interp_30_min_data(day_span,NO2_time,NO2);
    [snow_O3_time O3]=interp_30_min_data(day_span,snow_O3_time, O3);
    [O3_time O3_2 O3_1]=interp_30_min_data(day_span,O3_time, O3_2,O3_1);
    [NO_time2 NO_2]=interp_30_min_data(day_span,NO_time2,NO_2);
    [NO2_time2 NO2_2]=interp_30_min_data( day_span,NO2_time2, NO2_2);
    
    [NO_time1 NO_1]=interp_30_min_data(day_span,NO_time1,NO_1);
    [NO2_time1 NO2_1]=interp_30_min_data(day_span, NO2_time1, NO2_1);
    [noaa_jd u_ws v_ws ]=interp_30_min_data(day_span,noaa_jd, u_ws, v_ws);
    [rad_jd rad_down rad_up]=interp_30_min_data(day_span,rad_jd, rad_down, rad_up);
    [ht_jd in_1_ht in_2_ht]=interp_30_min_data(day_span, ht_jd, in_1_ht, in_2_ht);
    
    [temp_jd snow_temp]=interp_30_min_data(day_span,temp_jd, snow_temp);
    [MET_temp_jd MET_temp_bot MET_temp_mid]=interp_30_min_data( day_span,MET_temp_jd,MET_temp_bot, MET_temp_mid); 
    [EC_jd EC_monin EC_ustar]=interp_30_min_data(day_span, EC_jd, EC_monin, EC_ustar);

    
    
    NO_time=NO_time';
    NO=NO';
    [depths_jd yy]=meshgrid(depths_jd,1:size(NO_time,2));
    
    
    snow_O3_time=snow_O3_time';
    O3=O3';
    
    
    depths_jd=depths_jd';
% %     
% %     O3_depths=nearest_neighbor(snow_O3_time,depths_jd,depths);
% %     depths=nearest_neighbor(NO_time,depths_jd,depths);
% %     in_1_ht=nearest_neighbor(NO_time1,ht_jd,in_1_ht);
% %     in_2_ht=nearest_neighbor(NO_time2,ht_jd,in_2_ht);
% %     
% %     
% %     NO=NO';
% %     NO_time=NO_time';
% %     O3=O3';
% %     snow_O3_time=snow_O3_time';
% %     O3_depths=O3_depths';
% %     depths=depths';
% %     
% %     
% %     
% %     %make snow and Met data in same martix
% %     MET_depth=[in_2_ht;in_1_ht];
% %     depths=[MET_depth;depths];
% %     O3_depths=[MET_depth;O3_depths];
% %     
% %     NO=[NO_2;NO_1;NO];
% %     NO2=[NO2_2;NO2_1;NO2];
% %     O3=[O3_2;O3_1;O3];
% %     
% %     NO_time=[NO_time2;NO_time1;NO_time];
% %     NO2_time=[NO2_time2;NO2_time1;NO2_time];
% %     snow_O3_time=[O3_time;O3_time;snow_O3_time];
% %     
% %     snow_temp=[MET_temp_mid;MET_temp_bot;snow_temp];
% %     temp_jd=[temp_jd(1:2,:);temp_jd];
% %     
% %  
% %     
% %     dz=zeros(size(depths,1)-1,size(depths,2));
% %     dz_ozone=zeros(size(O3_depths,1)-1,size(O3_depths,2));
% %     for i=1:size(depths,2)
% %        dz(:,i) =diff(depths(:,i));
% %         dz_ozone(:,i)=diff(O3_depths(:,i));
% %     end
% %     dz=[dz;dz(end,:)];
% %     dz_ozone=[dz_ozone;dz_ozone(end,:)];
% %     
% %     for i=1:size(depths,1)
% %        NO_time2((2*i-1),:)=NO_time(i,:);    
% %        NO_time2((2*i),:)=NO_time(i,:);
% %        depths2((2*i-1),:)=depths(i,:)-dz(i,:)/2;
% %        depths2((2*i),:)=depths(i,:)+dz(i,:)/2+0.001;
% %        NO_2((2*i-1),:)=NO(i,:);    
% %        NO_2((2*i),:)=NO(i,:);
% %        
% %        NO2_time2((2*i-1),:)=NO2_time(i,:);    
% %        NO2_time2((2*i),:)=NO2_time(i,:);
% %         NO2_2((2*i-1),:)=NO2(i,:);    
% %        NO2_2((2*i),:)=NO2(i,:);
% %        
% %        snow_O3_time2((2*i-1),:)=snow_O3_time(i,:);
% %          snow_O3_time2((2*i),:)=snow_O3_time(i,:);
% %          O3_2((2*i-1),:)=O3(i,:);
% %          O3_2((2*i),:)=O3(i,:);
% %          
% %          O3_depths2((2*i-1),:)=O3_depths(i,:)-dz_ozone(i,:)/2;
% %        O3_depths2((2*i),:)=O3_depths(i,:)+dz_ozone(i,:)/2+0.001;
% %         
% %     end
% %     
    vonK=0.4;
    grad=NO2_2-NO2_1;
    C=(NO2_2+NO2_1)/2.0;
    dz=in_2_ht-in_1_ht;
    mid_z=(in_2_ht+in_1_ht)/2;
    F=nan(size(EC_jd));
    F2=nan(size(EC_jd));
    F1=nan(size(EC_jd));
    dC1=C-NO2_1;
    dC2=NO2_2-C;
    NO2_2_theory=nan(size(EC_jd));
    for i=1:length(EC_jd)
        if(EC_monin(i)==0)
            
        elseif(EC_monin(i)<=-2)
            F(i)=1/(int_unstable(in_2_ht(i),EC_monin(i))-int_unstable(in_1_ht(i),EC_monin(i)));
            F2(i)=1/(int_unstable(in_2_ht(i),EC_monin(i))-int_unstable(mid_z(i),EC_monin(i)));
            F1(i)=1/(int_unstable(mid_z(i),EC_monin(i))-int_unstable(in_1_ht(i),EC_monin(i)));
            NO2_2_theory(i)=C(i)*((int_unstable(mid_z(i),EC_monin(i))-int_unstable(in_1_ht(i),EC_monin(i))+(int_unstable(in_2_ht(i),EC_monin(i))-int_unstable(mid_z(i),EC_monin(i)))))-NO2_1(i)*(int_unstable(in_2_ht(i),EC_monin(i))-int_unstable(mid_z(i),EC_monin(i)));
        elseif(EC_monin(i)>-2)
            F(i)=1/(int_stable(in_2_ht(i),EC_monin(i))-int_stable(in_1_ht(i),EC_monin(i)));
            F2(i)=1/(int_stable(in_2_ht(i),EC_monin(i))-int_stable(mid_z(i),EC_monin(i)));
            F1(i)=1/(int_stable(mid_z(i),EC_monin(i))-int_stable(in_1_ht(i),EC_monin(i)));
            
            NO2_2_theory(i)=C(i)*((int_stable(mid_z(i),EC_monin(i))-int_stable(in_1_ht(i),EC_monin(i))+(int_stable(in_2_ht(i),EC_monin(i))-int_stable(mid_z(i),EC_monin(i)))))-NO2_1(i)*(int_stable(in_2_ht(i),EC_monin(i))-int_stable(mid_z(i),EC_monin(i)));
        end
    end
    F=F.*vonK.*EC_ustar.*grad;
    F1=F1.*vonK.*EC_ustar.*dC1;
    F2=F2.*vonK.*EC_ustar.*dC2;
%     F=-Kh*grad./dz;
    Vd=F./C;
    max_c=max([max(NO2_1) max(NO2_2)])
   figure
   subplot(3,1,1)
   plot(NO2_time2,F)
    datetick('x')
    ylabel('Flux [ppt_v s^(-1)')
   
    subplot(3,1,2)
    
    plot(NO2_time,NO2_1,'d')
    datetick('x')
    ylabel('Inlet 2 NO_2 [ppt_v]')
     ylim([0 max_c])
    %ylim([-10^-3 10^-3])
    subplot(3,1,3)
    plot(NO2_time,NO2_2,'sq')
    datetick('x')
    
     ylabel('Inlet 1 NO_2 [ppt_v]')
    xlabel('Time [MM/DD]')
     ylim([0 max_c])
    
     
     
     
     figure 
     plot(NO2_time2,Vd)
     datetick('x')
    %Comparions of sub fluxes to overall flux
     max_F=max([F F1 F2]);
     min_F=min([F F1 F2]);
     figure
     subplot(3,1,1)
     plot(NO2_time,F2)
     datetick('x')
     ylim([min_F max_F])
     
     subplot(3,1,2)
     plot(NO2_time,F1)
     datetick('x')
      ylim([min_F max_F])
      
     subplot(3,1,3)
     plot(NO2_time,F)
     datetick('x')
      ylim([min_F max_F])
      
      figure 
      plot(NO2_time, NO2_2,'*')
      hold all
      plot(NO2_time, NO2_2_theory,'sq')
      ylim([0 200])
      
end
