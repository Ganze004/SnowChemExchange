function U=WP(U10,z,k,L,H,rho,u,alpha)


delta=1/2*alpha/sqrt(alpha^2+1)*L/pi;

U=6*k*rho*H/(pi*u*L^2)*sqrt(alpha^2+1)/alpha*U10.^2*exp(-z/delta);


end