clc
clear all
close all


file='Dist2Surf_Snowtower.xlsx'
sheet='Before_ST_addition'
[num,txt,raw] = xlsread(file,sheet);

dates_b=txt(3:end,1);
depth_b=num(3:end,:);
inlet_b=num(1,:);

sheet='After_ST_addition'
[num,txt,raw] = xlsread(file,sheet);
dates_a=txt(3:end,1);
depth_a=num(3:end,:);
inlet_a=num(1,:);

depths=NaN(size(dates_a,1)+size(dates_b,1),size(depth_a,2));

depths(1:size(dates_b,1),(size(depth_a,2)-size(depth_b,2)+1):end)=depth_b;
depths((size(dates_b,1)+1):end,:)=depth_a;
jd_a=datenum(dates_a,'mm/dd/yyyy');
jd_b=datenum(dates_b,'mm/dd/yyyy');
depths_jd=[jd_b;jd_a];

save('depths_data.mat','depths','depths_jd')