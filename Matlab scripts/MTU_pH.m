clc
clear all
close all

%dir='/Users/Keenan/Documents/MTU_long1_output/';
%dir='/Users/Keenan/Documents/Air_output/';
%dir='/Users/Keenan/Documents/MTU_long2_output/';
dir='MTU2_output/';
path=dir;
mkdir(dir)
directory='MTU2/output_March29_May_1_30/';
%directory='/Users/Keenan/Documents/MTU_long1/output/';
%directory='/Users/Keenan/Documents/output_Feb23/';
%directory='/Users/Keenan/Documents/MTU_long2/output_Feb28/';



%  %May reserve
dir='~/Documents/May/';
%dir='~/Documents/April/';
path=dir;
mkdir(dir)
directory='~/Documents/output_March31_May_1_30_Final/';

year=2009
month=5
end_month=month
day=1
end_day=30
hour=0
m=0


% 
% % 
% % 
% %April reserve
% dir='~/Documents/April/';
% %dir='/Users/Keenan/Documents/MTU_long2_output/';
% 
% path=dir;
% mkdir(dir)
%  directory='~/Documents/output_March29_April_1_30/'
% %directory='/Users/Keenan/Documents/MTU_long2/output/';
% 
% year=2009
% month=4
% end_month=month
% day=1
% end_day=30
% hour=0
% m=0

spc_name=MTU_get_spcs(directory);
depths=MTU_get_depths(directory);
time=MTU_get_times(directory);
depths2=[];
time2=[];


for i=1:(length(depths)-1)
   depths2(2*i-1) =depths(i);
    depths2(2*i)=depths(i+1)*0.99;
    time2(2*i-1)=time(i);
    time2(2*i)=time(i);
end
%time=time2;
depths=depths2;







conc_file=[directory 'model_gas_ppx.out']
para_file=[directory 'model_para.out']
data=load(para_file);
NUM_DEPTH=data(1);
NUM_SPEC=data(2);
NUM_TIME=data(3);
MAX_j=data(4);
NUM_TEND=data(5);
NUM_EQ=data(6);
data=load(conc_file);


 

data=reshape(data,NUM_DEPTH,NUM_TIME,NUM_SPEC);
data2=zeros(2*NUM_DEPTH,NUM_TIME,NUM_SPEC);

for i=1:NUM_TIME
    for j=1:NUM_DEPTH
        data2(2*j-1,i,:)=data(j,i,:);
        data2(2*j,i,:)=data(j,i,:);
        
    end
end
data=data2;

 time=MTU_get_times(directory);
 date_tick=linspace(time(1),time(end),5);
 % date_tick=linspace(datenum(2009,4,11,0,0,0),datenum(2009,4,30,0,0,0),5);
 %keyboard
[time, depths]=meshgrid(time,depths);


 %MAX_j=1
 index=find(strcmp('Hp_aq',spc_name)==1);
 chem_choice='HNO4'
 
  index=find(strcmp(chem_choice,spc_name)==1);
 figure('Visible','off')
 conc=-log10(squeeze(data(:,:,index)));
 conc=squeeze(data(:,:,index));
 contourf(time,depths,conc,100,'LineStyle','none')
  colorbar
  %caxis([1 7])
  set(gca,'XTick',date_tick);
  datetick('x',2,'keepticks')
  ylabel('Depth')
  xlabel('Date')
  saveas(gcf,[path 'model_' chem_choice '.jpeg'])
  

