function varargout=horz_arrays(varargin)
% if nargin==0
%     varargout(1)=[];
%     
% end

for i=1:nargin
   array=varargin{i};
   temp=size(array,1);
   if temp~=1
       array=array';
   end
   temp=size(array,1);
   if temp~=1
       error(['Input ' num2str(i) ' is a matrix, not an array'])
   end
   varargout{i}=array;
    
end

end