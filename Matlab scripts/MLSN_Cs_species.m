clc
clear all
close all

path='model_obs_com/';
mkdir(path)
file='MLSN_CHEM/output/Cs.out';
data=load(file);

year=2009
month=4
day=11
hour=0
m=0

IDT_O3=2+2;
IDT_NO=47+2;
IDT_NO2=48+2;
IDT_RAD=65;
index=find(data(:,2)==data(1,2),2,'first');
num_depth=index(2)-index(1);

time=data(:,1);
conc_time=datenum(year,month,day,hour,m,time);
num_time=length(time)/num_depth;
conc_time=reshape(conc_time,num_depth,num_time);
conc_depths=reshape(data(:,2),num_depth,num_time);
conc_data=(10^12)*data(:,3:end);

date_tick=linspace(conc_time(1,1),conc_time(1,end),5);


%Observations
file2='contour_plots_2009_4_11_2009_4_25/obs.out';

data=load(file2);
index=find(data(:,2)==data(1,2),2,'first');
num_depth_obs=index(2)-index(1);

time_obs=data(:,1);
time_obs=datenum(year,month,day,hour,m,time_obs)-datenum(0,0,0,2,0,0);%offset for time zone

num_time_obs=length(time_obs)/num_depth_obs;
IDT_NO_obs=3;
IDT_NO2_obs=4;
IDT_O3_obs=5;
time_obs=reshape(time_obs,num_depth_obs,num_time_obs);
depths_obs=reshape(data(:,2),num_depth_obs,num_time_obs);
NO_obs=reshape(data(:,IDT_NO_obs),num_depth_obs,num_time_obs);
NO2_obs=reshape(data(:,IDT_NO2_obs),num_depth_obs,num_time_obs);
O3_obs=reshape(data(:,IDT_O3_obs),num_depth_obs,num_time_obs);
snow_temp_obs=reshape(data(:,6),num_depth_obs,num_time_obs);
u_ws_obs=reshape(data(:,7),num_depth_obs,num_time_obs);
v_ws_obs=reshape(data(:,8),num_depth_obs,num_time_obs);
rad_obs=reshape(data(:,9),num_depth_obs,num_time_obs);


index=~isnan(depths_obs(:,1));
depths_obs=depths_obs(index,:);
O3_obs=O3_obs(index,:);
time_obs=time_obs(index,:);
snow_temp_obs=snow_temp_obs(index,:);
NO_obs=NO_obs(index,:);
NO2_obs=NO2_obs(index,:);

%RAD
i=63;
 rad=reshape(conc_data(:,i),num_depth,num_time)/(10^12);
    
 %clear sky -RAD
i=64;
 CS_rad=reshape(conc_data(:,i),num_depth,num_time)/(10^12);

%"boxing" the layers
dx=abs(diff(conc_depths(:,1)));
dx(1)=2*conc_depths(1,1);
dx(end+1)=dx(end);
for i=1:num_depth
   depths2((2*i-1),:)=conc_depths(i,:)+dx(i)/2 ;
   depths2((2*i),:)=conc_depths(i,:)-dx(i)/2 ;
   
    time2((2*i-1),:)=conc_time(i,:);
    time2((2*i),:)=conc_time(i,:);
end
conc_depths=depths2;
conc_time=time2;

load MLSN_species.mat
for i=[2 36 47 48]%1%:size(MLSN_names,1)%;
    i
    
     conc_now=reshape(conc_data(:,i),num_depth,num_time);
    conc_array=conc_now(:);
    conc_std=std(conc_array);
    conc_mean=mean(conc_array);
    index=conc_array<(3*conc_std+conc_mean) & conc_array>(-3*conc_std+conc_mean);
       if conc_std>0
    clim_conc=[0 max(conc_array(index))];
       else
           clim_conc=[0 max(conc_array)];
       end
    clim_conc=linspace(clim_conc(1),clim_conc(2),100);
    
    
    for j=1:num_depth
        conc2((2*j-1),:)=conc_now(j,:);
        conc2((2*j),:)=conc_now(j,:);
    end
    conc_now=conc2;
    index=[MLSN_names{:,2}]==i;
figure('Visible','off') 
subplot(2,1,1)
contourf(conc_time,conc_depths,conc_now,clim_conc,'LineStyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([min(conc_depths(:)) 0.5])
title(['Modeled profile of snowflake ' MLSN_names{index,1} ' [ppt_v]'])
P=get(gca,'Position');

subplot(2,1,2)
plot(conc_time(1,:),rad(1,:),'-o')
hold all
plot(conc_time(1,:),CS_rad(1,:),'-sq')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Irradiance [W/(m^2)]')
legend('Modeled','Clear Sky','Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)


saveas(gcf,[path 'Cs_' MLSN_names{index,1} '_rad.jpeg'])
close all
end
figure('Visible','off') 


close all




