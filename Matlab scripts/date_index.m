function varargout=date_index(date_span,varargin)
%date span will minimize the data to include only the data for the period
%defined by date_span. Date_span is a 2 element vector with the datenum of
%the begining and the end of the period. The first varargin should be the
%time matrix

time_x=varargin{1};

   index=cell(size(time_x,1),1);
    for i=1:size(time_x,1)
        index{i}=find(time_x(i,:)>=date_span(1) & time_x(i,:)<=date_span(2));
    end


[varargout{1:nargout}]=index_matrix(index,varargin{:});

end