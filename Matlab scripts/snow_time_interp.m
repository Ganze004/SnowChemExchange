function [varargout]=snow_time_interp(varargin)



for i=1:nargin
   cmd_str=['matrix' num2str(i) '=varargin{i};'];
       eval(cmd_str)
    
end


index=find(sum(isnan(matrix1),2)~=size(matrix1,2));

for i=1:nargin
    cmd_str=['matrix' num2str(i) '=matrix' num2str(i) '(index,:);'];
    eval(cmd_str)
end



matrix1_I=zeros(size(matrix1));
for i=1:size(matrix1,2)
    temp=matrix1(:,i);
    matrix1_I(1,i)=mean(temp(~isnan(temp)));
end

matrix3_I=zeros(size(matrix3));
for i=1:size(matrix3,2)
    temp=matrix3(:,i);
    matrix3_I(1,i)=mean(temp(~isnan(temp)));
end



for i=2:size(matrix1_I,1)
    
    matrix1_I(i,:)=matrix1_I(1,:);
end

for i=2:size(matrix3_I,1)
    
    matrix3_I(i,:)=matrix3_I(1,:);
end



matrix1=matrix1_I;
matrix3=matrix3_I;


for i=1:nargin
   cmd_str=['varargout{i}=matrix' num2str(i) ';'];
       eval(cmd_str)
    
end



end