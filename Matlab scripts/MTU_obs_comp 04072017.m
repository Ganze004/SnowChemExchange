clc
clear all
close all

% 
dir='MTU4/output/';
path=dir;
mkdir(dir)
directory='MTU4/output/';

year=2009
month=5
end_month=month
day=1
end_day=1
hour=0
m=0

%file2='contour_plots_2009_4_11_2009_4_25/obs.out';
file2=['contour_plots_' num2str(year) '_' num2str(month) '_' num2str(day) '_' num2str(year) '_' num2str(end_month) '_' num2str(end_day) '/obs.out'];

spc_name=MTU_get_spcs(directory);
depths=MTU_get_depths(directory);

conc_file=[directory 'model_gas_ppx.out']
para_file=[directory 'model_para.out']
data=load(para_file);
NUM_DEPTH=data(1)%+1;
NUM_SPEC=data(2);

NUM_TIME=data(3);
MAX_j=data(4);
NUM_TEND=data(5);
data=load(conc_file);

%rj=MTU_get_rj(directory);
%tend=MTU_get_tend(directory);
%rj=reshape(data,MAX_j,NUM_TIME,NUM_SPEC,NUM_DEPTH);
%temperature=MTU_get_temp(directory);
% keyboard

[obs_irr model_irr]=MTU_get_irr(directory);
conc=reshape(data,NUM_DEPTH,NUM_TIME,NUM_SPEC);
O3=conc(:,:,strcmp(spc_name,'O3'))*10^9;
NO2=conc(:,:,strcmp(spc_name,'NO2'))*10^12;
NO=conc(:,:,strcmp(spc_name,'NO'))*10^12;
HNO4=conc(:,:,strcmp(spc_name,'HNO4'))*10^12;
HO2=conc(:,:,strcmp(spc_name,'HO2'))*10^12;
HO2_aq=conc(:,:,strcmp(spc_name,'HO2_aq'));

time=MTU_get_times(directory);
 %date_tick=linspace(time(1),time(end),5);
 %keyboard
 edge_times=time;
 edge_depths=depths;
 [edge_times, edge_depths]=meshgrid(edge_times,edge_depths);
for i=1:NUM_DEPTH
    depths(i)=(depths(i)+depths(i+1))/2;
end
depths=depths(1:end-1);
[time, depths]=meshgrid(time,depths);



data=load(file2);
index=find(data(:,2)==data(1,2),2,'first');
num_depth_obs=index(2)-index(1);

time_obs=data(:,1);
time_obs=datenum(year,month,day,hour,m,time_obs);%-datenum(0,0,0,2,0,0);%offset for time zone
num_time_obs=length(time_obs)/num_depth_obs;
IDT_NO_obs=3;
IDT_NO2_obs=4;
IDT_O3_obs=5;
time_obs=reshape(time_obs,num_depth_obs,num_time_obs);
depths_obs=reshape(data(:,2),num_depth_obs,num_time_obs);
NO_obs=reshape(data(:,IDT_NO_obs),num_depth_obs,num_time_obs);
NO2_obs=reshape(data(:,IDT_NO2_obs),num_depth_obs,num_time_obs);
O3_obs=reshape(data(:,IDT_O3_obs),num_depth_obs,num_time_obs);
snow_temp_obs=reshape(data(:,6),num_depth_obs,num_time_obs);
u_ws_obs=reshape(data(:,7),num_depth_obs,num_time_obs);
v_ws_obs=reshape(data(:,8),num_depth_obs,num_time_obs);
rad_obs=reshape(data(:,9),num_depth_obs,num_time_obs);
wind_obs=sqrt(u_ws_obs.^2+v_ws_obs.^2);
clim_NOx=linspace(0,600,100);%linspace(0,600,100);
clim_NO2=clim_NOx;
clim_NO=linspace(0,200,100);
clim_O3=linspace(0,70,100);
clim_temp=linspace(-100,10,100);
clim_diff=linspace(-200,200,100);

index=~isnan(depths_obs(:,1));
depths_obs=depths_obs(index,:);
O3_obs=O3_obs(index,:);
time_obs=time_obs(index,:);
snow_temp_obs=snow_temp_obs(index,:);
NO_obs=NO_obs(index,:);
NO2_obs=NO2_obs(index,:);


% index=depths_obs(:,1)<0;
% depths_obs=depths_obs(index,:);
% O3_obs=O3_obs(index,:);
% time_obs=time_obs(index,:);
% snow_temp_obs=snow_temp_obs(index,:);
% NO_obs=NO_obs(index,:);
% NO2_obs=NO2_obs(index,:);
% 
% index=depths(:,1)<0;
% depths=depths(index,:);
% O3=O3(index,:);
% time=time(index,:);
% NO=NO(index,:);
% NO2=NO2(index,:);

%we use the top NO,NO2, and ozone measuremnts to drive the model,so im
%setting the height of the top layer reference to the heights measured
O3_int=interp2(time,depths,O3,time_obs,depths_obs,'linear');
NO_int=interp2(time,depths,NO,time_obs,depths_obs,'linear');
NO2_int=interp2(time,depths,NO2,time_obs,depths_obs,'linear');
% rad_int=interp2(time,depths,rad,time_obs,depths_obs,'linear');
% grad_int=interp2(time,depths,grad,time_obs,depths_obs,'linear');
% usnow_int=interp2(time,depths,usnow,time_obs,depths_obs,'linear');
% Ts_int=interp2(time,depths,Ts,time_obs,depths_obs,'linear');


% 
% O3_obs_int=interp2(time_obs,depths_obs,O3_obs,time,depths,'linear');
% NO_obs_int=interp2(time_obs,depths_obs,NO_obs,time,depths,'linear');
% NO2_obs_int=interp2(time_obs,depths_obs,NO2_obs,time,depths,'linear');
F=TriScatteredInterp(time_obs(:),depths_obs(:),NO_obs(:));
NO_obs_int=F(time,depths);
F=TriScatteredInterp(time_obs(:),depths_obs(:),NO2_obs(:));
NO2_obs_int=F(time,depths);
F=TriScatteredInterp(time_obs(:),depths_obs(:),O3_obs(:));
O3_obs_int=F(time,depths);



% O3_int(1,:)=O3_obs(1,:);
% NO_int(1,:)=NO_obs(1,:);
% NO2_int(1,:)=NO2_obs(1,:);

date_tick=linspace(time_obs(1,1),time_obs(1,end),5);
date_tick=linspace(datenum(2009,month,15,0,0,0),datenum(2009,month,30,0,0,0),5);
date_tick2=linspace(time_obs(1,1),time_obs(1,end),3);

date_x=floor(time(1)):floor(time(end));
    date_y=[min(depths(:)) max(depths(:))];
    [date_x date_y]=meshgrid(date_x,date_y);




set(0,'defaultaxeslinestyleorder',{'*','+','o','sq'}) %or whatever you want
% %NO diff plots
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% for i=1:size(usnow,1)
%    plot(usnow(i,:),[NO_obs_int(i,:)-NO(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Vertical Wind [m/s]')
% ylabel('Observed-Model NO [ppt_v]')
% title('Model NO versus vertical windprofile')
% legend(list,'Location','EastOutside')
% %saveas(gcf,[path 'plot_diff_NO_usnow.jpeg'])
% 
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% for i=1:size(rad,1)
%    plot(rad(i,:),[NO_obs_int(i,:)-NO(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Net Radiance [W/m^2]')
% ylabel('Observed-Model NO [ppt_v]')
% title('Model NO versus irradiance')
% legend(list,'Location','EastOutside')
% %saveas(gcf,[path 'plot_diff_NO_rad.jpeg'])
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% for i=1:size(Ts,1)
%    plot(Ts(i,:),[NO_obs_int(i,:)-NO(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Surface Temperature [K]')
% ylabel('Observed-Model NO [ppt_v]')
% title('Model NO versus surface temperature')
% legend(list,'Location','EastOutside')
% %saveas(gcf,[path 'plot_diff_NO_Ts.jpeg'])
% 
% %NO2 diff plots
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% for i=1:size(usnow,1)
%    plot(usnow(i,:),[NO2_obs_int(i,:)-NO2(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Vertical Wind [m/s]')
% ylabel('Observed-Model NO2 [ppt_v]')
% title('Model NO2 versus vertical windprofile')
% legend(list,'Location','EastOutside')
% %saveas(gcf,[path 'plot_diff_NO2_usnow.jpeg'])
% 
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% for i=1:size(rad,1)
%    plot(rad(i,:),[NO2_obs_int(i,:)-NO2(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Net Radiance [W/m^2]')
% ylabel('Observed-Model NO2 [ppt_v]')
% title('Model NO2 versus irradiance')
% legend(list,'Location','EastOutside')
% %saveas(gcf,[path 'plot_diff_NO2_rad.jpeg'])
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% for i=1:size(Ts,1)
%    plot(Ts(i,:),[NO2_obs_int(i,:)-NO2(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Surface Temperature [K]')
% ylabel('Observed-Model NO2 [ppt_v]')
% title('Model NO2 versus surface temperature')
% legend(list,'Location','EastOutside')
% %saveas(gcf,[path 'plot_diff_NO2_Ts.jpeg'])
% 
% 
% 
% 
% %O3 diff plots
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% for i=1:size(usnow,1)
%    plot(usnow(i,:),[O3_obs_int(i,:)-O3(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Vertical Wind [m/s]')
% ylabel('Observed-Model O3 [ppb_v]')
% title('Model O3 versus vertical windprofile')
% legend(list,'Location','EastOutside')
% %saveas(gcf,[path 'plot_diff_O3_usnow.jpeg'])
% 
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% for i=1:size(rad,1)
%    plot(rad(i,:),[O3_obs_int(i,:)-O3(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Net Radiance [W/m^2]')
% ylabel('Observed-Model O3 [ppb_v]')
% title('Model O3 versus irradiance')
% legend(list,'Location','EastOutside')
% %saveas(gcf,[path 'plot_diff_O3_rad.jpeg'])
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% for i=1:size(Ts,1)
%    plot(Ts(i,:),[O3_obs_int(i,:)-O3(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Surface Temperature [K]')
% ylabel('Observed-Model O3 [ppb_v]')
% title('Model O3 versus surface temperature')
% legend(list,'Location','EastOutside')
% %saveas(gcf,[path 'plot_diff_O3_Ts.jpeg'])
% 
% 


O3_2=zeros(size(O3,1)*2,size(O3,2));
NO_2=zeros(size(NO,1)*2,size(NO,2));
HNO4_2=zeros(size(HNO4,1)*2,size(HNO4,2));
HO2_2=zeros(size(HO2,1)*2,size(HO2,2));
HO2_aq_2=zeros(size(HO2_aq,1)*2,size(HO2_aq,2));
NO2_2=zeros(size(NO2,1)*2,size(NO2,2));
time_2=zeros(size(time,1)*2,size(time,2));
depths_2=zeros(size(depths,1)*2,size(depths,2));


for i=1:NUM_DEPTH
   O3_2(2*i-1,:)=O3(i,:);
   O3_2(2*i,:)=O3(i,:);
    NO_2(2*i-1,:)=NO(i,:);
   NO_2(2*i,:)=NO(i,:);
   HNO4_2(2*i-1,:)=HNO4(i,:);
   HNO4_2(2*i,:)=HNO4(i,:);
   HO2_2(2*i-1,:)=HO2(i,:);
   HO2_2(2*i,:)=HO2(i,:);
   HO2_aq_2(2*i-1,:)=HO2_aq(i,:);
   HO2_aq_2(2*i,:)=HO2_aq(i,:);
   NO2_2(2*i-1,:)=NO2(i,:);
   NO2_2(2*i,:)=NO2(i,:);
   time_2(2*i-1,:)=time(i,:);
   time_2(2*i,:)=time(i,:);
   depths_2(2*i-1,:)=edge_depths(i,:);
   depths_2(2*i,:)=edge_depths(i+1,:);
end

%keyboard
spin_up=3;  %days
X=[time_obs(end,1),time_obs(end,1),time_obs(end,1)+spin_up,time_obs(end,1)+spin_up];
Y=[depths_obs(end,1),0,0,depths_obs(end,1)];
C=zeros(size(Y));

%HNO4
figure('units','normalized','outerposition',[0 0 1 1])
contourf(time_2,depths_2,HNO4_2,'Linestyle','none')
%caxis([min(clim_O3) max(clim_O3)])
ch=colorbar;
ylabel(ch,'HO_2NO_2 Mixing Ratio [ppt_v]')
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
%rectangle('Position',[time_obs(end,1),depths_obs(end,1),spin_up,abs(depths_obs(end,1))],'FaceColor','k')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Modeled O_3 [ppb_v] ')

% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% %title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)


% axis tight
% size = get(gcf,'Position');
% size = size(3:4); % the last two elements are width and height of the figure
% set(gcf,'PaperUnit','normalized'); % unit for the property PaperSize
% set(gcf,'PaperSize',size);
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'model_vs_obs_HNO4'],'-djpeg','-r800')

%HO2
figure('units','normalized','outerposition',[0 0 1 1])
contourf(time_2,depths_2,HO2_2,0:5,'Linestyle','none')
%caxis([min(clim_O3) max(clim_O3)])
ch=colorbar;
ylabel(ch,'HO_2 Mixing Ratio [ppt_v]')
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
%rectangle('Position',[time_obs(end,1),depths_obs(end,1),spin_up,abs(depths_obs(end,1))],'FaceColor','k')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Modeled O_3 [ppb_v] ')

% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% %title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)


% axis tight
% size = get(gcf,'Position');
% size = size(3:4); % the last two elements are width and height of the figure
% set(gcf,'PaperUnit','normalized'); % unit for the property PaperSize
% set(gcf,'PaperSize',size);
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'model_HO2'],'-djpeg','-r800')


%Aqueous HO2
figure('units','normalized','outerposition',[0 0 1 1])
contourf(time_2,depths_2,HO2_aq_2,'Linestyle','none')
%caxis([min(clim_O3) max(clim_O3)])
ch=colorbar;
ylabel(ch,'Aqueous HO_2 Concentration [M]')
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
%rectangle('Position',[time_obs(end,1),depths_obs(end,1),spin_up,abs(depths_obs(end,1))],'FaceColor','k')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Modeled O_3 [ppb_v] ')

% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% %title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)


% axis tight
% size = get(gcf,'Position');
% size = size(3:4); % the last two elements are width and height of the figure
% set(gcf,'PaperUnit','normalized'); % unit for the property PaperSize
% set(gcf,'PaperSize',size);
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'model_HO2_aq'],'-djpeg','-r800')


figure('units','normalized','outerposition',[0 0 1 1])
index=[20 42 60];
for jkl=1:numel(index)
plot(time_2(index(jkl),:),HO2_2(index(jkl),:),'-o')
hold all
end
%caxis([min(clim_O3) max(clim_O3)])
ylabel('HO_2 Mixing Ratio [ppt_v]')
%rectangle('Position',[time_obs(end,1),depths_obs(end,1),spin_up,abs(depths_obs(end,1))],'FaceColor','k')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
%ylim([-2 0])
legend(['Height  ' num2str(depths_2(index(1),1)) ' [m]'],['Height  ' num2str(depths_2(index(2),1)) ' [m]'],['Height  ' num2str(depths_2(index(3),1)) ' [m]'],'Location','EastOutside')
%title('Modeled O_3 [ppb_v] ')

% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% %title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)


% axis tight
% size = get(gcf,'Position');
% size = size(3:4); % the last two elements are width and height of the figure
% set(gcf,'PaperUnit','normalized'); % unit for the property PaperSize
% set(gcf,'PaperSize',size);
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'model_heights_HO2'],'-djpeg','-r800')

%HNO4 heights
figure('units','normalized','outerposition',[0 0 1 1])
index=[20 42 60];
for jkl=1:numel(index)
plot(time_2(index(jkl),:),HNO4_2(index(jkl),:),'-o')
hold all
end
%caxis([min(clim_O3) max(clim_O3)])
ylabel('HNO_4 Mixing Ratio [ppt_v]')
%rectangle('Position',[time_obs(end,1),depths_obs(end,1),spin_up,abs(depths_obs(end,1))],'FaceColor','k')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
%ylim([-2 0])
legend(['Height  ' num2str(depths_2(index(1),1)) ' [m]'],['Height  ' num2str(depths_2(index(2),1)) ' [m]'],['Height  ' num2str(depths_2(index(3),1)) ' [m]'],'Location','EastOutside')
%title('Modeled O_3 [ppb_v] ')

% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% %title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)


% axis tight
% size = get(gcf,'Position');
% size = size(3:4); % the last two elements are width and height of the figure
% set(gcf,'PaperUnit','normalized'); % unit for the property PaperSize
% set(gcf,'PaperSize',size);
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'model_heights_HNO4'],'-djpeg','-r800')

% %Ozone Section
% 
figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1)
contourf(time_obs,depths_obs,O3_obs,clim_O3,'Linestyle','none')
ch=colorbar;
ylabel(ch,'O_3 Mixing Ratio [ppb_v]')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
%xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Interpolated 30-minute measurements of O_3 [ppb_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',2.5)
P=get(gca,'Position');
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
set(gca,'xtick',[])
set(gca,'xticklabel',[])
            x_lim=get(gca,'XLim');
            y_lim=get(gca,'YLim');
            text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'a','FontSize',15,'Color','white')
            
subplot(2,1,2)
contourf(time_2,depths_2,O3_2,clim_O3,'Linestyle','none')
caxis([min(clim_O3) max(clim_O3)])
ch=colorbar;
ylabel(ch,'O_3 Mixing Ratio [ppb_v]')
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
%rectangle('Position',[time_obs(end,1),depths_obs(end,1),spin_up,abs(depths_obs(end,1))],'FaceColor','k')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Modeled O_3 [ppb_v] ')
            x_lim=get(gca,'XLim');
            y_lim=get(gca,'YLim');
            text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'b','FontSize',15,'Color','white')
% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% %title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
if(exist([path 'model_vs_obs_O3.jpeg'],'file'))
    delete([path 'model_vs_obs_O3_old.jpeg'])
    movefile([path 'model_vs_obs_O3.jpeg'],[path 'model_vs_obs_O3_old.jpeg'])
end
% size = get(gcf,'Position');
% size = size(3:4); % the last two elements are width and height of the figure
% set(gcf,'PaperUnit','normalized'); % unit for the property PaperSize
% set(gcf,'PaperSize',size);
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_O3.jpeg'])
print(gcf,[path 'model_vs_obs_O3'],'-djpeg','-r800')

% %NO2
% 
figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1)
contourf(time_obs,depths_obs,NO2_obs,clim_NO2,'Linestyle','none')
ch=colorbar;
ylabel(ch,'NO_2 Mixing Ratio [ppt_v]')
caxis([min(clim_NO2) max(clim_NO2)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
%xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Interpolated 30-minute measurements of NO_2 [ppt_v]')
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',2.5)
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');
set(gca,'xtick',[])
set(gca,'xticklabel',[])
            x_lim=get(gca,'XLim');
            y_lim=get(gca,'YLim');
            text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'a','FontSize',15,'Color','white')
subplot(2,1,2)
contourf(time_2,depths_2,NO2_2,clim_NO2,'Linestyle','none')
ch=colorbar;
ylabel(ch,'NO_2 Mixing Ratio [ppt_v]')
caxis([min(clim_NO2) max(clim_NO2)])
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Modeled NO_2 [ppt_v] ')
            x_lim=get(gca,'XLim');
            y_lim=get(gca,'YLim');
            text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'b','FontSize',15,'Color','white')% subplot(3,1,3)
% plot(time_obs(1,:),rad_obs(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
if(exist([path 'model_vs_obs_NO2.jpeg'],'file'))
    delete([path 'model_vs_obs_NO2_old.jpeg'])
    movefile([path 'model_vs_obs_NO2.jpeg'],[path 'model_vs_obs_NO2_old.jpeg'])
end
% size = get(gcf,'Position');
% size = size(3:4); % the last two elements are width and height of the figure
% set(gcf,'PaperUnit','normalized'); % unit for the property PaperSize
% set(gcf,'PaperSize',size);
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_NO2.jpeg'])
print(gcf,[path 'model_vs_obs_NO2'],'-djpeg','-r800')

% %NO
% 
figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1)
contourf(time_obs,depths_obs,NO_obs,clim_NO,'Linestyle','none')
ch=colorbar;ylabel(ch,'NO Mixing Ratio [ppt_v]');
caxis([min(clim_NO) max(clim_NO)])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
%xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Interpolated 30-minute measurements of NO [ppt_v]')
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)

plot(time_obs,depths_obs,'.','Color','k','MarkerSize',2.5)
P=get(gca,'Position');
set(gca,'xtick',[])
set(gca,'xticklabel',[])
            x_lim=get(gca,'XLim');
            y_lim=get(gca,'YLim');
            text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'a','FontSize',15,'Color','white')
            
subplot(2,1,2)
contourf(time_2,depths_2,NO_2,clim_NO,'Linestyle','none')
ch=colorbar;ylabel(ch,'NO Mixing Ratio [ppt_v]');
caxis([min(clim_NO) max(clim_NO)])
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Modeled NO [ppt_v] ')
            x_lim=get(gca,'XLim');
            y_lim=get(gca,'YLim');
            text(x_lim(1)*0.97+x_lim(2)*.03,y_lim(2)*.9+y_lim(1)*.1,'b','FontSize',15,'Color','white')
% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
if(exist([path 'model_vs_obs_NO.jpeg'],'file'))
    delete([path 'model_vs_obs_NO_old.jpeg'])
    movefile([path 'model_vs_obs_NO.jpeg'],[path 'model_vs_obs_NO_old.jpeg'])
end
% size = get(gcf,'Position');
% size = size(3:4); % the last two elements are width and height of the figure
% set(gcf,'PaperUnit','normalized'); % unit for the property PaperSize
% set(gcf,'PaperSize',size);
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_NO.jpeg'])
print(gcf,[path 'model_vs_obs_NO'],'-djpeg','-r800')

figure('units','normalized','outerposition',[0 0 1 1])
subplot(2,2,1)
contourf(time_obs,depths_obs,NO_obs,clim_NO,'Linestyle','none')
ch=colorbar;ylabel(ch,'Concentration [ppt_v]');
set(gca,'XTick',date_tick2);
datetick('x',2,'keepticks')
%xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Interpolated 30-minute measurements of NO [ppt_v]')
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)


P=get(gca,'Position');

subplot(2,2,3)
contourf(time_2,depths_2,NO_2,clim_NO,'Linestyle','none')
ch=colorbar;ylabel(ch,'Concentration [ppt_v]');
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
set(gca,'XTick',date_tick2);
datetick('x',2,'keepticks')
%xlabel('Date [mm/dd/yy]')
ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Modeled NO [ppt_v] ')


subplot(2,2,2)
contourf(time_obs,depths_obs,NO2_obs,clim_NO2,'Linestyle','none')
ch=colorbar;ylabel(ch,'Concentration [ppt_v]');
set(gca,'XTick',date_tick2);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
%ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Interpolated 30-minute measurements of NO_2 [ppt_v]')
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',2.5)
ttt=min(time_obs(:)):1/48:max(time_obs(:));
ddd=zeros(size(ttt));
dddd=ddd+min(depths_obs(:));

ttt=[ttt;ttt];
ddd=[ddd;dddd];

%plot(ttt,ddd,'-','Color','k','MarkerSize',0.5)
P=get(gca,'Position');

subplot(2,2,4)
contourf(time_2,depths_2,NO2_2,clim_NO2,'Linestyle','none')
ch=colorbar;ylabel(ch,'Concentration [ppt_v]');
hold all
plot(date_x,date_y,'-','Color','w','LineWidth',1.5)
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
set(gca,'XTick',date_tick2);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
%ylabel('Depth [m]')
%ylim([-2 0])
ylim([min(depths_obs(:)) max(depths_obs(:))])
%title('Modeled NO_2 [ppt_v] ')
% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% size = get(gcf,'Position');
% size = size(3:4); % the last two elements are width and height of the figure
% set(gcf,'PaperUnit','normalized'); % unit for the property PaperSize
% set(gcf,'PaperSize',size);
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_vs_obs_NO_NO2.jpeg'])
print(gcf,[path 'model_vs_obs_NO_NO2'],'-djpeg','-r800')
% 
% %Modeled snow density
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% 
% contourf(time,depths,DS,'Linestyle','none')
% ch=colorbar;
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Depth [m]')
% ylim([-2 0])
% title('Modeled snow density [g/cm^3]')
% %saveas(gcf,[path 'Snow_Density.jpeg'])
% 
% %Modeled vs obs wind 
% 
% figure('units','normalized','outerposition',[0 0 1 1])
% plot(time(1,:),wind(1,:))
% hold all
% plot(time_obs(1,:),wind_obs(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Wind Speed [m/s]')
% title('Modeled vs Observed wind speed [m/s]')
%    legend('Model','Observed','Location','EastOutside')
% %saveas(gcf,[path 'wind.jpeg'])
% 
% 
% 
% 
%  
% figure('units','normalized','outerposition',[0 0 1 1])
% subplot(4,1,1)
% contourf(time_obs,depths_obs,O3_obs,clim_O3,'Linestyle','none')
% ch=colorbar;
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Depth [m]')
% ylim([-2 0])
% title('Interpolated 30-minute measurements of O_3 [ppb_v]')
% 
% 
% subplot(4,1,2)
% contourf(time,depths,O3,clim_O3,'Linestyle','none')
% ch=colorbar;
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Depth [m]')
% ylim([-2 0])
% title('Modeled O_3 [ppb_v] ')
% 
% subplot(4,1,3)
% plot(time(1,:),rad(1,:),'-o')
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylim([0 500])
% ylabel('Irradiance [W/(m^2)]')
% title('Modeled Irradiance')
% 
% subplot(4,1,4)
% plot(time_obs(1,:),rad_obs(1,:),'-o')
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylim([0 500])
% ylabel('Irradiance [W/(m^2)]')
% title('Observed Irradiance')
% 
% %saveas(gcf,[path 'model_vs_obs_O3_rad.jpeg'])
% 
% 
% % figure('units','normalized','outerposition',[0 0 1 1])
% % 
% % subplot(2,1,1)
% % contourf(time_obs,depths_obs,O3_obs-O3_int,'Linestyle','none')
% % ch=colorbar;
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date [mm/dd/yy]')
% % ylabel('Depth [m]')
% % title('Observed - Modeled O_3 [ppb_v]')
% % P=get(gca,'Position');
% % 
% % subplot(2,1,2)
% % plot(time_obs(1,:),u_ws_obs(1,:),'-o')
% % hold all
% % plot(time_obs(1,:),v_ws_obs(1,:),'-sq');plot(time_obs(1,:),zeros(size(time_obs(1,:))),'-','Color','k');
% % 
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date [mm/dd/yy]')
% % ylabel('Wind Speed [m/s]')
% % legend('North-South','East-West','No Wind','Location','Best')
% % P2=get(gca,'Position');
% % P2(3)=P(3);
% % set(gca,'Position',P2)
% % %saveas(gcf,[path 'diff_O3_wind.jpeg'])
% % 
% % 
% % 
% % figure('units','normalized','outerposition',[0 0 1 1])
% % subplot(2,1,1)
% % contourf(time_obs,depths_obs,O3_obs-O3_int,'Linestyle','none')
% % ch=colorbar;
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date [mm/dd/yy]')
% % ylabel('Depth [m]')
% % title('Observed - Modeled O_3 [ppb_v]')
% % P=get(gca,'Position');
% % 
% % subplot(2,1,2)
% % plot(time_obs(1,:),rad_obs(1,:),'-o')
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date [mm/dd/yy]')
% % ylabel('Irradiance [W/(m^2)]')
% % P2=get(gca,'Position');
% % P2(3)=P(3);
% % set(gca,'Position',P2)
% % %saveas(gcf,[path 'diff_O3_rad.jpeg'])
% % 
% % 
% % figure('units','normalized','outerposition',[0 0 1 1])
% % subplot(2,1,1)
% % contourf(time_obs,depths_obs,O3_obs-O3_int,'Linestyle','none')
% % ch=colorbar;
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date [mm/dd/yy]')
% % ylabel('Depth [m]')
% % title('Observed - Modeled O_3 [ppb_v]')
% % P=get(gca,'Position');
% % 
% % subplot(2,1,2)
% % contourf(time_obs,depths_obs,snow_temp_obs,'Linestyle','none')
% % ch=colorbar;
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date [mm/dd/yy]')
% % ylabel('Depth [m]')
% % title('Temperature  [C]')
% % P2=get(gca,'Position');
% % P2(3)=P(3);
% % set(gca,'Position',P2)
% % 
% % %saveas(gcf,[path 'diff_O3_temp.jpeg'])
% 
% close all

% figure('units','normalized','outerposition',[0 0 1 1])
% set(0,'defaultaxeslinestyleorder',{'*','+','o','sq'}) %or whatever you want
% for i=1:size(depths_obs,1)
%    plot(time_obs(i,:),depths_obs(i,:))
%    hold all 
% end
% 
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% %xlabel('Date [mm/dd/yy]')
% ylabel('Depth [m]')
% 
% title('Height of measurement inlets')
% %saveas(gcf,[path 'measurement_heights.jpeg'])
% 
% % %NO2
% % 
% figure('units','normalized','outerposition',[0 0 1 1])
% subplot(4,1,1)
% hold all
% NO2_d=(NO2_obs-NO2_int)./NO2_obs*100;
% NO2_d=[NO2_d(3,:);NO2_d(end-2,:)];
% NO2_d_time=[time_obs(3,:);time_obs(end-2,:)];
% plot(NO2_d_time(1,:),NO2_d(1,:),'MarkerSize',2)
% hold all
% plot(NO2_d_time(2,:),NO2_d(2,:),'MarkerSize',2)
% Y=[-100,100,100,-100];
% 
% 
% set(gca,'XTick',date_tick);
% 
% datetick('x',2,'keepticks')
% 
% patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
% %xlabel('Date [mm/dd/yy]')
% ylabel('Error [%]')
% ylim([-100 100])
% title({'Model error of NO_2 ';'Negative values are model overestimations';' Blue is near the surface, Green is around 2m deep'})
% %legend(['Depth = ' num2str(depths_obs(3,1)) ' m'],['Depth = ' num2str(depths_obs(end-2,1)) ' m'],'Location','NorthWest')
% P=get(gca,'Position');
% P2=P;
% P2(4)=P2(4)*1.25;
% set(gca,'Position',P2)
% 
% 
% 
% subplot(4,1,2)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% %xlabel('Date [mm/dd/yy]')
% ylabel('Irradiance [W/m^2]')
% title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% 
% subplot(4,1,3)
% plot(time(1,:),wind(1,:))
% 
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% %xlabel('Date [mm/dd/yy]')
% ylabel('Wind Speed [m/s]')
% title('Wind speed [m/s]')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% 
%    
% 
% subplot(4,1,4)
% contourf(time_obs,depths_obs,snow_temp_obs,'Linestyle','none')
% ch=colorbar;
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date [mm/dd/yy]')
% ylabel('Depth [m]')
% ylim([-2 0])
% title('Temperature [C]')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)


figure('Visible','off')
max_irr=max(max(obs_irr),max(model_irr));
plot(time_2(1,:),obs_irr)
hold all
plot(time_2(1,:),model_irr,'sq')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date [mm/dd/yy]')
ylabel({'Irradiance';'W m^-^2'})
ylim([0 max_irr])
legend('obs','model','Location','EastOutside')
set(gcf, 'Renderer', 'painters')
%saveas(gcf,[path 'model_obs_irr.jpeg'])