clc
clear all
close all

file='summit_MET_data.mat'
path='min_10_plots/';
mkdir(path)
min_10=datenum(0,0,0,0,10,0);
load(file)
inlet_1=[];
inlet_2=[];

%m=20:25 bad
m_bad=20:25;
for m=1:25
    
    if (max(m==m_bad)==1)
        continue
    end
    close all
    
    time_span=datenum(2008,6+m,0,0,0,0):min_10:datenum(2008,6+(m+1),0,0,0,0);
    tick_dates=linspace(time_span(1),time_span(end),5);
    
    
    NO_time1_t=time_span(1:end-1)+diff(time_span)/2;
    NO_1_t=nan(1,length(time_span)-1);
    NO_sd1_t=nan(1,length(time_span)-1);
    NO2_time1_t=time_span(1:end-1)+diff(time_span)/2;
    NO2_1_t=nan(1,length(time_span)-1);
    NO2_sd1_t=nan(1,length(time_span)-1);
    
    
    for j=1:length(NO_time1_t)
       
        index=NO_time1>=time_span(j) & NO_time1<time_span(j+1);
        if max(index)==1
           NO_time1_t(j)=NO_time1(index);
           NO_1_t(j)=NO_1(index);
           NO_sd1_t(j)=NO_sd1(index);
        end
        
        index=NO2_time1>=time_span(j) & NO2_time1<time_span(j+1);
        if max(index)==1
           NO2_time1_t(j)=NO2_time1(index);
           NO2_1_t(j)=NO2_1(index);
           NO2_sd1_t(j)=NO2_sd1 (index);
        end
        
        
    end
    
    
    
    inlet_1=[inlet_1,[ NO_time1_t;NO_1_t;NO_sd1_t;NO2_time1_t;NO2_1_t;NO2_sd1_t]];
    
    
    NO_time2_t=time_span(1:end-1)+diff(time_span)/2;
    NO_2_t=nan(1,length(time_span)-1);
    NO_sd2_t=nan(1,length(time_span)-1);
    NO2_time2_t=time_span(1:end-1)+diff(time_span)/2;
    NO2_2_t=nan(1,length(time_span)-1);
    NO2_sd2_t=nan(1,length(time_span)-1);
    
    
    for j=1:length(NO_time2_t)
       
        index=NO_time2>=time_span(j) & NO_time2<time_span(j+1);
        if max(index)==1
           NO_time2_t(j)=NO_time2(index);
           NO_2_t(j)=NO_2(index);
           NO_sd2_t(j)=NO_sd2(index);
        end
        
        index=NO2_time2>=time_span(j) & NO2_time2<time_span(j+1);
        if max(index)==1
           NO2_time2_t(j)=NO2_time2(index);
           NO2_2_t(j)=NO2_2(index);
           NO2_sd2_t(j)=NO2_sd2(index);
        end
        
        
    end
    
    inlet_2=[inlet_2,[ NO_time2_t;NO_2_t;NO_sd2_t;NO2_time2_t;NO2_2_t;NO2_sd2_t]];
   
    
    figure('Visible','off')
    subplot(2,1,1)
     index=NO_time1>=time_span(1) & NO_time1<time_span(end);
    plot(NO_time1(index),NO_1(index),'-o')
    hold all
    plot(NO_time1_t,NO_1_t,'sq')
    xlabel('Date')
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    ylabel('NO [ppt_v]')
    title('Inlet 1 data')
    legend('Raw Data','10 min Data','Location','EastOutside')
    
    
    subplot(2,1,2)
    index=NO2_time1>=time_span(1) & NO2_time1<time_span(end);
    
    plot(NO2_time1(index),NO2_1(index),'-o')
    hold all
    plot(NO2_time1_t,NO2_1_t,'sq')
    xlabel('Date')
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    ylabel('NO2 [ppt_v]')
    title('Inlet 1 data')
    legend('Raw Data','10 min Data','Location','EastOutside')
    saveas(gcf,[path 'inlet1_' num2str(m) '.jpeg'])
    
    figure('Visible','off')
    subplot(2,1,1)
     index=NO_time2>=time_span(1) & NO_time2<time_span(end);
    plot(NO_time2(index),NO_2(index),'-o')
    hold all
    plot(NO_time2_t,NO_2_t,'sq')
    xlabel('Date')
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    ylabel('NO [ppt_v]')
    title('Inlet 2 data')
    legend('Raw Data','10 min Data','Location','EastOutside')
    
    
    subplot(2,1,2)
    index=NO2_time2>=time_span(1) & NO2_time2<time_span(end);
    
    plot(NO2_time2(index),NO2_2(index),'-o')
    hold all
    plot(NO2_time2_t,NO2_2_t,'sq')
    xlabel('Date')
    set(gca,'XTick',tick_dates)
    datetick('x',2,'keepticks')
    ylabel('NO2 [ppt_v]')
    title('Inlet 2 data')
    legend('Raw Data','10 min Data','Location','EastOutside')
    saveas(gcf,[path 'inlet2_' num2str(m) '.jpeg'])
    
    
end


