function D=gas_diffusion(T,M,P)
lambda_air=mean_free_path(T,P);


 v=molec_vel(T,M);
 
 D=lambda_air*v/3;


end