% MAQ_lg_20180320+ 
% Matlab script to read the output files with photolysis rates
% --------------------------------------------------------------

clc
clear all
close all

% determining the output directory (where to write diagnostics?)
dir='../MTU4/output/May/';
path=dir;
mkdir(dir)

directory='../MTU4/output/'

year=2009
month=5
end_month=month
day=1
end_day=11
hour=0
m=0

spc_name=MTU_get_spcs(directory)
rj_file=[directory 'model_rj.out']
para_file=[directory 'model_para.out'];
data=load(para_file);

NUM_DEPTH=data(1);
NUM_SPEC=data(2);
NUM_SPEC=1; % MAQ_lg_20180322+ temporarily only reading in jNO2
spc_name{1}='NO2'

NUM_TIME=data(3);
MAX_j=data(4);

depths=MTU_get_depths(directory);
time=MTU_get_times(directory);

% for i=1,length(depths)-1
%    depths(i) =mean(depths(i:i+1));
%     
% end
% 
% depths=depths(1:end-1);
% time=1:NUM_TIME;
[time, depths]=meshgrid(time,depths);

date_tick=linspace(datenum(2009,month,1,0,0,0),datenum(2009,month,11,0,0,0),5);

%data=load(rj_file);
data=dlmread(rj_file);  % using dlmread instead of load to deal with a matrix that is not completely filled

data=reshape(data,MAX_j,NUM_TIME,NUM_SPEC,NUM_DEPTH);
 
for j=1:MAX_j
  for i=1:NUM_SPEC
    rj=squeeze(data(j,:,i,:))'; % to reduce the 4-dimensional array to a 2-d array
    if (max(rj(:))>10.0^(-8))
      figure('Visible','on') 
      contourf(time(1:NUM_DEPTH,:),depths(1:NUM_DEPTH,:),rj,'LineStyle','none')
      colorbar
      set(gca,'XTick',date_tick);
      datetick('x',2,'keepticks')
      ylim([min(depths(:)) 0.05])
      xlabel('Date')
      ylabel('Depth [m] ')
      saveas(gcf,[dir 'rj_' spc_name{i} '_' num2str(j) '.jpeg'])
      %close(gcf)
    end
  end
end