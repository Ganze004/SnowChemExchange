clc
clear all
close all


path='model_obs_com/';
mkdir(path)
file='MLSN_CHEM/output/conc.out';
data=load(file);

year=2009
month=4
day=11
hour=0
m=0
load MLSN_species.mat
IDT_O3=85+2;
IDT_NO=87+2;
IDT_NO2=95+2;
IDT_RAD=size(MLSN_names,1)+1;
IDT_GRAD=size(MLSN_names,1)+2;
IDT_USNOW=size(MLSN_names,1)+3;
IDT_TS=size(MLSN_names,1)+4;
IDT_DS=size(MLSN_names,1)+5;
IDT_wind=size(MLSN_names,1)+6;
IDT_O3_flux=size(MLSN_names,1)+7;
IDT_NO_flux=size(MLSN_names,1)+8;
IDT_NO2_flux=size(MLSN_names,1)+9;
IDT_NO2_snrf=size(MLSN_names,1)+10;
index=find(data(:,2)==data(1,2),2,'first');
num_depth=index(2)-index(1);

time=data(:,1);
time=datenum(year,month,day,hour,m,time);
num_time=length(time)/num_depth;
time=reshape(time,num_depth,num_time);
depths=reshape(data(:,2),num_depth,num_time);
NO=reshape(data(:,IDT_NO),num_depth,num_time);
NO2=reshape(data(:,IDT_NO2),num_depth,num_time);
O3=reshape(data(:,IDT_O3),num_depth,num_time);
rad=reshape(data(:,IDT_RAD),num_depth,num_time);
grad=reshape(data(:,IDT_GRAD),num_depth,num_time);
usnow=reshape(data(:,IDT_USNOW),num_depth,num_time);
Ts=reshape(data(:,IDT_TS),num_depth,num_time);
DS=reshape(data(:,IDT_DS),num_depth,num_time);
wind=reshape(data(:,IDT_wind),num_depth,num_time);
por=1-DS/0.917;
flux_O3=reshape(data(:,IDT_O3_flux),num_depth,num_time);
flux_NO=reshape(data(:,IDT_NO_flux),num_depth,num_time);
flux_NO2=reshape(data(:,IDT_NO2_flux),num_depth,num_time);
%snrf_NO2=reshape(data(:,IDT_NO2_snrf),num_depth,num_time);

O3=O3*10^9;
NO=NO*10^12;
NO2=NO2*10^12;



file2='contour_plots_2009_4_11_2009_4_25/obs.out';

data=load(file2);
index=find(data(:,2)==data(1,2),2,'first');
num_depth_obs=index(2)-index(1);

time_obs=data(:,1);
time_obs=datenum(year,month,day,hour,m,time_obs);%-datenum(0,0,0,2,0,0);%offset for time zone
num_time_obs=length(time_obs)/num_depth_obs;
IDT_NO_obs=3;
IDT_NO2_obs=4;
IDT_O3_obs=5;
time_obs=reshape(time_obs,num_depth_obs,num_time_obs);
depths_obs=reshape(data(:,2),num_depth_obs,num_time_obs);
NO_obs=reshape(data(:,IDT_NO_obs),num_depth_obs,num_time_obs);
NO2_obs=reshape(data(:,IDT_NO2_obs),num_depth_obs,num_time_obs);
O3_obs=reshape(data(:,IDT_O3_obs),num_depth_obs,num_time_obs);
snow_temp_obs=reshape(data(:,6),num_depth_obs,num_time_obs);
u_ws_obs=reshape(data(:,7),num_depth_obs,num_time_obs);
v_ws_obs=reshape(data(:,8),num_depth_obs,num_time_obs);
rad_obs=reshape(data(:,9),num_depth_obs,num_time_obs);
wind_obs=sqrt(u_ws_obs.^2+v_ws_obs.^2);
clim_NOx=linspace(0,600,100);%linspace(0,600,100);
clim_NO2=clim_NOx;
clim_NO=linspace(0,200,100);
clim_O3=linspace(0,70,100);
clim_temp=linspace(-100,10,100);
clim_diff=linspace(-200,200,100);

index=~isnan(depths_obs(:,1));
depths_obs=depths_obs(index,:);
O3_obs=O3_obs(index,:);
time_obs=time_obs(index,:);
snow_temp_obs=snow_temp_obs(index,:);
NO_obs=NO_obs(index,:);
NO2_obs=NO2_obs(index,:);


% index=depths_obs(:,1)<0;
% depths_obs=depths_obs(index,:);
% O3_obs=O3_obs(index,:);
% time_obs=time_obs(index,:);
% snow_temp_obs=snow_temp_obs(index,:);
% NO_obs=NO_obs(index,:);
% NO2_obs=NO2_obs(index,:);
% 
% index=depths(:,1)<0;
% depths=depths(index,:);
% O3=O3(index,:);
% time=time(index,:);
% NO=NO(index,:);
% NO2=NO2(index,:);

%we use the top NO,NO2, and ozone measuremnts to drive the model,so im
%setting the height of the top layer reference to the heights measured
O3_int=interp2(time,depths,O3,time_obs,depths_obs,'linear');
NO_int=interp2(time,depths,NO,time_obs,depths_obs,'linear');
NO2_int=interp2(time,depths,NO2,time_obs,depths_obs,'linear');
rad_int=interp2(time,depths,rad,time_obs,depths_obs,'linear');
grad_int=interp2(time,depths,grad,time_obs,depths_obs,'linear');
usnow_int=interp2(time,depths,usnow,time_obs,depths_obs,'linear');
Ts_int=interp2(time,depths,Ts,time_obs,depths_obs,'linear');
% 
% O3_obs_int=interp2(time_obs,depths_obs,O3_obs,time,depths,'linear');
% NO_obs_int=interp2(time_obs,depths_obs,NO_obs,time,depths,'linear');
% NO2_obs_int=interp2(time_obs,depths_obs,NO2_obs,time,depths,'linear');
F=TriScatteredInterp(time_obs(:),depths_obs(:),NO_obs(:));
NO_obs_int=F(time,depths);
F=TriScatteredInterp(time_obs(:),depths_obs(:),NO2_obs(:));
NO2_obs_int=F(time,depths);
F=TriScatteredInterp(time_obs(:),depths_obs(:),O3_obs(:));
O3_obs_int=F(time,depths);



% O3_int(1,:)=O3_obs(1,:);
% NO_int(1,:)=NO_obs(1,:);
% NO2_int(1,:)=NO2_obs(1,:);

date_tick=linspace(time_obs(1,1),time_obs(1,end),5);







set(0,'defaultaxeslinestyleorder',{'*','+','o','sq'}) %or whatever you want
% %NO diff plots
% 
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% for i=1:size(usnow,1)
%    plot(usnow(i,:),[NO_obs_int(i,:)-NO(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Vertical Wind [m/s]')
% ylabel('Observed-Model NO [ppt_v]')
% title('Model NO versus vertical windprofile')
% legend(list,'Location','EastOutside')
% saveas(gcf,[path 'plot_diff_NO_usnow.jpeg'])
% 
% 
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% for i=1:size(rad,1)
%    plot(rad(i,:),[NO_obs_int(i,:)-NO(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Net Radiance [W/m^2]')
% ylabel('Observed-Model NO [ppt_v]')
% title('Model NO versus irradiance')
% legend(list,'Location','EastOutside')
% saveas(gcf,[path 'plot_diff_NO_rad.jpeg'])
% 
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% for i=1:size(Ts,1)
%    plot(Ts(i,:),[NO_obs_int(i,:)-NO(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Surface Temperature [K]')
% ylabel('Observed-Model NO [ppt_v]')
% title('Model NO versus surface temperature')
% legend(list,'Location','EastOutside')
% saveas(gcf,[path 'plot_diff_NO_Ts.jpeg'])
% 
% %NO2 diff plots
% 
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% for i=1:size(usnow,1)
%    plot(usnow(i,:),[NO2_obs_int(i,:)-NO2(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Vertical Wind [m/s]')
% ylabel('Observed-Model NO2 [ppt_v]')
% title('Model NO2 versus vertical windprofile')
% legend(list,'Location','EastOutside')
% saveas(gcf,[path 'plot_diff_NO2_usnow.jpeg'])
% 
% 
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% for i=1:size(rad,1)
%    plot(rad(i,:),[NO2_obs_int(i,:)-NO2(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Net Radiance [W/m^2]')
% ylabel('Observed-Model NO2 [ppt_v]')
% title('Model NO2 versus irradiance')
% legend(list,'Location','EastOutside')
% saveas(gcf,[path 'plot_diff_NO2_rad.jpeg'])
% 
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% for i=1:size(Ts,1)
%    plot(Ts(i,:),[NO2_obs_int(i,:)-NO2(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Surface Temperature [K]')
% ylabel('Observed-Model NO2 [ppt_v]')
% title('Model NO2 versus surface temperature')
% legend(list,'Location','EastOutside')
% saveas(gcf,[path 'plot_diff_NO2_Ts.jpeg'])
% 
% 
% 
% 
% %O3 diff plots
% 
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% for i=1:size(usnow,1)
%    plot(usnow(i,:),[O3_obs_int(i,:)-O3(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Vertical Wind [m/s]')
% ylabel('Observed-Model O3 [ppb_v]')
% title('Model O3 versus vertical windprofile')
% legend(list,'Location','EastOutside')
% saveas(gcf,[path 'plot_diff_O3_usnow.jpeg'])
% 
% 
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% for i=1:size(rad,1)
%    plot(rad(i,:),[O3_obs_int(i,:)-O3(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Net Radiance [W/m^2]')
% ylabel('Observed-Model O3 [ppb_v]')
% title('Model O3 versus irradiance')
% legend(list,'Location','EastOutside')
% saveas(gcf,[path 'plot_diff_O3_rad.jpeg'])
% 
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% for i=1:size(Ts,1)
%    plot(Ts(i,:),[O3_obs_int(i,:)-O3(i,:)])
%    hold all
%    list{i}=['Layer ' num2str(i)];
% end
% xlabel('Surface Temperature [K]')
% ylabel('Observed-Model O3 [ppb_v]')
% title('Model O3 versus surface temperature')
% legend(list,'Location','EastOutside')
% saveas(gcf,[path 'plot_diff_O3_Ts.jpeg'])
% 
% 







spin_up=3;  %days
X=[time_obs(end,1),time_obs(end,1),time_obs(end,1)+spin_up,time_obs(end,1)+spin_up];
Y=[depths_obs(end,1),0,0,depths_obs(end,1)];
C=zeros(size(Y));

% %Ozone Section
% 
figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1)
contourf(time_obs,depths_obs,O3_obs,clim_O3,'Linestyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-2 0])
title('Interpolated 30-minute measurements of O_3 [ppb_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
P=get(gca,'Position');

subplot(2,1,2)
contourf(time,depths,O3,clim_O3,'Linestyle','none')
colorbar
hold all

patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
%rectangle('Position',[time_obs(end,1),depths_obs(end,1),spin_up,abs(depths_obs(end,1))],'FaceColor','k')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-2 0])
title('Modeled O_3 [ppb_v] ')

% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date')
% ylabel('Irradiance [W/m^2]')
% title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)


saveas(gcf,[path 'model_vs_obs_O3.jpeg'])


% %NO2
% 
figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
subplot(3,1,1)
contourf(time_obs,depths_obs,NO2_obs,clim_NO2,'Linestyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-2 0])
title('Interpolated 30-minute measurements of NO_2 [ppt_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
P=get(gca,'Position');

subplot(3,1,2)
contourf(time,depths,NO2,clim_NO2,'Linestyle','none')
colorbar
hold all
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-2 0])
title('Modeled NO_2 [ppt_v] ')

subplot(3,1,3)
plot(time(1,:),rad(1,:))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Irradiance [W/m^2]')
title('Irradiance ')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)

saveas(gcf,[path 'model_vs_obs_NO2.jpeg'])


% %NO
% 
figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1)
contourf(time_obs,depths_obs,NO_obs,clim_NO,'Linestyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-2 0])
title('Interpolated 30-minute measurements of NO [ppt_v]')
hold all
plot(time_obs,depths_obs,'.','Color','k','MarkerSize',0.5)
P=get(gca,'Position');

subplot(2,1,2)
contourf(time,depths,NO,clim_NO,'Linestyle','none')
colorbar
hold all
patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-2 0])
title('Modeled NO [ppt_v] ')

% subplot(3,1,3)
% plot(time(1,:),rad(1,:))
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date')
% ylabel('Irradiance [W/m^2]')
% title('Irradiance ')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)

saveas(gcf,[path 'model_vs_obs_NO.jpeg'])


%Modeled snow density

figure('Visible','off','units','normalized','outerposition',[0 0 1 1])

contourf(time,depths,DS,'Linestyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-2 0])
title('Modeled snow density [g/cm^3]')
saveas(gcf,[path 'Snow_Density.jpeg'])

%Modeled vs obs wind 

figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
plot(time(1,:),wind(1,:))
hold all
plot(time_obs(1,:),wind_obs(1,:))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Wind Speed [m/s]')
title('Modeled vs Observed wind speed [m/s]')
   legend('Model','Observed','Location','EastOutside')
saveas(gcf,[path 'wind.jpeg'])

% 
% 
% 
%  
% figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% subplot(4,1,1)
% contourf(time_obs,depths_obs,O3_obs,clim_O3,'Linestyle','none')
% colorbar
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date')
% ylabel('Depth [m]')
% ylim([-2 0])
% title('Interpolated 30-minute measurements of O_3 [ppb_v]')
% 
% 
% subplot(4,1,2)
% contourf(time,depths,O3,clim_O3,'Linestyle','none')
% colorbar
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date')
% ylabel('Depth [m]')
% ylim([-2 0])
% title('Modeled O_3 [ppb_v] ')
% 
% subplot(4,1,3)
% plot(time(1,:),rad(1,:),'-o')
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date')
% ylim([0 500])
% ylabel('Irradiance [W/(m^2)]')
% title('Modeled Irradiance')
% 
% subplot(4,1,4)
% plot(time_obs(1,:),rad_obs(1,:),'-o')
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date')
% ylim([0 500])
% ylabel('Irradiance [W/(m^2)]')
% title('Observed Irradiance')
% 
% saveas(gcf,[path 'model_vs_obs_O3_rad.jpeg'])
% 
% 
% % figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% % 
% % subplot(2,1,1)
% % contourf(time_obs,depths_obs,O3_obs-O3_int,'Linestyle','none')
% % colorbar
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date')
% % ylabel('Depth [m]')
% % title('Observed - Modeled O_3 [ppb_v]')
% % P=get(gca,'Position');
% % 
% % subplot(2,1,2)
% % plot(time_obs(1,:),u_ws_obs(1,:),'-o')
% % hold all
% % plot(time_obs(1,:),v_ws_obs(1,:),'-sq');plot(time_obs(1,:),zeros(size(time_obs(1,:))),'-','Color','k');
% % 
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date')
% % ylabel('Wind Speed [m/s]')
% % legend('North-South','East-West','No Wind','Location','Best')
% % P2=get(gca,'Position');
% % P2(3)=P(3);
% % set(gca,'Position',P2)
% % saveas(gcf,[path 'diff_O3_wind.jpeg'])
% % 
% % 
% % 
% % figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% % subplot(2,1,1)
% % contourf(time_obs,depths_obs,O3_obs-O3_int,'Linestyle','none')
% % colorbar
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date')
% % ylabel('Depth [m]')
% % title('Observed - Modeled O_3 [ppb_v]')
% % P=get(gca,'Position');
% % 
% % subplot(2,1,2)
% % plot(time_obs(1,:),rad_obs(1,:),'-o')
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date')
% % ylabel('Irradiance [W/(m^2)]')
% % P2=get(gca,'Position');
% % P2(3)=P(3);
% % set(gca,'Position',P2)
% % saveas(gcf,[path 'diff_O3_rad.jpeg'])
% % 
% % 
% % figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
% % subplot(2,1,1)
% % contourf(time_obs,depths_obs,O3_obs-O3_int,'Linestyle','none')
% % colorbar
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date')
% % ylabel('Depth [m]')
% % title('Observed - Modeled O_3 [ppb_v]')
% % P=get(gca,'Position');
% % 
% % subplot(2,1,2)
% % contourf(time_obs,depths_obs,snow_temp_obs,'Linestyle','none')
% % colorbar
% % set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% % set(gca,'FontSize',15);xlabel('Date')
% % ylabel('Depth [m]')
% % title('Temperature  [C]')
% % P2=get(gca,'Position');
% % P2(3)=P(3);
% % set(gca,'Position',P2)
% % 
% % saveas(gcf,[path 'diff_O3_temp.jpeg'])
% 
% close all

figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
set(0,'defaultaxeslinestyleorder',{'*','+','o','sq'}) %or whatever you want
for i=1:size(depths_obs,1)
   plot(time_obs(i,:),depths_obs(i,:))
   hold all 
end

set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
%xlabel('Date')
ylabel('Depth [m]')

title('Height of measurement inlets')
saveas(gcf,[path 'measurement_heights.jpeg'])

% %NO2
% 
figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
subplot(4,1,1)
hold all
NO2_d=(NO2_obs-NO2_int)./NO2_obs*100;
NO2_d=[NO2_d(3,:);NO2_d(end-2,:)];
NO2_d_time=[time_obs(3,:);time_obs(end-2,:)];
plot(NO2_d_time(1,:),NO2_d(1,:),'MarkerSize',2)
hold all
plot(NO2_d_time(2,:),NO2_d(2,:),'MarkerSize',2)
Y=[-100,100,100,-100];


set(gca,'XTick',date_tick);

datetick('x',2,'keepticks')

patch(X,Y,C,'FaceColor','k','FaceAlpha',0.5)
%xlabel('Date')
ylabel('Error [%]')
ylim([-100 100])
title({'Model error of NO_2 ';'Negative values are model overestimations';' Blue is near the surface, Green is around 2m deep'})
%legend(['Depth = ' num2str(depths_obs(3,1)) ' m'],['Depth = ' num2str(depths_obs(end-2,1)) ' m'],'Location','NorthWest')
P=get(gca,'Position');
P2=P;
P2(4)=P2(4)*1.25;
set(gca,'Position',P2)



subplot(4,1,2)
plot(time(1,:),rad(1,:))
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
%xlabel('Date')
ylabel('Irradiance [W/m^2]')
title('Irradiance ')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)

subplot(4,1,3)
plot(time(1,:),wind(1,:))

set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
%xlabel('Date')
ylabel('Wind Speed [m/s]')
title('Wind speed [m/s]')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)

   

subplot(4,1,4)
contourf(time_obs,depths_obs,snow_temp_obs,'Linestyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([-2 0])
title('Temperature [C]')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)




saveas(gcf,[path 'diff_NO2.jpeg'])

figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
plot(time(1,:),flux_O3(1,:),'-o')

set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Flux [molecules/ m^2 s]')
title({'Fluxes of ozone  into/out of snowpack','Negative values are fluxes into snowpack'})

saveas(gcf,[path 'ozone_fluxe.jpeg'])



flux=flux_NO(1,:)+flux_NO2(1,:);


flux_mean=mean(flux);
flux_std=std(flux);
flux(flux<=(flux_mean-3*flux_std) &flux>=(flux_mean+3*flux_std))=NaN;

time_floor=floor(time(1,:));
min_time=min(time_floor);
max_time=max(time_floor)+1;

flux_24_average=[];
time_24_hour=min_time:max_time;
for i=time_24_hour
    index=i==time_floor;
    flux_24_average(end+1)=mean(flux(index & ~isnan(flux)));
end

figure('Visible','off','units','normalized','outerposition',[0 0 1 1])
subplot(2,1,1)
plot(time(1,:),flux,'-o')

% flux_mean=mean(flux);
% flux_std=std(flux);
% ylim([flux_mean-flux_std  flux_mean+flux_std])
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
%xlabel('Date')
ylabel('Flux [molecules/ m^2 s]')
title({'Fluxes of NOx into/out of snowpack','Negative values are fluxes into snowpack'})

subplot(2,1,2)
plot(time_24_hour,flux_24_average,'-o','LineWidth',2)
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Flux [molecules/ m^2 s]')
title({'24-hour average fluxes of NOx into/out of snowpack'})

saveas(gcf,[path 'NOx_fluxe.jpeg'])

