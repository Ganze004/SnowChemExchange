clc
clear all
close all

c_no3=4.6*10^-6; % M
V_snow=1; %volume of system
rho_snow=0.3; %g/cm^3
rho_ice=0.917; %g/cm^3
rho_water=0.9987; %g/cm^3
r=0.0003962992248388017; %snowgrain radius in m
ssa=-308.2*log(rho_snow)-205.96;
por=1-rho_snow/rho_ice;



c_no3=c_no3*1000*(100^-3)*6.022*10^23;  %molecules per cm^3 water

M=c_no3*rho_snow/(rho_water*por)

% M=c_no3*(rho_ice/rho_water)*(r/3)/(100^2)  %molecules per cm^2 ice? 
% 
% M=M*ssa*rho_snow/por   %molecules per cm^3 air
dr=10*10^-9;

1-((r-dr)^3)/(r^3)