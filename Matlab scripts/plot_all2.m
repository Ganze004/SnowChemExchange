% clc
% clear all
% close all
%
% % file='noxiv_final_conc_sscnt_reprocessed.dat'
% % dates=[datenum(6722,6,25) datenum(6722,7,10)]
% % plot_conc(file,dates)%
%
% %2009
% % 4/11-4/25
% % 4/27-5/6
% % 5/11-5/18
% % 5/28-6/11
% % 6/25-7/12
%
%
%
% %2010
% %4/13-4/27
% %4/28-5/10
% %5/10-5/18
%
% Y=[
%     2009
%     2009
%     2009
%     2009
%     2009
%     2010
%     2010
%     2010];
%
% M_min=[
%     4
%     4
%     5
%     5
%     6
%     4
%     4
%     5];
%
% M_max=[
%     4
%     5
%     5
%     6
%     7
%     4
%     5
%     5];
% D_min=[
%     11
%     27
%     11
%     28
%     25
%     13
%     26
%     10];
% D_max=[
%     25
%     6
%     18
%     11
%     12
%     27
%     10
%     18];
%
%
%
% %
% % %Gradients with wind/irradiance 1 plot per month, 2009-2010
% % %March-Sept
% %
% %
% % %[NO_time NO NO_sd NO2_time NO2 NO2_sd] =get_snow_level_data(files);
% %
% kk=1
% date_min=datenum(Y,M_min,D_min);
% date_max=datenum(Y,M_max,D_max);
function plot_all2(date_min,date_max,kk,Y,M_min,M_max,D_min,D_max)
load noaa.mat
load cu_rad.mat
load summit_snow_data.mat
load depths_data.mat
load summit_ozone.mat
load summit_temperature.mat

master_ozone=ozone_NO;
master_NO_time=NO_time;
master_NO2_time=NO2_time;
master_NO=NO;
master_NO2=NO2;
master_NO_sd=NO_sd;
master_NO2_sd=NO2_sd;
master_temp_jd=temp_jd;
master_temperature=temperature;

scrsz = get(0,'ScreenSize');

%for kk=7%:length(date_min)

days=[date_min(kk) date_max(kk)];

for jjj=1:length(days)-1
    
    load noaa.mat
    load cu_rad.mat
    %load summit_snow_data.mat
    load depths_data.mat
    
    index=find(noaa_jd>days(jjj) & noaa_jd<days(jjj+1));
    
    wind_jd=noaa_jd(index);
    wind_speed=noaa_ws(index);
    wind_dir=noaa_wd(index);
    
    
    
    
    wind_speed(wind_speed==9999)=NaN;
    
    index=find(rad_jd>days(jjj) & rad_jd<days(jjj+1));
    irr_down=rad_down(index);
    irr_up=rad_up(index);
    irr_jd=rad_jd(index);
    irr_down(irr_down>2000)=NaN;
    
    
    
    index=find(master_temp_jd>days(jjj) & master_temp_jd<days(jjj+1));
    
    temp_jd=horz_arrays(master_temp_jd(index));
    temperature=master_temperature(:,index);
    
    for i=2:size(master_temperature,1)
        temp_jd(i,:)=temp_jd(1,:);
        
    end
    
    
    
    
    
    index=find(depths_jd>days(jjj) & depths_jd<days(jjj+1));
    
    if isempty(index)
        index(1,1)=find(depths_jd<date_min(kk),1,'last');
        index(2,1)=find(depths_jd>date_max(kk),1,'first');
        
    end
    
    index=[index(1)-1 ;index ; index(end)+1];
    
    depths=depths(index,:);
    depths_jd=depths_jd(index,:);
    %
    
    
    
    
    index=cell(numel(files),1);
    for i=1:numel(files)
        index{i}=find(master_NO_time(i,:)>days(jjj) & master_NO_time(i,:)<days(jjj+1));
    end
    
    
    
    index2=cell(numel(files),1);
    for i=1:numel(files)
        index2{i}=find(master_temp_jd>days(jjj) & master_temp_jd<days(jjj+1));
    end
    %         NO_time_2(i,1:numel(master_NO_time(i,index)))=master_NO_time(i,index);
    %         NO2_time_2(i,1:numel(master_NO2_time(i,index)))=master_NO2_time(i,index);
    %         NO_2(i,1:numel(master_NO(i,index)))=master_NO(i,index);
    %         NO2_2(i,1:numel(master_NO2(i,index)))=master_NO2(i,index);
    %         NO_sd_2(i,1:numel(master_NO_sd(i,index)))=master_NO_sd(i,index);
    %         NO2_sd_2(i,1:numel(master_NO2_sd(i,index)))=master_NO2_sd(i,index);
    %         ozone_NO_2(i,1:numel(master_NO2_sd(i,index)))=master_ozone(i,index);
    %     end
    %
    %
    %     NO_time=NO_time_2;
    %     NO2_time=NO2_time_2;
    %     NO=NO_2;
    %     NO2=NO2_2;
    %     NO_sd=NO_sd_2;
    %     NO2_sd=NO2_sd_2;
    %     ozone_NO=ozone_NO_2;
    
    
    
    
    [NO_time NO ozone_NO]=index_matrix(index,master_NO_time,master_NO,master_ozone);
    [NO2_time NO2]=index_matrix(index,master_NO2_time,master_NO2);
    depths_grid=NaN(size(NO_time));
    depths=depths';
    depths_temp_grid=NaN(size(temp_jd));
    
    
    
    NO_time(NO_time==0)=NaN;
    NO2_time(NO2_time==0)=NaN;
    NO(NO==0)=NaN;
    NO2(NO2==0)=NaN;
    ozone_NO(ozone_NO==0)=NaN;
    
        
    NO(NO<0)=1;
NO2(NO2<0)=1;
ozone_NO(ozone_NO<0)=1;
    
    %     NO(isnan(NO))=1;
    %     NO2(isnan(NO2))=1;
    %     ozone_NO(isnan(ozone_NO))=1;
    %
    
    [NO_time NO NO2_time NO2 depths_grid ozone_NO depths temp_jd temperature depths_temp_grid]=snow_time_interp(NO_time, NO, NO2_time, NO2,depths_grid,ozone_NO,depths,temp_jd,temperature,depths_temp_grid);

    
    for jj=1:size(depths,1)
        
        
        yi=find_nearest(depths_jd,depths(jj,:),NO_time(jj,:));
        
        
        depths_grid(jj,:)=yi;
        
    end
   % keyboard
    for jj=1:size(depths,1)
        
        
        yi=find_nearest(depths_jd,depths(jj,:),temp_jd(jj,:));
        
        
        depths_temp_grid(jj,:)=yi;
        
    end
    
    
    
    [x y]=meshgrid(NO_time(1,:),1:numel(files));
    
    n=100;
    top_inlet_num=2;
    bot_inlet_num=6;
    clim_NOx=linspace(0,1000,100);%linspace(0,600,100);
    clim_NO2=clim_NOx;
    clim_NO=linspace(0,200,100);
    clim_O3=linspace(0,60,100);
    clim_temp=linspace(-70,0,100);
    lim_depths=[min(depths_grid(:)) 0];%max(depths_grid(:))
    tick_depths=linspace(lim_depths(1),lim_depths(2),5);
    tick_dates=linspace(days(jjj),days(jjj+1),5);
    lim_ws=[0 25];
    tick_ws=lim_ws(1):5:lim_ws(2);
    lim_irr=[0 1000];
    tick_irr=lim_irr(1):200:lim_irr(2);
    
    
  
    
    
    NOx=NO+NO2;
  
    % NOx(isnan(NOx))=0;
    num_interp=3;
    XI=NO_time;%interp2(NO_time,num_interp);
    YI=depths_grid;%interp2(depths_grid,num_interp);
    NOx_I=NOx;%interp2(NOx,num_interp);
    
    
    
    path=['Contour_' num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '\'];
    mkdir(path)
    
    figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
    subplot(2,1,1)
    
    
    contourf(NO_time, depths_grid,NOx,clim_NOx,'LineStyle','none')%NO_time, y,
    %contourf(NO_time, y,interp2(NOx,4),n,'LineStyle','none')%NO_time, y,
    
    colorbar
    set(gca,'FontSize',15)
    set(gca,'XTick',tick_dates)
    datetick('x','mm/dd/yy','keepticks')
    set(gca,'XLim',[days(jjj) days(jjj+1)])
    set(gca,'YLim',lim_depths)
    set(gca,'YTick',tick_depths)
    
    xlabel('Dates')
    ylabel('Depth [m]')
    title('NO_x [ppt_v]')
    P=get(gca,'Position');
    
    subplot(2,1,2)
    
    [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
    
    
    set(AX(1),'XTick',tick_dates)
    set(AX(2),'XTick',tick_dates)
    set(AX(1),'FontSize',15)
    set(AX(2),'FontSize',15)
    xlabel('Date')
    set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
    set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
    ylim(AX(1),lim_irr)
    ylim(AX(2),lim_ws)
    set(AX(1),'YTick',tick_irr)
    set(AX(2),'YTick',tick_ws)
    title('Surface Irradiance and Wind Speed Measurements')
    datetick(AX(1),'x','mm/dd/yy','keepticks')
    datetick(AX(2),'x','mm/dd/yy','keepticks')
    set(AX(1),'XLim',[days(jjj) days(jjj+1)])
    set(AX(2),'XLim',[days(jjj) days(jjj+1)])
    set(h1,'Linestyle','o')
    legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
    P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
    file=[path 'NOx_depth_Wind_Irradiance_'    num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '_' num2str(jjj) '.jpeg']
    saveas(gcf,file); close all; close all
    
    
    
    figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
    subplot(2,1,1)
    contourf(NO_time, depths_grid,NO,clim_NO,'LineStyle','none')%NO_time, y,
    %contourf(NO_time, y,interp2(NOx,4),n,'LineStyle','none')%NO_time, y,
    
    colorbar
    set(gca,'FontSize',15)
    set(gca,'XTick',tick_dates)
    datetick('x','mm/dd/yy','keepticks')
    set(gca,'XLim',[days(jjj) days(jjj+1)])
    set(gca,'YLim',lim_depths)
    set(gca,'YTick',tick_depths)
    xlabel('Dates')
    ylabel('Depth [m]')
    title('NO [ppt_v]')
    P=get(gca,'Position');
    subplot(2,1,2)
    
    [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
    
    
  
    set(AX(1),'FontSize',15)
    set(AX(2),'FontSize',15)
      set(AX(1),'XTick',tick_dates)
        set(AX(2),'XTick',tick_dates)
    xlabel('Date')
    set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
    set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
    ylim(AX(1),lim_irr)
    ylim(AX(2),lim_ws)
    set(AX(1),'YTick',tick_irr)
    set(AX(2),'YTick',tick_ws)
    title('Surface Irradiance and Wind Speed Measurements')
    datetick(AX(1),'x','mm/dd/yy','keepticks')
    datetick(AX(2),'x','mm/dd/yy','keepticks')
    set(AX(1),'XLim',[days(jjj) days(jjj+1)])
    set(AX(2),'XLim',[days(jjj) days(jjj+1)])
    set(h1,'Linestyle','o')
    legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
    P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
    file=[path 'NO_depth_Wind_Irradiance_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '_' num2str(jjj) '.jpeg']
    saveas(gcf,file); close all
    
    figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
    subplot(2,1,1)
    contourf(NO2_time, depths_grid,NO2,clim_NO2,'LineStyle','none')%NO_time, y,
    %contourf(NO_time, y,interp2(NOx,4),n,'LineStyle','none')%NO_time, y,
    
    colorbar
    set(gca,'FontSize',15)
      set(gca,'XTick',tick_dates)
    datetick('x','mm/dd/yy','keepticks')
    set(gca,'XLim',[days(jjj) days(jjj+1)])
    set(gca,'YLim',lim_depths)
    set(gca,'YTick',tick_depths)
    xlabel('Dates')
    ylabel('Depth [m]')
    title('NO_2 [ppt_v]')
    P=get(gca,'Position');
    subplot(2,1,2)
    
    [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
    
   
    set(AX(1),'FontSize',15)
    set(AX(2),'FontSize',15)
      set(AX(1),'XTick',tick_dates)
        set(AX(2),'XTick',tick_dates)
     xlabel('Date')
    set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
    set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
    ylim(AX(1),lim_irr)
    ylim(AX(2),lim_ws)
    set(AX(1),'YTick',tick_irr)
    set(AX(2),'YTick',tick_ws)
    title('Surface Irradiance and Wind Speed Measurements')
    datetick(AX(1),'x','mm/dd/yy','keepticks')
    datetick(AX(2),'x','mm/dd/yy','keepticks')
    set(AX(1),'XLim',[days(jjj) days(jjj+1)])
    set(AX(2),'XLim',[days(jjj) days(jjj+1)])
    set(h1,'Linestyle','o')
    legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
    P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)
    file=[path 'NO2_depth_Wind_Irradiance_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '_' num2str(jjj) '.jpeg']
    saveas(gcf,file); close all
    
    % keyboard
    figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
    subplot(2,1,1)
    contourf(NO_time, depths_grid,ozone_NO,clim_O3,'LineStyle','none')%NO_time, y,
    %contourf(NO_time, y,interp2(NOx,4),n,'LineStyle','none')%NO_time, y,
    
    colorbar
    set(gca,'FontSize',15)
      set(gca,'XTick',tick_dates)
    datetick('x','mm/dd/yy','keepticks')
    set(gca,'XLim',[days(jjj) days(jjj+1)])
    set(gca,'YLim',lim_depths)
    set(gca,'YTick',tick_depths)
    xlabel('Dates')
    ylabel('Depth [m]')
    title('O_3 [ppb_v]')
    P=get(gca,'Position');
    subplot(2,1,2)
    
    [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
    
    
    set(AX(1),'FontSize',15)
    set(AX(2),'FontSize',15)
      set(AX(1),'XTick',tick_dates)
        set(AX(2),'XTick',tick_dates)
    xlabel('Date')
    set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
    set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
    ylim(AX(1),lim_irr)
    ylim(AX(2),lim_ws)
    set(AX(1),'YTick',tick_irr)
    set(AX(2),'YTick',tick_ws)
    title('Surface Irradiance and Wind Speed Measurements')
    datetick(AX(1),'x','mm/dd/yy','keepticks')
    datetick(AX(2),'x','mm/dd/yy','keepticks')
    set(AX(1),'XLim',[days(jjj) days(jjj+1)])
    set(AX(2),'XLim',[days(jjj) days(jjj+1)])
    set(h1,'Linestyle','o')
    legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
    P2=get(gca,'Position');
    P2(3)=P(3);
    set(gca,'Position',P2)
    file=[path 'O3_depth_Wind_Irradiance_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '_' num2str(jjj) '.jpeg']
    saveas(gcf,file); close all
    
   
    
    figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
    subplot(2,1,1)
    contourf(temp_jd, depths_temp_grid,temperature,clim_temp,'LineStyle','none')%NO_time, y,
    %contourf(NO_time, y,interp2(NOx,4),n,'LineStyle','none')%NO_time, y,
    
    colorbar
    set(gca,'FontSize',15)
      set(gca,'XTick',tick_dates)
    caxis([min(clim_temp) max(clim_temp)])
    datetick('x','mm/dd/yy','keepticks')
    set(gca,'XLim',[days(jjj) days(jjj+1)])
    set(gca,'YLim',lim_depths)
    set(gca,'YTick',tick_depths)
    xlabel('Dates')
    ylabel('Depth [m]')
    title('Temperature [C]')
    
    subplot(2,1,2)
    
    [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
    
    
    set(AX(1),'FontSize',15)
    set(AX(2),'FontSize',15)
      set(AX(1),'XTick',tick_dates)
        set(AX(2),'XTick',tick_dates)
    xlabel('Date')
    set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
    set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
    ylim(AX(1),lim_irr)
    ylim(AX(2),lim_ws)
    set(AX(1),'YTick',tick_irr)
    set(AX(2),'YTick',tick_ws)
    title('Surface Irradiance and Wind Speed Measurements')
    datetick(AX(1),'x','mm/dd/yy','keepticks')
    datetick(AX(2),'x','mm/dd/yy','keepticks')
    set(AX(1),'XLim',[days(jjj) days(jjj+1)])
    set(AX(2),'XLim',[days(jjj) days(jjj+1)])
    set(h1,'Linestyle','o')
    legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
    
    file=[path 'Temp_depth_Wind_Irradiance_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '_' num2str(jjj) '.jpeg']
    saveas(gcf,file); close all
    
    
     
    
%     figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
%     subplot(2,1,1)
%     efold_depth=0.1;%m
%     efold=exp(depths_grid./efold_depth);
%     efold(efold>1)=1;
%     NO_I=10.*NO2./ozone_NO.*efold;
%     contourf(NO_time, depths_grid,NO_I,clim_NO,'LineStyle','none')%NO_time, y,
%     %contourf(NO_time, y,interp2(NOx,4),n,'LineStyle','none')%NO_time, y,
%     
%     colorbar
%     datetick('x','mm/dd/yy','keepticks')
%     set(gca,'XLim',[days(jjj) days(jjj+1)])
%     set(gca,'YLim',lim_depths)
%     set(gca,'YTick',tick_depths)
%     xlabel('Dates')
%     ylabel('Depth [m]')
%     title('NO interp [ppt_v]')
%     
%     subplot(2,1,2)
%     
%     [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
%     
%     xlabel('Date')
%     set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
%     set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
%     ylim(AX(1),lim_irr)
%     ylim(AX(2),lim_ws)
%     set(AX(1),'YTick',tick_irr)
%     set(AX(2),'YTick',tick_ws)
%     title('Surface Irradiance and Wind Speed Measurements')
%     datetick(AX(1),'x','mm/dd/yy','keepticks')
%     datetick(AX(2),'x','mm/dd/yy','keepticks')
%     set(AX(1),'XLim',[days(jjj) days(jjj+1)])
%     set(AX(2),'XLim',[days(jjj) days(jjj+1)])
%     set(h1,'Linestyle','o')
%     legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
%     
%     file=[path 'NO_I_depth_Wind_Irradiance_'  num2str(M_min(kk)) '_' num2str(D_min(kk)) '_' num2str(Y(kk)) '_' num2str(M_max(kk)) '_' num2str(D_max(kk)) '_' num2str(Y(kk)) '_' num2str(jjj) '.jpeg']
%     saveas(gcf,file); close all
%     
%     
    
    close all
    
    
end
% end
%end
