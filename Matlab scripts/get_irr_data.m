clc
clear all
close all

fid=fopen('cu_rad_hrly.txt','r');


line=fgetl(fid);
rad_jd=[];
rad_down=[];
rad_up=[];
while line~=-1
   [temp1 line]=strtok(line); 
   [temp2 line]=strtok(line); 
    
    rad_jd(end+1)=datenum([temp1 ' ' temp2],'mm/dd/yyyy HH:MM');
    
    [temp1 line]=strtok(line); 
   rad_down(end+1)=str2double(temp1);
   
   [temp1 line]=strtok(line); 
   rad_up(end+1)=str2double(temp1);
    
    line=fgetl(fid);
end
fclose(fid);
save('cu_rad.mat','rad_jd','rad_down','rad_up')
