clc
clear all
close all
load noaa.mat
index=find(noaa_jd>datenum(2009,3,16) & noaa_jd<datenum(2009,8,13));

noaa_ws(noaa_ws==9999)=NaN;
plot(noaa_jd(index),noaa_ws(index))
datetick('x','mm/dd')