clc
clear all
close all

path='model_obs_com/';
mkdir(path)
file='MLSN_CHEM/output/jval.out';
data=load(file);

year=2009
month=4
day=11
hour=0
m=0
timdiff=2; %time difference in hours

load MLSN_species.mat

IDT_O3=85+2;
IDT_NO=87+2;
IDT_NO2=95+2;
IDT_RAD=size(MLSN_names,1)+3;
IDT_CS_RAD=size(MLSN_names,1)+4;
index=find(data(:,2)==data(1,2),2,'first');
num_depth=index(2)-index(1);

time=data(:,1);
j_time=datenum(year,month,day,hour,m,time);
num_time=length(time)/num_depth;
j_time=reshape(j_time,num_depth,num_time);
j_depths=reshape(data(:,2),num_depth,num_time);
j_data=data(:,3:end);
rad=reshape(data(:,IDT_RAD),num_depth,num_time);
CS_rad=reshape(data(:,IDT_CS_RAD),num_depth,num_time);
date_tick=linspace(j_time(1,1),j_time(1,end),5);


%Observations
file2='contour_plots_2009_4_11_2009_4_25/obs.out';

data=load(file2);
index=find(data(:,2)==data(1,2),2,'first');
num_depth_obs=index(2)-index(1);
time_diff=0%datenum(0,0,0,4,0,0); %2hour time difference
time_obs=data(:,1);
time_obs=datenum(year,month,day,hour,m,time_obs)-datenum(0,0,0,2,0,0);
num_time_obs=length(time_obs)/num_depth_obs;
IDT_NO_obs=3;
IDT_NO2_obs=4;
IDT_O3_obs=5;
time_obs=reshape(time_obs,num_depth_obs,num_time_obs);
depths_obs=reshape(data(:,2),num_depth_obs,num_time_obs);
NO_obs=reshape(data(:,IDT_NO_obs),num_depth_obs,num_time_obs);
NO2_obs=reshape(data(:,IDT_NO2_obs),num_depth_obs,num_time_obs);
O3_obs=reshape(data(:,IDT_O3_obs),num_depth_obs,num_time_obs);
snow_temp_obs=reshape(data(:,6),num_depth_obs,num_time_obs);
u_ws_obs=reshape(data(:,7),num_depth_obs,num_time_obs);
v_ws_obs=reshape(data(:,8),num_depth_obs,num_time_obs);
rad_obs=reshape(data(:,9),num_depth_obs,num_time_obs);


index=~isnan(depths_obs(:,1));
depths_obs=depths_obs(index,:);
O3_obs=O3_obs(index,:);
time_obs=time_obs(index,:);
snow_temp_obs=snow_temp_obs(index,:);
NO_obs=NO_obs(index,:);
NO2_obs=NO2_obs(index,:);

for i=1:size(j_data,2)

    j_now=reshape(j_data(:,i),num_depth,num_time);
    if max(j_now(:))<10^-15
        continue
    end
    
    index=[MLSN_names{:,2}]==i;
figure('Visible','off') 
subplot(2,1,1)
contourf(j_time,j_depths,j_now,'LineStyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([min(j_depths(:)), 0.5])
title(['Modeled photolysis rate of ' MLSN_names{index,1} ' [s^-1]'])
P=get(gca,'Position');

subplot(2,1,2)
plot(j_time(1,:)-time_diff,rad(1,:),'-o')
hold all
plot(j_time(1,:),CS_rad(1,:),'-sq')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Irradiance [W/(m^2)]')
legend('Modeled','Clear Sky','Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)

saveas(gcf,[path 'j' MLSN_names{index,1} '_rad.jpeg'])
close all
end


