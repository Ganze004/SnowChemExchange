clc
clear all
close all

file='gas_no_header';
% fid=fopen(file);
% 
% d=fscanf(fid,'%f',3);
% 
num_depth=68;
num_spc=35;
num_steps=(96*7)+1;

data=load(file)';
% 
% data=fscanf(fid,'%f',[num_spc+5,inf]);
% fclose(fid);


depth=data(1,:);
jday=data(2,:);
jtime=data(3,:);
temperature=data(4,:);
sza=data(5,:);
spc=data(6:end,:);

index=depth<0;
depth=depth(index);
jday=jday(index);
jtime=jtime(index);
temperature=temperature(index);
sza=sza(index);
spc=spc(:,index);
num_depth=19;

depth=reshape(depth,num_depth, num_steps);
depth=flipud(depth);


linear_time=(jday-jday(1))*86400; %Keeps the time in order for new days when plotting
jtime=jtime+linear_time;
jtime=reshape(jtime,num_depth, num_steps);
for i=1:num_steps
    M=6;
    D=floor(jtime(:,i)/(24*60*60));
    jtime(:,i)=jtime(:,i)-D*(24*60*60);
    H=floor(jtime(:,i)/(60*60));
    jtime(:,i)=jtime(:,i)-H*60*60;
    minute=floor(jtime(:,i)/60);
    jtime(:,i)=jtime(:,i)-minute*60;
    S=jtime(:,i);
    
    D=D+7;
   jtime(:,i)=datenum(2011,M,D,H,minute,S); 
    
end




spc_labels={'O';
            'O2';
            'O3';
            'OH';
            'HO2';
            'H2O2';
            'H2O';
            'NO';
            'NO2';
            'NO3';
            'HONO';
            'N2O5';
            'HNO3';
            'PAN';
            'HNO4';
            'CO';
            'CH4';
            'NH3';
            'CO2';
            'C2H4';
            'ALD';
            'HCHO';
            'HCOOH';
            'CH3OH';
            'CH3O2';
            'C2H6';
            'CH3O3';
            'C2H5O2';
            'C2H4OHO2';
            'CH3CO3'
            'H2';
            'RONO2';
            'N2';
            'ROOH';
            'CH3COOH';
           };
            
          
            
            
spc_cell=cell(num_spc,1);
for i=1:num_spc
  sp=spc(i,:);
  spc_cell{i}=flipud(reshape(sp,num_depth,num_steps));
 
end

for i=1:num_spc
   x=jtime;
   y=depth;
   z=spc_cell{i};
   
   figure
   contourf(x,y,z,100,'LineColor','none')
   datetick('x',6,'keepticks')
   xlabel('Date')
   ylabel('Depth [m]')
   colorbar
  
   title({'Concentration [ppt_v]';spc_labels{i};})
   mkdir(spc_labels{i})
    saveas(gcf,[spc_labels{i} '/gas_ppt_' spc_labels{i} '.jpeg'])
     saveas(gcf,[ 'gas_ppt_' spc_labels{i} '.jpeg'])
end
