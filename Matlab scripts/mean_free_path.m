function lambda=mean_free_path(T,P)

%P is in atmosphere
d=6.2*10^-10; %radius of diatomic nitrgoen
R=8.205746*10^-5; %m^3 atm / K mol
Na=6.022*10^23; %Avagadros number

lambda=R*T/(sqrt(2)*pi*d^2*Na*P);

end