clc
clear all
close all

file='aq_no_header';
% fid=fopen(file);
% 
% d=fscanf(fid,'%f',3);
% 
num_depth=20;
num_spc=35;
num_steps=(96*7)+1;

data=load(file)';
% 
% data=fscanf(fid,'%f',[num_spc+5,inf]);
% fclose(fid);


depth=data(1,:);
jday=data(2,:);
jtime=data(3,:);
temperature=data(4,:);
spc=data(6:end,:);



depth=reshape(depth,num_depth, num_steps);
depth=flipud(depth);


linear_time=(jday-jday(1))*86400; %Keeps the time in order for new days when plotting
jtime=jtime+linear_time;
jtime=reshape(jtime,num_depth, num_steps);
for i=1:num_steps
    M=6;
    D=floor(jtime(:,i)/(24*60*60));
    jtime(:,i)=jtime(:,i)-D*(24*60*60);
    H=floor(jtime(:,i)/(60*60));
    jtime(:,i)=jtime(:,i)-H*60*60;
    minute=floor(jtime(:,i)/60);
    jtime(:,i)=jtime(:,i)-minute*60;
    S=jtime(:,i);
    
    D=D+7;
   jtime(:,i)=datenum(2011,M,D,H,minute,S); 
    
end




spc_labels={'O';
            'O2';
            'O3';
            'OH';
            'HO2';
            'H2O2';
            'H2O';
            'NO';
            'NO2';
            'NO3';
            'HONO';
            'NO2-';
            'HNO3';
            'NO3-';
            'HNO4';
            'NO4-';
            'NH4';
            'NH3';
            'CO2';
            'HCO3-';
            'CO32-';
            'HCHO';
            'HCOOH';
            'HCOO-';
            'CH3OH';
            'CH3O2';
            'CH3OOH';
            'DOM';
            'H+';
            'OH-';
            'O2-';
            'CO3-';
            'HO2-';
            'CH3COOH';
            'CH3COO-'};
            
z=[0 0 0 0 0 0 0 0 0 0 0 -1 0 -1 0 -1 0 0 0 -1 -2 0 0 -1 0 0 0 0 1 -1 -1 -1 -1 0 -1];            
            
            
I=zeros(num_depth,num_steps);
spc_cell=cell(num_spc,1);
for i=1:num_spc
  sp=spc(i,:);
  spc_cell{i}=flipud(reshape(sp,num_depth,num_steps));
  I=I+0.5.*spc_cell{i}*z(i)^2;
end
I
for i=1:num_spc
   x=jtime;
   y=depth;
   z=spc_cell{i};
   
   figure
   contourf(x,y,-log10(z),0:0.1:14,'LineColor','none')
   datetick('x',6,'keepticks')
   xlabel('Date')
   ylabel('Depth [m]')
   colorbar
  
   title({'Concentration [pM]';spc_labels{i};})
   mkdir(spc_labels{i})
    saveas(gcf,[spc_labels{i} '/aq_M_' spc_labels{i} '.jpeg'])
     saveas(gcf,[ 'aq_M_' spc_labels{i} '.jpeg'])
end
