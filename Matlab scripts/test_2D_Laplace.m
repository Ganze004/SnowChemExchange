clc
clear all
close all


num_nodes=5;
x=linspace(0,4,num_nodes);
z=2*linspace(0,4,num_nodes);
y=0;

dc_dz=linspace(-1,1,num_nodes);

dz=abs(diff(z(1:2)));
dx=abs(diff(x(1:2)));

[x z]=meshgrid(x,z);





n=num_nodes-1;
[ C] = D2_laplace( n,dc_dz,dx,dz);

figure
contourf(x,z,C)
xlabel('X axis - parallel to wind [m]')
ylabel('Z axis - depth in snowpack [m]')
colorbar