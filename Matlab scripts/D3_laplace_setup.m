clc
clear all
close all

plot_debug=0;
debug=0;
%Atmospheric Parameters
T=270;
P=1;
M=14+16*2;
R=8.205746*10^-5; %m^3 atm mol^-1 K^-1
rho_air=P/(R*T);


%Snowpack Parameters
rho_snow=0.3; %g/cm^3
rho_ice=0.9167; %g/cm^3
por=1-rho_snow/rho_ice;
constric=1;
tort=0.5*pi;


%Gas Transport parameters
D=gas_diffusion(T,M,P);
%D=2.82*10^-5; %m^2/s
D=D*constric*por/tort;


%Microtopography paramters
H=1.5*10^-1; %m
L=H/0.03; %m
alpha=1;
U10=5; %m/s

%Micro Grid
surf_num_nodes=53;
a=L/2;
b=alpha*L/2;
c=2*H;

[x,y,surf_z]=surf_ellipsoid(a,b,c,surf_num_nodes);
if plot_debug==1
figure
contourf(x,y,surf_z)
axis equal
colorbar
end




u_firn=wind_pump_ellipsoid(x,y,surf_z,L,H,alpha,U10);

if plot_debug==1
    figure
    contourf(x,y,u_firn)
    axis equal
    colorbar
end

%Surface flux and concetrations
 ppb_surface=60;
 surf_conc=ppb_surface*rho_air*10^-9;
top_nueman=-u_firn/D ;
num_depths=53;
[x y z]=D3_zgrid(x,y,surf_z,3,num_depths);


scrsz = get(0,'ScreenSize');
figure('Position',[1 0 1.5*scrsz(3) 1.5*scrsz(4)])
surfc(squeeze(x(:,:,1)),squeeze(y(:,:,1)),squeeze(z(:,:,1)))
set(gca,'FontSize',15)
xlabel('X Direction [m] - Parallel with wind')
ylabel('Y Direction [m] - Perpendicular with wind')
zlabel('Altitude [m]')
title({'Surface microtopography';['Relief Amplitude = ' num2str(H) ' [m] Relief Wavelength = ' num2str(L) ' [m]'];['Horizontal aspect ratio = ' num2str(alpha)]})
set(gca,'ZTick',[0 2*H])
axis equal
dir='Surf_micro/';
mkdir(dir)
file=[dir 'Surface_mircotopgraphy_H_' num2str(H) '_L_' num2str(L) '_alpha_' num2str(alpha) '.jpeg'];
saveas(gcf,file)





[C]=D3_laplace(x,y,z,top_nueman,surf_conc);
 C=C/(rho_air*10^-9);
 



index=(surf_num_nodes-1)/2+1;
temp_x=squeeze(x(index,:,:));
temp_z=squeeze(z(index,:,:));
temp_C=squeeze(C(index,:,:));

figure('Position',[1 0 1.5*scrsz(3) 1.5*scrsz(4)])
contourf(temp_x,temp_z,temp_C,'LineStyle','none')
set(gca,'FontSize',15)
colorbar
xlabel('X Direction [m] - Parallel with wind')
ylabel('Depth in snowpack [m]')
title({'Wind pumping steady state concentration profile [ppb_v] along X-axis';['Relief Amplitude = ' num2str(H) ' [m] Relief Wavelength = ' num2str(L) ' [m]'];['Horizontal aspect ratio = ' num2str(alpha) ' Windspeed = ' num2str(U10) ' [m/s]'];['ABL concentration = ' num2str(ppb_surface) ' [ppt_v]']})
dir='WP_X_Z/';
mkdir(dir)
file=[dir 'X_Z_conc_profile_U10_' num2str(U10) '_H_' num2str(H) '_L_' num2str(L) '_alpha_' num2str(alpha) '.jpeg'];
saveas(gcf,file)

figure('Position',[1 0 1.5*scrsz(3) 1.5*scrsz(4)])

 contourf(x(:,:,1),y(:,:,1),C(:,:,1),'LineStyle','none')
 set(gca,'FontSize',15)
 xlabel('X Direction [m] - Parallel with wind')
 ylabel('Y Direction [m] - Perpendicular with wind')
 colorbar
 title({'Wind pumping steady state concentration profile [ppb_v] on surface';['Relief Amplitude = ' num2str(H) ' [m] Relief Wavelength = ' num2str(L) ' [m]'];['Horizontal aspect ratio = ' num2str(alpha) ' Windspeed = ' num2str(U10) ' [m/s]'];['ABL concentration = ' num2str(surf_conc) ' [ppt_v]']})
dir='WP_surf/';
mkdir(dir)
file=[dir 'X_Y_surf_conc_profile_U10_' num2str(U10) '_H_' num2str(H) '_L_' num2str(L) '_alpha_' num2str(alpha) '.jpeg'];
saveas(gcf,file)
 
figure('Position',[1 0 1.5*scrsz(3) 1.5*scrsz(4)])

contourf(x(:,:,1),y(:,:,1),u_firn,'LineStyle','none')
set(gca,'FontSize',15)
xlabel('X Direction [m] - Parallel with wind')
 ylabel('Y Direction [m] - Perpendicular with wind')
 colorbar
 title({'Vertical volumetric flux on surface of snowpack [m/s]';['Relief Amplitude = ' num2str(H) ' [m] Relief Wavelength = ' num2str(L) ' [m]'];['Horizontal aspect ratio = ' num2str(alpha) ' Windspeed = ' num2str(U10) ' [m/s]'];['Flux into the snowpack is negative']})

 
 dir='WP_surf_flux/';
mkdir(dir)
file=[dir 'surface_flux_U10_' num2str(U10) '_H_' num2str(H) '_L_' num2str(L) '_alpha_' num2str(alpha) '.jpeg'];
saveas(gcf,file)

 
%  figure
% v=u_firn*alpha*L^2*surf_conc
% v(v>=0)=NaN;
% v=abs(v);
% distance=0.3;
% 
%  time=distance./v/60; %time in min
%  
%  
%  spec=0:10;
%  contourf(x(:,:,1),y(:,:,1),time,spec,'LineStyle','none')
%  xlabel('X Direction [m] - Parallel with wind')
%  ylabel('Y Direction [m] - Perpendicular with wind')
%  colorbar
% 
%  title({['Time for voumetric flux into snow to travel vertically ' num2str(distance) ' meter [min]'];['Relief Amplitude = ' num2str(H) ' [m] Relief Wavelength = ' num2str(L) ' [m]'];['Horizontal aspect ratio = ' num2str(alpha) ' Windspeed = ' num2str(U10) ' [m/s]'];['Tortuosity = ' num2str(tort) ' Porosity = ' num2str(por)]})
% 
%  
%  dir='WP_surf_flux/';
% mkdir(dir)
% file=[dir 'surface_flux_time_U10_' num2str(U10) '_H_' num2str(H) '_L_' num2str(L) '_alpha_' num2str(alpha) '.jpeg'];
% saveas(gcf,file)
