function plot_conc(file,varargin)


if nargin==2
    
    dates=varargin{1};
    date_min=dates(1);
    date_max=dates(2);
    [Y, M_min, D_min] = datevec(date_min);
    [Y, M_max, D_max] = datevec(date_max);
end



data=load(file);
load noaa.mat
load cu_rad.mat


index=find(noaa_jd>date_min & noaa_jd<date_max);

wind_jd=noaa_jd(index);
wind_speed=noaa_ws(index);
wind_dir=noaa_wd(index);

wind_speed(wind_speed==9999)=NaN;

index=find(rad_jd>date_min & rad_jd<date_max);
irr_down=rad_down(index);
irr_up=rad_up(index);
irr_jd=rad_jd(index);

irr_down(irr_down>2000)=NaN;

scrsz = get(0,'ScreenSize');
load jd_idl_matlab_diff.mat
NO_time_2=data(:,1)+JD_IDL_MATLAB_DIFF;
NO2_time_2=data(:,4)+JD_IDL_MATLAB_DIFF;
%date filter
%keyboard
if nargin==2
    index=find(NO_time_2>date_min & NO_time_2<date_max);
else
    
    index=1:length(NO_time_2);
end

NO_time_2=NO_time_2(index);
NO_2=data(index,2);
NO_sscnt_2=data(index,3);

if nargin==2
    index=find(NO2_time_2>date_min & NO2_time_2<date_max);
else
    
    index=1:length(NO2_time_2);
end


NO2_time_2=NO2_time_2(index);
NO2_2=data(index,5);
NO2_sscnt_2=data(index,6);

%NOx_2=NO_2+NO2_2;

NO_time_1=data(:,10)+JD_IDL_MATLAB_DIFF;
NO2_time_1=data(:,13)+JD_IDL_MATLAB_DIFF;
%date filter

if nargin==2
    index=find(NO_time_1>date_min & NO_time_1<date_max);
else
    
    index=1:length(NO_time_1);
end

NO_time_1=NO_time_1(index);
NO_1=data(index,11);
NO_sscnt_1=data(index,12);

if nargin==2
    index=find(NO2_time_1>date_min & NO2_time_1<date_max);
else
    
    index=1:length(NO2_time_1);
end


NO2_time_1=NO2_time_1(index);
NO2_1=data(index,14);
NO2_sscnt_1=data(index,15);





if isequal(size(NO_1),size(NO2_1))
NOx_1=NO_1+NO2_1;
else
    if length(NO_1) > length(NO2_1)
        NOx_1=NO_1(1:length(NO2_1))+NO2_1;
        NO_time_1=NO_time_1(1:length(NO2_1));
        NO_1=NO_1(1:length(NO2_1));
    else
         NOx_1=NO_1+NO2_1(1:length(NO_1));
        NO2_time_1=NO2_time_1(1:length(NO_1));
         NO2_1=NO2_1(1:length(NO_1));
    end
end


if isequal(size(NO_2),size(NO2_2))
NOx_2=NO_2+NO2_2;
else
    if length(NO_2) > length(NO2_2)
        NOx_2=NO_2(1:length(NO2_2))+NO2_2;
        NO_time_2=NO_time_2(1:length(NO2_2));
        NO_2=NO_2(1:length(NO2_2));
    else
         NOx_2=NO_2+NO2_2(1:length(NO_2));
        NO2_time_2=NO2_time_2(1:length(NO_2));
         NO2_2=NO2_2(1:length(NO_2));
    end
end



lim_NO2=[-200 1000];
lim_NO=[-200 500];
lim_NOx=lim_NO+lim_NO2;
lim_depths=[-3 2];
tick_depths=lim_depths(1):lim_depths(2);
lim_ws=[0 25];
tick_ws=lim_ws(1):5:lim_ws(2);
lim_irr=[0 1000];
tick_irr=lim_irr(1):200:lim_irr(2);
num_date_ticks=10;
num_y_ticks=5;


path=['Surf_conc_' num2str(M_min) '_' num2str(D_min) '_' num2str(M_max) '_' num2str(D_min) '_' num2str(Y) '\'];
    mkdir(path)



figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
%subplot(2,1,1)


%keyboard
[AX h1 h2]=plotyy(NO_time_2,NO_2,NO_time_1,NO_1);  %,wind_jd,wind_speed);
hold all


xlabel('Date')
set(get(AX(2),'Ylabel'),'String','NO - Inlet 1 [ppt_v]')
set(get(AX(1),'Ylabel'),'String','NO - Inlet 2 [ppt_v] ')
title(['Surface NO' ])
set(AX(1),'XLim',[date_min date_max])
set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
set(AX(2),'XLim',[date_min date_max])
set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
datetick(AX(1),'x','mm/dd/yy','keepticks')
datetick(AX(2),'x','mm/dd/yy','keepticks')
set(AX(1),'YLim',lim_NO)
set(AX(2),'YLim',lim_NO)
set(h1,'Linestyle',':')
set(h2,'Linestyle',':')
set(h1,'Marker','o')
set(h2,'Marker','sq')
set(AX(1),'YTick',linspace(lim_NO(1),lim_NO(2),num_y_ticks))
set(AX(2),'YTick',linspace(lim_NO(1),lim_NO(2),num_y_ticks))

legend([h1 h2],'Inlet 2','Inlet 1','Location','NorthWest')
% subplot(2,1,2)
% 
% if ~isempty(irr_jd) && ~isempty(wind_jd)
% [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
% 
% xlabel('Date')
% set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
% set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
% title('Surface Irradiance and Wind Speed Measurements')
% set(AX(1),'XLim',[date_min date_max])
% set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
% set(AX(2),'XLim',[date_min date_max])
% set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
% datetick(AX(1),'x','mm/dd/yy','keepticks')
% datetick(AX(2),'x','mm/dd/yy','keepticks')
% 
% ylim(AX(1),lim_irr)
% set(AX(1),'YTick',tick_irr)
% ylim(AX(2),lim_ws)
% set(AX(2),'YTick',tick_ws)
% set(h1,'Linestyle','o')
% 
% legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
% 
% end

if nargin ==2
file=[path 'NO_' num2str(M_min) '_' num2str(D_min)  '-' num2str(M_max) '_' num2str(D_max) '_' num2str(Y) '.jpeg'];
else
    file=[path 'NO_NO2_Wind_Irradiance_'];
end


saveas(gcf,file)


figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
%subplot(2,1,1)



[AX h1 h2]=plotyy(NO2_time_2,NO2_2,NO2_time_1,NO2_1);  %,wind_jd,wind_speed);
hold all


xlabel('Date')
set(get(AX(2),'Ylabel'),'String','NO2 - Inlet 1 [ppt_v]')
set(get(AX(1),'Ylabel'),'String','NO2 - Inlet 2 [ppt_v] ')
title(['Surface NO2' ])
set(AX(1),'XLim',[date_min date_max])
set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
set(AX(2),'XLim',[date_min date_max])
set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
datetick(AX(1),'x','mm/dd/yy','keepticks')
datetick(AX(2),'x','mm/dd/yy','keepticks')
set(AX(1),'YLim',lim_NO2)
set(AX(2),'YLim',lim_NO2)
set(h1,'Linestyle',':')
set(h2,'Linestyle',':')
set(h1,'Marker','o')
set(h2,'Marker','sq')
set(AX(1),'YTick',linspace(lim_NO2(1),lim_NO2(2),num_y_ticks))
set(AX(2),'YTick',linspace(lim_NO2(1),lim_NO2(2),num_y_ticks))

legend([h1 h2],'Inlet 2','Inlet 1','Location','NorthWest')
% subplot(2,1,2)
% 
% if ~isempty(irr_jd) && ~isempty(wind_jd)
% [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
% 
% xlabel('Date')
% set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
% set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
% title('Surface Irradiance and Wind Speed Measurements')
% set(AX(1),'XLim',[date_min date_max])
% set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
% set(AX(2),'XLim',[date_min date_max])
% set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
% datetick(AX(1),'x','mm/dd/yy','keepticks')
% datetick(AX(2),'x','mm/dd/yy','keepticks')
% 
% ylim(AX(1),lim_irr)
% set(AX(1),'YTick',tick_irr)
% ylim(AX(2),lim_ws)
% set(AX(2),'YTick',tick_ws)
% set(h1,'Linestyle','o')
% 
% legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
% 
% end

if nargin ==2
file=[path 'NO2_' num2str(M_min) '_' num2str(D_min)  '-' num2str(M_max) '_' num2str(D_max) '_' num2str(Y) '.jpeg'];
else
    file=[path 'NO2_NO2_Wind_Irradiance_'];
end


saveas(gcf,file)

figure('Position',[1 0 scrsz(3) scrsz(4)*9/10],'Visible','off')
%subplot(2,1,1)



[AX h1 h2]=plotyy(NO_time_2,NOx_2,NO_time_1,NOx_1);  %,wind_jd,wind_speed);
hold all


xlabel('Date')
set(get(AX(2),'Ylabel'),'String','NO_x - Inlet 1 [ppt_v]')
set(get(AX(1),'Ylabel'),'String','NO_x - Inlet 2 [ppt_v] ')
title(['Surface NO_x' ])
set(AX(1),'XLim',[date_min date_max])
set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
set(AX(2),'XLim',[date_min date_max])
set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
datetick(AX(1),'x','mm/dd/yy','keepticks')
datetick(AX(2),'x','mm/dd/yy','keepticks')
set(AX(1),'YLim',lim_NOx)
set(AX(2),'YLim',lim_NOx)
set(h1,'Linestyle',':')
set(h2,'Linestyle',':')
set(h1,'Marker','o')
set(h2,'Marker','sq')
set(AX(1),'YTick',linspace(lim_NOx(1),lim_NOx(2),num_y_ticks))
set(AX(2),'YTick',linspace(lim_NOx(1),lim_NOx(2),num_y_ticks))

legend([h1 h2],'Inlet 2','Inlet 1','Location','NorthWest')
% subplot(2,1,2)
% 
% if ~isempty(irr_jd) && ~isempty(wind_jd)
% [AX h1 h2]=plotyy(irr_jd,irr_down,wind_jd,wind_speed);
% 
% xlabel('Date')
% set(get(AX(1),'Ylabel'),'String','Irradiance [W/m^2]')
% set(get(AX(2),'Ylabel'),'String','Wind Speed [m/s] ')
% title('Surface Irradiance and Wind Speed Measurements')
% set(AX(1),'XLim',[date_min date_max])
% set(AX(1),'XTick',linspace(date_min,date_max,num_date_ticks))
% set(AX(2),'XLim',[date_min date_max])
% set(AX(2),'XTick',linspace(date_min,date_max,num_date_ticks))
% datetick(AX(1),'x','mm/dd/yy','keepticks')
% datetick(AX(2),'x','mm/dd/yy','keepticks')
% 
% ylim(AX(1),lim_irr)
% set(AX(1),'YTick',tick_irr)
% ylim(AX(2),lim_ws)
% set(AX(2),'YTick',tick_ws)
% set(h1,'Linestyle','o')
% 
% legend([h1 h2],'Irradiance','Wind Speed','Location','NorthWest')
% 
% end

if nargin ==2
file=[path 'NOx_' num2str(M_min) '_' num2str(D_min)  '-' num2str(M_max) '_' num2str(D_max) '_' num2str(Y) '.jpeg'];
else
    file=[path 'NO_NO2_Wind_Irradiance_'];
end


saveas(gcf,file)









close all


end