clc
clear all
close all

path='data/';
files={'methts_summitflux_20080601.txt';
    'methts_summitflux_20080701.txt';
    'methts_summitflux_20080801.txt';
    'methts_summitflux_20080901.txt';
    'methts_summitflux_20081001.txt';
    'methts_summitflux_20081101.txt';
    'methts_summitflux_20081201.txt';
    'methts_summitflux_20090101.txt';
    'methts_summitflux_20090201.txt';
    'methts_summitflux_20090301.txt';
    'methts_summitflux_20090401.txt';
    'methts_summitflux_20090501.txt';
    'methts_summitflux_20090601.txt';
    'methts_summitflux_20090701.txt';
    'methts_summitflux_20090801.txt';
    'methts_summitflux_20090901.txt';
    'methts_summitflux_20091001.txt';
    'methts_summitflux_20091101.txt';
    'methts_summitflux_20091201.txt';
    'methts_summitflux_20100101.txt';
    'methts_summitflux_20100201.txt';
    'methts_summitflux_20100301.txt';
    'methts_summitflux_20100401.txt';
    'methts_summitflux_20100501.txt';
    'methts_summitflux_20100601.txt';
    'methts_summitflux_20100701.txt';
    };



in_1_ht=[];
in_2_ht=[];
son_1_ht=[];
son_2_ht=[];
son_3_ht=[];
grad_ht=[];
ht_jd=[];


for i=1:numel(files)
    file=files{i};
    data=load([path file]);
    year=str2num(file(19:22));
    
    time=data(:,1);
    year=year*ones(size(time));
    ht_jd=[ht_jd;datenum(year,0,time,0,0,0)];
    in_1_ht=[in_1_ht;data(:,2)];
    in_2_ht=[in_2_ht;data(:,4)];
    son_1_ht=[ son_1_ht;data(:,3)];
    son_2_ht=[son_2_ht;data(:,5)];
    son_3_ht=[son_3_ht;data(:,6)];
    grad_ht=[grad_ht;data(:,7)];
    
end

[ht_jd in_1_ht in_2_ht son_1_ht son_2_ht son_3_ht grad_ht]=horz_arrays(ht_jd, in_1_ht, in_2_ht, son_1_ht, son_2_ht, son_3_ht, grad_ht);



 save('summit_MET_hts.mat','ht_jd', 'in_1_ht', 'in_2_ht', 'son_1_ht', 'son_2_ht', 'son_3_ht', 'grad_ht')


