clc
clear all
close all


path='data/';
files={'temps_summitflux_20080701_R1.txt';
'temps_summitflux_20080801_R1.txt';
'temps_summitflux_20080901_R1.txt';
'temps_summitflux_20081001_R1.txt';
'temps_summitflux_20081101_R1.txt';
'temps_summitflux_20081201_R1.txt';
'temps_summitflux_20090101_R1.txt';
'temps_summitflux_20090201_R1.txt';
'temps_summitflux_20090301_R1.txt';
'temps_summitflux_20090401_R1.txt';
'temps_summitflux_20090501_R1.txt';
'temps_summitflux_20090601_R1.txt';
'temps_summitflux_20090701_R1.txt';
'temps_summitflux_20090801_R1.txt';
'temps_summitflux_20090901_R1.txt';
'temps_summitflux_20091001_R1.txt';
'temps_summitflux_20091101_R1.txt';
'temps_summitflux_20091201_R1.txt';
'temps_summitflux_20100101_R1.txt';
'temps_summitflux_20100201_R1.txt';
'temps_summitflux_20100301_R1.txt';
'temps_summitflux_20100401_R1.txt';
'temps_summitflux_20100501_R1.txt';
'temps_summitflux_20100601_R1.txt';    
};
data=[];

num_header=35;

for i=1:numel(files)
   file=files{i};
   year=str2double(file(18:21));
   month=str2double(file(22:23));
   day=str2double(file(24:25));
   fid=fopen([path file]);
   for ii=1:num_header
       line=fgetl(fid);
       
   end
   
   data2=fscanf(fid,'%f %f %f',[4,inf]);
   data2=data2';
   sec=data2(:,1);
   %year=year*ones(size(sec));
   %month=month*ones(size(sec));
   %day=day*ones(size(sec));
   
   data2(:,1)=datenum(year,month,day,0,0,sec);
   data=[data;data2];
   
    
end

MET_temp_jd=data(:,1)';
MET_temp_bot=data(:,4)';
MET_temp_mid=data(:,3)';
MET_temp_top=data(:,2)';

save('summit_MET_temperature.mat','MET_temp_jd','MET_temp_bot','MET_temp_mid','MET_temp_top')