clc
clear all
close all


R=8.3144621 % J/mol K
Tf=273.0  %Temperature of formation of ice in Kelvin
mH2O= 18.0  %molecular weight of water
H=334000
L2cm3=1/1000;
g2kg=1000;
d_ice=0.917
d_water=0.9984