clc
clear all
close all

[num,txt,raw] = xlsread('null_tests.xlsx');
raw_index=4:29;

dates=[datenum(raw(raw_index,1),'mm/dd/yyyy')];
% Plot NO2 data
NO2=[raw{raw_index,2}];
NO2_var=[raw{raw_index,3}];
NO2_SD=[raw{raw_index,4}];

errorbar(dates,NO2,NO2_SD,'o')
datetick('x','mm/dd')
ylabel('\DeltaNO_2 [ppt_v]')
xlabel('Dates [mm/dd]')
title({' 2010 NO_2 Null tests';'Errorbars are 1 SD'})

saveas(gcf,'NO2_null_test.jpeg')
close(gcf)

% Plot NO data
NO=[raw{raw_index,5}];
NO_var=[raw{raw_index,6}];
NO_SD=[raw{raw_index,7}];

errorbar(dates,NO,NO_SD,'o')
datetick('x','mm/dd')
ylabel('\DeltaNO [ppt_v]')
xlabel('Dates [mm/dd]')
title({' 2010 NO Null tests';'Errorbars are 1 SD'})

saveas(gcf,'NO_null_test.jpeg')