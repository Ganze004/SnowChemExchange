clc
clear all
close all

file='observation_input.inp';
in_path='MLSN_CHEM/input/';

outpath='MLSN_CHEM/avg_comp/';

mkdir(outpath);

num_header_lines=4;

fid=fopen([in_path file]);

for i=1: num_header_lines
    
    line=fgetl(fid);
    
end

data=fscanf(fid,'%f',[9 inf]);
data=data';



reference_rad=250;
jno3=2.2*10^-5;    %s^-1
e_fold=0.07;        %efold depth in meters 0.07
j_clim=linspace(10-10,10^-5,100);

num_hr_depths=10;
num_depths=2;

max_depth=2;
depth_lim=[-2 0];
hr_depths=linspace(0,max_depth,num_hr_depths+1)*-1;
hr_dz=diff(hr_depths);
hr_dz=[hr_dz,hr_dz(end)];
%hr_depths=(hr_depths(1:end-1)+hr_dz/2)*-1;

depths=linspace(0,max_depth, num_depths+1)*-1;
dz=diff(depths);
dz=[dz, dz(end)];
%depths=(depths(1:end-1)+dz/2)*-1;



sec=data(:,1);
irradiance=data(:,9);

time=datenum(2009,4,11,0,0,sec);




[dummy hr_depth_grid]=meshgrid(time,hr_depths);
[hr_time_grid hr_dz_grid]=meshgrid(time,hr_dz);
[ hr_irr_grid]=meshgrid(irradiance, hr_dz);


[dummy depth_grid]=meshgrid(time,depths);
[time_grid dz_grid]=meshgrid(time,dz);
[ irr_grid]=meshgrid(irradiance, dz);



j_calc_avg=@(irr,z,zz) (e_fold./abs(zz)).*irr./reference_rad.*(exp((z)./e_fold)-exp((z+zz)./e_fold))*jno3;
j_calc=@(irr,z) irr./reference_rad.*exp(z./e_fold)*jno3;

hr_j=j_calc(hr_irr_grid,hr_depth_grid+hr_dz_grid/2);
j=j_calc_avg(irr_grid,depth_grid,dz_grid);
hr_j_calc_avg=j_calc_avg(hr_irr_grid,hr_depth_grid,hr_dz_grid);

hr_j_lin_avg=zeros(size(j));



for i=1:length(time)
    
index=abs(hr_depth_grid(:,i))>=abs(max_depth/2);

hr_j_lin_avg(1,i)=mean(hr_j(~index,i));
hr_j_lin_avg(2,i)=mean(hr_j(index,i));

    
end



time_grid=[time_grid(1,:);time_grid(1,:);time_grid(2,:);time_grid(2,:)];
depth_grid=[depth_grid(1,:);depth_grid(2,:)+0.01;depth_grid(2,:);depth_grid(3,:)];
hr_j_lin_avg=[hr_j_lin_avg(1,:);hr_j_lin_avg(1,:);hr_j_lin_avg(2,:);hr_j_lin_avg(2,:)];
j=[j(1,:);j(1,:);j(2,:);j(2,:)];    



temp_hr_depth=[];
temp_hr_j=[];
temp_hr_time_grid=[];
temp_hr_j_calc_avg=[];
for i=1:size(hr_depth_grid,1)
    
    temp_hr_j(i*2-1,:)=hr_j(i,:);
    temp_hr_j(i*2,:)=hr_j(i,:);
    temp_hr_time_grid(i*2-1,:)=hr_time_grid(i,:);
    temp_hr_time_grid(i*2,:)=hr_time_grid(i,:);
    temp_hr_j_calc_avg(i*2-1,:)=hr_j_calc_avg(i,:);
     temp_hr_j_calc_avg(i*2,:)=hr_j_calc_avg(i,:);
    
    
    
    temp_hr_depth(i*2-1,:)=hr_depth_grid(i,:);
    temp_hr_depth(i*2,:)=hr_depth_grid(i,:)+hr_dz_grid(i,:)*.999;
 
 
    
end

hr_j=temp_hr_j;
hr_time_grid=temp_hr_time_grid;
hr_depth_grid=temp_hr_depth;
hr_j_calc_avg=temp_hr_j_calc_avg;

figure


subplot(2,2,1)
contourf(hr_time_grid, hr_depth_grid, hr_j,j_clim,'LineStyle','none')
colorbar
set(gca,'CLim',[min(j_clim) max(j_clim)])
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(depth_lim)
title({['Nitrate photolysis rate constant [s^-1] for ' num2str(num_hr_depths) ' layers'];'Calculated at center of layer'})

subplot(2,2,2)
contourf(time_grid, depth_grid, hr_j_lin_avg,j_clim,'LineStyle','none')
colorbar
set(gca,'CLim',[min(j_clim) max(j_clim)])
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(depth_lim)
title({['Nitrate photolysis rate constant [s^-1]  for ' num2str(num_depths) ' layers'];['Linearly averaged from ' num2str(num_hr_depths) ' layers']})

subplot(2,2,3)
contourf(hr_time_grid, hr_depth_grid, hr_j_calc_avg,j_clim,'LineStyle','none')
colorbar
set(gca,'CLim',[min(j_clim) max(j_clim)])
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(depth_lim)
title({['Nitrate photolysis rate constant [s^-1]  for ' num2str(num_hr_depths) ' layers'];'Calculated using calculus average for each layer'})

subplot(2,2,4)
contourf(time_grid, depth_grid, j,j_clim,'LineStyle','none')
colorbar
set(gca,'CLim',[min(j_clim) max(j_clim)])
datetick('x','keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim(depth_lim)
title({['Nitrate photolysis rate constant [s^-1]  for ' num2str(num_depths) ' layers'];'Calcualted from calculus average for each layer'})

saveas(gcf,[outpath 'averaging_comparision.jpeg'])

