clc
clear all
close all

dir='../MTU4/output/';
path=dir;
mkdir(dir)
directory='../MTU4/output/';

path=dir;
mkdir(dir)

year=2009
month=5
end_month=month
day=1
end_day=2
hour=0
m=0

spc_name=MTU_get_spcs(directory);
depths=MTU_get_depths(directory);
time=MTU_get_times(directory);
depths2=[];
time2=[];


for i=1:(length(depths)-1)
   depths2(2*i-1) =depths(i);
   depths2(2*i)=depths(i+1)*0.99;
   time2(2*i-1)=time(i);
   time2(2*i)=time(i);
end
%time=time2;
depths=depths2;


eqs=MTU_get_eqs(directory);
chemr=MTU_get_chemr(directory);

conc_file=[directory 'model_gas_PPX.out']
para_file=[directory 'model_para.out']
data=load(para_file);
NUM_DEPTH=data(1);
NUM_SPEC=data(2);
NUM_TIME=data(3);
MAX_j=data(4);
NUM_TEND=data(5);
NUM_EQ=data(6);

%data=load(conc_file);
data=dlmread(conc_file);  % using dlmread instead of load to deal with a matrix that is not completely filled

rj=MTU_get_rj(directory);
tend=MTU_get_tend(directory);
%rj=reshape(data,MAX_j,NUM_TIME,NUM_SPEC,NUM_DEPTH);
temperature=MTU_get_temp(directory);
QLL=MTU_get_QLL(directory);
% keyboard

data=reshape(data,NUM_DEPTH,NUM_TIME,NUM_SPEC);
data2=zeros(2*NUM_DEPTH,NUM_TIME,NUM_SPEC);
rj2=zeros(MAX_j,NUM_TIME,NUM_SPEC,2*NUM_DEPTH);
tend2=zeros(NUM_TEND,NUM_TIME,NUM_SPEC,2*NUM_DEPTH);
temperature2=zeros(2*NUM_DEPTH,NUM_TIME);
QLL2=zeros(2*NUM_DEPTH,NUM_TIME);
chemr2=zeros(2*NUM_DEPTH,NUM_TIME,NUM_EQ);

for i=1:NUM_TIME
    for j=1:NUM_DEPTH
        data2(2*j-1,i,:)=data(j,i,:);
        data2(2*j,i,:)=data(j,i,:);
        temperature2(2*j-1,i)=temperature(j,i);
        temperature2(2*j,i)=temperature(j,i);
         QLL2(2*j-1,i)=QLL(j,i);
        QLL2(2*j,i)=QLL(j,i);
        chemr2(2*j-1,i,:)=chemr(j,i,:);
        chemr2(2*j,i,:)=chemr(j,i,:);
        for k=1:MAX_j
           rj2(k,i,:,2*j-1)=rj(k,i,:,j); 
           rj2(k,i,:,2*j)=rj(k,i,:,j);
        end
        for k=1:NUM_TEND
           tend2(k,i,:,2*j-1)=tend(k,i,:,j); 
           tend2(k,i,:,2*j)=tend(k,i,:,j);
        end
    end
end
data=data2;
rj=rj2;
tend=tend2;
temperature=temperature2;
QLL=QLL2;
chemr=chemr2;
 time=MTU_get_times(directory);
 date_tick=linspace(time(1),time(end),5);
  date_tick=linspace(datenum(2009,month,15,0,0,0),datenum(2009,month,30,0,0,0),5);
 %keyboard
[time, depths]=meshgrid(time,depths);


 %MAX_j=1
 index=find(strcmp('Hp_aq',spc_name)==1);
 figure('Visible','off')
 conc=-log10(squeeze(data(:,:,index)));
 contourf(time,depths,conc,100,'LineStyle','none')
 

%  tend=tend*10^12;
%  for i =1:NUM_SPEC%[67 73 79 123 130 124]%[108 123]%
% figure('Visible','off')
% subplot(2,1,1)
% 
%      conc=squeeze(data(:,:,i));
%  conc=conc*10^12;
%         clim_vec=linspace(0.01*max(conc(:)),max(conc(:)),100);
%      contourf(time,depths,conc,100,'LineStyle','none')
%  colorbar
%  set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
%  %ylim([min(depths(:)) 1.0])
% set(gca,'xtick',[])
%         set(gca,'xticklabel',[])
%         title([spc_name{i} ' [ppt_v]'])
%         ylabel('Depth [m]')
%         tend_total=abs(squeeze(tend(1,:,i,:))')+abs(squeeze(tend(3,:,i,:))');
%         
%         
%         
%     subplot(2,1,2)
%     clim_vec=linspace(0,2,2);
%     tend_temp=abs(squeeze(tend(3,:,i,:))'./squeeze(tend(1,:,i,:))');
%     contourf(time,depths,tend_temp,clim_vec,'LineStyle','none')
%     colorbar
%  set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
%      %set(gca,'xtick',[])
%         %set(gca,'xticklabel',[])
%         title(['Damkohler Number'])
%         ylabel('Depth [m]')
%         
% % subplot(3,1,3)
% %     clim_vec=linspace(-100,100,100);
% %     tend_temp=squeeze(tend(3,:,i,:))'./tend_total*100;
% %     contourf(time,depths,tend_temp,clim_vec,'LineStyle','none')
% %     colorbar
% %  set(gca,'XTick',date_tick);
% % datetick('x',2,'keepticks')
% %      title(['Chemsitry and Mass transfer [ppt_v/hr]'])
% %         ylabel('Depth [m]')
%  
%  
%  saveas(gcf,[dir 'conc_' spc_name{i} '.jpeg'])
%  close(gcf)
%      
%  end
%  
 
%  for i =1:NUM_EQ
% figure('Visible','off')
% 
%      conc=squeeze(chemr(:,:,i));
% 
%         clim_vec=linspace(0.01*max(conc(:)),max(conc(:)),100);
%      contourf(time,depths,conc,100,'LineStyle','none')
%  colorbar
%  set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
%  ylim([-2 2])
%  xlabel('Date [mm/dd/yy]')
%  ylabel('Depth')
%  
%  title_str=eqs{i};
%  title_str=strrep(title_str,'_to_',' -> ');
%  title_str=strrep(title_str,'_aq','*');
%  title_str=strrep(title_str,'_',' + ');
%  title_str=strrep(title_str,'*','_aq');
%  title_str=[title_str ' [molecules cm^(-3) s^(-1)'];
%  title(title_str)
%  
%  saveas(gcf,[dir 'chemr_' eqs{i} '_' num2str(i) '.jpeg'])
%  close(gcf)
%      
%  end
 
 
 figure('Visible','off')
 contourf(time,depths,temperature,100,'LineStyle','none')
 colorbar
 set(gca,'XTick',date_tick);
datetick('x',13,'keepticks')
 ylim([min(depths(:)) 1.0])
 saveas(gcf,[dir 'model_temp.jpeg'])
 close(gcf)
 
 figure('Visible','off')
 contourf(time,depths,QLL,100,'LineStyle','none')
 colorbar
 set(gca,'XTick',date_tick);
datetick('x',13,'keepticks')
 ylim([min(depths(:)) 1.0])
 saveas(gcf,[dir 'model_QLL_thickness.jpeg'])
 close(gcf)
 
 