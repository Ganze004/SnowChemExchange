clc
clear all
close all

path='model_obs_com/';
mkdir(path)
file='MLSN_CHEM/output/conc.out';
data=load(file);
load MLSN_species.mat
year=2009
month=4
day=11
hour=0
m=0

IDT_O3=85+2;
IDT_NO=87+2;
IDT_NO2=95+2;
IDT_RAD=size(MLSN_names,1)+1;
IDT_CS_RAD=size(MLSN_names,1)+2;
IDT_vws=size(MLSN_names,1)+3;
index=find(data(:,2)==data(1,2),2,'first');
num_depth=index(2)-index(1);

time=data(:,1);
conc_time=datenum(year,month,day,hour,m,time);
num_time=length(time)/num_depth;
conc_time=reshape(conc_time,num_depth,num_time);
conc_depths=reshape(data(:,2),num_depth,num_time);
conc_data=(10^12)*data(:,3:end);

date_tick=linspace(conc_time(1,1),conc_time(1,end),5);


%Observations
file2='contour_plots_2009_4_11_2009_4_25/obs.out';

data=load(file2);
index=find(data(:,2)==data(1,2),2,'first');
num_depth_obs=index(2)-index(1);

time_obs=data(:,1);
time_obs=datenum(year,month,day,hour,m,time_obs)-datenum(0,0,0,2,0,0);%offset for time zone

num_time_obs=length(time_obs)/num_depth_obs;
IDT_NO_obs=3;
IDT_NO2_obs=4;
IDT_O3_obs=5;
time_obs=reshape(time_obs,num_depth_obs,num_time_obs);
depths_obs=reshape(data(:,2),num_depth_obs,num_time_obs);
NO_obs=reshape(data(:,IDT_NO_obs),num_depth_obs,num_time_obs);
NO2_obs=reshape(data(:,IDT_NO2_obs),num_depth_obs,num_time_obs);
O3_obs=reshape(data(:,IDT_O3_obs),num_depth_obs,num_time_obs);
snow_temp_obs=reshape(data(:,6),num_depth_obs,num_time_obs);
u_ws_obs=reshape(data(:,7),num_depth_obs,num_time_obs);
v_ws_obs=reshape(data(:,8),num_depth_obs,num_time_obs);
rad_obs=reshape(data(:,9),num_depth_obs,num_time_obs);


index=~isnan(depths_obs(:,1));
depths_obs=depths_obs(index,:);
O3_obs=O3_obs(index,:);
time_obs=time_obs(index,:);
snow_temp_obs=snow_temp_obs(index,:);
NO_obs=NO_obs(index,:);
NO2_obs=NO2_obs(index,:);

%RAD
i=IDT_RAD;
 rad=reshape(conc_data(:,i),num_depth,num_time)/(10^12);
    
 %clear sky -RAD
i=IDT_CS_RAD;
 CS_rad=reshape(conc_data(:,i),num_depth,num_time)/(10^12);
 
 %vertical windspeed in snowpack (m/s)
   i=IDT_vws;
 
 u_snow=reshape(conc_data(:,i),num_depth,num_time)/(10^12);
 
 
 figure('Visible','off')
 
 contourf(conc_time,conc_depths,u_snow,'LineStyle','none')
 colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([min(conc_depths(:)) 0.5])
title(['Modeled profile of u_snow [m/s]'])
saveas(gcf,[path 'u_snow.jpeg'])
close all

%"boxing" the layers
dx=abs(diff(conc_depths(:,1)));
dx(1)=2*conc_depths(1,1);
dx(end+1)=dx(end);
for i=1:num_depth
   depths2((2*i-1),:)=conc_depths(i,:)+dx(i)/2 ;
   depths2((2*i),:)=conc_depths(i,:)-dx(i)/2 ;
   
    time2((2*i-1),:)=conc_time(i,:);
    time2((2*i),:)=conc_time(i,:);
end
conc_depths=depths2;
conc_time=time2;

load MLSN_species.mat
for i=[29 30 31 36 37 43 44 85 87 95 ]%1:size(MLSN_names,1)%;[29 33 34 81 83 87]%
    i
    
     conc_now=reshape(conc_data(:,i),num_depth,num_time);
    conc_array=conc_now(:);
    conc_std=std(conc_array);
    conc_mean=mean(conc_array);
    index=conc_array<(3*conc_std+conc_mean) & conc_array>(-3*conc_std+conc_mean);
    clim_conc=[0 max(conc_array(index))];
    clim_conc=linspace(clim_conc(1),clim_conc(2),100);
    
    
    for j=1:num_depth
        conc2((2*j-1),:)=conc_now(j,:);
        conc2((2*j),:)=conc_now(j,:);
    end
    conc_now=conc2;
    index=[MLSN_names{:,2}]==i;
figure('Visible','off') 
subplot(2,1,1)
contourf(conc_time,conc_depths,conc_now,clim_conc,'LineStyle','none')
colorbar
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Depth [m]')
ylim([min(conc_depths(:)) 0.5])
title(['Modeled profile of ' MLSN_names{index,1} ' [ppt_v]'])
P=get(gca,'Position');

subplot(2,1,2)
plot(conc_time(1,:),rad(1,:),'-o')
hold all
plot(conc_time(1,:),CS_rad(1,:),'-sq')
set(gca,'XTick',date_tick);
datetick('x',2,'keepticks')
xlabel('Date')
ylabel('Irradiance [W/(m^2)]')
legend('Modeled','Clear Sky','Location','EastOutside')
P2=get(gca,'Position');
P2(3)=P(3);
set(gca,'Position',P2)


saveas(gcf,[path 'conc_' MLSN_names{index,1} '_rad.jpeg'])
close all
end
figure('Visible','off') 

% %ozone
% i=2;
%  conc_now=reshape(conc_data(:,i),num_depth,num_time);
%     conc_array=conc_now(:);
%     conc_std=std(conc_array);
%     conc_mean=mean(conc_array);
%     index=conc_array<(3*conc_std+conc_mean) & conc_array>(-3*conc_std+conc_mean);
%     clim_conc=[0 max(conc_array(index))];
%     clim_conc=linspace(clim_conc(1),clim_conc(2),100);
%     
%     
%     for j=1:num_depth
%         conc2((2*j-1),:)=conc_now(j,:);
%         conc2((2*j),:)=conc_now(j,:);
%     end
%     conc_now=conc2;
%     index=[MLSN_names{:,2}]==i;
% 
% subplot(4,1,1)
% contourf(conc_time,conc_depths,conc_now,clim_conc,'LineStyle','none')
% colorbar
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% %xlabel('Date')
% ylabel('Depth [m]')
% ylim([min(conc_depths(:)) 0.5])
% title(['Modeled profile of ' MLSN_names{index,1} ' [ppt_v]'])
% P=get(gca,'Position');
% P(4)=P(4)*3/4;
% 
% %NO2
% i=48;
%  conc_now=reshape(conc_data(:,i),num_depth,num_time);
%     conc_array=conc_now(:);
%     conc_std=std(conc_array);
%     conc_mean=mean(conc_array);
%     index=conc_array<(3*conc_std+conc_mean) & conc_array>(-3*conc_std+conc_mean);
%     clim_conc=[0 max(conc_array(index))];
%     clim_conc=linspace(clim_conc(1),clim_conc(2),100);
%     
%     
%     for j=1:num_depth
%         conc2((2*j-1),:)=conc_now(j,:);
%         conc2((2*j),:)=conc_now(j,:);
%     end
%     conc_now=conc2;
%     index=[MLSN_names{:,2}]==i;
% 
% subplot(4,1,2)
% contourf(conc_time,conc_depths,conc_now,clim_conc,'LineStyle','none')
% colorbar
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% %xlabel('Date')
% ylabel('Depth [m]')
% ylim([min(conc_depths(:)) 0.5])
% title(['Modeled profile of ' MLSN_names{index,1} ' [ppt_v]'])
% P2=get(gca,'Position');
% P2(3)=P(3);
% P2(4)=P(4);
% set(gca,'Position',P2)
%     
%   %NO
% i=47;
%  conc_now=reshape(conc_data(:,i),num_depth,num_time);
%     conc_array=conc_now(:);
%     conc_std=std(conc_array);
%     conc_mean=mean(conc_array);
%     index=conc_array<(3*conc_std+conc_mean) & conc_array>(-3*conc_std+conc_mean);
%     clim_conc=[0 max(conc_array(index))];
%     clim_conc=linspace(clim_conc(1),clim_conc(2),100);
%     
%     
%     for j=1:num_depth
%         conc2((2*j-1),:)=conc_now(j,:);
%         conc2((2*j),:)=conc_now(j,:);
%     end
%     conc_now=conc2;
%     index=[MLSN_names{:,2}]==i;
% 
% subplot(4,1,3)
% contourf(conc_time,conc_depths,conc_now,clim_conc,'LineStyle','none')
% colorbar
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% %xlabel('Date')
% ylabel('Depth [m]')
% ylim([min(conc_depths(:)) 0.5])
% title(['Modeled profile of ' MLSN_names{index,1} ' [ppt_v]'])
% P2=get(gca,'Position');
% P2(3)=P(3);
% P2(4)=P(4);
% set(gca,'Position',P2)  
%     
%     
% subplot(4,1,4)
% plot(conc_time(1,:),rad(1,:),'-o')
% hold all
% plot(conc_time(1,:),CS_rad(1,:),'-sq')
% set(gca,'XTick',date_tick);
% datetick('x',2,'keepticks')
% xlabel('Date')
% ylabel('Rad. [W/(m^2)]')
% legend('Modeled','Clear Sky','Location','EastOutside')
% P2=get(gca,'Position');
% P2(3)=P(3);
% set(gca,'Position',P2)
% 
% saveas(gcf,[path 'conc_O3_NO2_NO_rad.jpeg'])
% close all






