clc
clear all
close all

T=270;
P=1;
M=[14+16*2, 14+16, 16*3];
U10=3; %m/s
z=logspace(-4,log10(3),20);
depths=z*-1;
z_diff=diff([0 z]);

dt=1; %seconds
tort=0.5;

D=gas_diffusion(T,M,P);


k=8*10^-10; %m^2
H=1.5*10^-2; %m
L=3.0*10^-2; %m



rho=1.3; %kg/m^3
u=1.6*10^-5;  % Pa s
alpha=1;
U=WP(U10,z,k,L,H,rho,u,alpha);



Pe=U.*z_diff./D(1);



Cr=U.*dt./z_diff;

figure
subplot(2,1,1)

plot(Pe,depths,'o')
title(['Peclet and Cront numbers based on Thomas et. al model and U_1_0 = ' num2str(U10) ' m/s'])
xlabel('Peclet')
ylabel('depth [m]')
ylim([-3 0])

subplot(2,1,2)
plot(Cr,depths,'o')
title([' dt of ' num2str(dt) ' second'])
xlabel('Cront')
ylabel('depth [m]')
ylim([-3 0])
saveas(gcf,['Thomas_U10_' num2str(U10) '.jpeg'])

U_thomas=U;



%cunningham model

k=8*10^-10; %m^2
H=1.5*10^-2; %m
L=H/0.03; %m
rho=1.3; %kg/m^3
u=1.6*10^-5;  % Pa s
alpha=1;
U=WP(U10,z,k,L,H,rho,u,alpha);



Pe=U.*z_diff./D(1);



Cr=U.*dt./z_diff;

figure
subplot(2,1,1)

plot(Pe,depths,'o')
title({'Peclet and Cront numbers based of Cunningham';['relief amplitude/wavelength and U_1_0 = ' num2str(U10) ' m/s']})
xlabel('Peclet')
ylabel('depth [m]')
ylim([-3 0])

subplot(2,1,2)
plot(Cr,depths,'o')
title(['dt of ' num2str(dt) ' second'])
xlabel('Cront')
ylabel('depth [m]')
ylim([-3 0])
saveas(gcf,['Cunningham_U10_' num2str(U10) '.jpeg'])


figure
subplot(2,1,1)
semilogx(U,depths,'o')
title({'Wind Speed in snowpack based on Cunningham';['relief amplitude/wavelength and U_1_0 = ' num2str(U10) ' m/s']})
xlabel('Wind speed in snowpack [m/s]')
ylabel('depth [m]')
ylim([-3 0])

subplot(2,1,2)

semilogx(U_thomas,depths,'o')
title(['Wind Speed in snowpack based on Thomas et. al model and U_1_0 = ' num2str(U10) ' m/s'])
xlabel('Wind speed in snowpack [m/s]')
ylabel('depth [m]')
ylim([-3 0])
saveas(gcf,['Windspeeds_U10_' num2str(U10) '.jpeg'])


saveas(gcf,['Transport_U10_' num2str(U10) '.jpeg'])

figure

subplot(2,1,1)
plot(tort.*D(1).*ones(size(depths)),depths)
hold all
plot(U.*z_diff,depths,'o')
xlabel('Coefficent of diffusion and advection [m^2/s]')
ylabel('Depth [m]')
title({'Effective Diffusion and Advection of Cunningham and Waddington'; [' U_1_0 = ' num2str(U10) ]})
legend('Diffusion','Advection','Location','SouthEast')

subplot(2,1,2)
plot(tort.*D(1).*ones(size(depths)),depths)
hold all
plot(U_thomas.*z_diff,depths,'o')
xlabel('Coefficent of diffusion and advection [m^2/s]')
ylabel('Depth [m]')
title({'Effective Diffusion and Advection of Thomas model'; [' U_1_0 = ' num2str(U10) ]})
legend('Diffusion','Advection','Location','SouthEast')

saveas(gcf,['Transport_U10_' num2str(U10) '.jpeg'])

% U10 = 25 m/s
U10=15;


k=8*10^-10; %m^2
L=3.0*10^-2; %m
H=1.5*10^-2; %m
rho=1.3; %kg/m^3
u=1.6*10^-5;  % Pa s
alpha=1;
U=WP(U10,z,k,L,H,rho,u,alpha);



Pe=U.*z_diff./D(1);



Cr=U.*dt./z_diff;

figure
subplot(2,1,1)

plot(Pe,depths,'o')
title(['Peclet and Cront numbers based on Thomas et. al model and U_1_0 = ' num2str(U10) ' m/s'])
xlabel('Peclet')
ylabel('depth [m]')
ylim([-3 0])

subplot(2,1,2)
plot(Cr,depths,'o')
title(['dt of ' num2str(dt) ' second'])
xlabel('Cront')
ylabel('depth [m]')
ylim([-3 0])
saveas(gcf,['Thomas_U10_' num2str(U10) '.jpeg'])

U_thomas=U;



%cunningham model



k=8*10^-10; %m^2
H=1.5*10^-2; %m
L=H/0.03; %m
rho=1.3; %kg/m^3
u=1.6*10^-5;  % Pa s
alpha=1;U=WP(U10,z,k,L,H,rho,u,alpha);



Pe=U.*z_diff./D(1);



Cr=U.*dt./z_diff;

figure
subplot(2,1,1)

plot(Pe,depths,'o')
title({'Peclet and Cront numbers based of Cunningham';['relief amplitude/wavelength and U_1_0 = ' num2str(U10) ' m/s']})
xlabel('Peclet')
ylabel('depth [m]')
ylim([-3 0])
subplot(2,1,2)
plot(Cr,depths,'o')
title([' dt of ' num2str(dt) ' second'])
xlabel('Cront')
ylabel('depth [m]')

saveas(gcf,['Cunningham_U10_' num2str(U10) '.jpeg'])


figure
subplot(2,1,1)
semilogx(U,depths,'o')
title({'Wind Speed in snowpack based on Cunningham';['relief amplitude/wavelength and U_1_0 = ' num2str(U10) ' m/s']})
xlabel('Wind speed in snowpack [m/s]')
ylabel('depth [m]')
ylim([-3 0])

subplot(2,1,2)

semilogx(U_thomas,depths,'o')
title(['Wind Speed in snowpack based on Thomas et. al model and U_1_0 = ' num2str(U10) ' m/s'])
xlabel('Wind speed in snowpack [m/s]')
ylabel('depth [m]')
ylim([-3 0])
saveas(gcf,['Windspeeds_U10_' num2str(U10) '.jpeg'])



figure

%subplot(2,1,1)
plot(tort.*D(1).*ones(size(depths)),depths)
hold all
plot(U.*z_diff,depths,'o')
xlabel('Coefficent of diffusion and advection [m^2/s]')
ylabel('Depth [m]')
title({'Effective Diffusion and Advection of Cunningham and Waddington'; [' U_1_0 = ' num2str(U10) ]})
legend('Diffusion','Advection','Location','SouthEast')

% subplot(2,1,2)
% plot(tort.*D(1).*ones(size(depths)),depths)
% hold all
% plot(U_thomas.*z_diff,depths,'o')
% xlabel('Coefficent of diffusion and advection [m^2/s]')
% ylabel('Depth [m]')
% title({'Effective Diffusion and Advection of Thomas model'; [' U_1_0 = ' num2str(U10) ]})
% legend('Diffusion','Advection','Location','SouthEast')

saveas(gcf,['Transport_U10_' num2str(U10) '.jpeg'])


