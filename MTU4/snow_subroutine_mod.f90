MODULE snow_subroutine_mod

  USE tropo_halo_Parameters
  USE tropo_halo_Precision
  USE tropo_halo_Global
  USE tropo_halo_Monitor
  USE snow_parameters
  USE snow_global

  IMPLICIT NONE

  PUBLIC :: snow_gas_nd_update
  PUBLIC :: snow_initialize
  PUBLIC :: JX
  PUBLIC :: JX_KPP_index
  PUBLIC :: calc_time_arrays
  PUBLIC :: check_day
  PUBLIC :: check_month
  PUBLIC :: check_year
  PUBLIC :: calc_depths
  PUBLIC :: snow_photo_efold
  PUBLIC :: tropo_halo_drvr
  PUBLIC :: snow_physics_drvr
  PUBLIC :: solve_tridiag
  PUBLIC :: molec_Dg
  PUBLIC :: molec_vel
  PUBLIC :: mean_free_path
  PUBLIC :: snow_windpumping
  PUBLIC :: snow_mass_transfer_coef
  PUBLIC :: update_snow_properties
  PUBLIC :: update_Henrys
  PUBLIC :: snow_aqueous_equilibrium
  PUBLIC :: snow_mass_transfer
  PUBLIC :: match_gas_aq
  PUBLIC :: snow_initialize_temperature
  PUBLIC :: snow_read_input
  PUBLIC :: snow_temperature

CONTAINS

!---------------------------------------------------------------

SUBROUTINE snow_initialize(ppx,nd,rj,Henrys,Ct,theta,QLL_thickness,snow_density,porosity,snow_radius,ssa,depths,L)

  !This function is used to set the intital conditions of chemical concentrations a default value for photolysis rates incase they are not calculated on-line, and sets the surface_pressure global variable. 
  !ppx - for gas phase the units are parts per part, for aqueous phase the units are molarity
  !nd - For both gas and aqueous phase the concentrations are in number density (molecules/cm3)

  IMPLICIT NONE

  REAL(dp),INTENT(inout) :: &
  ppx(:,:,:),&
  nd(:,:,:),&
  rj(:,:,:),&
  Ct(:,:),&
  theta(:,:),&
  QLL_thickness(:),&
  snow_density(:),&
  porosity(:),&
  snow_radius(:),&
  ssa(:),&
  L(:)
  REAL(dp),INTENT(in) :: Henrys(:,:),depths(:)
  INTEGER :: i,jk,jt 

  !Initializing global variable
  !Setting molecular weights in grams per mol
  mol_w(:)=DEFAULT_MOL_w
  mol_w(ind_NH3)=14.0+3.0
  mol_w(ind_RAD)=222.0
  mol_w(ind_CH3O2H)=12.0+4.0+2.0*16.0
  mol_w(ind_NO4n_aq)=14.0 +4.0*16.0
  mol_w(ind_NH3_aq)=14.0+3.0
  mol_w(ind_NH4p_aq)=14.0+ 4.0
  mol_w(ind_HCOOH)=12.0+ 2.0*16.0 + 2.0
  mol_w(ind_ACTA_aq)=60.0
  mol_w(ind_CO3n2_aq)=12.0+3.0*16.0
  mol_w(ind_ACTAn_aq)=59.9
  mol_w(ind_SO4)=32.0+4.0*16.0
  mol_w(ind_CH3O)=12.0+ 3.0+ 16.0
  mol_w(ind_H2O_aq)=2.0*16.0+1.0
  mol_w(ind_HNO3_aq)=14.0+ 3.0*16.0 +1.0
  mol_w(ind_HNO4_aq)=14.0 + 4.0*16.0 +1.0
  mol_w(ind_CO2_aq)=12.0 +2.0* 16.0
  mol_w(ind_ACTA)=60.0
  mol_w(ind_HO2n_aq)=2.0*16.0+1.0
  mol_w(ind_DMS)=62.0
  mol_w(ind_C2H6)=2.0*12.0+ 6.0
  mol_w(ind_C3H8)=3.0*12.0+8.0
  mol_w(ind_HOBr)=1.0+16.0+80.0
  mol_w(ind_HOCl)=1.0+16.0+35.5
  mol_w(ind_BrCl)=35.5+80.0
  mol_w(ind_H2O2)=2.0+2.0*16.0
  mol_w(ind_N2O5)=2.0*14.0+5.0*16.0
  mol_w(ind_HONO)=1.0+2.0*16.0+14.0
  mol_w(ind_Br)=80.0
  mol_w(ind_Cl)=35.5
  mol_w(ind_CH4)=12.0+4.0
  mol_w(ind_HNO4)=1.0+14.0+4.0*16.0
  mol_w(ind_PAN)=2.0*12.0+3.0+14.0
  mol_w(ind_ClO)=35.5+16.0
  mol_w(ind_BrO)=80.0+16.0
  mol_w(ind_HONO_aq)=1.0+2.0*16.0+14.0
  mol_w(ind_HCHO_aq)=2.0+12.0+16.0
  mol_w(ind_CH3OH_aq)=12.0 +4.0+16.0
  mol_w(ind_HCO3n_aq)=1.0+12.0+3.0*16.0
  mol_w(ind_O_aq)=16.0
  mol_w(ind_NO_aq)=14.0+16.0
  mol_w(ind_CH3O2H_aq)=12.0+4.0+2.0*16.0
  mol_w(ind_Hp_aq)=1.0
  mol_w(ind_CO)=12.0+16.0
  mol_w(ind_HCOOn_aq)=1.0+12.0+2.0*16.0
  mol_w(ind_O2_aq)=16.0*2.0
  mol_w(ind_OHn_aq)=16.0+1.0
  mol_w(ind_CO3n_aq)=12.0+3.0*16.0
  mol_w(ind_HNO3)=1.0+14.0+3.0*14.0
  mol_w(ind_HCOOH_aq)=12.0+2.0+2.0*16.0
  mol_w(ind_ISOP)=68.12
  mol_w(ind_O2n_aq)=2.0*16.0
  mol_w(ind_HO2_aq)=2.0*16.0+1.0
  mol_w(ind_H2O2_aq)=2.0+2.0*16.0
  mol_w(ind_NO2n_aq)=14.0+2.0*16.0
  mol_w(ind_O3_aq)=2.0*16.0
  mol_w(ind_NO2_aq)=14.0+2.0*14.0
  mol_w(ind_OH_aq)=16.0+1.0
  mol_w(ind_NO3_aq)=14.0 +3.0*14.0
  mol_w(ind_NO3n_aq)=14.0 + 3.0*14.0
  mol_w(ind_ROOH)=12.0+2.0+2.0*16.0
  mol_w(ind_O3)=3.0*16.0
  mol_w(ind_NO)=14.0+16.0
  mol_w(ind_HCHO)=2.0+12.0+16.0
  mol_w(ind_ALD2)=12.0+2.0+16.0
  mol_w(ind_NO3)=14.0+3.0*16.0
  mol_w(ind_HO2)=1.0+ 2.0*16.0
  mol_w(ind_OH)=16.0+ 1.0
  mol_w(ind_NO2)=14.0+ 2.0*16.0
  mol_w(ind_SO2)=32.0+2.0*16.0
  mol_w(ind_CHO2)=12.0+1.0+2.0*16.0

  surface_pressure=1.0   !Surface pressure set to 1 atmosphere
  rj(:,:,:)=1.0E-10      !Default value of photolysis rates
  CALL snow_gas_nd_update

  snow_density(1:NUM_ATM)=0.0

  DO jk=NUM_ATM+1,NUM_DEPTH+NUM_ATM
    snow_density(jk)=(NUM_DEPTH-(jk-2.0))/NUM_DEPTH*FRESH_SNOW_DENSITY+(jk-2.0)/NUM_DEPTH*FIRN_SNOW_DENSITY
    snow_density(jk)=(NUM_DEPTH-((jk-NUM_ATM)-1.0))/NUM_DEPTH*FRESH_SNOW_DENSITY+((jk-NUM_ATM)-1.0)/NUM_DEPTH*FIRN_SNOW_DENSITY
    snow_density(jk)=(NUM_DEPTH-((jk-NUM_ATM)-1.0))/NUM_DEPTH*FIRN_SNOW_DENSITY+((jk-NUM_ATM)-1.0)/NUM_DEPTH*ICE_DENSITY
    snow_density(jk)=(SNOW_DEPTH-abs(depths(jk)))/SNOW_DEPTH*(FRESH_SNOW_DENSITY+FIRN_SNOW_DENSITY)/2.0&
      + abs(depths(jk))/SNOW_DEPTH*ICE_DENSITY

    !snow_density(jk)=(SNOW_DEPTH-abs(depths(jk)))/SNOW_DEPTH*(FRESH_SNOW_DENSITY)&
    !+ abs(depths(jk))/SNOW_DEPTH*FIRN_SNOW_DENSITY

    snow_density(jk)=FIRN_SNOW_DENSITY
    snow_density(jk)=FRESH_SNOW_DENSITY

    !snow_density(jk)=(SNOW_DEPTH-abs(depths(jk)))/SNOW_DEPTH*0.2&
    !+ abs(depths(jk))/SNOW_DEPTH*ICE_DENSITY

    IF( depths(jk) .GT. -1.0*DENSITY_GRADIENT_REGION) THEN
      snow_density(jk)=(DENSITY_GRADIENT_REGION-abs(depths(jk)))/DENSITY_GRADIENT_REGION*(FRESH_SNOW_DENSITY)&
  +     abs(depths(jk))/DENSITY_GRADIENT_REGION*FIRN_SNOW_DENSITY
    ELSE
      snow_density(jk)=((SNOW_DEPTH)-abs(depths(jk)))/(SNOW_DEPTH-DENSITY_GRADIENT_REGION)&
  +   (FIRN_SNOW_DENSITY)+ abs(depths(jk)+DENSITY_GRADIENT_REGION)/(SNOW_DEPTH-DENSITY_GRADIENT_REGION)*ICE_DENSITY

    ENDIF

    !snow_density(jk)=FIRN_SNOW_DENSITY
    !print *,snow_density(jk)

  ENDDO

  !read(*,*)

  porosity(1:NUM_ATM)=1.0
  ssa(1:NUM_ATM)=0.0
  snow_radius(1:NUM_ATM)=0.0

  DO jk=NUM_ATM+1, NUM_DEPTH+NUM_ATM
    porosity(jk)        = 1.-(snow_density(jk)/ICE_DENSITY)          ! porosity of snow
    ssa(jk)             =  -308.2*LOG(snow_density(jk))-205.96       ! Specific Surface Area (SSA) [cm2 g-1], f(rhoasnow) -308.2*LOG(rhoasnow(jk))-205.96
    IF (ssa(jk).LT.01.0) ssa(jk)=01.0                                ! ESS_km_20130308 added a minimum SSA for high density snow
    snow_radius(jk)          = (1.e-2)*6./(ICE_DENSITY*ssa(jk))/2.0  ! grain diameter [m]

    !IF (snow_density(jk).GT.0.0) snow_radius(jk)          = (1.e-2)*3./(snow_density(jk)*ssa(jk))

  ENDDO

  ! MAQ_lg_20140812+ calculation of the QLL
  QLL_thickness(:)=0.0

  DO jk=NUM_ATM+1, NUM_DEPTH+NUM_ATM
    IF( depths(jk) .GT. -1.0*DENSITY_GRADIENT_REGION) THEN
       QLL_thickness(jk)=10.0**((DENSITY_GRADIENT_REGION-abs(depths(jk)))/DENSITY_GRADIENT_REGION*LOG10(MAX_QLL)&
  +      abs(depths(jk))/DENSITY_GRADIENT_REGION*LOG10(MIN_QLL))
    ELSE
       QLL_thickness(jk)=MIN_QLL
    ENDIF
    ! MAQ_lg_20140812+ here the QLL is simply set at the maximum value of 3e-8m; reason???
    QLL_thickness(jk)=MAX_QLL
    !print *, QLL_thickness(jk),depths(jk)
  ENDDO

  !read(*,*)

  theta(:,:)=0.0
  DO jk=1, NUM_DEPTH+NUM_ATM
    IF (snow_radius(jk).GT.0.0) theta(jk,:)=1.0/(1.0 + (snow_radius(jk)-QLL_thickness(jk))**3.0/&
   +   (snow_radius(jk)**3.0-(snow_radius(jk)-QLL_thickness(jk))**3.0)*ICE_DENSITY/WATER_DENSITY)
  ENDDO

  DO jk=1, NUM_DEPTH+NUM_ATM
    IF (porosity(jk).GE.1.0 .OR. porosity(jk).LE.0.0) THEN
      L(jk)=0.0
    ELSE
      L(jk)=((1.0-porosity(jk))*(1.0-(((snow_radius(jk)-QLL_thickness(jk))**3)/(snow_radius(jk)**3))))/porosity(jk)
    ENDIF
  ENDDO

  DO jt=1, NSPEC
    IF (is_aqueous(jt)) THEN
      ppx(:,jt,:)=DEFAULT_MIN_M
      nd(:,jt,:)=DEFAULT_MIN_ND
    ELSE
      ppx(:,jt,:)=DEFAULT_MIN_PPX
      nd(:,jt,:)=DEFAULT_MIN_ND
    ENDIF
  ENDDO

  !Setting initial concentrations of gas species
  ppx(1:NUM_ATM,ind_O3,:)=10.e-9_dp
  ppx(:,ind_CO,:)=80.e-9_dp
  ppx(:,ind_CH4,:)=1800.e-9_dp
  ppx(1:NUM_ATM,ind_NO,:)=002.0e-12_dp
  ppx(1:NUM_ATM,ind_NO2,:)=002.02e-12_dp
  !ppx(NUM_ATM+1:NUM_ATM+NUM_DEPTH,ind_NO2,:)=001000.02e-12_dp
  ppx(:,ind_ISOP,:)=0.2e-9_dp
  ppx(:,ind_NH3,:)=0.005e-12_dp
  ppx(:,ind_Br,:)=01.00e-12_dp
  ppx(:,ind_BrO,:)=01.00e-12_dp
  ppx(:,ind_HOBr,:)=01.00e-12_dp
  ppx(:,ind_SO4,:)=01.00e-9_dp
  ppx(1:NUM_ATM,ind_H2O2,:)=02.00e-9_dp
  ppx(1:NUM_ATM,ind_HNO4,:)=01.00e-12_dp
  ppx(:,ind_SO2,:)=05.00e-9_dp
  ppx(1:NUM_ATM,ind_NO3,:)=0.100e-12_dp
  ppx(:,ind_O2,:)=02.00e-1_dp
  ppx(:,ind_H2O,:)=0500.00e-9_dp
  ppx(:,ind_CO2,:)=05.00e-4_dp
  ppx(:,ind_H2,:)=0500.00e-9_dp
  ppx(:,ind_Cl,:)=02.00e-12_dp
  ppx(:,ind_ClO,:)=02.00e-12_dp
  ppx(:,ind_HOCl,:)=02.00e-12_dp
  ppx(:,ind_HCOOH,:)=1000.0E-12_dp
  ppx(:,ind_ACTA,:)=5000.0E-12_dp

  !ppx(:,ind_HONO,:)=100.0E-12_dp
  !ppx(:,ind_PAN,:)=1000.0E-12_dp
  !ppx(:,ind_MCO3,:)=10000.0E-12_dp
  !ppx(NUM_ATM+1:NUM_ATM+NUM_DEPTH,ind_HNO3,:)=050000.5200E-12!0.0300E-12!400.00E-12
  !ppx(NUM_ATM+1:NUM_ATM+NUM_DEPTH,ind_HONO,:)=0040.00E-12!20.0E-12!80000.0E-12
  !ppx(NUM_ATM+1:NUM_ATM+NUM_DEPTH,ind_HONO,:)=100.0E-12!2500.0E-12!0500.00E-12!20.0E-12!80000.0E-12
  !ppx(1:NUM_ATM,ind_HNO4,:)=100e-12
  !ppx(1:NUM_ATM,ind_NO3,:)=12000E-12

  DO jk=NUM_ATM+1, NUM_ATM+NUM_DEPTH

    IF ( depths(jk).LT.-0.10000) THEN
      ppx(jk,ind_HNO4,:)=600.0E-12!65000E-12
    ENDIF

    IF ( depths(jk).LT.-0.01000) THEN
      !ppx(jk,ind_N2O5,:)=4000.0E-12
      !ppx(jk,ind_HNO3,:)=040.705200E-12!0.0300E-12!400.00E-12
      !ppx(jk,ind_HONO,:)=0100.705200E-12!0.0300E-12!400.00E-12
    ENDIF

  ENDDO
  
  !ppx(NUM_ATM+1:NUM_ATM+NUM_DEPTH,ind_NO,:)=600.0E-12
 
  !Setting inital concentrations in aqueous phase
  !ppx(:,ind_HNO3_aq,:)=5.0E-9
  ppx(:,ind_Hp_aq,:)=1.0E-7
  !ppx(:,ind_Nap_aq,:)=1.0E-4

  !ppx(:,ind_NO3n_aq,:)=1.0E-2
  !ppx(:,ind_NO4n_aq,:)=1.0E-2

  !converting concentrations in ppx to nd

  DO jt=1,NSPEC

    IF (is_aqueous(jt)) THEN
      nd(:,jt,:)=ppx(:,jt,:)*M2ND
    ELSE
      nd(:,jt,:)=ppx(:,jt,:)*gas_nd(:,:)
    ENDIF

  ENDDO

  !set Aqueous species in Equilibrium with gas phase

  DO jt=1,NSPEC
    IF (is_aqueous(jt)) THEN
      IF (aq_gas_index(jt,1).NE.0) THEN
	DO i=1, NUM_TIME_SAVE
          nd(:,jt,i)=nd(:,aq_gas_index(jt,1),i)*Henrys(:,jt)
          ppx(:,jt,i)=nd(:,jt,i)/M2ND
	ENDDO
      ENDIF
    ENDIF
  ENDDO

  !ppx(:,ind_NO2n_aq,:)=1.0E-4
  !nd(:,ind_NO2n_aq,:)=ppx(:,ind_NO2n_aq,:)*M2ND

  Ct(:,:)=0.0
  DO jk=1, NUM_DEPTH+NUM_ATM
    DO jt=1,NSPEC
      IF(is_ion(jt,1).EQ.1) Ct(jk,:)=Ct(jk,:)+ppx(jk,jt,NUM_TIME_SAVE)
    ENDDO
  ENDDO

  !print *,ppx(:,ind_HONO_aq,NUM_TIME_SAVE)
  !print *,nd(:,ind_HONO_aq,NUM_TIME_SAVE)
  !read(*,*)

ENDSUBROUTINE snow_initialize

!---------------------------------------------------------------

SUBROUTINE JX(IDAY,MONTH,GMTAU,YLAT,XLNG,PSURF,ALBEDO,rj)

  ! This subroutine uses Fast-JX subroutines to calculate photolysis rates and is based off the standalone drvr supplied with the download package

  USE FJX_CMN_MOD
  USE FJX_SUB_MOD
  USE FJX_INIT_MOD

  implicit none

  real*8, parameter::  PI180 = 3.141592653589793d0/180.d0
  real*8, parameter::  MASFAC=100.d0*6.022d+23/(28.97d0*9.8d0*10.d0)

  real*8, dimension(L1_) :: ETAA,ETAB, RI,TI,CLDP,AER1,AER2
  real*8 XGRD,YGRD
  real*8  SCALEH
  real*8, INTENT(in) :: GMTAU,ALBEDO, XLNG,YLAT,PSURF
  real*8, INTENT(inout) :: rj(:,:,:)
  integer, dimension(L1_):: NCLD,NAA1,NAA2
  integer,INTENT(in) ::  MONTH, IDAY
  integer J,K,L


  !-----------------------------------------------------------------------

  !--------------------key params sent to fast JX-------------------------
  !---SZA = solar zenith angle, U0 = cos(SZA)
  !---SOLF = solar flux factor for sun-earth distance
  !---REFLB = Lambertian reflectivity at the Lower Boundary
  real*8                    :: U0,SZA,REFLB,SOLF
  !---ZPJ(layer, JV#) = 2D array of J-values, indexed to CTM chemistry code
  real*8, dimension(JVL_,JVN_)::   ZPJ
  !---turn internal PHOTO_JX print (unit=6) on/off
  logical                     :: LPRTJ
  !---Independent Column Atmosphere data passed to PHOTO_JX:
  !--- P = edge press (hPa), Z = edge alt (cm), T = layer temp (K)
  !--- D = layer dens-path (# molec /cm2), O = layer O3 path (# O3 /cm2)
  !---
  !--- R = layer rel.hum.(fraction)
  !--- CLDWP = cloud water path (g/m2), AERSP = aerosol path (g/m2)
  !--- NDXCLD = cld index type (liq & size, ice & scatt-phase)
  !--- NDXAER = aerosol index type
  !--- NB. clouds have only a single type within an ICA layer, pick liquid or ice
  !---     aerosols are dimensioned with up to AN_ different types in an ICA layer
  real*8,  dimension(L1_+1)  :: PPP,ZZZ
  real*8,  dimension(L1_  )  :: TTT,DDD,RRR,OOO,LWP,IWP,REFFL,REFFI
  real*8,  dimension(L1_,AN_):: AERSP
  integer, dimension(L1_  )  :: NDXCLD
  integer, dimension(L1_,AN_):: NDXAER

  real*8        PCLD,IWC,FACTOR

  !---these are the key results coming out of fast-JX core
  !---they need to be dimensioned here and passed to fast-JX to fill.
  integer, parameter             ::  NJX_ = 100
  character*6, dimension(NJX_)   ::  TITLJXX
  real*8, dimension(L_,NJX_)     ::  VALJXX
  integer :: NJXX

  !-----------------------------------------------------------------------
  !---fast-JX:  INIT_JX is called only once to read in and store all fast-JX data:
  call INIT_FJX (TITLJXX,NJX_,NJXX)
  !-----------------------------------------------------------------------


  !-------READ IN test atmosphere:
  open(1,file='FJX_test.dat',status='old')
  read(1,*)
  read(1,*) !IDAY
  read(1,*) !MONTH
  read(1,*) !GMTAU
  read(1,*) !YLAT
  read(1,*) !XLNG
  YGRD = YLAT*PI180
  XGRD = XLNG*PI180
  read(1,*) !PSURF
  read(1,*) !ALBEDO
  read(1,*)
  !---for L_=57-layer EC - top layer, L_+1, does not use cloud and aerosol data
  do L=1,L_+1
  read(1,'(i3,1x,2f11.6,2x,2f5.1,f7.3,i4,f7.3,i4,f7.3,i4)')     &
  J,ETAA(L),ETAB(L),TI(L),RI(L),CLDP(L),NCLD(L),              &
  AER1(L),NAA1(L),AER2(L),NAA2(L)
  enddo
  close(1)



  !-----------------------------------------------------------------------
  !---fast-JX:  SOLAR_JX is called only once per grid-square to set U0, etc.

  call SOLAR_JX(GMTAU,IDAY,YGRD,XGRD, SZA,U0,SOLF)
  !print *,SZA,GMTAU,IDAY,YGRD,XGRD,U0,SOLF
  !-----------------------------------------------------------------------

  !write(6,'(a,f8.3,f8.5)')'solar zenith angle, solar-f',SZA,SOLF
  !write(6,'(a,f8.3,f8.3)') 'lat/lng',YLAT,XLNG

  do L = 1,L1_
  PPP(L) = ETAA(L) + ETAB(L)*PSURF
  enddo

  !-----------------------------------------------------------------------
  !---fast-JX:  ACLIM_FJX sets up climatologies for O3, T, Density and Z.

  call ACLIM_FJX (YLAT, MONTH, PPP,TTT,ZZZ,DDD,OOO, L1_)
  !-----------------------------------------------------------------------

  !-----------------------------------------------------------------------
  !      call JP_ATM0(PPP,TTT,DDD,OOO,ZZZ, L_)
  !-----------------------------------------------------------------------

  !---load CTM-based ICA atmospheric column data on top of climatology (1:L_)
  !--e.g.
  !---to convert kg (STT) in grid cell w/AREA (m2) to # molecules/cm^2
  !      D_O3(I,J,L) = 6.023d26*STT(I,J,L,1)/48.d0  *1.d-4 /AREAXY(I,J)
  !---to convert kg (STT) in grid cell w/AREA (m2) to PATH (g/m^2)
  !      P_AERSL(I,J,L) = STT(I,J,L,1)*1.d-3/AREAXY(I,J)


  LWP(:)  =  0.d0
  IWP(:)  =  0.d0
  REFFL(:) = 0.d0
  REFFI(:) = 0.d0
  AERSP(:,:)  = 0.d0
  NDXAER(:,:) = 0
  do L = 1,L1_
  PPP(L) = ETAA(L) + ETAB(L)*PSURF
  enddo
  do L = 1,L_
  TTT(L) = TI(L)
  RRR(L) = RI(L)
  if (TTT(L) .gt. 253.d0) then
  LWP(L) = CLDP(L)
  else
  IWP(L) = CLDP(L)
  endif
  NDXAER(L,1) = NAA1(L)
  AERSP(L,1)  = AER1(L)
  NDXAER(L,2) = NAA2(L)
  AERSP(L,2)  = AER2(L)
  enddo
  ZZZ(1) = 0.d0
  do L = 1,L_
  DDD(L)  = (PPP(L)-PPP(L+1))*MASFAC
  SCALEH      = 1.3806d-19*MASFAC*TTT(L)
  ZZZ(L+1) = ZZZ(L) -( LOG(PPP(L+1)/PPP(L)) * SCALEH )
  !print *, ZZZ(L+1)
  enddo
  ZZZ(L1_+1) = ZZZ(L1_) + 5.d5
  REFLB = ALBEDO
  LPRTJ = .true.
  !read(*,*)
  !>>>R-effective of clouds determined by main code, not FJX
  !   REFF determined by user - some recommendations below
  !       REFFI is a simple function of ice water content IWC (g/m3, 0.0001 to 0.1)
  !          IWC = IWP / delta-Z (of layer in m, approx OK)
  !   Heymsfield++ 2003 JAM, log-log fit ext(/m) vs. IWC, Fig B1a, p.1389
  !              EXT (/m) = 1.7e-3 * (IWC/0.1)**0.77
  !          REFFI = 164. * IWC**0.23     (33 microns at 0.001 --- 164 at 1.0)
  !          REFFL is a simple function of pressure (PCLD):
  !            FACTOR = (PCLD - 610.) / 200.
  !            FACTOR = min(1.0, max(0.0, FACTOR))
  !          REFFL = 9.60*FACTOR + 12.68*(1.-FACTOR)
  do L = 1,L_
  if (IWP(L) .gt. 1.d-5) then
  IWC = IWP(L) *100.d0 / (ZZZ(L+1)-ZZZ(L))
  IWC = max(0.001d0, IWC)
  REFFI(L) = 164.d0 * IWC**0.23d0
  !       write(6,'(a,i3,3f10.4)') 'ICE:',L,IWP(L),IWC,REFFI(L)
  endif
  if (LWP(L) .gt. 1.d-5) then
  PCLD = 0.5d0*(PPP(L)+PPP(L+1))
  FACTOR = min(1.d0, max(0.d0, (PCLD-610.d0)/200.d0))
  REFFL(L) = 9.60d0*FACTOR + 12.68d0*(1.-FACTOR)
  endif
  enddo

          call JP_ATM0(PPP,TTT,DDD,OOO,ZZZ, L_)

  !-----------------------------------------------------------------------

  !-----------------------------------------------------------------------
  !---fast-JX:  PHOTO_JX is called once per ICA, calculates all J-values

  call PHOTO_JX                                                 &
  (U0,SZA,REFLB,SOLF, LPRTJ, PPP,ZZZ,TTT,DDD,RRR,OOO,         &
  LWP,IWP,REFFL,REFFI,AERSP,NDXAER,L1_,AN_,       VALJXX,NJX_)
  !-----------------------------------------------------------------------

  !---map the J-values from fast-JX onto CTM (ZPJ) using JIND & JFACTA
  do L = 1,L_
  do J = 1,NRATJ
  if (JIND(J).gt.0) then
  ZPJ(L,J) = VALJXX(L,JIND(J))*JFACTA(J)
  print *,VALJXX(L,JIND(J)),JFACTA(J),J
  read(*,*)
  else
  ZPJ(L,J) = 0.d0
  endif
  enddo
  enddo


  !---Printout J's:
  !write(6,'(a)')
  !write(6,'(a)') ' >>>>CTM J-values taken from fast-JX----'
  !write(6,'(a,i3,a,f4.1,a,f7.2,a,2f7.2,a,f8.5)')                &
  !'  Day=',IDAY,' UT(hr)=',GMTAU,'  SZA=',SZA,                  &
  !'  LAT x LONG=',YLAT,XLNG,' SolarFlx=',SOLF
  !write(6,'(1x,a,72(i6,3x))') ' ',(K, K=1,NRATJ)
  !write(6,'(1x,a,72(a6,3x))') 'L=  ',(JLABEL(K), K=1,NRATJ)
  !do L=JVL_,1,-1
  !write(6,'(i3,1p, 72e9.2)') L,(ZPJ(L,K),K=1,NRATJ)
  !enddo
  !print *,VALJXX(1,:)
  !read(*,*)
  CALL JX_KPP_index(VALJXX,rj)

END SUBROUTINE JX

!---------------------------------------------------------------

SUBROUTINE JX_KPP_index(JX_rj,rj)

  !This subroutine takes the JX photolysis rates and places them into the correct indices of the KPP model. Indices for FAST-JX can be found in from execution of the FAST-JX code
  real*8, INTENT(in) :: JX_rj(:,:)
  real*8, INTENT(inout) :: rj(:,:,:)

  INTEGER :: jt

  rj(:,ind_O3,1)     = JX_rj(1,3)
  rj(:,ind_H2O2,1)   = JX_rj(1,7)
  rj(:,ind_NO2,1)    = JX_rj(1,9)
  rj(:,ind_NO3,1)    = JX_rj(1,12)
  rj(:,ind_N2O5,1)   = JX_rj(1,11)
  rj(:,ind_HNO3,1)   = JX_rj(1,13)
  rj(:,ind_CH3O2H,1) = JX_rj(1,8)
  rj(:,ind_HCHO,1)   = JX_rj(1,6)
  rj(:,ind_HNO4,1)   = JX_rj(1,14)
  rj(:,ind_HONO,1)   = JX_rj(1,12)
  rj(:,ind_HOBr,1)   = JX_rj(1,24)
  rj(:,ind_HOCl,1)   = JX_rj(1,18)
  rj(:,ind_HOBr_aq,1)   = JX_rj(1,24)
  rj(:,ind_HOCl_aq,1)   = JX_rj(1,18)
  rj(:,ind_BrCl,1)   = JX_rj(1,25)
  rj(:,ind_BrCl_aq,1)   = JX_rj(1,25)
  rj(:,ind_O3_aq,1)  = JX_rj(1,3)!*100.0
  rj(:,ind_H2O2_aq,1) = JX_rj(1,7)
  rj(:,ind_NO3n_aq,1)= JX_rj(1,63)!/100.0!*75.0
  rj(:,ind_NO3n_aq,2)= JX_rj(1,64)!/100.0!*75.0
  rj(:,ind_NO2n_aq,1)= JX_rj(1,65)!/100.0!*75.0
  rj(:,ind_NO3_aq,1)= JX_rj(1,66)
  rj(:,ind_NO3_aq,2)= JX_rj(1,67)
  rj(:,ind_ALD2,1)=JX_rj(1,5) !?maybe?
  rj(:,ind_ALD2,2)= JX_rj(1,6)
  rj(:,ind_Cl2,1)= JX_rj(1,17)
  rj(:,ind_Cl2_aq,1)= JX_rj(1,17)
  rj(:,ind_Br2,1)= JX_rj(1,68)
  rj(:,ind_Br2_aq,1)= JX_rj(1,68)
  rj(:,ind_BrO,1)=JX_rj(1,22)
  rj(:,ind_ClO,1)=JX_rj(1,21)
  rj(:,ind_NO2_aq,1)=JX_rj(1,9)
  rj(:,ind_BrNO3,1)=JX_rj(1,23)
  rj(:,ind_ClNO3,1)=JX_rj(1,16)

  DO jt=1, NSPEC
  !if (is_aqueous(jt)) rj(NUM_ATM+1:NUM_ATM+NUM_DEPTH,jt,:)=rj(NUM_ATM+1:NUM_ATM+NUM_DEPTH,jt,:)*1.0E-3

  ENDDO

END SUBROUTINE JX_KPP_index

!---------------------------------------------------------------

SUBROUTINE calc_time_arrays(start_time,year, month,day, hour, j_day)

  ! This Function is responsible for creating arrays containing the time information 
  ! for every time step

  USE tropo_halo_Precision
  USE snow_parameters

  CHARACTER(*) , INTENT (in) :: start_time  !Start date in form of YYYYMMDDHH
  REAL(dp), INTENT (inout) :: hour(:)
  INTEGER, INTENT (inout) :: j_day(:),month(:),year(:),day(:)
  INTEGER :: dummy_int
  CHARACTER(100) STRING, dummy_string
  CHARACTER(10) :: format_int
  DATA format_int /'(i10)'/
  CHARACTER(10) :: format_real
  DATA format_real /'(F10.4)'/
  INTEGER :: i

  ! MAQ_lg_20180321+
  ! print *,'snow_subroutine_mod.f90: calc_time_arrays'
  ! print *,'give ENTER to continue' 
  ! read (*,*)
  ! MAQ_lg_20180321-


  !Change the start date from characters to numbers
  read(start_time(1:4),format_int) year(1)
  read(start_time(5:6),format_int) month(1)
  read(start_time(7:8),format_int) day(1)
  read(start_time(9:10),format_int) dummy_int
  hour(1)=real(dummy_int)

  j_day(1)=day(1)
  DO i=1, month(1)-1
    j_day(1)=j_day(1)+ndaymonth(i)
  ENDDO

  DO i=2,NUM_TIME
    hour(i)=hour(i-1)+TIME_STEP*SEC2HRS  !hour is in hours, TIME_STEP is in seconds,
    CALL check_day(day,hour,j_day,i)
    CALL check_month(month,day,i)
    CALL check_year(year,month,j_day,i)
  ENDDO

  ! MAQ_lg_20180321+
  ! print *,'snow_subroutine_mod.f90: calc_time_arrays, enddo',day,hour,j_day,year, month
  ! print *,'give ENTER to continue' 
  ! read (*,*)
  ! MAQ_lg_20180321-

END SUBROUTINE calc_time_arrays

!---------------------------------------------------------------

SUBROUTINE snow_gas_nd_update
!This subroutine calculates the number density of air in molecules per cm3 and stores it in the global variable gas_nd

gas_nd(:,:)=surface_pressure*AV_N/(R_gas*temperature(:,:))


END SUBROUTINE snow_gas_nd_update

!---------------------------------------------------------------

SUBROUTINE check_day(day,hour,j_day,i)

  !This routine checks to see if the current time step is the next day and changes the time arrays accordingly

  REAL(dp), INTENT(inout)::hour(:)
  INTEGER, INTENT(inout)::day(:),j_day(:)
  INTEGER, INTENT(IN):: i
  LOGICAL :: check_again
  check_again=.TRUE.

  j_day(i)=j_day(i-1)
  day(i)=day(i-1)

  DO WHILE (check_again)
    !If statement to change days when needed
    IF (hour(i).GT.DAY2HRS) THEN
      hour(i)=hour(i)-DAY2HRS
      j_day(i)=j_day(i)+1
      day(i)=day(i)+1
      check_again=.TRUE.
    ELSE
      check_again=.FALSE.
    ENDIF
  END DO

END SUBROUTINE check_day

!---------------------------------------------------------------

SUBROUTINE check_month(month,day,i)

  !This routine checks to see if the current time step is the next month and changes the time arrays accordingly

  INTEGER, INTENT(inout)::day(:),month(:)
  INTEGER, INTENT(IN):: i
  LOGICAL :: check_again
  check_again=.TRUE.

  month(i)=month(i-1)

  DO WHILE (check_again)
    !IF statment to change months when needed
    IF (day(i).GT.ndaymonth(month(i-1))) THEN
      day(i)=day(i)-ndaymonth(month(i-1))
      month(i)=month(i)+1
      check_again=.TRUE.
    ELSE
      check_again=.FALSE.
    ENDIF
  END DO

END SUBROUTINE check_month

!---------------------------------------------------------------

SUBROUTINE check_year(year,month,j_day,i)

   !This routine checks to see if the current time step is the next year and changes the time arrays accordingly

   INTEGER, INTENT(inout)::year(:),month(:),j_day(:)
   INTEGER, INTENT(IN):: i
   LOGICAL :: check_again
   check_again=.TRUE.

   year(i)=year(i-1)

   DO WHILE (check_again)
     !IF statement to change years when needed
     IF (month(i).GT.YRS2MON) THEN
       month(i)=month(i)-YRS2MON
       j_day(i)=j_day(i)-YRS2DAY 
       year(i)=year(i)+1
       check_again=.TRUE.
     ELSE
       check_again=.FALSE.
     ENDIF
   END DO

END SUBROUTINE check_year

!---------------------------------------------------------------

SUBROUTINE snow_photo_drvr(day,month,GMTAU,YLAT,XLNG,PSURF,ALBEDO,rj,edge_depths, obs_irr,model_irr)

  !This routine is the driver of the photolysis rate calculations calling FAST-JX code to calculate photolysis rates and using routine
  !snow_photo_efold to adjust the photolysis rates for depth in the snow.


  INTEGER, INTENT(in) :: day,month
  REAL(dp), INTENT(in) :: &
      GMTAU, &     !GMT time
      YLAT, &     !Latitiude
      XLNG, &     !Longitude
      PSURF, &  !Pressure in hPascal
      ALBEDO, &     !Surface albedo
  edge_depths(:),&    !Depths of the edge of the layers
  obs_irr             !observed irradiance
  REAL(dp), INTENT(inout) :: rj(:,:,:), model_irr

  INTEGER :: i, jt,jk,jjk

  !print *,day,month,GMTAU,YLAT,XLNG,PSURF,ALBEDO,edge_depths
  !read(*,*)

  CALL JX(day,month,GMTAU,YLAT,XLNG,PSURF,ALBEDO,rj)
  CALL snow_photo_efold(rj,edge_depths)
  CALL photo_clearsky_comp(obs_irr, rj ,month, day, GMTAU-GMT_DIFF,model_irr)

  !DO jk=1, NUM_DEPTH+NUM_ATM
  !    DO jt=1, NSPEC
  !      DO jjk=1, MAX_J
  !        IF (rj(jk,jt,jjk) .LT. 1.0E-15) rj(jk,jt,jjk)=1.0E-15
  !      ENDDO
  !    ENDDO
  !ENDDO


END SUBROUTINE snow_photo_drvr

!---------------------------------------------------------------

SUBROUTINE snow_photo_efold(rj,edge_depths)

  !This routine adjusts photolysis rates for depth in the snow based upon a given e-fold depth.  
  !There is a set default value but indivdual values for each species can be set
  REAL(dp), INTENT(inout) :: rj(:,:,:)
  REAL(dp), INTENT(in) :: edge_depths(:)

  REAL(dp), DIMENSION(:), ALLOCATABLE ::&
      e_fold
  REAL(dp), DIMENSION(:,:), ALLOCATABLE ::&
      unity       !Holds a value of 0-1 reflecting the percentage of the surface photolysis rate at a given depth for a species

  INTEGER :: i,ii,j

  ALLOCATE(e_fold(NSPEC))
  ALLOCATE(unity(NUM_DEPTH+NUM_ATM,NSPEC))
  !Default efold depths in meter
  e_fold(:)=DEFAULT_EFOLD

  !e_fold(ind_NO3n_aq)=0.04!0.15 !Galbavy2007
  !e_fold(ind_HNO3_aq)=e_fold(ind_NO3n_aq)
  !e_fold(ind_HNO3)=e_fold(ind_NO3n_aq)

  e_fold(ind_O3)=0.15
  e_fold(ind_NO2)=0.25
  e_fold(ind_NO2_aq)=e_fold(ind_NO2)
  e_fold(ind_H2O2_aq)=0.133 !Galbavy2007
  e_fold(ind_H2O2)=0.133 !Galbavy2007
  e_fold(ind_NO2n_aq)=0.163  !Galbavy2007

  !e_fold(ind_HONO_aq)=e_fold(ind_NO2n_aq)
  !e_fold(ind_HONO)=e_fold(ind_NO2n_aq)

  e_fold(ind_HNO4)=0.01

  !e_fold(ind_N2O5)=0.01

  unity(:,:)=1.0
  DO j=1, MAX_J
    DO i=NUM_ATM+1, NUM_DEPTH+NUM_ATM
      ii=i+1
      unity(i,:)=e_fold/(edge_depths(i)-edge_depths(ii))*(EXP(edge_depths(i)/e_fold)-EXP(edge_depths(ii)/e_fold))
      rj(i,:,j)=unity(i,:)*rj(i,:,j)
      !print *,i,NUM_ATM,edge_depths(i),edge_depths(ii),edge_depths(NUM_ATM+1)
    ENDDO
  ENDDO
  !read(*,*)

  DEALLOCATE(e_fold)
  DEALLOCATE(unity)

END SUBROUTINE snow_photo_efold

!---------------------------------------------------------------

SUBROUTINE calc_depths(depths,edge_depths)

  ! This subroutine creates arrays contiainng the cetroid of the layers and the edge depths of the model
  ! Currently this subroutine generates depths that are "thinner" near the surface of the snowpack and thicker deep down

  REAL(dp), INTENT(inout):: depths(:),edge_depths(:)
  REAL(dp) :: DEPTH_STEP, ATM_STEP
  INTEGER :: i, ii,counter,counter_atm

  counter=0
  DO i=1, NUM_DEPTH
    counter=counter+i
  ENDDO

  counter_atm=0
  DO i=1, NUM_ATM
    counter_atm=counter_atm+i
  ENDDO

  DEPTH_STEP=SNOW_DEPTH/counter
  ATM_STEP=ABL_DEPTH/counter_atm
  !DEPTH_STEP=SNOW_DEPTH/NUM_DEPTH

  edge_depths(NUM_DEPTH+NUM_ATM+1)=-SNOW_DEPTH
  edge_depths(1)=ABL_DEPTH
  DO i= 2, NUM_DEPTH+NUM_ATM
    ii=i-1
    IF (i.LE.NUM_ATM) THEN

      !edge_depths(i)=ABL_DEPTH-ABL_DEPTH/NUM_ATM*(i-1)

      edge_depths(i)=edge_depths(ii)-(NUM_ATM-ii+1)*ATM_STEP
      depths(ii)=(edge_depths(i)+edge_depths(ii))/2.0

      !ELSEIF (i.EQ.2.) THEN

      !edge_depths(i)=0.0
      !depths(ii)=ABL_DEPTH/2.0

    ELSEIF(i.EQ.NUM_ATM+1) THEN
      edge_depths(i)=0.0
      depths(ii)=(edge_depths(i)+edge_depths(ii))/2.0

    ELSE

      !edge_depths(i)=edge_depths(ii)-(ii-1)*DEPTH_STEP
      edge_depths(i)=edge_depths(ii)-(ii-NUM_ATM)*DEPTH_STEP
      !edge_depths(i)=edge_depths(ii)-DEPTH_STEP
      depths(ii)=(edge_depths(ii)+edge_depths(i))/2.0
    ENDIF

  ENDDO
  depths(NUM_DEPTH+NUM_ATM)=(edge_depths(NUM_DEPTH+NUM_ATM)+edge_depths(NUM_DEPTH+NUM_ATM+1))/2.0

  !print *,'depths',depths
  !print *,edge_depths
  !read(*,*)

END SUBROUTINE calc_depths

!---------------------------------------------------------------

SUBROUTINE tropo_halo_drvr(timeday,nd,ppx,rj,tslm1,Henrys,chem_rates,snow_radius,kt,L )

  !This function is based off the standalone KPP driver and is the chemical driver for the  model

  !USE tropo_halo_Model
  !USE tropo_halo_Initialize, ONLY: Initialize

  REAL(dp), INTENT(inout):: nd(:,:,:), ppx(:,:,:),chem_rates(:,:),kt(:,:)   !Species concetration above and in the snow and their chemical rates
  REAL(dp), INTENT(in):: rj(:,:,:),&!Photolysis rates in snow
     tslm1(:,:),&     !Temperature
     timeday, &       !Time of day in decimalhour
     Henrys(:,:),&    !Henrys law constants
     snow_radius(:),&
     L(:)
  REAL(dp), DIMENSION(:), ALLOCATABLE :: &
     RPROD

  REAL(kind=dp) :: T, DVAL(NSPEC)! Time and rate of change variable
  REAL(kind=dp) :: RSTATE(20)!Integrator variable
  INTEGER :: i,jk,jjk,jt,jl

  ALLOCATE(RPROD(NREACT))

  !CALL Initialize()

  !~~~> Initialization
  CALL snow_gas_nd_update

  !CALL snow_sun_efold(depths,e_fold)

  !Place chemical species into variables used by KPP integrator
  !!$OMP  PARALLEL DO                &
  !!$OMP  DEFAULT(private)           ! &
  !!$OMP  SHARED(nd,chem_rates,tslm1,rj,timeday)
  !!$OMP  PRIVATE(jt,C,KPP_rj,TEMP,VAR,FIX,RCONST,TIME,SUN,TSTART,TEND,DT,ATOL,RTOL,STEPMIN,STEPMAX,CFACTOR,DDMTYPE)

  IF (CHEM_MT) THEN
    DO jl=1, NUM_SUBSTEP_CHEM_MT

      DO jk=1,NUM_DEPTH+NUM_ATM

        call parallel_chem(nd(jk,:,NUM_TIME_SAVE), tslm1(jk,NUM_TIME_SAVE),&
          rj(jk,:,:),chem_rates(jk,:),timeday,kt(jk,:),Henrys(jk,:),L(jk))

        DO jt=1,NSPEC
          IF (isnan(nd(jk,jt,NUM_TIME_SAVE))) nd(jk,jt,NUM_TIME_SAVE)=DEFAULT_MIN_ND
          IF (nd(jk,jt,NUM_TIME_SAVE).LT.DEFAULT_MIN_ND) nd(jk,jt,NUM_TIME_SAVE)=DEFAULT_MIN_ND

          IF (is_aqueous(jt)) THEN
             ppx(jk,jt,:)=nd(jk,jt,:)/M2ND
          ELSE
             ppx(jk,jt,:)=nd(jk,jt,:)/gas_nd(jk,:)
          ENDIF

        ENDDO

      ENDDO

      call snow_aqueous_equilibrium(ppx,nd,tslm1(:,NUM_TIME_SAVE))
      call snow_mass_transfer_coef(tslm1(:,NUM_TIME_SAVE),snow_radius,kt)
      call snow_mass_transfer(tslm1(:,NUM_TIME_SAVE),snow_radius,kt,L,nd,ppx,Henrys)
      call snow_aqueous_equilibrium(ppx,nd,tslm1(:,NUM_TIME_SAVE))

    ENDDO

  ELSE

    DO jk=1,NUM_DEPTH+NUM_ATM

      call parallel_chem(nd(jk,:,NUM_TIME_SAVE), tslm1(jk,NUM_TIME_SAVE),&
        rj(jk,:,:),chem_rates(jk,:),timeday,kt(jk,:),Henrys(jk,:),L(jk))

    ENDDO

    !Convert ND to ppx and molarity
    DO jt=1,NSPEC

      IF (is_aqueous(jt)) THEN
        ppx(:,jt,:)=nd(:,jt,:)/M2ND
      ELSE
        ppx(:,jt,:)=nd(:,jt,:)/gas_nd(:,:)
      ENDIF

    ENDDO

  ENDIF ! IF (CHEM_MT) THEN

  !!$OMP END PARALLEL DO
  DO jk=1,NUM_DEPTH+NUM_ATM
    DO jt=1,NSPEC
      IF (isnan(nd(jk,jt,NUM_TIME_SAVE))) nd(jk,jt,NUM_TIME_SAVE)=DEFAULT_MIN_ND
      IF (nd(jk,jt,NUM_TIME_SAVE).LT.DEFAULT_MIN_ND) nd(jk,jt,NUM_TIME_SAVE)=DEFAULT_MIN_ND
    ENDDO
  ENDDO

  DEALLOCATE(RPROD)

END SUBROUTINE tropo_halo_drvr

!---------------------------------------------------------------

SUBROUTINE snow_physics_drvr(temperature,edge_depths,depths,wind,nd,ppx,porosity,Vd,ustar,flux,monin,all_flux)

  !This routine is the driver for the advection and diffusion in the model. 
  !The numerical approximation is based off finite volume and Crank-Nicholson discretization
  !Currently the diffusion constant is calculated assuming the molecular weight of of chemical species in N2

  REAL(dp), INTENT(in) :: temperature(:,:),edge_depths(:),depths(:),wind,porosity(:),Vd(:,:),ustar,monin
  REAL(dp), INTENT(inout) :: nd(:,:,:),ppx(:,:,:),flux(:),all_flux(:,:)
  REAL(dp), DIMENSION(:), ALLOCATABLE :: &
      dz3, &        !distance between centroids downward
      dz2, &        !thickness of layer
      dz1, &        !Thickness between centroid upward
      MM, &         !MM-Weight of lower layer's concentration on bottom edge concentration
      MN, &         !MN-Weight of current layer's concentration on bottom edge concentration
      NN, &         !NN-Weight of above layer's concentration on top edge concentration
      NM, &         !NM-Weight of current layer's concentration on top edge concentration

  !    terma, &     !Holds the upward diffusion term in the numerical approximation
  !    termb,  &    !Holds the downward diffusion term in the numerical approximation
  !    termv, &     !Holds the upward advection term in the numerical approximation
  !    termw, &     !Holds the downward advection term in the numerical approximation
  !    trid_a, &    !Lower diaganol of matrix for Thomas algorithim
  !    trid_b,  &   !Main diagonal of matrix for Thomas Alg.
  !    trid_c,  &   !Upper diagonal of matrix for Thomas Alg
  !    trid_d,  &   !Input vector for Thomas equation

      trid_x,  &    !Output vector for Thomas equation

  !    Dg,      &   !Diffusion coefficent m^2/s

      u_snow        !Vertical velcoty in snowpack in m/s

  REAL(dp) :: time_sub_step, &
      ts_wp         !Timescale of windpumping in seconds
  INTEGER :: jk,jt,jl,i, num_sub_step

  ALLOCATE(dz3(NUM_DEPTH+NUM_ATM))
  ALLOCATE(dz2(NUM_DEPTH+NUM_ATM))
  ALLOCATE(dz1(NUM_DEPTH+NUM_ATM))
  ALLOCATE(NN(NUM_DEPTH+NUM_ATM))
  ALLOCATE(NM(NUM_DEPTH+NUM_ATM))
  ALLOCATE(MN(NUM_DEPTH+NUM_ATM))
  ALLOCATE(MM(NUM_DEPTH+NUM_ATM))

  !ALLOCATE(terma(NUM_DEPTH+NUM_ATM))
  !ALLOCATE(termb(NUM_DEPTH+NUM_ATM))
  !ALLOCATE(termv(NUM_DEPTH+NUM_ATM))
  !ALLOCATE(termw(NUM_DEPTH+NUM_ATM))
  !ALLOCATE(trid_a(NUM_DEPTH+NUM_ATM))
  !ALLOCATE(trid_b(NUM_DEPTH+NUM_ATM))
  !ALLOCATE(trid_c(NUM_DEPTH+NUM_ATM))
  !ALLOCATE(trid_d(NUM_DEPTH+NUM_ATM))

  ALLOCATE(trid_x(NUM_DEPTH+NUM_ATM))

  !ALLOCATE(Dg(NUM_DEPTH+NUM_ATM))

  ALLOCATE(u_snow(NUM_DEPTH+NUM_ATM))

  call  snow_gas_nd_update

  DO jk=1, NUM_DEPTH+NUM_ATM

    IF (jk.EQ.1) THEN

      dz1(jk)=0.0
      dz2(jk)=edge_depths(jk)-edge_depths(jk+1)
      dz2(jk)=dz2(jk)*porosity(jk)
      dz3(jk)=depths(jk)-depths(jk+1)

      NN(jk)=0.0
      NM(JK)=0.0

      MN(JK)=(edge_depths(jk+1)-depths(jk))/(-dz3(jk))
      MM(JK)=(edge_depths(jk+1)-depths(jk+1))/(dz3(jk))!1!

    ELSEIF(jk.EQ.NUM_DEPTH+NUM_ATM) THEN

      dz3(jk)=0._dp
      dz1(jk)=depths(jk-1)-depths(jk)
      dz2(jk)=edge_depths(jk)-edge_depths(jk+1)
      dz2(jk)=dz2(jk)*porosity(jk)    
      MN(JK)=0.0
      MM(JK)=0.0

      NN(jk)=(edge_depths(jk)-depths(jk-1))/(-dz1(jk))!-1!
      NM(JK)=(edge_depths(jk)-depths(jk))/(dz1(jk))!1!
    ELSE

      dz1(jk)=depths(jk-1)-depths(jk)
      dz2(jk)=edge_depths(jk)-edge_depths(jk+1)
      dz2(jk)=dz2(jk)*porosity(jk)
      dz3(jk)=depths(jk)-depths(jk+1)

      NN(jk)=(edge_depths(jk)-depths(jk-1))/(-dz1(jk))!-1!
      NM(JK)=(edge_depths(jk)-depths(jk))/(dz1(jk))!1!

      MN(JK)=(edge_depths(jk+1)-depths(jk))/(-dz3(jk))
      MM(JK)=(edge_depths(jk+1)-depths(jk+1))/(dz3(jk))!1!

    ENDIF

    !print *,'NN',NN(jk),jk
    !print *,'NM',NM(jk),jk
    !print *,'MN',Mn(jk),jk
    !print *,'MM',MM(jk),jk
    !print *,'dz1',dz1(jk),jk
    !print *,'dz2',dz2(jk),jk
    !print *,'dz3',dz3(jk),jk
  ENDDO
  !read(*,*)

  ! MAQ_lg_20180412+ calling the subroutine to calculate wind pumping
  call snow_windpumping( &
    edge_depths(2:NUM_DEPTH+NUM_ATM+1),wind,u_snow)

  !!$OMP  PARALLEL DO                &
  !!$OMP  DEFAULT(shared)

  DO jt=1, NSPEC
    IF (is_aqueous(jt).EQV. .FALSE. ) THEN
      !print *,'old nd',nd(:,jt,NUM_TIME_SAVE)

      call parallel_physics(u_snow,NN,NM,MN,MM,dz1,dz2,dz3,mol_w(jt),nd(:,jt,NUM_TIME_SAVE),Vd(:,jt),ustar,edge_depths,jt,flux(jt),monin,&
        all_flux(:,jt))

      !print *,'new nd', nd(:,jt,NUM_TIME_SAVE)
      !read(*,*)
      !nd(:,jt,NUM_TIME_SAVE)=trid_x(:)

     ENDIF  !END of if not aqueous statement

  ENDDO  !END of species loop
  !!$OMP END PARALLEL DO

  DO jt=1,NSPEC
    IF (is_aqueous(jt).EQV. .FALSE. ) ppx(:,jt,NUM_TIME_SAVE)=nd(:,jt,NUM_TIME_SAVE)/gas_nd(:,NUM_TIME_SAVE)
  ENDDO

  DEALLOCATE(dz3)
  DEALLOCATE(dz2)
  DEALLOCATE(dz1)
  DEALLOCATE(NN)
  DEALLOCATE(NM)
  DEALLOCATE(MN)
  DEALLOCATE(MM)

  !DEALLOCATE(terma)
  !DEALLOCATE(termb)
  !DEALLOCATE(termv)
  !DEALLOCATE(termw)
  !DEALLOCATE(trid_c)
  !DEALLOCATE(trid_b)
  !DEALLOCATE(trid_a)
  !DEALLOCATE(trid_d)

  DEALLOCATE(trid_x)

  !DEALLOCATE(Dg)

  DEALLOCATE(u_snow)

END SUBROUTINE snow_physics_drvr

!---------------------------------------------------------------

SUBROUTINE solve_tridiag(a,b,c,d,x,n)

  !Tridiagonal solver using the Thomas method

  implicit none
  !        a - sub-diagonal (means it is the diagonal below the main diagonal)
  !        b - the main diagonal
  !        c - sup-diagonal (means it is the diagonal above the main diagonal)
  !        d - right part
  !        x - the answer
  !        n - number of equations

  integer,intent(in) :: n
  real(8),dimension(n),intent(in) :: a,b,c,d
  real(8),dimension(n),intent(out) :: x
  real(8),dimension(n) :: cp,dp
  real(8) :: m
  integer i

  ! initialize c-prime and d-prime
  cp(1) = c(1)/b(1)
  dp(1) = d(1)/b(1)
  ! solve for vectors c-prime and d-prime
  do i = 2,n
  m = b(i)-cp(i-1)*a(i)
  cp(i) = c(i)/m
  dp(i) = (d(i)-dp(i-1)*a(i))/m
  enddo
  ! initialize x
  x(n) = dp(n)
  ! solve for x from the vectors c-prime and d-prime
  do i = n-1, 1, -1
  x(i) = dp(i)-cp(i)*x(i+1)
  end do

END SUBROUTINE solve_tridiag

!---------------------------------------------------------------

SUBROUTINE molec_Dg(T,Dg,M)

  !This subroutine calculates the moelcular diffusion coefficent
  USE snow_parameters
  USE tropo_halo_Parameters
  !USE messy_emdep_mem
  implicit none
  !        Dg - diffusion coefficient [m2/s]
  !        kt - (m3 gas)/(m3 aq)/s


  integer :: n

  REAL(dp), INTENT(in) :: T,M !Temperature [k] and molecular weight [g per mol]
  REAL(dp), INTENT(out) :: Dg
  REAL(dp) :: v,alpha,lambda,H

  integer :: i

  !M=28.0  !Assume for now that the molecule has the mass of diatomic nitrogen
  !read(*,*)
  call molec_vel(T,M,v)
  call mean_free_path(T,lambda)

  Dg=lambda*v/3.0

END SUBROUTINE molec_Dg

!---------------------------------------------------------------

SUBROUTINE molec_vel(T,M,v)

  !This subroutine calculates the molecular velcoity of a molecule
  USE snow_parameters 
  USE tropo_halo_Precision
  implicit none
  !        T - Temperature [K]
  !        M - molecualr weight [g/mol]
  !        v - molecular velocity [m/s]

  integer :: n

  REAL(dp), INTENT(in) :: T,M
  REAL(dp), INTENT(out) :: v
  REAL(dp), parameter :: R=8.314e+3 !  g m^2/ s^2 K mol

  v=sqrt(8*R*T/(M*pi));

END SUBROUTINE molec_vel

!---------------------------------------------------------------

SUBROUTINE mean_free_path(T,lambda)

  !This routine calculates the mean free path of air

  USE snow_parameters
  USE tropo_halo_Precision

  implicit none
  !        Dg - diffusion coefficient [m2/s]
  !        r - radius of snowflake [m]
  !        kt - (m3 gas)/(m3 aq)/s


  integer :: n

  REAL(dp), INTENT(in) :: T
  REAL(dp), INTENT(out) :: lambda
  REAL(dp) :: v,alpha,M,P
  REAL(dp), PARAMETER :: R=8.205e-5 !m3 atm/K mol
  REAL(dp), PARAMETER :: d=3.0e-10 !diameter of molecule of air in [m]

  integer :: i

  P=surface_pressure !1 atmosphere
  lambda=R*T/(sqrt(2.0)*pi*(d**2)*AV_N*P)

END SUBROUTINE mean_free_path

!---------------------------------------------------------------

SUBROUTINE snow_windpumping(edge_depth,wind2m,u_snow)            ! ESS_lg_20110416+ ZWIND instead of ZWIND2M

  ! -------------------------------------------------------------------------
  ! LG- calculation of wind pumping speed profile in snow pack from snow
  !     properties and 2m wind speed according to Toyota and McConnell (2005)
  ! -------------------------------------------------------------------------

  IMPLICIT NONE


  REAL(dp), intent(in)  :: edge_depth(:),wind2m
  REAL(dp), intent(out) :: u_snow(:)
  REAL(dp), DIMENSION(:), ALLOCATABLE::&
    zc1,&
    zc2
  INTEGER  :: jk,jjk,jl,klon
  REAL(dp) :: zk,zvisc,zh,zlabda,zalpha,zsndepth,zdelta, &
    zref,rhoaair  ! ESS_lg_20110416+ NLEVT instead of NLEV+NLEVS_ML, ZWIND instead of ZWIND2M

  ALLOCATE(zc1(NUM_DEPTH+NUM_ATM))
  ALLOCATE(zc2(NUM_DEPTH+NUM_ATM))

  zk=17.5e-10!50.e-10     ! [m2] permeability, se also table 1 of Domine et al. 2008 paper
  zvisc=1.73e-5           ! [kg m-1 s-1] dynamic viscosity of air for 273K

  ! snow pack characteristics according to Liao and Tan, Figure 2, wfor the curve with largest enhancement
  !     by wind pumping

  zh=01.0!3.0!0.6!0.3!0.15!0.50 !0.05!       ! [m] relief amplitude, value of 0.2 seems to reflect the average of values used
  ! by Liao and Tan for their ANTCI2003 analysis (Anctartica)

  zlabda=zh/.03  !15.0!2.2     ! [m] relief wavelenght
  zalpha=01.0    ! [m] horizontal aspect ratio of relief

  ! ESS_lg_20090707+ calculation of e-folding depth
  zdelta=(zalpha/SQRT(zalpha**2+1))*(zlabda/(2*pi))

  call snow_gas_nd_update
  rhoaair=gas_nd(1,NUM_TIME_SAVE)/(CM2M**3)/AV_N*MW_AIR*G2KG 

  ! ESS_lg_20090707+ calculation of constant C1 (equation 5 paper Liao and Tan, 2008)
  ! assigning snow depth   !ESS_km_20130312 change it to 15 cm as the paper says, it lowers u_snow

  zc1(:)=EXP(abs(FRESH_SNOW_DEPTH)/zdelta)/(EXP(abs(FRESH_SNOW_DEPTH)/zdelta)+EXP(-abs(FRESH_SNOW_DEPTH)/zdelta))

  ! ESS_lg_20090707+ calculation of constant C2 (equation 6 paper Liao and Tan, 2008)

  zc2(:)=EXP(-abs(FRESH_SNOW_DEPTH)/zdelta)/(EXP(abs(FRESH_SNOW_DEPTH)/zdelta)+EXP(-abs(FRESH_SNOW_DEPTH)/zdelta))

  !print *,'c1,c2,zdelta',zc1,zc2,zdelta
  !print *,'zc1',zc1
  !print *,'zc2',zc2
  !read(*,*)

  !ESS_km_20130422 Return windpumping model to Cunningham Waddington model
  !zc1=1.0
  !zc2=0.0
  !DO JK=1,NUM_DEPTH
  !  zref=edge_depth(jk)
  !  !EES_km_20130415 This windpumping scheme calculates the velocities at the edges of the finite volumes
  u_snow(:)=0.0
  
  DO jk=1,NUM_DEPTH+NUM_ATM

    IF(edge_depth(jk).GT.0.0) THEN
      !u_snow(jk)=0.0
    !ELSEIF (edge_depth(jk).EQ.0.0) THEN
      !u_snow(jk)=(zk/zvisc)*((6.*rhoaair*wind2m**2)/pi)*(zh/zlabda)*(1./zlabda)*  &! ESS_lg_20110416+ ZWIND
      !(SQRT(zalpha**2+1.)/zalpha)
    ELSE
      u_snow(jk)=(zk/zvisc)*((6.*rhoaair*wind2m**2)/pi)*(zh/zlabda)*(1./zlabda)*  &! ESS_lg_20110416+ ZWIND
        (SQRT(zalpha**2+1.)/zalpha)*(zc1(jk)*EXP(-abs(edge_depth(jk))/zdelta)-zc2(jk)*exp(abs(edge_depth(jk))/zdelta))
    ENDIF

    IF (u_snow(jk).LT. 0.0) u_snow(jk)=0.0

  ENDDO
 
  !print *,'u_nsow',u_snow(:)
  !read(*,*)
  !ENDDO

  !ESS_km_20130415 Acccording to Cunnigham and Waddington 1993 transport in the snowpack is by diffusion
  !u_snow(NUM_ATM+1:NUM_DEPTH+NUM_ATM)=0.0
  !u_snow(:)=0.0

  !DO jk=1, NUM_ATM+NUM_DEPTH
  !print *, u_snow(jk),edge_depth(jk)
  !ENDDO
  !read(*,*)

  DEALLOCATE(zc1)
  DEALLOCATE(zc2)

END SUBROUTINE snow_windpumping

!---------------------------------------------------------------

SUBROUTINE snow_mass_transfer_coef(T,r,kt)

  !This subroutine calculates the mass transfer constant based upon Sanders 1999
  !Alpha and H values are , for the most part, from JPL

  USE snow_parameters
  USE tropo_halo_Parameters

  implicit none
  !        r - radius of snowflake [m]
  !        kt - (m3 gas)/(m3 aq)/s
  !       L - volumrtric ratio of Interstital air to QLL
  !       H - Enthalpys divided by R [K]
  !       alpha - Mass Accommodation Coefficent
  integer :: n

  REAL(dp), INTENT(in) :: r(:),T(:)
  REAL(dp), INTENT(out) :: kt(:,:)
  REAL(dp) :: v,alpha,M,lambda,H

  INTEGER :: jt
  integer :: i

  kt(:,:)=DEFAULT_KT

  DO jt=1, NSPEC
    DO i=1, NUM_DEPTH+NUM_ATM

      IF (r(i).GT.0.0) THEN


        IF (jt.EQ.ind_NO2 .OR. jt.EQ.ind_NO2_aq ) THEN
          alpha=1.0e-4

          H=2646.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

        ELSEIF (jt.EQ.ind_NO .OR. jt.EQ.ind_NO_aq ) THEN
          alpha=5.0E-5
          !alpha=5.0E-4
          H=2405.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)


        ELSEIF (JT.EQ.ind_HONO .OR. jt.EQ.ind_HONO_aq) THEN
          alpha=1.0e-3
          H=3849.0
          
	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
            (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

        ELSEIF (JT.EQ.ind_O3 .OR. jt.EQ.ind_O3_aq ) THEN
          alpha=0.04
          H=0.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)


        ELSEIF (jt.EQ.ind_HNO3 .OR. jt.EQ.ind_HNO3_aq )THEN
          alpha=3.0e-3
          H=5292.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

        ELSEIF (jt.EQ.ind_O2 .OR. jt.EQ.ind_O2_aq )THEN
          alpha=1.0e-2
          H=2000.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

        ELSEIF (jt.EQ.ind_OH .OR. jt.EQ.ind_OH_aq )THEN
          alpha=1.0e-1
          H=0.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

        ELSEIF (jt.EQ.ind_HO2 .OR. jt.EQ.ind_HO2_aq )THEN
          alpha=2.0e-2
          H=(00.0e+3)!/R_gas_J

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

        ELSEIF (jt.EQ.ind_H2O2 .OR. jt.EQ.ind_H2O2_aq )THEN
          alpha=7.70e-2
          H=2729.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))
 
          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_NO3 .OR. jt.EQ.ind_NO3_aq )THEN
          alpha=4.0e-2
          H=0.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_HNO4 .OR. jt.EQ.ind_HNO4_aq )THEN
          alpha=1.0e-1
          H=0.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_NH3 .OR. jt.EQ.ind_NH3_aq )THEN
          alpha=6.0e-2
          H=0.0

          IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

          call molec_vel(T(i),mol_w(jt),v)
          call mean_free_path(T(i),lambda)
          kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_CO2 .OR. jt.EQ.ind_CO2_aq )THEN
	  alpha=1.0e-2
	  H=2000.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_HCHO .OR. jt.EQ.ind_HCHO_aq )THEN
	  alpha=4.0e-2
	  H=0.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
             (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_HCOOH .OR. jt.EQ.ind_HCOOH_aq )THEN
	  alpha=1.40e-2
	  H=3978.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_CH3O2H .OR. jt.EQ.ind_CH3O2H_aq )THEN
	  alpha=0.046e-2
	  H=3273.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_MO2 .OR. jt.EQ.ind_MO2_aq )THEN
	  alpha=1.0e-2
	  H=2000.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_ACTA .OR. jt.EQ.ind_ACTA_aq )THEN
	  alpha=6.0e-2
	  H=0.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_HCl .OR. jt.EQ.ind_HCl_aq )THEN
	  alpha=3.0e-1
	  H=15.0E3/R_gas_J

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_HBr .OR. jt.EQ.ind_HBr_aq )THEN
	  alpha=2.0e-1
	  H=0.015!.0E3/R_gas_J

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
   	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_HOBr .OR. jt.EQ.ind_HOBr_aq )THEN
	  alpha=3.0e-3
	  H=4900.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_HOCl .OR. jt.EQ.ind_HOCl_aq )THEN
	  alpha=3.0e-3
	  H=4900.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)!

	ELSEIF (jt.EQ.ind_Cl2 .OR. jt.EQ.ind_Cl2_aq )THEN
	  alpha=1.0e-4
	  H=0.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
    	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_Br2 .OR. jt.EQ.ind_Br2_aq )THEN
	  alpha=3.8e-2
	  H=6546.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSEIF (jt.EQ.ind_BrCl .OR. jt.EQ.ind_BrCl_aq )THEN
	  alpha=0.15
	  H=0.0

	  IF (H.NE.0.0) alpha=EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.)))/&
	     (1-EXP(LOG(alpha/(1.-alpha))*(H*(1./T(i)-1./298.))))

	  call molec_vel(T(i),mol_w(jt),v)!
	  call mean_free_path(T(i),lambda)
	  kt(i,jt)=((r(i)**2)/(lambda*v)+(4.*r(i))/(3.*v*alpha))**(-1)

	ELSE

          kt(i,jt)=DEFAULT_KT

        ENDIF

      ELSE

        kt(i,jt)=DEFAULT_KT

      ENDIF

    ENDDO

  ENDDO

  !kt(:,:)=0.0
  !kt(:,:)=kt(:,:)*10000000.0!3.0!*10.0!/10.0

END SUBROUTINE snow_mass_transfer_coef

!---------------------------------------------------------------

SUBROUTINE update_snow_properties(snow_density,porosity,ssa,snow_radius,QLL_thickness,L,T,ppx,nd,Ct,theta)

  !   This subroutine updates the following propertis of the snowpack
  !   snow_density - density of the snowpack as a function of depth  [g/cm3]
  !   porosity - porosity of the snowpack as a function of depth [cm3 air/cm3 snow]
  !   ssa - specific surface area of snowflakes as a function of depth [cm2 / g]
  !   snow_radius - snowflake radius as a function of depth [m]
  !   QLL_thickness - thickness of the QLL layer as a function of depth [m]
  !   L - volumetric ratio of QLL to air as a function of depth[cm3 qll/cm3 air]
  !   T - Temperature of snowpack as a function of depth [K]
  !   ppx - Concentration of aqueous and gas phase as a function of depth in[M] and [parts], respectively
  !   nd - Concentraion of aqueous and gas phase in number density as a function of depth [mole/cm3]
  !   Ct - Total mass concentration of ions in QLL [m]
  !   theta - mass fraction of liquid water of snowflakes

  REAL(dp), INTENT(inout) :: snow_density(:),porosity(:),ssa(:),snow_radius(:),QLL_thickness(:),L(:),&
  ppx(:,:,:),nd(:,:,:),Ct(:,:),theta(:,:)
  REAL(dp), INTENT(in) :: T(:,:)
  INTEGER :: jk

  !snow_density(1)=0.0
  !DO jk=NUM_ATM+1,NUM_DEPTH+NUM_ATM
  !  snow_density(jk)=(NUM_DEPTH-(jk-2.0))/NUM_DEPTH*FRESH_SNOW_DENSITY+(jk-2.0)/NUM_DEPTH*FIRN_SNOW_DENSITY
  !ENDDO

  !porosity(1)=1.0
  !ssa(1)=0.0
  !snow_radius(1)=0.0
  DO jk=NUM_ATM+1, NUM_DEPTH+NUM_ATM
    porosity(jk)        = 1.-(snow_density(jk)/ICE_DENSITY)       ! porosity of snow
    ssa(jk)             =  -308.2*LOG(snow_density(jk))-205.96    ! Specific Surface Area (SSA) [cm2 g-1], f(rhoasnow) -308.2*LOG(rhoasnow(jk))-205.96
    IF (ssa(jk).LT.1.0) ssa(jk)=01.0                              ! ESS_km_20130308 added a minimum SSA for high density snow
    snow_radius(jk)          = (1.e-2)*6./(ICE_DENSITY*ssa(jk))/2.0  ! grain diameter [m]
    !IF (snow_density(jk).GT.0.0) snow_radius(jk) = (1.e-2)*3./(snow_density(jk)*ssa(jk))
  ENDDO

  !print *, snow_radius
  QLL_thickness(1)=0.0
  call update_QLL(ppx,nd,T,snow_radius,QLL_thickness,Ct,theta)
  QLL_thickness(1)=0.0
  !print *,QLL_thickness

  DO jk=1, NUM_DEPTH+NUM_ATM

    IF (porosity(jk).GE.1.0 .OR. porosity(jk).LE.0.0) THEN
      L(jk)=0.0
    ELSE
      L(jk)=((1.0-porosity(jk))*(1.0-(((snow_radius(jk)-QLL_thickness(jk))**3)/(snow_radius(jk)**3))))/porosity(jk)
    ENDIF

    !print *, porosity(jk)
    !print *,L(jk),jk

  ENDDO

  !read(*,*)

ENDSUBROUTINE update_snow_properties

!---------------------------------------------------------------

SUBROUTINE update_Henrys(T,Henrys)

  ! This subroutine adjusts the Henry's Law constants for temperature
  ! T - Temperature as a function of depth [K]
  ! Henrys - Dimensionless Henrys Law Constants as a function of depth

  REAL(dp), INTENT(in) :: T(:) !Temperature in Kelvin
  REAL(dp), INTENT(inout) :: Henrys(:,:)
  REAL(DP) :: dH
  INTEGER :: JT,jk

  Henrys(:,:)=DEFAULT_HENRYS

  DO JT=1, NSPEC

    DO jk =1, NUM_DEPTH+NUM_ATM

      IF (jt.EQ.ind_O3 .OR. jt.EQ.ind_O3_aq) THEN
        dH=2560.0
	Henrys(jk,jt)=1.2E-2
	Henrys(jk,jt)=Henrys(jk,jt)*EXP(dH*(1.0/T(jk)-1.0/298.0))
      ELSEIF (jt.EQ.ind_NO .OR. jt.EQ.ind_NO_aq )THEN
	dH=1480.0
	Henrys(jk,jt)=1.9E-3
      ELSEIF (jt.EQ.ind_NO2 .OR. jt.EQ.ind_NO2_aq )THEN
	dH=2500.0
	Henrys(jk,jt)=6.4E-3
      ELSEIF (jt.EQ.ind_HNO3 .OR. jt.EQ.ind_HNO3_aq )THEN
	dH=8694.0
	Henrys(jk,jt)=1.7E5
      ELSEIF (jt.EQ.ind_HONO .OR. jt.EQ.ind_HONO_aq )THEN
	dH=4780.0
	Henrys(jk,jt)=4.9E1
      ELSEIF (jt.EQ.ind_O2 .OR. jt.EQ.ind_O2_aq )THEN
	dH=1500.0
	Henrys(jk,jt)=1.3E-3
      ELSEIF (jt.EQ.ind_OH .OR. jt.EQ.ind_OH_aq )THEN
	dH=4300.0
	Henrys(jk,jt)=3.0E+1
      ELSEIF (jt.EQ.ind_HO2 .OR. jt.EQ.ind_HO2_aq )THEN
	dH=5900.0
	Henrys(jk,jt)=3.9E+3
      ELSEIF (jt.EQ.ind_H2O2 .OR. jt.EQ.ind_H2O2_aq )THEN
	dH=6338.0
	Henrys(jk,jt)=1.0E+5
      ELSEIF (jt.EQ.ind_NO3 .OR. jt.EQ.ind_NO3_aq )THEN
	dH=2000.0
	Henrys(jk,jt)=2.0
      ELSEIF (jt.EQ.ind_HNO4 .OR. jt.EQ.ind_HNO4_aq )THEN
	dH=6900.0
	Henrys(jk,jt)=1.2E+4
      ELSEIF (jt.EQ.ind_NH3 .OR. jt.EQ.ind_NH3_aq )THEN
	dH=4085.0
	Henrys(jk,jt)=5.8E+1
      ELSEIF (jt.EQ.ind_CO2 .OR. jt.EQ.ind_CO2_aq )THEN
	dH=2423.0
	Henrys(jk,jt)=3.1E-2
      ELSEIF (jt.EQ.ind_HCHO .OR. jt.EQ.ind_HCHO_aq )THEN
	dH=6425.0
	Henrys(jk,jt)=7.0E+3
      ELSEIF (jt.EQ.ind_HCOOH .OR. jt.EQ.ind_HCOOH_aq )THEN
	dH=5700.0
	Henrys(jk,jt)=3.7E+3
      ELSEIF (jt.EQ.ind_CH3O2H .OR. jt.EQ.ind_CH3O2H_aq )THEN
	dH=5322.0
	Henrys(jk,jt)=3.0E+2
      ELSEIF (jt.EQ.ind_MO2 .OR. jt.EQ.ind_MO2_aq )THEN
	dH=5900.0
	Henrys(jk,jt)=6.0
      ELSEIF (jt.EQ.ind_ACTA .OR. jt.EQ.ind_ACTA_aq )THEN
	dH=6400.0
	Henrys(jk,jt)=8.8E+3
      ELSEIF (jt.EQ.ind_HCl .OR. jt.EQ.ind_HCl_aq )THEN
	dH=9001.0  !Brimblecombe and Clegg 1989 via thomas
	Henrys(jk,jt)=1.2
      ELSEIF (jt.EQ.ind_HBr .OR. jt.EQ.ind_HBr_aq )THEN
	dH=10239.0  !Brimblecombe and Clegg 1989 via thomas
	Henrys(jk,jt)=1.3
      ELSEIF (jt.EQ.ind_HOBr .OR. jt.EQ.ind_HOBr_aq )THEN
	dH=5862.0  !Vogt et al. 1996 and Huthwelker et all 1995  via thomas
	Henrys(jk,jt)=9.3E1
      ELSEIF (jt.EQ.ind_HOCl .OR. jt.EQ.ind_HOCl_aq )THEN
	dH=5862.0  !Vogt et al. 1996 via thomas
	Henrys(jk,jt)=6.7E2
      ELSEIF (jt.EQ.ind_Cl2 .OR. jt.EQ.ind_Cl2_aq )THEN
	dH=2500.0  !Vogt et al. 1996 via thomas
	Henrys(jk,jt)=9.1E-2
      ELSEIF (jt.EQ.ind_Br2 .OR. jt.EQ.ind_Br2_aq )THEN
	dH=4094.0  !Vogt et al. 1996 via thomas
	Henrys(jk,jt)=7.6E-1
      ELSEIF (jt.EQ.ind_BrCl .OR. jt.EQ.ind_BrCl_aq )THEN
	dH=5600.0  !Vogt et al. 1996 via thomas
	Henrys(jk,jt)=9.4E-1
      ELSE
	dH=0.0
	Henrys(:,jt)=1.0E-15
      ENDIF
      Henrys(jk,jt)=Henrys(jk,jt)*EXP(dH*(1.0/T(jk)-1.0/298.0))
      Henrys(jk,jt)=Henrys(jk,jt)*M2ND*R_gas*T(jk)/AV_N  !Make Henrys law constants dimensionless

    ENDDO !Depth loop
  ENDDO ! Species loop

END SUBROUTINE update_Henrys

!---------------------------------------------------------------

SUBROUTINE snow_aqueous_equilibrium(ppx,nd,T)

  !   This subroutine places the aqueous phase chemistry in aqueous equilibirum
  !   T - Temperature of snowpack as a function of depth [K]
  !   ppx - Concentration of aqueous and gas phase as a function of depth in[M] and [parts], respectively
  !   nd - Concentraion of aqueous and gas phase in number density as a function of depth [mole/cm3]

  REAL(dp), INTENT(inout) :: ppx(:,:,:), nd(:,:,:)
  REAL(dp), INTENT(in) :: T(:)
  REAL(dp), DIMENSION(:), ALLOCATABLE:: ppx_old,Ct,Ct_old

  INTEGER :: jk,jt,count
  ALLOCATE(ppx_old(nspec))
  ALLOCATE(Ct_old(nspec))
  ALLOCATE(Ct(nspec))

  !$OMP  PARALLEL DO                &
  !$OMP  DEFAULT(shared)

  DO jk=1+NUM_ATM, NUM_DEPTH+NUM_ATM
    !Initalization at each depth
    !DO jt =1, NSPEC
       !ppx_old(jt)=ppx(jk,jt,NUM_TIME_SAVE)
    !ENDDO

    call aqueous_equilibrium_para(ppx(jk,:,NUM_TIME_SAVE),T(jk),jk)

    !DO jt =1, NSPEC
    !  ppx_old(jt)=ppx(jk,jt,NUM_TIME_SAVE)
    !1ENDDO

    DO jt=1,NSPEC

      IF (is_aqueous(jt)) THEN
        !print *,nd(jk,jt,:),SPC_NAMES(jt)
        nd(jk,jt,:)=ppx(jk,jt,:)*M2ND
        !print *,nd(jk,jt,:),SPC_NAMES(jt)
        !print *,ppx(jk,jt,:),SPC_NAMES(jt)
      ELSE
        !print *,nd(jk,jt,:),SPC_NAMES(jt)
        nd(jk,jt,:)=ppx(jk,jt,:)*gas_nd(jk,:)
        !print *,nd(jk,jt,:),SPC_NAMES(jt)
        !print *,ppx(jk,jt,:),SPC_NAMES(jt)
      ENDIF
      !read(*,*)

    ENDDO
  ENDDO !END of depth loop

  !$OMP END PARALLEL DO

  DEALLOCATE(Ct_old)
  DEALLOCATE(Ct)
  DEALLOCATE(ppx_old)

END SUBROUTINE snow_aqueous_equilibrium

!---------------------------------------------------------------

SUBROUTINE update_is_aqueous()

  !This subroutine updates a global variable determining which species are aqueous species
  !   is_aqueous - a logical array specifiying which species are aqueous
  INTEGER :: jt

  is_aqueous(:)=.FALSE.

  DO jt=1, NSPEC
    IF (INDEX(SPC_NAMES(jt),'_aq').GT.0) is_aqueous(jt)=.TRUE.
  ENDDO

END SUBROUTINE update_is_aqueous

!---------------------------------------------------------------

SUBROUTINE update_is_ion()

  !This subroutine determines which species are ions and keeps the information in a global matrix
  !   is_ion - this matrix will carry 1' and 0's in the first column for each chemical species to describe the species as an ion. The second column will contain the charge of the ion.
  !This algorthim will only map ions with -3 to +3 charges
  INTEGER :: jt

  is_ion(:,:)=0

  DO jt=1, NSPEC
    IF (INDEX(SPC_NAMES(jt),'p_aq').GT.0) THEN
      is_ion(jt,1)=1
      is_ion(jt,2)=1
    ELSEIF (INDEX(SPC_NAMES(jt),'p2_aq').GT.0) THEN
      is_ion(jt,1)=1
      is_ion(jt,2)=2
    ELSEIF (INDEX(SPC_NAMES(jt),'p3_aq').GT.0) THEN
      is_ion(jt,1)=1
      is_ion(jt,2)=3
    ELSEIF (INDEX(SPC_NAMES(jt),'n_aq').GT.0) THEN
      is_ion(jt,1)=1
      is_ion(jt,2)=-1
    ELSEIF (INDEX(SPC_NAMES(jt),'n2_aq').GT.0) THEN
      is_ion(jt,1)=1
      is_ion(jt,2)=-2
    ELSEIF (INDEX(SPC_NAMES(jt),'n3_aq').GT.0) THEN
      is_ion(jt,1)=1
      is_ion(jt,2)=-3
    ENDIF
    !print *,SPC_NAMES(jt),is_ion(jt,1),is_ion(jt,2)

  ENDDO
  !read(*,*)

END SUBROUTINE update_is_ion

!---------------------------------------------------------------

SUBROUTINE snow_mass_transfer(T,snow_radius,kt,L,nd,ppx,Henrys)

  REAL(dp), INTENT(in) :: T(:),snow_radius(:),L(:),Henrys(:,:)
  REAL(dp), INTENT(inout) :: kt(:,:),nd(:,:,:),ppx(:,:,:)


  REAL(dp), DIMENSION(:), ALLOCATABLE ::&
    trid_a,&
    trid_b,&
    trid_c,&
    trid_d,&
    trid_x

  REAL(dp), DIMENSION(:,:), ALLOCATABLE ::&
    nd_old

  INTEGER :: jt,jk,num_sub_step
  REAL(dp) :: sub_time_step,timescale,Ca_eq,Cg_eq,MT_diff

  ALLOCATE(trid_a(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_b(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_c(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_d(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_x(NUM_DEPTH+NUM_ATM))
  ALLOCATE(nd_old(NUM_DEPTH+NUM_ATM,NSPEC))

  call snow_mass_transfer_coef(T,snow_radius,kt)

  nd_old(:,:)=nd(:,:,NUM_TIME_SAVE)


  !$OMP  PARALLEL DO                &
  !$OMP  DEFAULT(shared)

  DO jt=1, NSPEC

    IF(is_aqueous(jt).AND. aq_gas_index(jt,1).NE.0 ) THEN
      call parallel_mass_transfer(nd_old,kt,Henrys,L,jt)
      nd(:,jt,NUM_TIME_SAVE)=nd_old(:,jt)
      nd(:,aq_gas_index(jt,1),NUM_TIME_SAVE)=nd_old(:,aq_gas_index(jt,1))
    ENDIF
  ENDDO !End of species loop

  !$OMP END PARALLEL DO
  !read(*,*)

  DO jt=1,NSPEC

    IF (is_aqueous(jt)) THEN

      IF (aq_gas_index(jt,1).NE.0) THEN
        ppx(:,jt,:)=nd(:,jt,:)/M2ND
        ppx(:,aq_gas_index(jt,1),:)=nd(:,aq_gas_index(jt,1),:)/gas_nd(:,:)
      ENDIF

    ENDIF

  ENDDO

  DEALLOCATE(trid_a)
  DEALLOCATE(trid_b)
  DEALLOCATE(trid_c)
  DEALLOCATE(trid_d)
  DEALLOCATE(trid_x)
  DEALLOCATE(nd_old)

END SUBROUTINE snow_mass_transfer

!---------------------------------------------------------------

SUBROUTINE match_gas_aq(aq_gas_index)

  INTEGER, INTENT(inout):: aq_gas_index(:,:)
  CHARACTER(LEN=15) :: aq_str,gas_str
  INTEGER :: jt,jt2,char_pos
  LOGICAL :: match_flag
  aq_gas_index(:,:)=0

  DO jt=1,NSPEC
    match_flag=.FALSE.
    IF (is_aqueous(jt)) THEN
      aq_str=SPC_NAMES(jt)
      char_pos=INDEX(aq_str,'_aq')

      DO jt2=1, NSPEC

        IF (jt.NE.jt2) THEN
          gas_str=SPC_NAMES(jt2)
          match_flag=aq_str(1:(char_pos-1))==gas_str(1:(char_pos))

          IF (match_flag) THEN
            aq_gas_index(jt,1)=jt2
            aq_gas_index(jt2,2)=jt
          ENDIF

        ENDIF

      ENDDO
    ENDIF
  ENDDO

END SUBROUTINE match_gas_aq

!---------------------------------------------------------------

SUBROUTINE snow_initialize_temperature()

  temperature(:,:)=240.0

END SUBROUTINE snow_initialize_temperature

!---------------------------------------------------------------

SUBROUTINE snow_read_input(year, month, day,hour, model_obs)

  REAL(dp), INTENT(in) :: hour(:)
  REAL(dp), INTENT(inout) :: model_obs(:,:)
  INTEGER, INTENT(in) :: year(:), month(:), day(:)

  INTEGER :: start_time, i, j, k,num_lines, obs_lines, ind 
  CHARACTER(1000):: line

  INTEGER, DIMENSION(:), ALLOCATABLE ::&
    year_obs,&
    month_obs,&
    day_obs,&
    sec_obs,&
    dummy_int

  REAL(dp), DIMENSION(:), ALLOCATABLE ::&
    hour_obs

  REAL(dp), DIMENSION(:,:), ALLOCATABLE ::&
    obs

  !Find the number of lines in te input file
  OPEN(INPUT_FILE_NUM,FILE=INPUT_FILE,STATUS='OLD',ACTION='READ')

  num_lines=0
  DO
    READ(INPUT_FILE_NUM,*,END=10)
    num_lines=num_lines+1
  ENDDO
  10 CLOSE(INPUT_FILE_NUM)
  !print *,'num_lines',num_lines

  obs_lines=num_lines-4
  !Allocate arrays
  ALLOCATE(year_obs(obs_lines))
  ALLOCATE(month_obs(obs_lines))
  ALLOCATE(day_obs(obs_lines))
  ALLOCATE(hour_obs(obs_lines))
  ALLOCATE(sec_obs(obs_lines))
  ALLOCATE(dummy_int(obs_lines))
  ALLOCATE(obs(obs_lines,NUM_OBS))

  obs(:,:)=DEFAULT_OBS

  OPEN(INPUT_FILE_NUM,FILE=INPUT_FILE,STATUS='OLD',ACTION='READ')
  READ(INPUT_FILE_NUM,*) line
  READ(INPUT_FILE_NUM,*) start_time
  !print *, start_time
  READ(INPUT_FILE_NUM,*) line
  !print*,line

  DO i=1, obs_lines
    READ(INPUT_FILE_NUM,*) sec_obs(i), obs(i,:)
    !print *,sec_obs(i)*SEC2HRS, obs(i,:)
  ENDDO

  year_obs(:)=0
  month_obs(:)=0
  day_obs(:)=0
  dummy_int(:)=0
  !print *,'start obs',start_time
  year_obs(1)=int(start_time/10**6)
  start_time=start_time-int(year_obs(1)*10**6)
  !print *,'start obs',start_time,10**6
  month_obs(1)=int(start_time/10**4)
  start_time=start_time-int(month_obs(1)*10**4)
  !print *,'start obs',start_time
  day_obs(1)=int(start_time/10**2)
  start_time=start_time-int(day_obs(1)*10**2)
  !print *,'start obs',start_time
  hour_obs(1)=start_time+real(sec_obs(1))*SEC2HRS

  !print *, year_obs(1), month_obs(1),day_obs(1),hour_obs(1),sec_obs(1)
  !read(*,*)

  DO i=2, obs_lines
    hour_obs(i)=hour_obs(i-1)+(real(sec_obs(i))-real(sec_obs(i-1)))*SEC2HRS
    CALL check_day(day_obs,hour_obs,dummy_int,i)
    CALL check_month(month_obs,day_obs,i)
    CALL check_year(year_obs,month_obs,dummy_int,i)

    !print *,year_obs(i),month_obs(i),day_obs(i),hour_obs(i),i
  ENDDO

  DO i=1,NUM_TIME
    ind=0
    DO j=1,obs_lines

      IF (year(i).EQ.year_obs(j)) THEN
        IF (month(i).EQ.month_obs(j)) THEN
          IF (day(i).EQ.day_obs(j)) THEN

            IF (hour_obs(j).LT.hour(i)) ind=j

          ENDIF
        ENDIF
      ENDIF
    ENDDO

    IF (ind.EQ.obs_lines) ind=0

    IF (ind.NE.0) THEN
      model_obs(i,:)=obs(ind,:)*(hour(i)-hour_obs(ind))/&
        (hour_obs(ind+1)-hour_obs(ind))&
        +obs(ind+1,:)*((hour(i)-hour_obs(ind+1))/(hour_obs(ind)-hour_obs(ind+1)))
    ENDIF

    !print *,model_obs(i,:)
    !IF (ind.NE.0) print *, hour_obs(ind),hour_obs(ind+1)
    !read(*,*)

  ENDDO

  !read(*,*)

  CLOSE(INPUT_FILE_NUM)

  DEALLOCATE(year_obs)
  DEALLOCATE(month_obs)
  DEALLOCATE(day_obs)
  DEALLOCATE(hour_obs)
  DEALLOCATE(sec_obs)
  DEALLOCATE(obs)
  DEALLOCATE(dummy_int)

END SUBROUTINE snow_read_input

!---------------------------------------------------------------

SUBROUTINE snow_temperature(snow_density, porosity, depths, edge_depths)

  REAL(dp), INTENT(IN) :: snow_density(:), porosity(:), depths(:), edge_depths(:)

  REAL(dp), DIMENSION(:), ALLOCATABLE::&
    kt,&
    dz1,&
    dz2,&
    dz3,&
    trid_a,&
    trid_b,&
    trid_c,&
    trid_d,&
    trid_x,&
    term_a,&
    term_b

  REAL(dp) :: k_eff   !Effective thermal conductivity of snow W/m k

  INTEGER :: j,jk

  k_eff=0.25
  ALLOCATE(kt(NUM_DEPTH+NUM_ATM))
  ALLOCATE(dz1(NUM_DEPTH+NUM_ATM))
  ALLOCATE(dz2(NUM_DEPTH+NUM_ATM))
  ALLOCATE(dz3(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_a(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_b(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_c(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_d(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_x(NUM_DEPTH+NUM_ATM))
  ALLOCATE(term_a(NUM_DEPTH+NUM_ATM))
  ALLOCATE(term_b(NUM_DEPTH+NUM_ATM))

  DO jk=1, NUM_DEPTH+NUM_ATM

    IF (jk.EQ.1) THEN
      dz1(jk)=0.0
      dz2(jk)=edge_depths(jk)-edge_depths(jk+1)    
      dz3(jk)=depths(jk)-depths(jk+1)

    ELSEIF(jk.EQ.NUM_DEPTH+NUM_ATM) THEN
      dz3(jk)=0._dp
      dz1(jk)=depths(jk-1)-depths(jk)
      dz2(jk)=edge_depths(jk)-edge_depths(jk+1)     
    ELSE
      dz1(jk)=depths(jk-1)-depths(jk)
      dz2(jk)=edge_depths(jk)-edge_depths(jk+1)
      dz3(jk)=depths(jk)-depths(jk+1)
    ENDIF

  ENDDO

  DO j=1, NUM_DEPTH+NUM_ATM

    IF (snow_density(j).GT.0.0) THEN
      kt(j)=k_eff/((Cp_ice*(1.0-porosity(j))+Cp_air*porosity(j))*snow_density(j))*CM2M**3
    ELSE
      kt(j)=0.0
    ENDIF

    IF (j.EQ.1) THEN
      term_a(j)=0.0
      term_b(j)=TIME_STEP*kt(j)/dz2(j)*(1.0/dz3(j))
      trid_d(j)=2*temperature(j,NUM_TIME_SAVE)+&
         term_b(j)*temperature(j+1,NUM_TIME_SAVE)-&
        (term_a(j)+term_b(j))*temperature(j,NUM_TIME_SAVE)
    ELSEIF(j.EQ.NUM_DEPTH+NUM_ATM) THEN
      term_a(j)=TIME_STEP*kt(j)/dz2(j)*(1.0/dz1(j))
      term_b(j)=0.0
      trid_d(j)=2*temperature(j,NUM_TIME_SAVE)+&
        term_a(j)*temperature(j-1,NUM_TIME_SAVE)-&
       (term_a(j)+term_b(j))*temperature(j,NUM_TIME_SAVE)
    ELSE
      term_a(j)=TIME_STEP*kt(j)/dz2(j)*(1.0/dz1(j))
      term_b(j)=TIME_STEP*kt(j)/dz2(j)*(1.0/dz3(j))
      trid_d(j)=2.0*temperature(j,NUM_TIME_SAVE)+&
        term_a(j)*temperature(j-1,NUM_TIME_SAVE)+&
        term_b(j)*temperature(j+1,NUM_TIME_SAVE)-&
       (term_a(j)+term_b(j))*temperature(j,NUM_TIME_SAVE)
    ENDIF

  ENDDO

  trid_a(:)=-term_a(:)
  trid_c(:)=-term_b(:)
  trid_b(:)=2.0+term_a(:)+term_b(:)
  call solve_tridiag(trid_a,trid_b,trid_c,trid_d,trid_x,NUM_DEPTH+NUM_ATM)

  !print *, kt
  !print *, temperature(:,NUM_TIME_SAVE)
  !print *, 'term_a',term_a(:)
  !print *, 'term_b',term_b(:)
  !print *, 'dz1', dz1
  !print *, 'dz2', dz2
  !print *, 'dz3', dz3
  !print *,'edge_depths',edge_depths
  !print *, trid_x(:)
  !read(*,*)
  temperature(:,NUM_TIME_SAVE)=trid_x(:)

  DEALLOCATE(kt)
  DEALLOCATE(dz1)
  DEALLOCATE(dz2)
  DEALLOCATE(dz3)
  DEALLOCATE(trid_a)
  DEALLOCATE(trid_b)
  DEALLOCATE(trid_c)
  DEALLOCATE(trid_d)
  DEALLOCATE(trid_x)
  DEALLOCATE(term_a)
  DEALLOCATE(term_b)

END SUBROUTINE snow_temperature

!---------------------------------------------------------------

SUBROUTINE update_QLL(ppx,nd,T,R,QLL_thickness,Ct,theta)

  REAL(dp), INTENT(in) :: T(:,:),R(:)
  REAL(dp), INTENT(inout) :: ppx(:,:,:),nd(:,:,:),QLL_thickness(:),Ct(:,:),theta(:,:)
  REAL(dp) :: old_QLL, theta_old, theta_new,V_ratio_old, V_ratio_new, Tf,A,B,C,K,Q1,Q2,theta_new1,theta_new2

  LOGICAL :: IT_FLAG
  INTEGER :: jk, it,jt

  Tf=273.0  !Temperature of formtation of ice

  K=MW_WATER*R_gas_J*Tf*G2KG/H_f_H20
  !Ct=0.0

  DO jk=1, NUM_DEPTH+NUM_ATM

    IF (QLL_thickness(jk).GT.0 .AND. NUM_TIME_SAVE.GT.1 ) THEN !

      IF (T(jk,NUM_TIME_SAVE).LT.Tf .AND. T(jk,NUM_TIME_SAVE-1).LT.Tf) THEN
        Q2=(T(jk,NUM_TIME_SAVE)/(Tf-T(jk,NUM_TIME_SAVE)))
        Q1=(T(jk,NUM_TIME_SAVE-1)/(Tf-T(jk,NUM_TIME_SAVE-1)))
        V_ratio_old=1.0-((R(jk)-QLL_thickness(jk))**3.0)/(R(jk)**3.0)

        !theta_old=1.0/(1.0+(R(jk)-QLL_thickness(jk))**3.0/(R(jk)**3-(R(jk)-QLL_thickness(jk))**3.0)*ICE_DENSITY/WATER_DENSITY)
        Ct(jk,NUM_TIME_SAVE)=0.0

        DO jt=1, NSPEC
          IF (is_ion(jt,1).EQ.1) THEN
            Ct(jk,NUM_TIME_SAVE)=Ct(jk,NUM_TIME_SAVE)+ppx(jk,jt,NUM_TIME_SAVE)
          ENDIF
        ENDDO
        Ct(jk,NUM_TIME_SAVE)=Ct(jk,NUM_TIME_SAVE)*L2cm3/WATER_DENSITY/G2KG
        !print *,'Ct',Ct

        theta(jk,NUM_TIME_SAVE)=theta(jk,NUM_TIME_SAVE-1)+K*theta(jk,NUM_TIME_SAVE-1)*(Ct(jk,NUM_TIME_SAVE)*Q2-Ct(jk,NUM_TIME_SAVE-1)*Q1)

        !A=1.0-K*Q2*Ct
        !B=-K*Ct*theta_old*(Q2-Q1)+2.0*K*Ct*Q2*theta_old
        !C= -K*Q2*Ct*theta_old**2
        !theta_new1=(-B+(B**2.0-4.0*A*C)**(0.5))/(2.0*A)
        !theta_new2=(-B-(B**2.0-4.0*A*C)**(0.5))/(2.0*A)
        !print *,'theta_old',theta_old
        !print *,'theta1,theta2',theta_new1,theta_new2
        !print *,'A,B,C',A,B,C
        !read(*,*)
        !theta_new=theta_new1

        IF (theta(jk,NUM_TIME_SAVE).GT.0) THEN
          QLL_thickness(jk)=R(jk)-(((R(jk)**3)*((1.0-theta(jk,NUM_TIME_SAVE))*WATER_DENSITY/(ICE_DENSITY*theta(jk,NUM_TIME_SAVE))))/&
            (1.0+(1.0-theta(jk,NUM_TIME_SAVE))*WATER_DENSITY/(ICE_DENSITY*theta(jk,NUM_TIME_SAVE))))**(1.0/3.0)
          V_ratio_new=1.0-((R(jk)-QLL_thickness(jk))**3.0)/(R(jk)**3.0)
        ELSEIF (theta(jk,NUM_TIME_SAVE).GT.1.0) THEN
          QLL_thickness(JK)=R(JK)
          V_ratio_new=1.0-((R(jk)-QLL_thickness(jk))**3.0)/(R(jk)**3.0)
        ELSE
          QLL_thickness(jk)=0.0
          V_ratio_new=0.0
        ENDIF

        !V_ratio_new=theta_new*ICE_DENSITY/WATER_DENSITY
        !QLL_thickness(jk)=R(jk)-((1.0-V_ratio_new)*R(jk)**3.0)**(1.0/3.0)

        !IF (QLL_thickness(jk).GT.5.0E-9) THEN
           !QLL_thickness(jk)=5.0E-9
           !V_ratio_new=1.0-((R(jk)-QLL_thickness(jk))**3.0)/(R(jk)**3.0)
        !ENDIF
        !V_ratio_old=1.0-((R(jk)-QLL_thickness(jk))**3.0)/(R(jk)**3.0)
        !V_ratio_new=V_ratio_old*(T(jk,NUM_TIME_SAVE)/(Tf-T(jk,NUM_TIME_SAVE)))&
        !  *(Tf-T(jk,NUM_TIME_SAVE-1))/T(jk,NUM_TIME_SAVE-1)

        !QLL_thickness(jk)=R(jk)-((1.0-V_ratio_new)*R(jk)**3.0)**(1.0/3.0)

        !IF (QLL_thickness(jk).GT. R(jk) .OR. V_ratio_new.GT. 1.0) THEN
          !QLL_thickness(jk)=R(jk)
          !V_ratio_new=1.0
        !ENDIF

      ENDIF

      !print *,'V_ratio_old, V_ratio_new,jk',V_ratio_old, V_ratio_new,jk

      DO jt=1, NSPEC
        IF (is_aqueous(jt) .AND. V_ratio_new.GT.0.0) then
          ppx(jk,jt,NUM_TIME_SAVE)=ppx(jk,jt,NUM_TIME_SAVE)*V_ratio_old/V_ratio_new
        ENDIF
      ENDDO

      Ct(jk,1:NUM_TIME_SAVE-1)=Ct(jk,2:NUM_TIME_SAVE)
      theta(jk,1:NUM_TIME_SAVE-1)=theta(jk,2:NUM_TIME_SAVE)

    ENDIF

  ENDDO

  !converting concentrations in ppx to nd
  DO jt=1,NSPEC
    IF (is_aqueous(jt)) THEN
      nd(:,jt,:)=ppx(:,jt,:)*M2ND
    ELSE
      nd(:,jt,:)=ppx(:,jt,:)*gas_nd(:,:)
    ENDIF
  ENDDO

  !print *,'QLL_thickness', QLL_thickness
  !print *,'theta',theta
  !print *,'Ct',Ct
  !read(*,*)

END SUBROUTINE update_QLL

!---------------------------------------------------------------

SUBROUTINE parallel_physics(u_snow,NN,NM,MN,MM,dz1,dz2,dz3,MW,nd,Vd,ustar,edge_depths,jjtt,flux,monin,all_flux)

  REAL(dp), INTENT(inout) ::&
  nd(:), flux, all_flux(:)

  REAL(dp), INTENT(in) ::&
    u_snow(:),&               !Vertical velocity in snowpack in m/s
    NN(:),&
    NM(:),&
    MN(:),&
    MM(:),&
    dz1(:),&
    dz2(:),&
    dz3(:),&
    MW,&
    Vd(:),&
    ustar,&
    edge_depths(:),&
    monin

  REAL(dp), DIMENSION(:), ALLOCATABLE ::&
    terma,&
    termb,&
    termw,&
    termv,&
    termg,&
    termh,&
    trid_a,&
    trid_b,&
    trid_c,&
    trid_d,&
    trid_x,&
    Dg       !Diffusion coefficent m^2/s

  REAL(dp) :: ts_wp,ts_vd,ts_Dg,ts_Cr, time_sub_step,Kh_top,dz1_top,flux_top,nd_top

  INTEGER :: jk,jl,num_sub_step,i
  INTEGER, INTENT(in):: jjtt

  ALLOCATE(terma(NUM_DEPTH+NUM_ATM))
  ALLOCATE(termb(NUM_DEPTH+NUM_ATM))
  ALLOCATE(termv(NUM_DEPTH+NUM_ATM))
  ALLOCATE(termw(NUM_DEPTH+NUM_ATM))
  ALLOCATE(termg(NUM_DEPTH+NUM_ATM))
  ALLOCATE(termh(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_a(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_b(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_c(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_d(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_x(NUM_DEPTH+NUM_ATM))
  ALLOCATE(Dg(NUM_DEPTH+NUM_ATM))

  time_sub_step=TIME_STEP

  !Initialize timescales  of windpumping as very large
  ts_wp=999999.9
  ts_vd=ts_wp
  ts_Dg=ts_wp
  ts_Cr=ts_wp

  ! Calculate windpumping timescale
  DO jk=1,NUM_DEPTH+NUM_ATM

    IF (u_snow(jk).GT.0) ts_wp=MIN(ts_wp,dz2(jk)/u_snow(jk))
    IF (Vd(jk).GT.0) ts_vd=MIN(ts_vd,dz2(jk)/Vd(jk))

  ENDDO

  !Loop through sub-time steps

  DO jk=1, NUM_DEPTH+NUM_ATM

    call molec_Dg(temperature(jk,NUM_TIME_SAVE),Dg(jk),MW)

    IF (jk.GT.NUM_ATM-1) THEN
      Dg(jk)=Dg(jk)*DG_TORT
    ELSEIF (edge_depths(jk+1).LE.MICRO_LAYER_DEPTH) THEN
      Dg(jk)=Dg(jk)
    ELSE
      call calculate_eddy_diff(ustar,edge_depths(jk+1), Dg(jk),monin)
    ENDIF

    !print *, Dg(jk),edge_depths(jk+1)
  ENDDO

  !read(*,*)
  call calculate_eddy_diff(ustar,edge_depths(1), Kh_top,monin) !Finds eddy diffusivty at the top of the ABL if fluxes BC want to be used
  !print *,monin
  !read(*,*)

  !Stability Criteria
  DO jk=1, NUM_ATM+NUM_DEPTH-1

    IF( u_snow(jk).NE. 0.0) ts_Cr=min(ts_Cr,STAB_U*dz3(jk)/u_snow(jk))
    IF( Dg(jk).NE. 0.0) ts_Dg=min(ts_Dg,STAB_Dg*dz3(jk)*dz2(jk)/Dg(jk))
    !print*,STAB_Dg*dz3(jk)*dz2(jk),Dg(jk),ts_Dg,jk

  ENDDO
  !read(*,*)

  !Calculate the number of sub time steps
  time_sub_step=MIN(time_sub_step,ts_wp,ts_vd,ts_Dg,ts_Cr)!0.10*
  num_sub_step=int(TIME_STEP/time_sub_step)+1
  time_sub_step=TIME_STEP/num_sub_step
  !print *,'time_sub_step',time_sub_step,ts_Dg,ts_Cr,ts_wp,ts_vd

  DO jl=1,num_sub_step
    DO jk=1, NUM_DEPTH+NUM_ATM

      IF (jk.EQ.1) THEN
        nd_top=0.0  !Default value to prevent errors when a flux is not desired

        !Diffusion
        IF (jjtt.EQ.ind_NO) THEN

          !This method uses a theoretical concentration above the top of the model for both the forward and backwards representation, which is an error, but on small time steps, should be ok
          flux_top=0.0  !1.0E6   !Flux in m molecules per cm3 s  , positve flux has possitve contribution to layer
          dz1_top=1.0   ! A theoretical distance in which a flux occurs across, shouldnt impact calculation)
          nd_top=dz1_top*flux_top/Kh_top + nd(1)
          terma(jk)=Kh_top*time_sub_step/(dz1_top*dz2(jk))
        ELSE
          terma(jk)=0.0
        ENDIF
        termb(jk)=Dg(jk)*time_sub_step/(dz2(jk)*dz3(jk))

        !Advection

        termw(jk)=0.0
        termv(jk)=u_snow(jk)*time_sub_step/dz2(jk)
        IF (nd(jk).GT.nd(jk+1)) termv(jk)=-1*abs(termv(jk))

        !Dry Deposition
        termg(jk)=0.0
        termh(jk)=-Vd(jk)*time_sub_step/dz2(jk)

        all_flux(jk)=0.0
        !Setting up arrays to use in Thomas Algorithm

        !Backward Euler
        IF (NUM_METHOD.EQ.1) THEN
          trid_a(jk)=-terma(jk)+(termw(jk)+termg(jk))*NN(jk)
          trid_b(jk)=(1._dp+terma(jk)+termb(jk)+(termw(jk)+termg(jk))*NM(jk)-(termv(jk)+termh(jk))*MN(jk))
          trid_c(jk)=-termb(jk)-(termv(jk)+termh(jk))*MM(jk)
          trid_d(jk)=nd(jk)
        ELSE
          !Crank-N
          trid_a(jk)=-terma(jk)+(termw(jk)+termg(jk))*NN(jk)
          trid_b(jk)=(2._dp+terma(jk)+termb(jk)+(termw(jk)+termg(jk))*NM(jk)-(termv(jk)+termh(jk))*MN(jk))
          trid_c(jk)=-termb(jk)-(termv(jk)+termh(jk))*MM(jk)

          !trid_d(jk)=(4.0-trid_b(jk))*nd(jk)&
          !-trid_c(jk)*nd(jk+1)
          trid_d(jk)=-trid_a(jk)*nd_top+&
            (4.0-trid_b(jk))*nd(jk)&
            -trid_c(jk)*nd(jk+1)

          !IF (jjtt.EQ.ind_NO) THEN
            !print *, trid_a(1),trid_b(1), trid_c(1), trid_d(1)
            !read(*,*)
          !ENDIF

        ENDIF

      ELSEIF(jk.EQ.NUM_DEPTH+NUM_ATM) THEN

        !Diffusion
        terma(jk)=Dg(jk-1)*time_sub_step/(dz2(jk)*dz1(jk))
        termb(jk)=0.0

        !Advection
        termw(jk)=u_snow(jk-1)*time_sub_step/dz2(jk)
        termv(jk)=0.0
        IF (nd(jk-1).GT.nd(jk)) termw(jk)=-1*abs(termw(jk))

        !Dry Deposition
        termg(jk)=-Vd(jk-1)*time_sub_step/dz2(jk)
        termh(jk)=0.0

        all_flux(jk)=all_flux(jk)+(termw(jk)*(NN(jk)*nd(jk-1) + NM(jk)*nd(jk)) -&
          terma(jk)*(nd(jk-1)-nd(jk)))/time_sub_step*dz2(jk)/num_sub_step
        
	!Setting up arrays to use in Thomas Algorithm

        !Backward Euler
        IF (NUM_METHOD.EQ.1) THEN
          trid_a(jk)=-terma(jk)+(termw(jk)+termg(jk))*NN(jk)
          trid_b(jk)=(1._dp+terma(jk)+termb(jk)+(termw(jk)+termg(jk))*NM(jk)-(termv(jk)+termh(jk))*MN(jk))
          trid_c(jk)=-termb(jk)-(termv(jk)+termh(jk))*MM(jk)
          trid_d(jk)=nd(jk)
        ELSE
          !Crank-N
          trid_a(jk)=-terma(jk)+(termw(jk)+termg(jk))*NN(jk)
          trid_b(jk)=(2._dp+terma(jk)+termb(jk)+(termw(jk)+termg(jk))*NM(jk)-(termv(jk)+termh(jk))*MN(jk))
          trid_c(jk)=-termb(jk)-(termv(jk)+termh(jk))*MM(jk)
          trid_d(jk)=-trid_a(jk)*nd(jk-1)+&
            (4.0-trid_b(jk))*nd(jk)
        ENDIF
      ELSE
        !Diffusion
        terma(jk)=Dg(jk-1)*time_sub_step/(dz2(jk)*dz1(jk))
        termb(jk)=Dg(jk)*time_sub_step/(dz2(jk)*dz3(jk))

        !Advection
        termw(jk)=u_snow(jk-1)*time_sub_step/dz2(jk)
        termv(jk)=u_snow(jk)*time_sub_step/dz2(jk)
        IF (nd(jk-1).GT.nd(jk))termw(jk)=-1*abs(termw(jk))
        IF (nd(jk).GT.nd(jk+1))termv(jk)=-1*abs(termv(jk))

        !Dry Deposition
        termg(jk)=-Vd(jk-1)*time_sub_step/dz2(jk)
        termh(jk)=-Vd(jk)*time_sub_step/dz2(jk)

        all_flux(jk)=all_flux(jk)+(termw(jk)*(NN(jk)*nd(jk-1) + NM(jk)*nd(jk)) -&
          terma(jk)*(nd(jk-1)-nd(jk)))/time_sub_step*dz2(jk)/num_sub_step
       
        !Setting up arrays to use in Thomas Algorithm

        !Backward Euler
        IF (NUM_METHOD.EQ.1) THEN
          trid_a(jk)=-terma(jk)+(termw(jk)+termg(jk))*NN(jk)
          trid_b(jk)=(1._dp+terma(jk)+termb(jk)+(termw(jk)+termg(jk))*NM(jk)-(termv(jk)+termh(jk))*MN(jk))
          trid_c(jk)=-termb(jk)-(termv(jk)+termh(jk))*MM(jk)
          trid_d(jk)=nd(jk)
        ELSE
          !Crank-N
          trid_a(jk)=-terma(jk)+(termw(jk)+termg(jk))*NN(jk)
          trid_b(jk)=(2._dp+terma(jk)+termb(jk)+(termw(jk)+termg(jk))*NM(jk)-(termv(jk)+termh(jk))*MN(jk))
          trid_c(jk)=-termb(jk)-(termv(jk)+termh(jk))*MM(jk)
          trid_d(jk)=-trid_a(jk)*nd(jk-1)+&
            (4.0-trid_b(jk))*nd(jk)&
            -trid_c(jk)*nd(jk+1)
        ENDIF
      ENDIF

      !print *,'num_sub_step',num_sub_step

    ENDDO

    !print *,'num_sub_step',num_sub_step
    !IF (jjtt.EQ. IND_HONO) THEN
    !print*,'terma', terma(19:21)
    !print*,'termb', termb(19:21)
    !print *,'trid_a',trid_a(20)
    !print *,'ttrid_b',trid_b(20)
    !print *,'trid_c',trid_c(20)
    !print *, trid_a(20)+trid_b(20)+trid_c(20)
    !print *,'old nd',nd(19:21)
    !read(*,*)
    !ENDIF
    !print *,'trid_d',trid_d
    !print *,'temperature',temperature(:,NUM_TIME_SAVE)
    !print *,'Dg',Dg
    !print *,'u_snow',u_snow

    flux=flux+(termw(NUM_ATM+1)*(NN(NUM_ATM+1)*nd(NUM_ATM) + NM(NUM_ATM+1)*nd(NUM_ATM+1)) -&
      terma(NUM_ATM+1)*(nd(NUM_ATM)-nd(NUM_ATM+1)))/time_sub_step*dz2(NUM_ATM+1)/num_sub_step
    call solve_tridiag(trid_a,trid_b,trid_c,trid_d,trid_x,NUM_DEPTH+NUM_ATM)
    nd(:)=trid_x(:)

    !IF (jjtt.EQ. IND_HONO) THEN
    !print *,'new nd',nd(19:21)
    !read(*,*)
    !ENDIF

  ENDDO

  DEALLOCATE(terma)
  DEALLOCATE(termb)
  DEALLOCATE(termv)
  DEALLOCATE(termw)
  DEALLOCATE(termg)
  DEALLOCATE(termh)
  DEALLOCATE(trid_a)
  DEALLOCATE(trid_b)
  DEALLOCATE(trid_c)
  DEALLOCATE(trid_d)
  DEALLOCATE(trid_x)

END SUBROUTINE parallel_physics

!---------------------------------------------------------------

SUBROUTINE parallel_mass_transfer(nd,kt,Henrys,L,jt)

  REAL(dp), INTENT(in)::&
    Henrys(:,:),&
    kt(:,:),&
    L(:)

  REAL(dp), INTENT(inout)::&
    nd(:,:)

  REAL(dp), DIMENSION(:), ALLOCATABLE ::&
    trid_a,&
    trid_b,&
    trid_c,&
    trid_d,&
    trid_x

  REAL(dp) :: Ca_eq,Cg_eq, MT_diff, sub_time_step, timescale

  INTEGER, INTENT(in) :: jt

  INTEGER :: jk, num_sub_step
  REAL(dp) :: CHEM_MT_SUB_STEP

  ALLOCATE(trid_a(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_b(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_c(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_d(NUM_DEPTH+NUM_ATM))
  ALLOCATE(trid_x(NUM_DEPTH+NUM_ATM))

  CHEM_MT_SUB_STEP=TIME_STEP/real(NUM_SUBSTEP_CHEM_MT)

  DO jk=1,NUM_DEPTH+NUM_ATM
    sub_time_step=TIME_STEP
    IF (CHEM_MT) sub_time_step=CHEM_MT_SUB_STEP
    timescale=1.0E+9

    !IF(is_aqueous(jt).AND. aq_gas_index(jt,1).NE.0 .AND. kt(jk,jt).GT.0.0) THEN

    Ca_eq=(nd(jk,aq_gas_index(jt,1))+nd(jk,jt)*L(jk))/(1.0/(Henrys(jk,jt)) +L(jk))
    Cg_eq=(nd(jk,aq_gas_index(jt,1))+nd(jk,jt)*L(jk))/(1.0 + Henrys(jk,jt)*L(jk))
    MT_diff=nd(jk,aq_gas_index(jt,1))-nd(jk,jt)/Henrys(jk,jt)

    IF (MT_diff.LT.0.0) THEN
      !Calculate the timescale to reach Eq. by MT for aq phase
      timescale=(Ca_eq-nd(jk,jt))/(kt(jk,jt)*MT_diff)
    ELSEIF( MT_diff.GT.0.0) THEN
      !Calculate the timescale to reach Eq. by MT for gas phase
      timescale=(Cg_eq-nd(jk,aq_gas_index(jt,1)))/(-L(jk)*kt(jk,jt)*MT_diff)
    ELSE
      timescale=sub_time_step*0.1   !This is the case that the species are in Equilibirum and thus passing a small timescale so the algorithm will put the species in equilibirum
    ENDIF

    sub_time_step=MIN(sub_time_step,timescale)

    !ENDIF

    IF (CHEM_MT) THEN
      IF (sub_time_step.LT.CHEM_MT_SUB_STEP) sub_time_step=sub_time_step*0.1
      num_sub_step=int(CHEM_MT_SUB_STEP/sub_time_step)
      sub_time_step=real(CHEM_MT_SUB_STEP)/real(num_sub_step)
    ELSE
      IF (sub_time_step.LT.TIME_STEP) sub_time_step=sub_time_step*0.1
      num_sub_step=int(TIME_STEP/sub_time_step)
      sub_time_step=real(TIME_STEP)/real(num_sub_step)
    ENDIF

    IF (Henrys(jk,jt).NE.DEFAULT_HENRYS .AND. L(jk).NE.0.0 .AND. kt(jk,jt).NE.0.0) THEN !.AND. kt(jk,jt).NE.0.0) THEN

      IF (num_sub_step.EQ.1) THEN

        IF (NUM_METHOD.EQ.1) THEN

          trid_b(jk)=1.0+ sub_time_step*L(jk)*kt(jk,jt)*(1.0-sub_time_step*kt(jk,jt)/&
            (Henrys(jk,jt)*(1.0+sub_time_step*kt(jk,jt)/Henrys(jk,jt))))
          trid_d(jk)=nd(jk,aq_gas_index(jt,1))+nd(jk,jt)*sub_time_step*L(jk)*kt(jk,jt)/&
            (Henrys(jk,jt)*(1.0+sub_time_step*kt(jk,jt)/Henrys(jk,jt)))

          !print *,'old gas',nd(jk,aq_gas_index(jt,1)), jt
          nd(jk,aq_gas_index(jt,1))=trid_d(jk)/trid_b(jk)

          !print *,'new gas',nd(jk,aq_gas_index(jt,1)), jt
          !print *,'old aq',nd(jk,aq_gas_index(jt,1)), jt

          nd(jk,jt)=(nd(jk,jt)+sub_time_step*kt(jk,jt)*nd(jk,aq_gas_index(jt,1)))/(1.0+sub_time_step*kt(jk,jt)/Henrys(jk,jt))

          !print *,'old aq',nd(jk,aq_gas_index(jt,1)), jt
          !read(*,*)
        ELSE
          trid_b(jk)=2.0+sub_time_step*L(jk)*kt(jk,jt)*(1.0-sub_time_step*kt(jk,jt)/&
            (Henrys(jk,jt)*(2.0+sub_time_step*kt(jk,jt)/Henrys(jk,jt))))

          trid_d(jk)=sub_time_step*L(jk)*kt(jk,jt)/(Henrys(jk,jt)*(2.0+sub_time_step*kt(jk,jt)/Henrys(jk,jt)))&
             *(2.0*nd(jk,jt)+sub_time_step*kt(jk,jt)*(nd(jk,aq_gas_index(jt,1))-nd(jk,jt)/Henrys(jk,jt)))

          trid_d(jk)=trid_d(jk)-&
            (nd(jk,aq_gas_index(jt,1))-nd(jk,jt)/Henrys(jk,jt))*sub_time_step*L(jk)*kt(jk,jt)

          trid_d(jk)=trid_d(jk)+&
             2.0*nd(jk,aq_gas_index(jt,1))

          !!call solve_tridiag(trid_a,trid_b,trid_c,trid_d,trid_x,NUM_DEPTH+NUM_ATM)
          nd(jk,aq_gas_index(jt,1))=trid_d(jk)/trid_b(jk)

          trid_b(jk)=2.0 + sub_time_step*kt(jk,jt)/Henrys(jk,jt)
          trid_d(jk)=2.0*nd(jk,jt)
          trid_d(jk)=trid_d(jk)+&
            sub_time_step*kt(jk,jt)*nd(jk,aq_gas_index(jt,1))
          trid_d(jk)=trid_d(jk)+&
            (nd(jk,aq_gas_index(jt,1))-nd(jk,jt)/Henrys(jk,jt))*sub_time_step*kt(jk,jt)
          nd(jk,jt)=trid_d(jk)/trid_b(jk)
        ENDIF

        !!call solve_tridiag(trid_a,trid_b,trid_c,trid_d,trid_x,NUM_DEPTH+NUM_ATM)

      ELSE
        nd(jk,jt)=Ca_eq
        nd(jk,aq_gas_index(jt,1))=Cg_eq
      ENDIF
    ENDIF

  ENDDO !End of depth loop

  DEALLOCATE(trid_a)
  DEALLOCATE(trid_b)
  DEALLOCATE(trid_c)
  DEALLOCATE(trid_d)
  DEALLOCATE(trid_x)

END SUBROUTINE parallel_mass_transfer

!---------------------------------------------------------------

SUBROUTINE parallel_chem(nd, TEMP_K,rj,chem_rates,timeday,kt,Henrys,L )

  !This subroutine will hopefully be parallizable in the future, the issue being that the KPP model uses global variables that unfortunately
  !do not run in  parallel.
  USE tropo_halo_Model
  USE tropo_halo_Initialize, ONLY: Initialize

  REAL(dp), INTENT(in) :: TEMP_k, rj(:,:),timeday,kt(:),Henrys(:),L
  REAL(dp), INTENT(inout) :: nd(:), chem_rates(:)
  REAL(dp), DIMENSION(:), ALLOCATABLE ::&
    RPROD

  CHARACTER(100) :: unix_command
  REAL(kind=dp) :: T, DVAL(NSPEC)! Time and rate of change variable
  REAL(kind=dp) :: RSTATE(20)!Integrator variable
  INTEGER :: i,jt,jl

  ALLOCATE(RPROD(NREACT))

  CALL Initialize()

  KPP_Henry(:)=DEFAULT_HENRYS
  KPP_kt(:)=0.0

  DO jt=1,NSPEC
    C(jt)=nd(jt)
    KPP_rj(jt,:)=rj(jt,:)
  ENDDO

  !print *,-1.0*kpp_kt(ind_O3)*(C(ind_O3)-C(ind_O3_aq)/kpp_Henry(ind_O3))
  !print *,kpp_kt(ind_O3),C(ind_O3),C(ind_O3_aq),kpp_Henry(ind_O3)
  !print *, kpp_kt(ind_O3_aq)*(C(ind_O3)-C(ind_O3_aq)/kpp_Henry(ind_O3_aq));
  !print *,kpp_kt(ind_O3_aq),C(ind_O3),C(ind_O3_aq),kpp_Henry(ind_O3_aq)
  !read(*,*)

  TEMP=TEMP_K

  !Initililize some parameters used by the KPP integrator
  STEPMIN = 0.0d0
  STEPMAX = 0.0d0

  DO i=1,NVAR
    RTOL(i) = 1.0d-2
    ATOL(i) = 1.0d-2
  END DO

  DT=TIME_STEP
  IF(CHEM_MT)DT=TIME_STEP/real(NUM_SUBSTEP_CHEM_MT)

  T = timeday/SEC2HRS
  TEND=T+DT

  !Time loop
  kron: DO WHILE (T < TEND)
    TIME = T
    CALL GetMass( C, DVAL )
    CALL Update_SUN()
    CALL Update_RCONST()

    CALL INTEGRATE( TIN = T, TOUT = T+DT, RSTATUS_U = RSTATE, &
    ICNTRL_U = (/ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 /) )
    T = RSTATE(1)

  END DO kron
  !~~~> End Time loop

  CALL GetMass( C, DVAL )
  TIME = T

  991   FORMAT(F6.1,'%. T=',E10.3,2X,200(A,'=',E11.4,'; '))

  call ReactantProd(VAR,FIX,RPROD)
  chem_rates(:)=RPROD(:)*RCONST(:)

  DO jt=1,NSPEC
    IF (isnan(nd(jt))) then
       print *,'CHEM sucks'
       unix_command='say -v Vicki " Your chemistry sucks"'
       DO i=1,3
         call SYSTEM(unix_command)
       ENDDO

    ELSE
      nd(jt)=C(jt)
    ENDIF
  ENDDO

  DEALLOCATE(RPROD)

END SUBROUTINE parallel_chem

!---------------------------------------------------------------

SUBROUTINE aqueous_equilibrium_para(ppx,T,jjkk)

  !This subroutine puts the aqueous phase at a given depth in aqueous equilibrium. This function was created to allow parallel processing of 
  !aqueous equilibirum over the depths
  !   ppx - Concentration of aqueous and gas phase  in[M] and [parts], respectively
  !   T - Temperture [K]
  !   K - Aqueous Equilibirum constant
  !   H - Enthalpy of K's
  !   Ct - Total mass concentrations indexed buy the non-ionic form
  !This routine is not morphic. If additional aqeous species are added to the model that affect pH, this routine needs to be modified accordingly
  !The pH is calculated based of aqueous equilibirum of acidic scpecies and solution nuetrality

  REAL(dp), INTENT(inout) :: &
    ppx(:)

  REAL(dp), INTENT(in) :: T
  REAL(dp), DIMENSION(:), ALLOCATABLE::&
    K, &
    H, &
    Ct,&
    Ct_old

  REAL(dp) :: ion_str,act_coef1,act_coef2, RHS, Hp_old,rel_error
  INTEGER, INTENT(in) :: jjkk
  INTEGER :: jk,jt,count,i,ii
  CHARACTER(100):: unix_command
  LOGICAL :: IT_FLAG,EXIT_FLAG

  ALLOCATE(K(NSPEC))
  ALLOCATE(H(NSPEC))
  ALLOCATE(Ct(NSPEC))
  ALLOCATE(Ct_old(NSPEC))

  K(:)=0.0
  H(:)=0.0
  Ct(:)=0.0

  !ppx(ind_Hp_aq)=1.0E-2

  IT_FLAG=.TRUE.
  EXIT_FLAG=.FALSE.
  count=0

  ! Total mass concentration
  Ct(ind_CO2_aq)=ppx(ind_CO2_aq)+ppx(ind_HCO3n_aq)&
    +ppx(ind_CO3n2_aq)

  Ct(ind_H2O2_aq)=ppx(ind_H2O2_aq)+ppx(ind_HO2n_aq)

  Ct(ind_NH3_aq)=ppx(ind_NH3_aq)+ppx(ind_NH4p_aq)

  Ct(ind_HCOOH_aq)=ppx(ind_HCOOH_aq)+ppx(ind_HCOOn_aq)

  Ct(ind_HO2_aq)=ppx(ind_HO2_aq)+ppx(ind_O2n_aq)

  Ct(ind_HNO3_aq)=ppx(ind_HNO3_aq)+ppx(ind_NO3n_aq)

  Ct(ind_HONO_aq)=ppx(ind_HONO_aq)+ppx(ind_NO2n_aq)

  Ct(ind_HNO4_aq)=ppx(ind_HNO4_aq)+ppx(ind_NO4n_aq)

  Ct(ind_ACTA_aq)=ppx(ind_ACTA_aq)+ppx(ind_ACTAn_aq)

  Ct(ind_HOBr_aq)=ppx(ind_HOBr_aq) + ppx(ind_BrOn_aq)

  Ct(ind_Brn_aq)= ppx(ind_Brn_aq)  + ppx(ind_HBr_aq)

  Ct(ind_HOCl_aq)=ppx(ind_HOCl_aq) + ppx(ind_ClOn_aq) 

  Ct(ind_Cln_aq)= ppx(ind_Cln_aq)  + ppx(ind_HCl_aq)

  DO JT=1, NSPEC
    Ct_old(jt)=Ct(jt)
  ENDDO

  DO WHILE (IT_FLAG)

    RHS=0.0
    K(ind_CO2_aq)=4.3E-7
    H(ind_CO2_aq)=-913.0

    K(ind_HCO3n_aq)=4.68E-11
    H(ind_HCO3n_aq)=-1760.0

    K(ind_H2O2_aq)=2.2E-12
    H(ind_H2O2_aq)=-3730.0

    K(ind_NH3_aq)=1.7E-5
    H(ind_NH3_aq)=-4325.0

    K(ind_H2O_aq)=1.0E-14
    H(ind_H2O_aq)=-6716.0

    K(ind_HCOOH_aq)=1.8E-4
    H(ind_HCOOH_aq)=-0.0

    K(ind_HO2_aq)=1.6E-5
    H(ind_HO2_aq)=-0.0

    K(ind_HNO3_aq)=1.5E1
    H(ind_HNO3_aq)=-0.0

    K(ind_HONO_aq)=5.1E-4
    H(ind_HONO_aq)=-1260.0

    K(ind_HNO4_aq)=1.0E-5
    H(ind_HNO4_aq)=8700.0

    K(ind_ACTA_aq)=1.7E-5
    !H(ind_ACTA_aq)=24.05
    H(ind_ACTA_aq)=0.0

    K(ind_HBr_aq)=1.0E9  !Lax 1969 via Thomas
    H(ind_HBr_aq)=0.0

    K(ind_HCl_aq)=1.7E-6  !Marsh and McElroy 1985 via Thomas
    H(ind_HCl_aq)=0.0

    K(ind_HOCl_aq)=3.2E-8  !Lax 1969 via Thomas
    H(ind_HOCl_aq)=0.0

    K(ind_HOBr_aq)=2.3E-9  !Kelly and Tartar 1956 via Thomas
    H(ind_HOBr_aq)=-3091.0

    !Adjust the equilibrium constants for temperature
    K(:)=K(:)*EXP(-H(:)*(1.0/T-1.0/298.0))

    !Calculate ionic strength
    ion_str=0.0

    DO jt=1,NSPEC
      IF (is_ion(jt,1).EQ.1) ion_str=ion_str+(real(is_ion(jt,2))**2.0)*ppx(jt)

      IF (is_aqueous(jt)) THEN
        IF (isnan(ppx(jt)) .OR. ppx(jt).LT.0.0) then

          print *,'aq eq sucks', SPC_NAMES(jt), jjkk, count
          print *,'ppx',ppx(jt)
          EXIT_FLAG=.TRUE.
          unix_command='say -v Vicki " Your aq sucks"'

          DO ii=1,3
            !call SYSTEM(unix_command)
          ENDDO

        ENDIF
      ENDIF
    ENDDO

    IF (EXIT_FLAG) call exit(0)
    ion_str=ion_str*0.5

    IF (ion_str>1.0E-3) ion_str=1.0E-3
    act_coef1=10.0**(-0.5102*(sqrt(ion_str)/(1.0+sqrt(ion_str))-0.15*ion_str))
    act_coef2=10.0**(-0.5102*4.0*(sqrt(ion_str)/(1.0+sqrt(ion_str))-0.15*ion_str))

    !act_coef1=01.0
    !act_coef2=01.0

    !Adjust K values for activity
    K(ind_CO2_aq)=K(ind_CO2_aq)/act_coef1**2.0

    K(ind_HCO3n_aq)=K(ind_HCO3n_aq)/(act_coef2)

    K(ind_H2O2_aq)=K(ind_H2O2_aq)/act_coef1**2.0

    K(ind_NH3_aq)=K(ind_NH3_aq)/act_coef1**2.0

    K(ind_H2O_aq)=K(ind_H2O_aq)/act_coef1**2.0

    K(ind_HCOOH_aq)=K(ind_HCOOH_aq)/act_coef1**2.0

    K(ind_HO2_aq)=K(ind_HO2_aq)/act_coef1**2.0

    K(ind_HNO3_aq)=K(ind_HNO3_aq)/act_coef1**2.0

    K(ind_HONO_aq)=K(ind_HONO_aq)/act_coef1**2.0

    K(ind_HNO4_aq)=K(ind_HNO4_aq)/act_coef1**2.0

    K(ind_ACTA_aq)=K(ind_ACTA_aq)/act_coef1**2.0

    K(ind_HCl_aq)=K(ind_HCl_aq)/act_coef1**2.0

    K(ind_HBr_aq)=K(ind_HBr_aq)/act_coef1**2.0

    K(ind_HOBr_aq)=K(ind_HOBr_aq)/act_coef1**2.0

    K(ind_HOCl_aq)=K(ind_HOCl_aq)/act_coef1**2.0

    DO jt=1,NSPEC
      IF (isnan(K(jt)).OR. K(jt).LT.0.0) then
        print *,'aq eq sucks',SPC_NAMES(jt)
        print *,'aq eq sucks',K(jt)
        print *,'act_coef1',act_coef1
        print *,'act_coef2',act_coef2
        print *,'ion_str',ion_str

        unix_command='say -v Vicki " Your aq sucks"'
        DO ii=1,3
          call SYSTEM(unix_command)
        ENDDO
        call exit(0)
      ENDIF
    ENDDO

    !CO2 system
    ppx(ind_CO2_aq)=Ct(ind_CO2_aq)/(1.0+K(ind_CO2_aq)/ppx(ind_Hp_aq)+&
    K(ind_HCO3n_aq)*K(ind_CO2_aq)/(ppx(ind_Hp_aq)**2))
    ppx(ind_HCO3n_aq)=K(ind_CO2_aq)/ppx(ind_Hp_aq)*ppx(ind_CO2_aq)
    ppx(ind_CO3n2_aq)=K(ind_HCO3n_aq)*K(ind_CO2_aq)/(ppx(ind_Hp_aq)**2)&
      *ppx(ind_CO2_aq)

    !H2O2 System
    ppx(ind_H2O2_aq)=Ct(ind_H2O2_aq)/(1.0+K(ind_H2O2_aq)/ppx(ind_Hp_aq))
    ppx(ind_HO2n_aq)=K(ind_H2O2_aq)/ppx(ind_Hp_aq)*ppx(ind_H2O2_aq)

    !NH3 system
    ppx(ind_NH4p_aq)=Ct(ind_NH3_aq)/(1.0+K(ind_H2O_aq)/K(ind_NH3_aq)/ppx(ind_Hp_aq))
    ppx(ind_NH3_aq)=K(ind_H2O_aq)/K(ind_NH3_aq)/ppx(ind_Hp_aq)*ppx(ind_NH4p_aq)

    !HCOOH System
    ppx(ind_HCOOH_aq)=Ct(ind_HCOOH_aq)/(1.0+K(ind_HCOOH_aq)/ppx(ind_Hp_aq))
    ppx(ind_HCOOn_aq)=K(ind_HCOOH_aq)/ppx(ind_Hp_aq)*ppx(ind_HCOOH_aq)

    !HO2 System
    ppx(ind_HO2_aq)=Ct(ind_HO2_aq)/(1.0+K(ind_HO2_aq)/ppx(ind_Hp_aq))
    ppx(ind_O2n_aq)=K(ind_HO2_aq)/ppx(ind_Hp_aq)*ppx(ind_HO2_aq)

    !HNO3 System
    ppx(ind_HNO3_aq)=Ct(ind_HNO3_aq)/(1.0+K(ind_HNO3_aq)/ppx(ind_Hp_aq))
    ppx(ind_NO3n_aq)=K(ind_HNO3_aq)/ppx(ind_Hp_aq)*ppx(ind_HNO3_aq)

    !HONO System
    ppx(ind_HONO_aq)=Ct(ind_HONO_aq)/(1.0+K(ind_HONO_aq)/ppx(ind_Hp_aq))
    ppx(ind_NO2n_aq)=K(ind_HONO_aq)/ppx(ind_Hp_aq)*ppx(ind_HONO_aq)

    !HNO4 System
    ppx(ind_HNO4_aq)=Ct(ind_HNO4_aq)/(1.0+K(ind_HNO4_aq)/ppx(ind_Hp_aq))
    ppx(ind_NO4n_aq)=K(ind_HNO4_aq)/ppx(ind_Hp_aq)*ppx(ind_HNO4_aq)

    !Acetatic Acid System
    ppx(ind_ACTA_aq)=Ct(ind_ACTA_aq)/(1.0+K(ind_ACTA_aq)/ppx(ind_Hp_aq))
    ppx(ind_ACTAn_aq)=K(ind_ACTA_aq)/ppx(ind_Hp_aq)*ppx(ind_ACTA_aq)

    !Bromine Systems
    ppx(ind_HOBr_aq)=Ct(ind_HOBr_aq)/(1.0+K(ind_HOBr_aq)/ppx(ind_Hp_aq))
    ppx(ind_BrOn_aq)=K(ind_HOBr_aq)/ppx(ind_Hp_aq)*ppx(ind_HOBr_aq)

    ppx(ind_Brn_aq)=Ct(ind_Brn_aq)/(1.0+ ppx(ind_Hp_aq)/K(ind_HBr_aq))
    ppx(ind_HBr_aq)=ppx(ind_Brn_aq)*ppx(ind_Hp_aq)/K(ind_HBr_aq)

    !Chlorine Systems
    ppx(ind_HOCl_aq)=Ct(ind_HOCl_aq)/(1.0+K(ind_HOCl_aq)/ppx(ind_Hp_aq))
    ppx(ind_ClOn_aq)=K(ind_HOCl_aq)/ppx(ind_Hp_aq)*ppx(ind_HOCl_aq)

    ppx(ind_Cln_aq)=Ct(ind_Cln_aq)/(1.0+ ppx(ind_Hp_aq)/K(ind_HCl_aq))
    ppx(ind_HCl_aq)=ppx(ind_Cln_aq)*ppx(ind_Hp_aq)/K(ind_HCl_aq)

    !Water system
    ppx(ind_OHn_aq)=K(ind_H2O_aq)/ppx(ind_Hp_aq)

    Hp_old=ppx(ind_Hp_aq)

    RHS=0.0
    DO jt=1, NSPEC
      IF (is_aqueous(jt)) THEN
        !IF (ppx(jt).LT.DEFAULT_MIN_M)ppx(jt)=DEFAULT_MIN_M
        IF (jt.NE.ind_Hp_aq .AND. is_ion(jt,1).EQ.1)RHS=RHS-real(is_ion(jt,2))*ppx(jt)
      ENDIF
    ENDDO

    IF (RHS.LT.0.0 .OR. isnan(RHS)) THEN
      DO jt=1,NSPEC
        IF (is_ion(jt,1).EQ.1) THEN
          print *,SPC_NAMES(jt),ppx(jt),is_ion(jt,2)
        ENDIF
      ENDDO
    ENDIF

    ppx(ind_Hp_aq)=RHS

    !ppx(ind_Hp_aq)=1.0E-3
    rel_error=abs((ppx(ind_Hp_aq)-Hp_old))

    IF (rel_error.LE.RTOL_AQ_EQ) THEN
      IT_FLAG=.FALSE.
      ppx(ind_Hp_aq)=Hp_old

      !CO2 system
      !ppx(ind_CO2_aq)=Ct(ind_CO2_aq)/(1.0+K(ind_CO2_aq)/ppx(ind_Hp_aq)+&
      !K(ind_HCO3n_aq)*K(ind_CO2_aq)/(ppx(ind_Hp_aq)**2))
      !ppx(ind_HCO3n_aq)=K(ind_CO2_aq)/ppx(ind_Hp_aq)*ppx(ind_CO2_aq)
      !ppx(ind_CO3n2_aq)=K(ind_HCO3n_aq)*K(ind_CO2_aq)/(ppx(ind_Hp_aq)**2)&
      !*ppx(ind_CO2_aq)


      !H2O2 System
      !ppx(ind_H2O2_aq)=Ct(ind_H2O2_aq)/(1.0+K(ind_H2O2_aq)/ppx(ind_Hp_aq))
      !ppx(ind_HO2n_aq)=K(ind_H2O2_aq)/ppx(ind_Hp_aq)*ppx(ind_H2O2_aq)

      !NH3 system
      !ppx(ind_NH4p_aq)=Ct(ind_NH3_aq)/(1.0+K(ind_H2O_aq)/K(ind_NH3_aq)/ppx(ind_Hp_aq))
      !ppx(ind_NH3_aq)=K(ind_H2O_aq)/K(ind_NH3_aq)/ppx(ind_Hp_aq)*ppx(ind_NH4p_aq)

      !HCOOH System
      !ppx(ind_HCOOH_aq)=Ct(ind_HCOOH_aq)/(1.0+K(ind_HCOOH_aq)/ppx(ind_Hp_aq))
      !ppx(ind_HCOOn_aq)=K(ind_HCOOH_aq)/ppx(ind_Hp_aq)*ppx(ind_HCOOH_aq)

      !HO2 System
      !ppx(ind_HO2_aq)=Ct(ind_HO2_aq)/(1.0+K(ind_HO2_aq)/ppx(ind_Hp_aq))
      !ppx(ind_O2n_aq)=K(ind_HO2_aq)/ppx(ind_Hp_aq)*ppx(ind_HO2_aq)

      !HNO3 System
      !ppx(ind_HNO3_aq)=Ct(ind_HNO3_aq)/(1.0+K(ind_HNO3_aq)/ppx(ind_Hp_aq))
      !ppx(ind_NO3n_aq)=K(ind_HNO3_aq)/ppx(ind_Hp_aq)*ppx(ind_HNO3_aq)

      !HONO System
      !ppx(ind_HONO_aq)=Ct(ind_HONO_aq)/(1.0+K(ind_HONO_aq)/ppx(ind_Hp_aq))
      !ppx(ind_NO2n_aq)=K(ind_HONO_aq)/ppx(ind_Hp_aq)*ppx(ind_HONO_aq)

      !HNO4 System
      !ppx(ind_HNO4_aq)=Ct(ind_HNO4_aq)/(1.0+K(ind_HNO4_aq)/ppx(ind_Hp_aq))
      !ppx(ind_NO4n_aq)=K(ind_HNO4_aq)/ppx(ind_Hp_aq)*ppx(ind_HNO4_aq)

      !Acetatic Acid System
      !ppx(ind_ACTA_aq)=Ct(ind_ACTA_aq)/(1.0+K(ind_ACTA_aq)/ppx(ind_Hp_aq))
      !ppx(ind_ACTAn_aq)=K(ind_ACTA_aq)/ppx(ind_Hp_aq)*ppx(ind_ACTA_aq)

      !Bromine Systems
      !ppx(ind_HOBr_aq)=Ct(ind_HOBr_aq)/(1.0+K(ind_HOBr_aq)/ppx(ind_Hp_aq))
      !ppx(ind_BrOn_aq)=K(ind_HOBr_aq)/ppx(ind_Hp_aq)*ppx(ind_HOBr_aq)

      !ppx(ind_Brn_aq)=Ct(ind_Brn_aq)/(1.0+ ppx(ind_Hp_aq)/K(ind_HBr_aq))
      !ppx(ind_HBr_aq)=ppx(ind_Brn_aq)*ppx(ind_Hp_aq)/K(ind_HBr_aq)

      !Chlorine Systems
      !ppx(ind_HOCl_aq)=Ct(ind_HOCl_aq)/(1.0+K(ind_HOCl_aq)/ppx(ind_Hp_aq))
      !ppx(ind_ClOn_aq)=K(ind_HOCl_aq)/ppx(ind_Hp_aq)*ppx(ind_HOCl_aq)

      !ppx(ind_Cln_aq)=Ct(ind_Cln_aq)/(1.0+ ppx(ind_Hp_aq)/K(ind_HCl_aq))
      !ppx(ind_HCl_aq)=ppx(ind_Cln_aq)*ppx(ind_Hp_aq)/K(ind_HCl_aq)

      !Water system
      !ppx(ind_OHn_aq)=K(ind_H2O_aq)/ppx(ind_Hp_aq)

    ELSE
      count=count+1
    ENDIF

    IF (count.GE.NUM_MAX_IT_AQ) THEN
      print *,'Iterations of Aqueous phase Eq. has reached max iteration of ', NUM_MAX_IT_AQ
      print *,'Old and new H+ molarity',Hp_old,ppx(ind_Hp_aq)
      print *,'Ionic Strength',ion_str
      unix_command='say -v Vicki " Your model failed, idiot"'

      DO i=1,3
        call SYSTEM(unix_command)
      ENDDO

      call exit(0)
    ENDIF

  ENDDO   !Iterative loop

  ! Total mass concentration
  Ct(ind_CO2_aq)=ppx(ind_CO2_aq)+ppx(ind_HCO3n_aq)&
    +ppx(ind_CO3n2_aq)

  Ct(ind_H2O2_aq)=ppx(ind_H2O2_aq)+ppx(ind_HO2n_aq)

  Ct(ind_NH3_aq)=ppx(ind_NH3_aq)+ppx(ind_NH4p_aq)

  Ct(ind_HCOOH_aq)=ppx(ind_HCOOH_aq)+ppx(ind_HCOOn_aq)

  Ct(ind_HO2_aq)=ppx(ind_HO2_aq)+ppx(ind_O2n_aq)

  Ct(ind_HNO3_aq)=ppx(ind_HNO3_aq)+ppx(ind_NO3n_aq)

  Ct(ind_HONO_aq)=ppx(ind_HONO_aq)+ppx(ind_NO2n_aq)

  Ct(ind_HNO4_aq)=ppx(ind_HNO4_aq)+ppx(ind_NO4n_aq)

  Ct(ind_ACTA_aq)=ppx(ind_ACTA_aq)+ppx(ind_ACTAn_aq)

  Ct(ind_HOBr_aq)=ppx(ind_HOBr_aq) + ppx(ind_BrOn_aq)
  Ct(ind_Brn_aq)= ppx(ind_Brn_aq)  + ppx(ind_HBr_aq)

  Ct(ind_HOCl_aq)=ppx(ind_HOCl_aq) + ppx(ind_ClOn_aq)
  Ct(ind_Cln_aq)= ppx(ind_Cln_aq)  + ppx(ind_HCl_aq)


  DO jt=1, NSPEC
    !if (Ct_old(jt).GT.0.0)print *,Ct_old(jt),Ct(jt),SPC_NAMES(jt)
  ENDDO
  !read(*,*)

  DEALLOCATE(K)
  DEALLOCATE(H)
  DEALLOCATE(Ct)
  DEALLOCATE(Ct_old)

END SUBROUTINE aqueous_equilibrium_para

!---------------------------------------------------------------

SUBROUTINE calc_Vd(T,Henrys,Vd)

  REAL(dp), INTENT(IN):: T(:),Henrys(:,:)
  REAL(dp), INTENT(INOUT):: Vd(:,:)

  REAl(dp), DIMENSION(:), ALLOCATABLE::&
  dryreac,&! reactivity coefficient [0:non-react., 0.1:semi react., 1:react.]
  rsnow    !Snow resistance [s/m]

  REAL(dp) :: rsnow_o3,rsnow_so2

  INTEGER:: jt,jk

  ALLOCATE(dryreac(NSPEC))
  ALLOCATE(rsnow(NSPEC))

  Vd(:,:)=0.0
  dryreac(:)=0.

  dryreac(ind_O3)=1.
  dryreac(ind_HNO3)=1.
  dryreac(ind_NO)=1.
  dryreac(ind_NO2)=1.
  dryreac(ind_SO2)=0.
  dryreac(ind_H2O2)=1.
  dryreac(ind_ISOP)=0.
  dryreac(ind_PAN)=0.1
  dryreac(ind_ACO2)=0.
  dryreac(ind_NH3)=0.
  dryreac(ind_Br)=1.    !ESS_km_20130510 Added Bromine chemistry
  dryreac(ind_BrO)=1.   !ESS_km_20130510 Added Bromine chemistry
  dryreac(ind_HOBr)=1.  !ESS_km_20130510 Added Bromine chemistry
  dryreac(ind_SO4)=1.
  dryreac(ind_HNO4)=1.
  dryreac(ind_NO3)=1.
  dryreac(ind_O2)=0.
  dryreac(ind_H2O)=0.
  dryreac(ind_CO2)=0.
  dryreac(ind_H2)=0.
  dryreac(ind_Cl)=1.
  dryreac(ind_ClO)=1.
  dryreac(ind_HOCl)=1.

  rsnow_so2=1._dp
  rsnow_o3=2000._dp

  rsnow(:)=0.0

  DO jt=1, NSPEC
    IF (is_aqueous(jt).EQV. .FALSE.  .AND. aq_gas_index(jt,2).NE.0) THEN
      ! mz_lg_20030811+, modified to deal with the fact that there might
      !     multiple tracers starting with the same basename but with
      !     with different subnames, e.g., SO2 (e.g., SO2, SO2_GM7)


      IF(jt.EQ.ind_SO2) THEN
        rsnow(jt)=rsnow_so2
      ELSEIF (jt.EQ.ind_O3) THEN
        rsnow(jt)=rsnow_o3
      ELSEIF (jt.EQ.ind_SO4) THEN
        rsnow(jt)=1.e5_dp
      ELSEIF (jt.EQ.ind_HNO3) THEN
        rsnow(jt)=1._dp
      ELSEIF (jt.EQ.ind_NO) THEN
        rsnow(jt)=1.e5_dp
      ELSEIF (jt.EQ.ind_NO2) THEN
        rsnow(jt)=1.e5_dp
      ELSE
        rsnow(jt)=1./(Henrys(NUM_ATM+1,jt)/(M2ND*R_gas*T(NUM_ATM+1)/AV_N)/(1.e5*rsnow_so2)+ &
          dryreac(jt)/rsnow_o3)

        ! MAQ_lg_20180412+ rsnow set for all other tracers to 0. (dangerous, when using rsnow in divisions....)
        rsnow(jt)=0.0
      ENDIF
    ENDIF
  ENDDO

  DO jk=1,NUM_ATM+NUM_DEPTH
    DO jt=1,NSPEC
      IF (jk.EQ. NUM_ATM ) THEN
        IF(rsnow(jt).NE.0.0) Vd(jk,jt)=1.0/(rsnow(jt) + 1000.0)  ! MAQ_lg_20180412+ note this addition of this term
	                                                         ! 1000 s m-1 (is this to account for limiting diffusion??)
        !print *,Vd(jk,jt),SPC_NAMES(jt)
      ENDIF
    ENDDO
  ENDDO

  ! MAQ_lg_20180412+ Vd set to 0 for all tracers 
  Vd(:,:)=0.0
  ! MAQ_lg_20180321+
  write(*,'(1a)')' subroutine calc_Vd: snow_main.f90: Vd set to 0 for all tracers!' 
  !print *,'give ENTER to continue' 
  !read (*,*)
  ! MAQ_lg_20180321-

  DEALLOCATE(dryreac)
  DEALLOCATE(rsnow)

END SUBROUTINE calc_Vd

!---------------------------------------------------------------

SUBROUTINE calculate_eddy_diff(friction_v,depth, Kh,monin)

  REAL(dp), INTENT(in) :: friction_v, depth,monin
  REAL(dp), INTENT(out):: Kh
  REAl(dp):: phi

  IF (monin .LT. 0) THEN
    phi=(1.0-16.0*depth/monin)**(-0.25)
  ELSEIF (monin .GT.0) THEN
    phi=1.0+4.6*depth/monin
  ELSE
    phi=1.0
  ENDIF
  
  Kh=MAX(Von_Karman*depth*friction_v/phi,MIN_Kh)

END SUBROUTINE calculate_eddy_diff

!---------------------------------------------------------------

SUBROUTINE photo_clearsky_comp(obs_irr, rj ,month, day, hour,model_irr)

  REAL(dp), INTENT(in) :: obs_irr,hour
  REAL(dp), INTENT(inout):: rj(:,:,:)
  INTEGER, INTENT(in) :: month, day
  REAL(dp), PARAMETER :: Cancer = 23.45 * (pi/180.)
  REAL(dp), INTENT(out) :: model_irr
  REAL(dp):: clearsky_frac,DayReal, zen,SoDecli, timeday,cossza, Latitu

  INTEGER :: jk,jt,jl

  timeday=hour
  ! Calculate radiation as a function of latitude, season and time of the day;
  ! see also emdep_update_jval for these calculations as well as:
  ! http://education.gsfc.nasa.gov/experimental/all98invproject.site/pages/science-briefs/ed-stickler/ed-irradiance.html
  ! Day as a real value (at start of spring DayReal = 0):
  DayReal = hour/24.0+day
  ! seasonal cycle, Solar declination angle:
  SoDecli = -Cancer * COS (2.*PI*(DayReal+10.)/365.25)
  ! to correct for latitude
  Latitu = LAT * (pi/180.)
  zen    =   ACOS(SIN (Latitu) * SIN (SoDecli) &        ! Zenith angle
     +COS (Latitu) * COS (SoDecli) * &
      COS ((15./360.)*(timeday-12)*2.*PI))
  cossza =   COS(zen)                                ! cosine of zenith angle
  model_irr=   MAX(0.,MAX_RAD*MAX(0.0,cossza))
  ! global radiation, and scaled with photon to consider lat, time and season
  clearsky_frac=1.0
  IF (model_irr .GT. 0.0) clearsky_frac=obs_irr/model_irr

  IF( clearsky_frac .GE.0.0 .AND. clearsky_frac.LE.1.0) rj(:,:,:)=rj(:,:,:)*clearsky_frac

  !print *,'Clear Sky frac', clearsky_frac
  !read(*,*)

END SUBROUTINE photo_clearsky_comp

!---------------------------------------------------------------

END MODULE snow_subroutine_mod
