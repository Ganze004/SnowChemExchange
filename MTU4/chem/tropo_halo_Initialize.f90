! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! 
! Initialization File
! 
! Generated by KPP-2.2.3 symbolic chemistry Kinetics PreProcessor
!       (http://www.cs.vt.edu/~asandu/Software/KPP)
! KPP is distributed under GPL, the general public licence
!       (http://www.gnu.org/copyleft/gpl.html)
! (C) 1995-1997, V. Damian & A. Sandu, CGRER, Univ. Iowa
! (C) 1997-2005, A. Sandu, Michigan Tech, Virginia Tech
!     With important contributions from:
!        M. Damian, Villanova University, USA
!        R. Sander, Max-Planck Institute for Chemistry, Mainz, Germany
! 
! File                 : tropo_halo_Initialize.f90
! Time                 : Mon Mar 17 23:06:27 2014
! Working directory    : /Users/Keenan/Documents/kpp
! Equation file        : tropo_halo.kpp
! Output root filename : tropo_halo
! 
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



MODULE tropo_halo_Initialize

  USE tropo_halo_Parameters, ONLY: dp, NVAR, NFIX
  IMPLICIT NONE

CONTAINS


! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! 
! Initialize - function to initialize concentrations
!   Arguments :
! 
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SUBROUTINE Initialize ( )


  USE tropo_halo_Global

  INTEGER :: i
  REAL(kind=dp) :: x

  CFACTOR = 1.000000e+00_dp

  x = (1.0E-8)*CFACTOR
  DO i = 1, NVAR
    VAR(i) = x
  END DO

  x = (1.0E-8)*CFACTOR
  DO i = 1, NFIX
    FIX(i) = x
  END DO

  VAR(16) = (1.0)*CFACTOR
  VAR(60) = (1700)*CFACTOR
  VAR(62) = (1E-8)*CFACTOR
  VAR(63) = (2.0)*CFACTOR
  VAR(87) = (100.0)*CFACTOR
  VAR(131) = (100.0)*CFACTOR
  VAR(133) = (5.0)*CFACTOR
  VAR(137) = (0.05)*CFACTOR
  VAR(138) = (0.05)*CFACTOR
  VAR(139) = (0.1)*CFACTOR
  FIX(1) = (2.E+8)*CFACTOR
  FIX(2) = (500.0)*CFACTOR
  FIX(3) = (5.E+7)*CFACTOR
  FIX(4) = (500)*CFACTOR
! constant rate coefficients
  RCONST(5) = 1.45e-12
  RCONST(6) = 5e-06
  RCONST(9) = 2.5e-12
  RCONST(11) = 7.218e-12
  RCONST(13) = 1.2794e-11
  RCONST(16) = 1.3e-21
  RCONST(17) = 1.7823e-13
  RCONST(23) = 4.7165e-12
  RCONST(26) = 4e-24
  RCONST(29) = 1e-20
  RCONST(32) = 1e-11
  RCONST(33) = 1e-14
  RCONST(35) = 2e-12
  RCONST(36) = 1e-13
  RCONST(37) = 3.2e-13
  RCONST(38) = 6e-16
  RCONST(40) = 2.7e-15
  RCONST(43) = 4.7e-12
  RCONST(54) = 4e-17
  RCONST(55) = 2e-12
  RCONST(58) = 5e-14
  RCONST(59) = 5e-14
  RCONST(60) = 3e-12
  RCONST(61) = 3e-12
  RCONST(62) = 3e-12
  RCONST(63) = 3e-12
  RCONST(64) = 3e-12
  RCONST(68) = 7e-16
  RCONST(69) = 3e-12
  RCONST(76) = 7e-12
  RCONST(77) = 7e-13
  RCONST(78) = 4e-18
  RCONST(79) = 7e-12
  RCONST(80) = 7e-13
  RCONST(81) = 4e-18
  RCONST(82) = 5e-14
  RCONST(83) = 5e-14
  RCONST(84) = 3e-12
  RCONST(85) = 3e-12
  RCONST(86) = 7e-14
  RCONST(87) = 7e-14
  RCONST(88) = 1.26e-13
  RCONST(89) = 6.8e-12
  RCONST(90) = 3e-12
  RCONST(92) = 1.36e-14
  RCONST(93) = 1.36e-14
  RCONST(94) = 1.36e-14
  RCONST(95) = 1.36e-14
  RCONST(96) = 1.52e-11
  RCONST(99) = 1.15e-11
  RCONST(101) = 1.73e-11
  RCONST(102) = 4.25e-11
  RCONST(103) = 1e-11
  RCONST(104) = 2.8e-11
  RCONST(105) = 4.7e-12
  RCONST(110) = 4e-12
  RCONST(111) = 4e-12
  RCONST(112) = 1e-12
  RCONST(116) = 3e-12
  RCONST(119) = 3e-12
  RCONST(120) = 1.02e-11
  RCONST(121) = 4.7e-12
  RCONST(124) = 3e-12
  RCONST(127) = 3e-12
  RCONST(128) = 1.5e-11
  RCONST(129) = 4.7e-12
  RCONST(132) = 3e-12
  RCONST(138) = 3.4e-18
  RCONST(141) = 3.4e-18
  RCONST(144) = 3.4e-18
  RCONST(147) = 1.7e-11
  RCONST(151) = 3e-12
  RCONST(152) = 6e-14
  RCONST(154) = 3e-12
  RCONST(155) = 3.3e-15
  RCONST(156) = 6.7e-15
  RCONST(158) = 3e-12
  RCONST(159) = 5.2e-16
  RCONST(160) = 3e-12
  RCONST(161) = 3e-12
  RCONST(162) = 7e-14
  RCONST(163) = 7e-14
  RCONST(164) = 7e-14
  RCONST(166) = 5e-14
  RCONST(167) = 5.44e-17
  RCONST(168) = 3.46e-17
  RCONST(169) = 2.21e-17
  RCONST(170) = 1.03e-15
  RCONST(171) = 2.16e-16
  RCONST(174) = 1e-11
  RCONST(175) = 8.3e-12
  RCONST(182) = 0
  RCONST(183) = 0
  RCONST(223) = 0
  RCONST(224) = 0
  RCONST(225) = 0
  RCONST(226) = 0
  RCONST(227) = 0
  RCONST(228) = 0
  RCONST(229) = 0
  RCONST(230) = 0
  RCONST(231) = 0
  RCONST(232) = 0
  RCONST(233) = 0
  RCONST(234) = 0
  RCONST(253) = 0
  RCONST(254) = 0
  RCONST(255) = 0
  RCONST(256) = 0
  RCONST(257) = 0
  RCONST(258) = 0
  RCONST(259) = 0
  RCONST(260) = 0
  RCONST(261) = 0
  RCONST(262) = 0
  RCONST(263) = 0
  RCONST(264) = 0
  RCONST(265) = 0
  RCONST(266) = 0
  RCONST(267) = 0
  RCONST(268) = 0
  RCONST(269) = 0
  RCONST(270) = 0
  RCONST(271) = 0
  RCONST(272) = 0
  RCONST(273) = 0
  RCONST(274) = 0
  RCONST(275) = 0
  RCONST(276) = 0
  RCONST(277) = 0
  RCONST(278) = 0
  RCONST(279) = 0
  RCONST(281) = 0
  RCONST(282) = 0
  RCONST(284) = 0
  RCONST(285) = 0
  RCONST(287) = 0
  RCONST(288) = 0
  RCONST(289) = 0
  RCONST(290) = 0
  RCONST(353) = 0
  RCONST(355) = 0
  RCONST(356) = 1
! END constant rate coefficients

! INLINED initializations

	TSTART = 12.D0*3600.D0
	TEND = TSTART + 24.D0*3600.D0 * 5
	DT = 3600.D0
        TEMP = 288.15

! End INLINED initializations

      
END SUBROUTINE Initialize

! End of Initialize function
! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



END MODULE tropo_halo_Initialize

