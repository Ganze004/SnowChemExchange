MODULE tropo_halo_Model

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!  Completely defines the model tropo_halo
!    by using all the associated modules
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  USE tropo_halo_Precision
  USE tropo_halo_Parameters
  USE tropo_halo_Global
  USE tropo_halo_Function
  USE tropo_halo_Integrator
  USE tropo_halo_Rates
  USE tropo_halo_Jacobian
  USE tropo_halo_Hessian
  USE tropo_halo_Stoichiom
  USE tropo_halo_LinearAlgebra
  USE tropo_halo_Monitor
  USE tropo_halo_Util

END MODULE tropo_halo_Model

