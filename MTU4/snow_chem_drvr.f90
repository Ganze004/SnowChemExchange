






SUBROUTINE snow_sun_efold(depths,e_fold)
!This subroutine calculates the photolysis efold percentage of a single unity variable over the layers of a model.

USE tropo_halo_Precision

REAL(dp), INTENT(in) :: depths(:)
REAL(dp), INTENT(inout) :: e_fold(:)


INTEGER :: jk

DO jk=1,nsnowmlay

e_fold(jk)=-j_efold/(depths(jk+1)- depths(jk))*(EXP(depths(jk)/j_efold)-EXP(depths(jk+1)/j_efold ))
!print *,'e_fold,depths(jk),depths(jk+1),jk',e_fold(jk),depths(jk),depths(jk+1),jk


ENDDO

END SUBROUTINE snow_sun_efold
