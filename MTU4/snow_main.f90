
    program snow_main

!$    use OMP_LIB

    USE tropo_halo_Parameters
    USE tropo_halo_Precision
    USE tropo_halo_Global
    USE tropo_halo_Monitor
    USE snow_parameters
    USE snow_global
    USE snow_subroutine_mod

    IMPLICIT NONE

REAL(dp), DIMENSION(:,:,:), ALLOCATABLE    :: &
    ppx,&           ! Concentrations in parts for gas phase, molarity for aq, will hold old and new values
    nd,&            ! Concentration in number density (molecules/cm3), will hold old and new values
    rj,&            ! Photolysis rates in and above snowpack, will hold old and new values
    tendencies      ! Tendencies

REAL(dp), DIMENSION(:), ALLOCATABLE   :: &
    hour,&          ! Time of day in decimal hrs
    depths,&        ! Depths at center of layers in meters. Depths below the surface are negative
    edge_depths,&   ! Depths at the edges of layers in meters
    snow_density,&  ! snow denisty in g/cm3
    snow_radius,&   ! radius of snowflake in 
    ssa,&           ! cm2/g of snow (Surface area to mass)
    QLL_thickness,& ! Thickness of QLL in meters
    porosity, &     ! Porosity of snow (0-1)
    L, &            ! Volumetric Ratio of QLL to Interstitial air
    flux            ! Flux in m molecules/cm3 s

REAL(dp), DIMENSION(:,:), ALLOCATABLE :: &
    kt, &           !mass transfer coefficents in (m3air)/(m3aq s)
    Henrys,&        !Henrys law constants
    model_obs,&
    chem_rates,&
    Ct,&            !Total concentration of ions
    theta,&         !Mass fraction of liquid water of snowflakes
    Vd,&            !Deposition velocity [m/s]
    all_flux        !Fluxes at all edges in m molecules/cm3 s

REAL (dp) :: output_time_past=0.0,&
    wind,&
    u_wind,&
    v_wind,&
    photo_time,&    !Used to track time between output writing events (in hrs)
    Ct_N,&          !Total nitrogen in system
    l_thick,&       !thickness of layer
    ustar,&         !friction velocity m/s
    monin,&         !monin-ob length m
    obs_irr=0.0,&   !Observed irradiance W/m2
    model_irr=0.0   !modeled irradiance W/m2

INTEGER, DIMENSION(:), ALLOCATABLE :: &
    j_day,&         !Julian day
    day,&           !Day of the month
    month,&         !Month
    year            !Year

CHARACTER(1000):: unix_command

INTEGER :: i,j,k,ii,jt,jk,output_counter=0

!ALLOCATE global variables
ALLOCATE(temperature(NUM_DEPTH+NUM_ATM,NUM_TIME_SAVE))
ALLOCATE(gas_nd(NUM_DEPTH+NUM_ATM,NUM_TIME_SAVE))
ALLOCATE(mol_w(NSPEC))

ALLOCATE(ppx(NUM_DEPTH+NUM_ATM,NSPEC,NUM_TIME_SAVE))
ALLOCATE(nd(NUM_DEPTH+NUM_ATM,NSPEC,NUM_TIME_SAVE))
ALLOCATE(rj(NUM_DEPTH+NUM_ATM,NSPEC,MAX_J))
ALLOCATE(j_day(NUM_TIME))
ALLOCATE(day(NUM_TIME))
ALLOCATE(month(NUM_TIME))
ALLOCATE(hour(NUM_TIME))
ALLOCATE(year(NUM_TIME))
ALLOCATE(depths(NUM_DEPTH+NUM_ATM))
ALLOCATE(edge_depths(NUM_DEPTH+NUM_ATM+1))
ALLOCATE(snow_density(NUM_DEPTH+NUM_ATM))
ALLOCATE(snow_radius(NUM_DEPTH+NUM_ATM))
ALLOCATE(ssa(NUM_DEPTH+NUM_ATM))
ALLOCATE(QLL_thickness(NUM_DEPTH+NUM_ATM))
ALLOCATE(porosity(NUM_DEPTH+NUM_ATM))
ALLOCATE(kt(NUM_DEPTH+NUM_ATM,NSPEC))
ALLOCATE(Henrys(NUM_DEPTH+NUM_ATM,NSPEC))
ALLOCATE(L(NUM_DEPTH+NUM_ATM))
ALLOCATE(is_aqueous(NSPEC))
ALLOCATE(aq_gas_index(NSPEC,2))
ALLOCATE(model_obs(NUM_TIME,NUM_OBS))
ALLOCATE(tendencies(NUM_DEPTH+NUM_ATM,NSPEC,NUM_TEND))
ALLOCATE(chem_rates(NUM_DEPTH+NUM_ATM,NREACT))
ALLOCATE(is_ion(NSPEC,2))
ALLOCATE(Ct(NUM_DEPTH+NUM_ATM,NUM_TIME_SAVE))
ALLOCATE(theta(NUM_DEPTH+NUM_ATM,NUM_TIME_SAVE))
ALLOCATE(Vd(NUM_DEPTH+NUM_ATM,NSPEC))
ALLOCATE(flux(NSPEC))
ALLOCATE(all_flux(NUM_ATM+NUM_DEPTH,NSPEC))

!Parallel
!$    call OMP_SET_NUM_THREADS(NSPEC)

!Initialization of the model

! MAQ_20180321+ modelflow file
OPEN(MODELFLOW_FILE_NUM,FILE=MODELFLOW_FILE)

! MAQ_lg_20180321+
write(*,'(1a)')' snow_main.f90: Initialization of the model: CALL calc_time_arrays'
!print *,'give ENTER to continue' 
!read (*,*)
! MAQ_lg_20180321-

CALL calc_time_arrays(START_DATE,year, month,day, hour,j_day)

! MAQ_lg_20180321+
write(*,'(3(1a,f5.2))') ' The model will run for ',(float(NUM_TIME)*TIME_STEP)/86400, &
   ' days, with a timestep of: ',TIME_STEP,' sec.'
!print *,'give ENTER to continue' 
!read (*,*)
! MAQ_lg_20180321-

CALL calc_depths(depths,edge_depths)

! MAQ_lg_20180321+
write(*,'(1a,100(f5.1,1x))') ' snow_main.f90: defined edges of the layers [m]: ',edge_depths
!print *,'give ENTER to continue' 
!read (*,*)
! MAQ_lg_20180321-

CALL update_is_aqueous()
CALL update_is_ion()
CALL match_gas_aq(aq_gas_index)
CALL snow_initialize_temperature()
CALL update_Henrys(temperature(:,NUM_TIME_SAVE),Henrys)
CALL snow_initialize(ppx,nd,rj,Henrys,Ct,theta,QLL_thickness,snow_density,porosity,snow_radius,ssa,depths,L)


!OPEN output files


OPEN(RJ_FILE_NUM,FILE=RJ_FILE, STATUS='REPLACE')
OPEN(GAS_FILE_ND_NUM,FILE=GAS_FILE_ND, STATUS='REPLACE')
OPEN(GAS_FILE_PPX_NUM,FILE=GAS_FILE_PPX, STATUS='REPLACE')
OPEN(SPCS_FILE_NUM,FILE=SPCS_FILE, STATUS='REPLACE')

! MAQ_lg_20180321+ formatted writing of all species names on 
!   one line to secure proper reading with the matlab scripts
!   was: WRITE(SPCS_FILE_NUM,*)
WRITE(SPCS_FILE_NUM,'(200(A10,1x))') SPC_NAMES

OPEN(DEPTHS_FILE_NUM,FILE=DEPTHS_FILE, STATUS='REPLACE')
WRITE(DEPTHS_FILE_NUM,'(200(f10.3,1x))') edge_depths
OPEN(TIME_FILE_NUM,FILE=TIME_FILE)
OPEN(TEMP_FILE_NUM,FILE=TEMP_FILE)
OPEN(TEND_FILE_NUM,FILE=TEND_FILE)
OPEN(CHEMR_FILE_NUM,FILE=CHEMR_FILE)
OPEN(CHEM_EQ_FILE_NUM,FILE=CHEM_EQ_FILE)
OPEN(QLL_FILE_NUM,FILE=QLL_FILE)
OPEN(N_MB_FILE_NUM,FILE=N_MB_FILE)
OPEN(IRR_FILE_NUM,FILE=IRR_FILE)
OPEN(FLUX_FILE_NUM,FILE=FLUX_FILE)
OPEN(ALL_FLUX_FILE_NUM,FILE=ALL_FLUX_FILE)
OPEN(VOL_FILE_NUM,FILE=VOL_FILE)

DO j=1, NREACT
  WRITE(CHEM_EQ_FILE_NUM,*) EQN_NAMES(j)
ENDDO

model_obs(:,:)=DEFAULT_OBS
tendencies(:,:,:)=0.0
call snow_read_input(year,month,day,hour,model_obs)

! MAQ_lg_20180321+
write(*,'(1a)')' snow_main.f90: wind speed is set at a fixed value of 5 m s-1!!'
!print *,'give ENTER to continue' 
!read (*,*)
! MAQ_lg_20180321-

wind=5.0 !set constant for the time being at 5 m/s

!Time loop for model

! MAQ_lg_20180321+
write(*,'(1a,i2)')' Numerical Method Set (0 = Default- Crank-N, 1 = Backwards Euler): ', NUM_METHOD
!print *,'give ENTER to continue' 
!read (*,*)
! MAQ_lg_20180321-

flux(:)=0.0
all_flux(:,:)=0.0

! MAQ_lg_20180321+ start time loop
DO i=1, NUM_TIME

   output_time_past=output_time_past+TIME_STEP*SEC2HRS

   IF ((output_time_past .GE. OUTPUT_TIME_STEP) .OR. i.EQ.1) THEN
     output_time_past=0.0
     output_counter=output_counter+1
     OPEN(MODEL_PARA_FILE_NUM,FILE=MODEL_PARA_FILE,STATUS='REPLACE')
     WRITE(MODEL_PARA_FILE_NUM,*) NUM_DEPTH+NUM_ATM,NSPEC,output_counter,MAX_J,NUM_TEND,NREACT
     CLOSE(MODEL_PARA_FILE_NUM)

     ! MAQ_lg_20180321+ formatted writing of incoming radiation (global, shortwave) the
     !    first column contains the potentially measured irradiance, the second one the model estimated (clear sky) irradiance.
     WRITE(IRR_FILE_NUM,*) obs_irr,model_irr

     DO k=1,MAX_J
       ! MAQ_lg_20180321+ formatted writing of photolysis rates to secure proper reading with the matlab scripts
       !   was: WRITE(RJ_FILE_NUM,*)
       !   for the time being, only writing the NO2 photolysis rates to reduce output
       WRITE(RJ_FILE_NUM,'(200(e10.3,1x))') (MAX(1.e-30,rj(j,ind_NO2,k)),j=1,NUM_DEPTH+NUM_ATM)
     ENDDO

     DO k=1,NUM_TEND
       ! MAQ_lg_20180321+ formatted writing of tendencies to secure proper reading with the matlab scripts
       !   was: WRITE(TEND_FILE_NUM,*)
       WRITE(TEND_FILE_NUM,'(10000(e10.3,1x))') (tendencies(j,:,k),j=1,NUM_DEPTH+NUM_ATM)
     ENDDO

     ! MAQ_lg_20180321+ formatted writing of fluxes to secure proper reading with the matlab scripts
     !   was: WRITE(FLUX_FILE_NUM,*)
     WRITE(FLUX_FILE_NUM,'(200(e10.3,1x))') (flux(j), j=1,NSPEC)

     Ct_N=0.0

     ! MAQ_lg_20180321+ loop over all layers

     DO j=1,NUM_DEPTH+NUM_ATM
       l_thick=edge_depths(j)-edge_depths(j+1)
       Ct_N=Ct_N+l_thick*porosity(j)*(nd(j,ind_NH3,NUM_TIME_SAVE) +L(j)*nd(j,ind_NO4n_aq,NUM_TIME_SAVE) + &
         L(j)*nd(j,ind_NH3_aq,NUM_TIME_SAVE)+nd(j,ind_NH4p_aq,NUM_TIME_SAVE)*L(j)+2.0*nd(j,ind_N2O5,NUM_TIME_SAVE)+&
         nd(j,ind_HONO,NUM_TIME_SAVE)+nd(j,ind_HNO4,NUM_TIME_SAVE)+nd(j,ind_PAN,NUM_TIME_SAVE)+&
         nd(j,ind_HONO_aq,NUM_TIME_SAVE)*L(j)+nd(j,ind_NO_aq,NUM_TIME_SAVE)*L(j)+nd(j,ind_HNO4_aq,NUM_TIME_SAVE)*L(j)+&
         nd(j,ind_HNO3,NUM_TIME_SAVE)+nd(j,ind_NO,NUM_TIME_SAVE)+nd(j,ind_NO2,NUM_TIME_SAVE)+nd(j,ind_NO3,NUM_TIME_SAVE)+&
         nd(j,ind_NO2_aq,NUM_TIME_SAVE)*L(j)+nd(j,ind_NO3_aq,NUM_TIME_SAVE)*L(j)+&
         nd(j,ind_NO2n_aq,NUM_TIME_SAVE)*L(j)+nd(j,ind_NO3n_aq,NUM_TIME_SAVE)*L(j) +nd(j,ind_HNO3_aq,NUM_TIME_SAVE)*L(j))

       ! MAQ_lg_20180321+ formatted writing of gas ND to secure proper reading with the matlab scripts
       !   was: WRITE(GAS_FILE_ND_NUM,*)
       WRITE(GAS_FILE_ND_NUM,'(200(e10.3,1x))') (nd(j,k ,NUM_TIME_SAVE), k=1,NSPEC)

       ! MAQ_lg_20180321+ formatted writing of gas concentrations to secure proper reading with the matlab scripts
       !   was: WRITE(GAS_FILE_PPX_NUM,*)
       WRITE(GAS_FILE_PPX_NUM,'(200(e10.3,1x))') (ppx(j,k,NUM_TIME_SAVE), k=1,NSPEC)

       ! MAQ_lg_20180321+ formatted writing of temp. to secure proper reading with the matlab scripts
       !   was: WRITE(TEMP_FILE_NUM,*)
       WRITE(TEMP_FILE_NUM,'(200(e10.3,1x))') temperature(j,NUM_TIME_SAVE)

       ! MAQ_lg_20180321+ formatted writing of QLL to secure proper reading with the matlab scripts
       !   was: WRITE(QLL_FILE_NUM,*)
       WRITE(QLL_FILE_NUM,'(200(e12.5,1x))') QLL_thickness(j)  ! MAQ_lg_20180412+ more numbers behind the commma...

       ! MAQ_lg_20180321+ formatted writing of L to secure proper reading with the matlab scripts
       !   was: WRITE(QLL_FILE_NUM,*)
       WRITE(VOL_FILE_NUM,'(200(e10.3,1x))') L(j)

       ! MAQ_lg_20180321+ formatted writing of chem_rates to secure proper reading with the matlab scripts
       !   was: WRITE(CHEMR__FILE_NUM,*)
       WRITE(CHEMR_FILE_NUM,'(500(e10.3,1x))') (chem_rates(j,k), k=1, NREACT) ! MAQ_lg_20180409+ increased number of reactions

       ! MAQ_lg_20180321+ formatted writing of all fluxes to secure proper reading with the matlab scripts
       !   was: WRITE(ALL_FLUX_FILE_NUM,*)
       WRITE(ALL_FLUX_FILE_NUM,'(200(e10.3,1x))') (all_flux(j,k), k=1,NSPEC)

     ENDDO

     ! MAQ_lg_20180321+ end loop over all layers

     WRITE(N_MB_FILE_NUM,*) Ct_N
     WRITE(TIME_FILE_NUM,*) year(i),month(i),day(i),hour(i)
   ENDIF

   call snow_gas_nd_update()

   !ppx(1,ind_HONO,NUM_TIME_SAVE)=8000.0E-12_dp
   !nd(1,ind_HONO,NUM_TIME_SAVE)=ppx(1,ind_HONO,NUM_TIME_SAVE)

   !print *,model_obs(i,:)
   !read(*,*)

   IF (model_obs(i,1).GE.0) THEN
     !ppx(1:NUM_ATM,ind_NO,NUM_TIME_SAVE)=model_obs(i,1)*1.0E-12
     !nd(1:NUM_ATM,ind_NO,NUM_TIME_SAVE)=ppx(1,ind_NO,NUM_TIME_SAVE)*gas_nd(1,NUM_TIME_SAVE)
   ENDIF

   IF (model_obs(i,2).GE.0) THEN
     !ppx(1:NUM_ATM,ind_NO2,NUM_TIME_SAVE)=model_obs(i,2)*1.0E-12
     !nd(1:NUM_ATM,ind_NO2,NUM_TIME_SAVE)=ppx(1,ind_NO2,NUM_TIME_SAVE)*gas_nd(1,NUM_TIME_SAVE)
   ENDIF

   IF (model_obs(i,3).GE.0) THEN
     ppx(1,ind_O3,NUM_TIME_SAVE)=model_obs(i,3)*1.0E-9
     nd(1,ind_O3,NUM_TIME_SAVE)=ppx(1,ind_O3,NUM_TIME_SAVE)*gas_nd(1,NUM_TIME_SAVE)

     DO jk=1, NUM_ATM
       IF (depths(jk).GT.OBSERVE_HEIGHT) THEN  !Place observations 1m and above the surface
         ppx(jk,ind_O3,NUM_TIME_SAVE)=model_obs(i,3)*1.0E-9
         nd(jk,ind_O3,NUM_TIME_SAVE)=ppx(jk,ind_O3,NUM_TIME_SAVE)*gas_nd(jk,NUM_TIME_SAVE)
       ENDIF
     ENDDO
   ENDIF

   IF (model_obs(i,4)+C2K  .GE.0) THEN
     temperature(1:NUM_ATM,NUM_TIME_SAVE)=model_obs(i,4)+C2K
   ENDIF

   IF (model_obs(i,5)+C2K .GE.0) THEN
     temperature(1+NUM_ATM,NUM_TIME_SAVE)=model_obs(i,5)+C2K
   ENDIF

   IF (model_obs(i,6) .NE.DEFAULT_OBS) THEN
     u_wind=model_obs(i,6)
   ELSE
     u_wind=3.0  !Default windspeed in u
   ENDIF

   IF (model_obs(i,7) .NE.DEFAULT_OBS) THEN
     v_wind=model_obs(i,7)
   ELSE
     v_wind=3.0  !Default windspeed in v
   ENDIF

   wind=sqrt(v_wind**2+u_wind**2)

   !if (wind.LT.3.0) wind=3.0
   !if (wind.GT.10.0) wind=10.0

   IF (model_obs(i,8) .NE.DEFAULT_OBS) THEN
     obs_irr=model_obs(i,8)
   ELSE
     obs_irr=DEFAULT_OBS
   ENDIF

   IF (model_obs(i,9) .NE.DEFAULT_OBS) THEN
     ustar=model_obs(i,9)
   ELSE
     ustar=MIN_USTAR
   ENDIF

   IF (model_obs(i,10) .NE.DEFAULT_OBS) THEN
     monin=model_obs(i,10)
   ELSE
     monin=MIN_MO
   ENDIF

   !print *,monin, model_obs(i,10)
   !read(*,*)

   ! MAQ_20180321+ added the writing of the timestepping
   write(*,'(3(1a,i4),(1a,f5.2))')'year: ',year(i),' month: ',month(i),' day: ',day(i),' hr: ',hour(i)

   IF ((photo_time.GE. PHOTO_TIME_STEP) .OR. i.EQ.1) THEN
     photo_time=0.0

     ! MAQ_lg_20180322+ calling of subroutine to calculate photolysis
     call snow_photo_drvr(j_day(i),month(i),hour(i)+GMT_DIFF,LAT,LON,surface_pressure*ATM2HPASCAL,&
                          ALBEDO,rj,edge_depths,obs_irr,model_irr)
     !read(*,*)
   ELSE
     photo_time=photo_time+TIME_STEP*SEC2HRS
   ENDIF

   call snow_temperature(snow_density, porosity,depths,edge_depths)
   call snow_aqueous_equilibrium(ppx,nd,temperature(:,NUM_TIME_SAVE))
   call update_Henrys(temperature(:,NUM_TIME_SAVE),Henrys)
   call update_snow_properties(snow_density,porosity,ssa,snow_radius,QLL_thickness,L,temperature,ppx,nd,Ct,theta)
   
   !print *,'L',L
   !print *,'QLL thickness',QLL_thickness
   !print *,'snow_radius',snow_radius
   !print *,'porosity',porosity
   !read(*,*)

   ! MAQ_lg_20180412+ calculation of removal by �dry deposition�. The subroutine actually now does the 
   ! assignment of the snow uptake resistance but then Vd is set to zero...
   call calc_Vd(temperature(:,NUM_TIME_SAVE),Henrys,Vd)

   !DO jk=1,NUM_ATM+NUM_DEPTH
     !print*, Vd(jk,ind_HNO3),edge_depths(jk+1)
   !ENDDO
   !read(*,*)

   tendencies(:,:,1)=ppx(:,:,NUM_TIME_SAVE)
   flux(:)=0.0
   all_flux(:,:)=0.0

   call snow_physics_drvr(temperature,edge_depths,depths,wind,nd,ppx,porosity,Vd,ustar,flux,monin,all_flux)
   !read(*,*)

   DO jt=1, NSPEC
     DO jk=1, NUM_ATM+NUM_DEPTH
       IF (ppx(jk,jt,NUM_TIME_SAVE).LT.0.0) THEN

         write(*,'(2(1a,i4))') 'ppx below zero after MT for tracer: ', SPC_NAMES(jt), 'and layer #: ',jk
         !call exit(0)
        
       ENDIF
     ENDDO
   ENDDO

   tendencies(:,:,1)=(ppx(:,:,NUM_TIME_SAVE)-tendencies(:,:,1))/(TIME_STEP*SEC2HRS)

   tendencies(:,:,2)=ppx(:,:,NUM_TIME_SAVE)

   IF (CHEM_MT.EQV. .FALSE.) call snow_mass_transfer(temperature(:,NUM_TIME_SAVE),snow_radius,kt,L,nd,ppx,Henrys)

   DO jt=1, NSPEC
     DO jk=1, NUM_ATM+NUM_DEPTH
       IF (ppx(jk,jt,NUM_TIME_SAVE).LT.0.0) THEN

          write(*,'(2(1a,i4))') 'ppx below zero after MT for tracer: ', SPC_NAMES(jt), 'and layer #: ',jk
          !call exit(0)

       ENDIF
     ENDDO
   ENDDO

   call snow_aqueous_equilibrium(ppx,nd,temperature(:,NUM_TIME_SAVE))

   tendencies(:,:,2)=(ppx(:,:,NUM_TIME_SAVE)-tendencies(:,:,2))/(TIME_STEP*SEC2HRS)
   tendencies(:,:,3)=ppx(:,:,NUM_TIME_SAVE)

   call tropo_halo_drvr(hour(i),nd,ppx,rj,temperature,Henrys,chem_rates,snow_radius,kt,L)

   DO jt=1, NSPEC
     DO jk=1, NUM_ATM+NUM_DEPTH
       IF (ppx(jk,jt,NUM_TIME_SAVE).LT.0.0) THEN

         write(*,'(2(1a,i4))') 'ppx below zero after MT for tracer: ', SPC_NAMES(jt), 'and layer #: ',jk
         !call exit(0)

       ENDIF

     ENDDO
   ENDDO

   call snow_aqueous_equilibrium(ppx,nd,temperature(:,NUM_TIME_SAVE))
   tendencies(:,:,3)=(ppx(:,:,NUM_TIME_SAVE)-tendencies(:,:,3))/(TIME_STEP*SEC2HRS)

   ! Write output

   IF (NUM_TIME_SAVE.GT.1) THEN

     !Chemical species held constant above snowpack
     ppx(1:NUM_ATM,ind_CH4,NUM_TIME_SAVE)=ppx(1:NUM_ATM,ind_CH4,NUM_TIME_SAVE-1)
     nd(1:NUM_ATM,ind_CH4,NUM_TIME_SAVE)=nd(1:NUM_ATM,ind_CH4,NUM_TIME_SAVE-1)

     nd(:,:,1:NUM_TIME_SAVE-1)=nd(:,:,2:NUM_TIME_SAVE)
     ppx(:,:,1:NUM_TIME_SAVE-1)=ppx(:,:,2:NUM_TIME_SAVE)
     gas_nd(:,1:NUM_TIME_SAVE-1)=gas_nd(:,2:NUM_TIME_SAVE)
     temperature(:,1:NUM_TIME_SAVE-1)=temperature(:,2:NUM_TIME_SAVE)

   ENDIF

ENDDO
! MAQ_lg_20180321+ end time loop

!Close output files

CLOSE(RJ_FILE_NUM)
CLOSE(GAS_FILE_ND_NUM)
CLOSE(GAS_FILE_PPX_NUM)
CLOSE(SPCS_FILE_NUM)
CLOSE(DEPTHS_FILE_NUM)
CLOSE(TIME_FILE_NUM)
CLOSE(TEMP_FILE_NUM)
CLOSE(TEND_FILE_NUM)
CLOSE(CHEMR_FILE_NUM)
CLOSE(CHEM_EQ_FILE_NUM)
CLOSE(QLL_FILE_NUM)
CLOSE(N_MB_FILE_NUM)
CLOSE(IRR_FILE_NUM)
CLOSE(FLUX_FILE_NUM)
CLOSE(VOL_FILE_NUM)
CLOSE(ALL_FLUX_FILE_NUM)

! MAQ_lg_20182103+ closing the modelflow file
CLOSE(MODELFLOW_FILE_NUM)

! deallocating all assigned arrays
DEALLOCATE(temperature)
DEALLOCATE(gas_nd)
DEALLOCATE(ppx)
DEALLOCATE(nd)
DEALLOCATE(rj)
DEALLOCATE(j_day)
DEALLOCATE(day)
DEALLOCATE(month)
DEALLOCATE(hour)
DEALLOCATE(year)
DEALLOCATE(depths)
DEALLOCATE(edge_depths)
DEALLOCATE(snow_density)
DEALLOCATE(snow_radius)
DEALLOCATE(ssa)
DEALLOCATE(QLL_thickness)
DEALLOCATE(porosity)
DEALLOCATE(kt)
DEALLOCATE(Henrys)
DEALLOCATE(L)
DEALLOCATE(is_aqueous)
DEALLOCATE(aq_gas_index)
DEALLOCATE(model_obs)
DEALLOCATE(tendencies)
DEALLOCATE(chem_rates)
DEALLOCATE(is_ion)
DEALLOCATE(mol_w)
DEALLOCATE(Ct)
DEALLOCATE(theta)
DEALLOCATE(Vd)
DEALLOCATE(flux)
DEALLOCATE(all_flux)
unix_command='say -v Vicki "The model is done"'

DO i=1,3
  call SYSTEM(unix_command)
ENDDO
      end


