
MODULE snow_global  

    USE tropo_halo_Precision
    USE snow_parameters
REAL(dp), PUBLIC, DIMENSION(:,:), ALLOCATABLE :: &
temperature, & !Temperature in Kelvin
gas_nd          !Number density of air in molecules/cm3

REAL(dp), PUBLIC, DIMENSION(:), ALLOCATABLE::&
mol_w

LOGICAL, PUBLIC, DIMENSION(:), ALLOCATABLE :: &
is_aqueous

INTEGER, PUBLIC,DIMENSION(:,:),ALLOCATABLE ::&
aq_gas_index,&
is_ion

REAL(dp), PUBLIC :: surface_pressure






END MODULE snow_global

