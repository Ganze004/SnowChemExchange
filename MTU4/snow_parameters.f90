MODULE snow_parameters

  USE tropo_halo_Precision

  !Constants and Conversions
  INTEGER, DIMENSION(12) :: &
  ndaymonth = (/31,28,31,30,31,30,31,31,30,31,30,31/) !Number of days in a month
  REAL(dp), PARAMETER :: R_gas =82.05746  ! Gas constant in cm3 atm/ K mol
  REAL(dp), PARAMETER :: R_gas_J=8.314409_dp  ! R [J/K/mol]
  REAL(dp), PARAMETER :: AV_N=6.022E23    !Avagadros number
  REAL(dp), PARAMETER :: SEC2HRS =1.0/3600. !Conversion from seconds to hours
  REAL(dp), PARAMETER :: DAY2HRS =24.0 !Conversion from days to hours
  REAL(dp), PARAMETER :: YRS2MON  =12.0 !Conversion from years to months
  REAL(dp), PARAMETER :: YRS2DAY  =365.0 !Conversion from years to days
  REAL(dp), PARAMETER :: GMT_DIFF = 3.0  !Difference between Summit, Greenland and GMT time
  REAL(dp), PARAMETER :: ATM2HPASCAL = 1013.25
  REAL(dp), PARAMETER :: pi      = 3.14159265358979323846_dp
  REAL(dp), PARAMETER :: CM2M = 0.01  !Centimeters to meters
  REAL(dp), PARAMETER :: G2KG =0.001  !Grams to kilograms
  REAL(dp), PARAMETER :: MW_AIR=28.0  !Molecular weight of air
  REAL(dp), PARAMETER :: MW_WATER=18.0  !Molecular weight of water
  REAL(dp), PARAMETER :: ICE_DENSITY=0.917 !Ice Density g/cm3 
  REAL(dp), PARAMETER :: FRESH_SNOW_DENSITY=0.3  !grams/cm3
  REAL(dp), PARAMETER :: FIRN_SNOW_DENSITY=0.6 !grams/cm3
  REAL(dp), PARAMETER :: L2M3= 0.001   !1000 Liters per m3
  REAL(dp), PARAMETER :: Cp_ice= 2.11   !J/g K
  REAL(dp), PARAMETER :: Cp_air= 2.11   !J/g K
  REAL(dp), PARAMETER :: C2K=273.15     !Celcius to Kelvin
  REAL(dp), PARAMETER :: WATER_DENSITY=0.9984 !Water Density g/cm3
  REAL(dp), PARAMETER :: L2cm3=1.0/1000.0 !L to cubic centimeters 
  REAL(dp), PARAMETER :: H_f_H20=334000.0 !Enthalpy of fusion in j/kg
  REAL(dp), PARAMETER :: von_Karman=0.4 !Von Karman cosntant
  REAL(dp), PARAMETER :: MIN_Kh=1.0E-7 !Miniumum Eddy Diffusivity in m2/s
  REAL(dp), PARAMETER :: MIN_USTAR=01.0e-3 !Miniumum friction velocity in m/s
  REAL(dp), PARAMETER :: MAX_RAD=1000.0 !max solar radiation in w/m2
  REAL(dp), PARAMETER :: MIN_MO=01.0 !Miniumum Monin-ob length m
  REAL(dp), PARAMETER :: DENSITY_GRADIENT_REGION=04.0 !Depth in which there is a density gradient [m]

  !Model Parameters
  !MAQ_lg_20180322+ levels/layers
  INTEGER, PARAMETER :: NUM_DEPTH  = 20 !Number of layers within the snow
  INTEGER, PARAMETER :: NUM_ATM = 21    !Number of layers in the atmosphere

  !MAQ_lg_20180322+ time control
  CHARACTER(*),PARAMETER :: START_DATE ='2009030100'  !Start date of model in YYYYMMDDHH
  INTEGER, PARAMETER :: NUM_TIME = 153*24*6       ! MAQ_lg_20180321+ Number of time steps in simulation; the first number is the no of days..
                                        	  ! 1*24*60*10 is the number of timesteps for a 1 day simulation using timesteps of 6 seconds
						  ! 153*24*6: 153 days (from 1st of March until end of July)

  !INTEGER, PARAMETER :: TIME_STEP = 0.60         !Length of time step in seconds
  REAL(dp), PARAMETER :: TIME_STEP = 600          !default=6...Length of time step in seconds
  INTEGER,  PARAMETER :: NUM_SUBSTEP_CHEM_MT = 1  !Length of time step in seconds
  REAL(dp), PARAMETER :: OUTPUT_TIME_STEP = 2.0   !default=0.25 ...Length of output time step in hours
  REAL(dp), PARAMETER :: PHOTO_TIME_STEP = 0.5    !How often to update photolysis rates in hours
  INTEGER,  PARAMETER :: OUTPUT_NUM_TIME =int(NUM_TIME*(TIME_STEP*SEC2HRS)/OUTPUT_TIME_STEP) !Number of time steps in output file
  INTEGER,  PARAMETER :: NUM_TIME_SAVE  = 12      !Number of time steps saved in variables such as concentrations

  !MAQ_lg_20180322+ general parameter settings
  REAL(dp), PARAMETER :: LAT=70.0                 !Latitude
  REAL(dp), PARAMETER :: LON=-38.5                !Longitude
  REAL(dp), PARAMETER :: ALBEDO=0.999             !Surface Albedo
  REAL(dp), PARAMETER :: SNOW_DEPTH=3.0           !Snow Depth in meter
  REAL(dp), PARAMETER :: MICRO_LAYER_DEPTH=0.010  !Micro Layer Depth in meter
  REAL(dp), PARAMETER :: OBSERVE_HEIGHT=0.10      !min height of chemical observation input in meter
  REAL(dp), PARAMETER :: FRESH_SNOW_DEPTH=02.000!.3!01.300!01.5   !Depth of fresh snow in meter
  REAL(dp), PARAMETER :: ABL_DEPTH=3.             ! ABL depth in meters; default 3m.

  REAL(dp), PARAMETER :: RTOL_AQ_EQ=1.0E-3
  REAL(dp), PARAMETER :: RTOL_QLL=1.0E-5
  INTEGER,  PARAMETER :: NUM_MAX_IT_AQ=10000000
  INTEGER,  PARAMETER :: NUM_MAX_IT_QLL=10000000
  INTEGER,  PARAMETER :: NUM_OBS=10                 !Maximum number of observations
  INTEGER,  PARAMETER :: NUM_TEND  = 3              !Number of different tendencies
  REAL(dp), PARAMETER :: DEFAULT_HENRYS =1.0E-15    !Default Henrys law constant
  REAL(dp), PARAMETER :: DEFAULT_OBS =-9999.999     !Default observation values
  REAL(dp), PARAMETER :: DEFAULT_EFOLD =0.10        !Default photlysis efold depth
  REAL(dp), PARAMETER :: DEFAULT_MIN_ND=0001.0E-5   !Default miniumum number density in molecules per cm3 for concentrations
  REAL(dp), PARAMETER :: DEFAULT_MIN_M=0001.0E-25   !Default miniumum molarity in aqueous phase
  REAL(dp), PARAMETER :: DEFAULT_MIN_PPX=0001.0E-25 !Default miniumum molarity in aqueous phase
  REAL(dp), PARAMETER :: DEFAULT_MOL_W=28.0         !Default molecular weight of species (diatomic nitrogen) in grams per mol
  REAL(dp), PARAMETER :: DEFAULT_KT=0.0             !Default mass transfer coefficent in (m3 gas)/(m3 aq)/s
  REAL(dp), PARAMETER :: DG_TORT=2.0/pi             !0.50 !tortuosity of diffusion in snowpack [unitless]
  INTEGER,  PARAMETER :: NUM_METHOD=0               !Picks numerical Approximation. Default- Crank-N, 1- Backwards Euler
  LOGICAL,  PARAMETER :: CHEM_MT=.true.             !When true, this variable sets the model to combine chemistry and mass transfer.
  REAL(dp), PARAMETER :: STAB_DG=0.95               !Minimum dimensionless stability criteria of diffusion/diffusivity
  REAL(dp), PARAMETER :: STAB_U=0.95                !Minimum dimensionless stability criteria of advection
  REAL(dp), PARAMETER :: MAX_QLL=3.0E-8             !03.0E-9!07.0E-15!Maximum QLL thickness in metere
  REAL(dp), PARAMETER :: MIN_QLL=3.0E-9             !MIN QLL thickness in metere
  REAL(dp), PARAMETER :: QLL_DEPTH=05.50            !depth at which QLL is significant in meters

  !FILES
  CHARACTER(*),PARAMETER :: OUTPUT_DIR ='output/'  
  CHARACTER(*),PARAMETER :: MODEL_PARA_FILE =OUTPUT_DIR//'model_para.out'  !output file for model parameters
  INTEGER, PARAMETER :: MODEL_PARA_FILE_NUM=50  !Output file unit number
  CHARACTER(*),PARAMETER :: RJ_FILE =OUTPUT_DIR//'model_rj.out'  !output file for photolysis rates
  INTEGER, PARAMETER :: RJ_FILE_NUM=51  !Output file unit number
  CHARACTER(*),PARAMETER :: GAS_FILE_ND =OUTPUT_DIR//'model_gas_ND.out'  !output file for photolysis rates
  INTEGER, PARAMETER :: GAS_FILE_ND_NUM=52  !Output file unit number
  CHARACTER(*),PARAMETER :: GAS_FILE_PPX =OUTPUT_DIR//'model_gas_PPX.out'  !output file for photolysis rates
  INTEGER, PARAMETER :: GAS_FILE_PPX_NUM=53  !Output file unit number
  CHARACTER(*),PARAMETER :: SPCS_FILE =OUTPUT_DIR//'model_spcs.out'  !output file for photolysis rates
  INTEGER, PARAMETER :: SPCS_FILE_NUM=80  !Output file unit number
  CHARACTER(*),PARAMETER :: DEPTHS_FILE =OUTPUT_DIR//'model_depths.out'  !output file for photolysis rates
  INTEGER, PARAMETER :: DEPTHS_FILE_NUM=81  !Output file unit number
  CHARACTER(*),PARAMETER :: TIME_FILE =OUTPUT_DIR//'model_time.out'  !output file for photolysis rates
  INTEGER, PARAMETER :: TIME_FILE_NUM=82  !Output file unit number
  CHARACTER(*),PARAMETER :: TEMP_FILE =OUTPUT_DIR//'model_temp.out'  !output file for photolysis rates
  INTEGER, PARAMETER :: TEMP_FILE_NUM=83  !Output file unit number
  CHARACTER(*),PARAMETER :: TEND_FILE =OUTPUT_DIR//'model_tend.out'  !output file for tendencies
  INTEGER, PARAMETER :: TEND_FILE_NUM=84  !Output file unit number
  CHARACTER(*),PARAMETER :: CHEMR_FILE =OUTPUT_DIR//'model_chem_rates.out'  !output file for chemical rates
  INTEGER, PARAMETER :: CHEMR_FILE_NUM=85  !Output file unit number
  CHARACTER(*),PARAMETER :: CHEM_EQ_FILE =OUTPUT_DIR//'model_chem_eq.out'  !output file for chemical equations
  INTEGER, PARAMETER :: CHEM_EQ_FILE_NUM=86  !Output file unit number
  CHARACTER(*),PARAMETER :: QLL_FILE =OUTPUT_DIR//'model_QLL_thickness.out'  !output file for QLL thickness
  INTEGER, PARAMETER :: QLL_FILE_NUM=87  !Output file unit number
  CHARACTER(*),PARAMETER :: N_MB_FILE =OUTPUT_DIR//'N_MB.out'  !output file for QLL thickness
  INTEGER, PARAMETER ::N_MB_FILE_NUM=88  !Output file unit number
  CHARACTER(*),PARAMETER :: IRR_FILE =OUTPUT_DIR//'IRR.out'  !output file for QLL thickness
  INTEGER, PARAMETER ::IRR_FILE_NUM=89  !Output file unit number
  CHARACTER(*),PARAMETER :: FLUX_FILE =OUTPUT_DIR//'FLUX.out'  !output file for QLL thickness
  INTEGER, PARAMETER ::FLUX_FILE_NUM=91  !Output file unit number
  CHARACTER(*),PARAMETER :: VOL_FILE =OUTPUT_DIR//'VOL.out'  !output file for Volumetric Ratio
  INTEGER, PARAMETER ::VOL_FILE_NUM=92  !Output file unit number
  CHARACTER(*),PARAMETER :: ALL_FLUX_FILE =OUTPUT_DIR//'ALL_FLUX.out'  !output file for QLL thickness
  INTEGER, PARAMETER ::ALL_FLUX_FILE_NUM=93  !Output file unit number

  ! MAQ_lg_20180321+ modelflow file to allow running diagnostics also when the run is done in bg/queuing system
  CHARACTER(*),PARAMETER :: MODELFLOW_FILE =OUTPUT_DIR//'modelflow.out'  !output file with run information
  INTEGER, PARAMETER ::MODELFLOW_FILE_NUM=94  !Output file unit number
  ! MAQ_lg_20180321-

  !INPUT File

  ! MAQ_lg_20180409+ definition of the input file; put now in subdirectory 
  ! input; the file observation_input.inp is produced by the Matlab script
  ! plot_snow_episode.m

  CHARACTER(*),PARAMETER :: INPUT_FILE ='input/observation_input.inp'
  INTEGER, PARAMETER :: INPUT_FILE_NUM=90  !Input file unit number

END MODULE snow_parameters
